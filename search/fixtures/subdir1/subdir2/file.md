---
title: "Jouez au Docteur House"
authors:
  - olivier-marquet
values:
  - partage
tags:
  - agile
  - seriousgame
  - vélocité
  - projection
date: 2022-05-09T06:30:00.000Z
thumbnail: ecg.png
subtitle: Un jeu sérieux pour découvrir et apprendre à manipuler la mesure de "vélocité"
---
Au travers de ce serious game venez découvrir comment calculer et utiliser la mesure de vélocité, mais aussi telle l'équipe du Docteur House, comment interpréter un historique de vélocité et poser un diagnostic.
