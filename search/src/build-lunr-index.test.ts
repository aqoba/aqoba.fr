import {
    allFilesIterator,
    buildSlug,
    getAllFileNames,
    loadPostsWithFrontMatter,
    makeFullPost,
    makeIndex,
} from "./lunr-indexer";


function samplePosts() {
    return [{
        attributes: {
            title: 'Jouez au Docteur House',
            authors: ['olivier-marquet', 'thomas-clavier'],
            values: ['partage'],
            tags: ['agile', 'seriousgame', 'vélocité', 'projection', 'tag with space'],
            date: new Date("2022-05-09"),
            thumbnail: 'ecg.png',
            subtitle: 'Un jeu sérieux pour découvrir et apprendre à manipuler la mesure de "vélocité"',
        },
        bodyBegin: 13,
        frontmatter: "frontmatter",
        body: "finissant\n" +
            "Au consignment travers de ce serious game venez découvrir comment calculer et utiliser la mesure de vélocité, mais aussi telle l'équipe du Docteur House, comment interpréter un historique de vélocité et poser un diagnostic.\n",
        withoutAccent: "Jouez au Docteur House Un jeu serieux pour decouvrir et apprendre a manipuler la mesure de \"velocite\" finissant Au consignment travers de ce serious game venez decouvrir comment calculer et utiliser la mesure de velocite, mais aussi telle l'equipe du Docteur House, comment interpreter un historique de velocite et poser un diagnostic."
    }];
}

function miniPosts() {
    return {
        attributes: {
            title: 'Mon titre'
        }

    };
}

describe("makeIndex", () => {
    it("should extract title", () => {
        const searchResult = makeIndex(samplePosts()).search("jouez");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract tags", () => {
        const searchResult = makeIndex(samplePosts()).search("agile");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should parse tags", () => {
        const searchResult = makeIndex(samplePosts()).search("space");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract body", () => {
        const searchResult = makeIndex(samplePosts()).search("travers");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract authors", () => {
        const searchResult = makeIndex(samplePosts()).search("thomas");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract subtitle", () => {
        const searchResult = makeIndex(samplePosts()).search("manipuler");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract word without accent", () => {
        const searchResult = makeIndex(samplePosts()).search("velocite");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract word with accent", () => {
        const searchResult = makeIndex(samplePosts()).search("sérieux");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract english words", () => {
        const searchResult = makeIndex(samplePosts()).search("consigned");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
    it("should extract french words", () => {
        const searchResult = makeIndex(samplePosts()).search("fini");
        expect(searchResult.length).toBe(1)
        expect(searchResult[0].ref).toBe("20220509-jouez-au-docteur-house")
    })
})
describe("makeFullPost", () => {
    it("should convert minimal post in full post", () => {
        const fullPost = {
            attributes: {
                title: 'Mon titre',
                authors: [],
                values: [],
                tags: [],
                date: undefined,
                thumbnail: '',
                subtitle: '',
            },
            bodyBegin: 1,
            frontmatter: "",
            body: "",
            withoutAccent: ""
        }
        expect(makeFullPost(miniPosts())).toEqual(fullPost)
    })

})

describe("buildSlug", () => {
    it("should compute url", () => {
        const afterbuild = buildSlug(new Date("2022-05-09"), "Un titre d'article");
        expect(afterbuild).toBe("20220509-un-titre-darticle")
    })
    it("should compute url with punctuation", () => {
        const afterbuild = buildSlug(new Date("2022-05-09"), "Un titre: article ?");
        expect(afterbuild).toBe("20220509-un-titre-article")
    })
    it("should compute url with hyphen", () => {
        const afterbuild = buildSlug(new Date("2022-05-09"), "Un titre-article");
        expect(afterbuild).toBe("20220509-un-titre-article")
    })
    it("should compute url with exclamation mark", () => {
        const slug = buildSlug(new Date("2022-10-05"), "Une coalition pour les guider tous !");
        expect(slug).toBe("20221005-une-coalition-pour-les-guider-tous")
    })
    it("should compute url without date", () => {
        const slug = buildSlug(undefined, "Une coalition pour les guider tous !");
        expect(slug).toBe("une-coalition-pour-les-guider-tous")
    })
})
describe("getFiles", () => {
    it('should find all files in subdirectories', async () => {
        const filesIterator = await allFilesIterator(`${__dirname}/../fixtures`)
        const allFiles = await getAllFileNames(filesIterator)
        expect(allFiles.length).toBe(5)
    })
    it('should parse all md files', async () => {
        const allPosts = await loadPostsWithFrontMatter(`${__dirname}/../fixtures`)
        expect(allPosts.length).toBe(2)
        expect(allPosts[0]).toStrictEqual({
            attributes: {
                authors: ["olivier-marquet"],
                date: new Date("2022-05-09"),
                subtitle: "Le sous titre de l'article",
                tags: ["agile"],
                thumbnail: "illstration.jpg",
                title: "Titre de l'article",
                values: ["partage"]
            },
            body: "Le contenu de l'article\n",
            bodyBegin: 13,
            frontmatter: `title: "Titre de l'article"
authors:
  - olivier-marquet
values:
  - partage
tags:
  - agile
date: 2022-05-09
thumbnail: illstration.jpg
subtitle: Le sous titre de l'article`
        })
    })

})
