import {normalizeUnicodeText} from "normalize-unicode-text";

export async function* allFilesIterator(dirName: string): AsyncIterableIterator<string[]> {
    const {readdir} = require("fs").promises
    const {resolve} = require("path")
    const dirEntries = await readdir(dirName, {withFileTypes: true});
    for (const dirEntry of dirEntries) {
        const res = resolve(dirName, dirEntry.name);
        if (dirEntry.isDirectory()) {
            yield* await allFilesIterator(res);
        } else {
            yield res;
        }
    }
}

export async function getAllFileNames(iter: AsyncIterableIterator<string[]>): Promise<string[]> {
    let items = []
    for await (const item of iter) {
        items.push(item)
    }
    return items
}

export async function loadPostsWithFrontMatter(postsDirectoryPath: string): Promise<any> {
    const filesIterator = await allFilesIterator(postsDirectoryPath)
    const allFiles = await getAllFileNames(filesIterator)
    const postNames = allFiles.filter(f => f.endsWith(".md"))
    const {readFileSync} = require("fs")
    const frontMatter = require('front-matter')

    return postNames
        .map(fileName => {
            return readFileSync(fileName, 'utf8')
        })
        .map(content => {
            return frontMatter(content)
        })
}

const lunr = require('lunr')
require("lunr-languages/lunr.stemmer.support")(lunr)
require('lunr-languages/lunr.multi')(lunr)
require("lunr-languages/lunr.fr")(lunr)

class Post {
    attributes: {
        title: string,
        authors: string[],
        values: string[],
        tags: string[],
        date: Date,
        thumbnail: string,
        subtitle: string,
    }
    bodyBegin: number
    frontmatter: string
    body: string
    withoutAccent: string
}
export function buildSlug(date: Date, title: string): string {
    const punctuationRegex = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,.\/:;<=>?@\[\]^_`{|}~]/g;
    const formattedTitle = title.toLowerCase().replaceAll(punctuationRegex, "").replaceAll("  ", " ").trim();
    if (date) {
        const formattedDate = date.toISOString().split("T")[0].replaceAll("-", "");
        return formattedDate + "-" + formattedTitle.replaceAll(" ", "-");
    }
    return formattedTitle.replaceAll(" ", "-");
    //const titlewithoutspace = title.toLowerCase().replace(/\s+(\W)/g, "$1");   //remove spaces before punctuation
}

const englishFrenchSupport = lunr.multiLanguage('fr', 'en')
export function makeFullPost(input: any): Post {
    return {
        attributes: {
            title: input.attributes.title ? input.attributes.title : "",
            authors: input.attributes.authors ? input.attributes.authors : [],
            values: input.attributes.values ? input.attributes.values : [],
            tags: input.attributes.tags ? input.attributes.tags : [],
            date: input.attributes.date ? input.attributes.date : undefined,
            thumbnail: input.attributes.thumbnail ? input.attributes.thumbnail : '',
            subtitle: input.attributes.subtitle ? input.attributes.subtitle : '',
        },
        bodyBegin: input.bodyBegin ? input.bodyBegin : 1,
        frontmatter: input.frontmatter ? input.frontmatter : "",
        body: input.body ? input.body : "",
        withoutAccent: input.withoutAccent ? input.withoutAccent : ""
    }

}

export function makeIndex(posts: Post[]) {
    return lunr(function () {
        this.use(englishFrenchSupport)
        this.ref('url');
        this.field('url');
        this.field('title');
        this.field('body');
        this.field('authors');
        this.field('tags');
        this.field('subtitle');
        this.field('withoutAccent')
        posts.forEach(p => {
            const url = buildSlug(p.attributes.date, p.attributes.title)
            const withoutAccent = normalizeUnicodeText(p.attributes.title + " " + p.attributes.subtitle + " " + p.body + " " + p.attributes.tags);
            const authors = p.attributes.authors.map(author => {
                return author.split("-")
            }).flat();
            const document = {
                url: url,
                title: p.attributes.title,
                authors: authors,
                tags: p.attributes.tags,
                subtitle: p.attributes.subtitle,
                body: p.body,
                withoutAccent: withoutAccent
            }
            this.add(document);
            console.log("[x] " + url);
        });
    });
}

export async function run() {
    const {writeFile} = require("fs").promises
    const posts = await loadPostsWithFrontMatter(`${__dirname}/../../content/posts`);
    const index = makeIndex(posts.map(p => makeFullPost(p)));
    const data = JSON.stringify(index, null, 4);
    await writeFile("../static/search.json", data);
}

