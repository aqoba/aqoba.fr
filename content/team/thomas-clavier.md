---
title: Thomas Clavier
subtitle: Coach agile, craft et devops
email: thomas.clavier@aqoba.fr
thumbnail: THOMAS2.jpg
mastodon: https://pleroma.tcweb.org/thomas
linkedin: https://www.linkedin.com/in/thomasclavier/
employee: yes
co2: 5,9
---
Expert reconnu du Software Craftsmanship : l’art de produire un code logiciel évolutif et de qualité. Cette culture, à la base du succès des entreprises du digital, Thomas la diffuse auprès de tous les profils, IT et Métiers, dans les transformations d’entreprise qu’il pilote depuis plus de 15 ans.

