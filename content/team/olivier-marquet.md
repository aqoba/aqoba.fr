---
employee: yes
title: Olivier Marquet
linkedin: https://www.linkedin.com/in/omarquet/
email: olivier.marquet@aqoba.fr
thumbnail: OLIVIER2.jpg
mastodon: https://mastodon.social/@omarquet
---
Olivier possède la force d’arrachage essentielle pour lancer et tirer une transformation d’entreprise à grande échelle. Il sait rassembler les collaborateurs de ses clients autour de pratiques et d’outils homogènes en les adaptant à leurs besoins et leur culture de travail.
