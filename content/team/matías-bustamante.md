---
employee: true
title: Matías Bustamante
linkedin: https://www.linkedin.com/in/matias-bustamante/
email: matias.bustamante@aqoba.fr
thumbnail: /uploads/matias.png
co2: 4,6
---
Tombé dans l’agilité en 2018, Matías est un homme de challenge !\
Il met son expertise de Scrum Master, son intelligence relationnelle et sa positivité au service d’équipes qui ont des problèmes concrets à régler.
