---
title: Anthony Yang
email: anthony.yang@aqoba.fr
thumbnail: anthony.jpg
linkedin: https://www.linkedin.com/in/anthony-yang-51877b1b9/
employee: false
---
Étudiant en informatique anthony a sue donner au site web d'aqoba toute son ampleur. En 3 mois de stage il a intégré tous les détails qui ont permis au site web d'atteindre le niveau d'excellence qui caractérise aqoba.
