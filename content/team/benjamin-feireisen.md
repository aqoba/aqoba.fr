---
employee: true
title: Benjamin Feireisen
linkedin: https://www.linkedin.com/in/bfeireisen/
email: benjamain.feireisen@aqoba.fr
thumbnail: /uploads/benjamin.png
co2: 6,9
---
Expérimenté dans la création de produits, formé au coaching professionnel et à diverses approches dont la systémie, Benjamin coache, forme et mentore des équipes de développements produit, des managers, des RTE dans leurs objectifs professionnels et leur transformation agile.


