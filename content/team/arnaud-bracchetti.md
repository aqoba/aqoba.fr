---
employee: yes
title: Arnaud Bracchetti
linkedin: https://www.linkedin.com/in/arnaud-bracchetti-9a8a392/
email: arnaud.bracchetti@aqoba.fr
thumbnail: ARNAUD2.jpg
---
Depuis 2007, Arnaud transforme des organisations françaises en se servant des outils et pratiques qu’il a lui-même aiguisés dans les organisations au sein desquelles il a travaillé. Ses 28 ans d’expérience dans des organisation de tous types, en font un interlocuteur efficace et pragmatique pour des équipes opérationnelles comme des dirigeants.
