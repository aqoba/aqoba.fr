---
employee: true
title: Marion Bialecki
linkedin: https://www.linkedin.com/in/marion-bialecki-1a67b53a/
email: marion.bialecki@aqoba.fr
thumbnail: marion-2.jpg
---
Par son expérience dans l’exploration des besoins et sa vision plaçant toujours le client et l’humain au cœur des systèmes, Marion est en mesure d’amener une transition souple vers l’agilité en prenant en compte toutes les contraintes d’une équipe et de son environnement.