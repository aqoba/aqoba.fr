---
employee: yes
title: Laurent Dussault
linkedin: https://www.linkedin.com/in/laurent-dussault
email: laurent.dussault@aqoba.fr
mastodon: https://mas.to/@Laurent_Dussault
thumbnail: LAURENT2.jpg
---
Spécialiste des flux de valeur et de la culture devops : Il l’adapte aux organisations de ses clients depuis 2015. Ainsi, il fait disparaître les silos traditionnels et fixe l’attention de chacun sur la valeur délivrée aux clients au travers d’outils de mesure concrets.
