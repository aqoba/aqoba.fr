---
employee: yes
title: Antoine Marcou
linkedin: https://www.linkedin.com/in/antoine-marcou-7613734/
email: antoine.marcou@aqoba.fr
thumbnail: ANTOINE1.jpg
---
Antoine accompagne le management et les directions d’entreprise pour cadrer et piloter leur transformation depuis 10 ans. Pour lui, des leaders convaincus et une conduite du changement puissante sont les clés d’une transformation pérenne qui ne laisse personne sur la touche.
