---
draft: false
title: La bibliothèque agile idéale 2
values:
  - partage
  - conviction
summary: Il y a 1 an, nous partagions une liste de 7 livres majeurs qui nous ont
  enrichis. A l’occasion de la première bougie d’aqoba, et après avoir lu de
  très nombreux nouveaux livres, nous vous proposons la lecture de 3 nouveaux
  ouvrages qui valent le détour. Et n’oubliez pas 1 jour, 1 chapitre.
authors:
  - antoine-marcou
  - olivier-marquet
  - laurent-dussault
  - thomas-clavier
  - arnaud-bracchetti
tags:
  - leadership
  - peopleops
  - lean
  - tranformation
  - coaching
  - bibliothèque
offers:
  - unlock-transformation
  - unlock-delivery
  - unlock-management
  - unlock-project
date: 2023-05-02T05:10:00.000Z
thumbnail: janko-ferlic-sfl_qonmy00-unsplash.jpg
subtitle: 3 nouveaux livres à mettre absolument sur votre table de chevet !
---
Il y a 1 an, nous partagions une liste de 7 livres majeurs qui nous ont enrichis.

A l’occasion de la première bougie d’aqoba, et après avoir lu de très nombreux nouveaux livres, nous vous proposons la lecture de 3 nouveaux ouvrages qui valent le détour. 

Et n’oubliez pas : 1 jour, 1 chapitre !

Retrouvez tous nos conseils lecture ici : [#Bibliothèque](../../tags/biblioth%C3%A8que/)

---


{{<toc>}}

***

## Livre #1 : L’art de motiver : les secrets pour booster son équipe 

{{<figure  src="art-de-motiver.jpg" alt="Couverture du livre L’art de motiver : les secrets pour booster son équipe" class="center-small">}}


**Thèmes** : People Ops, Leadership, Coaching d’équipe

**L’auteur** : Mickaël AGUILAR, ancien vendeur, fondateur d’un cabinet de conseil spécialisé dans la vente et ancien maître de conférences à HEC

**Pourquoi on aime** : Un livre qui retrace, de manière simple et illustrée, les différentes théories sur la motivation des individus. Et qui offre des outils concrets pour identifier les principaux leviers de motivation des membres d’une équipe et savoir comment les utiliser. Nous avons lu de nombreux livres sur ce thème : celui-ci fait l’unanimité chez aqoba. 

**A lire si...** : Vous avez la responsabilité d’une équipe que vous managez ou coachez.

**Niveau de difficulté** : 2 / 5. Un livre didactique et accessible

**Edition** : DUNOD

**Publié en** : 2009

***


## Livre #2 : Coacher une équipe avec la psychologie sociale


{{<figure  src="coacher-une-equipe.jpg" alt="Couverture du livre Coacher une équipe avec la psychologie sociale" class="center-small">}}


**Thèmes** : Transformation d’entreprise, coaching d’équipe

**L’auteur** : Rodéric MAUBRAS

**Pourquoi on aime** : Un livre “Boîte-à-Outils” qui assume ses partis-pris. Rodéric Maubras explique simplement et de manière synthétique les fondements de la psychologie sociale, c’est-à-dire l’étude du fonctionnement des groupes. Et présente comment l’appliquer de manière concrète au coaching d’équipe avec des outils décrits de manière précise et une méthodologie. Nous y retrouvons les principaux outils que nous mettons nous-mêmes en place au démarrage de nos interventions et apprécions la perspective nouvelle que l’auteur apporte.

**A lire si...** : Vous devez accompagner une équipe en tant que coach ou manager.

**Niveau de difficulté** : 2/5. Un livre synthétique et bien structuré, comprenant de nombreuses fiches outils.

**Edition** : Eyrolles

**Publié en** : 2021


***

## Livre #3 : Au cœur de l’ingénierie Toyota

{{<figure  src="au-coeur-toyota.jpg" alt="Couverture du livre Au cœur de l’ingénierie Toyota" class="center-small">}}

**Thèmes** : Culture Lean

**L’auteur** : Olivier Soulié

**Pourquoi on aime** : Un voyage initiatique dans le lean management merveilleusement illustré d’anecdotes pleines d’humour. Une véritable leçon de vie sur le développement de grands projets et le métier d'ingénieur, sur l’essence du lean et le rôle du manager.

**À lire si ...** : Vous êtes dirigeant ou manager et cherchez à faire évoluer votre culture lean en vous appuyant sur des expériences concrètes.

**Niveau de difficulté** : 2/5, une première lecture très facile qui demande un petit peu de recul pour en tirer des apprentissages.

**Edition** : L’Harmattan

**Publié en** : mars 2023
