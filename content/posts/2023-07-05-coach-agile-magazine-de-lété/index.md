---
draft: false
title: Coach agile magazine de l'été
authors:
  - antoine-marcou
  - arnaud-bracchetti
  - laurent-dussault
  - marion-bialecki
  - olivier-marquet
  - thomas-clavier
values: []
tags:
  - humour
  - CAM
date: 2023-07-05T07:55:19.844Z
thumbnail: magazine-coach-agile-ete23-2-.png
subtitle: Avec des révisions pour les vacances...
---
Toi aussi, tu t’apprêtes à monter dans le train des vacances pour souffler quelques semaines  ? Mais tu redoutes ce moment terrible où tu erreras dans les rayons du kiosque RELAY de la gare à la recherche du magazine qui accompagnera ton été ! 

Un magazine qui s’adresserait à toi, et rien qu’à TOI ! Un magazine que tu prendrais plaisir à poser sur la table basse de la terrasse, dans le panier du vélo, à côté de ta serviette à la plage, sous ta chaise-longue posée à côté de la piscine ?

Ce magazine, le voilà ! C’est ton Coach Agile Magazine de l’été, spécial Jeux Agiles pour toi et tes enfants : les 7 différences, le quiz de l’été, les mots fléchés… Pour s’amuser tout en révisant pour revenir d’attaque en septembre.

Alors, comme d’habitude (et qui plus est pendant l’été), on ne se prend pas au sérieux.

Et on se retrouve à la rentrée !

Bon été à tous et à toutes

Téléchargez ces jeux pour les mettre dans votre valise : **[le pdf à imprimer ici](CAM_cahier_de_vacances_2023.pdf)**

**L’équipe du CAM**
