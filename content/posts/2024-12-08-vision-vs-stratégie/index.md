---
draft: false
title: Vision vs Stratégie
authors:
  - marion-bialecki
values:
  - partage
  - conviction
tags:
  - vision
  - stratégie
offers:
  - unlock-management
date: 2024-12-08T16:10:00.000Z
thumbnail: visionvsstrategie.png
subtitle: Comprendre la différence pour mieux piloter votre entité
---
Comme souvent, c’est notre vécu qui nous inspire les sujets que nous avons envie de creuser et sur lesquels nous souhaitons échanger.

Je me souviens de cette discussion en Scrum of scrums. J’intervenais au sein d'un Train, dans une DSI de près de 400 personnes. Ce Train avait été identifié comme un des plus matures. Mais depuis quelque temps un ralentissement du delivery, accompagné d’ une perte de sens qui se traduisait par des “Inspect and Adapt” de moins en moins percutant et des échanges entre ses membres qui devenaient de plus en plus stériles. 

Les Scrums Master et le RTE souhaitaient donc faire une présentation des succès des équipes, mais voulaient également réfléchir à l’émergence d’une Vision du Train, pour ré-embarquer les équipes, donner un souffle nouveau.

Mais là, surprise, dans cette salle d’une dizaine de personnes seulement, experts en organisation ou agents du changement, il y avait presque autant de définition de la Vision que d’interlocuteur. 

Et il semblait difficile de nous aligner sur une compréhension commune de la stratégie de la DSI et de la raison d’être du train .

Cela m’a donc donné envie de clarifier quelques éléments, sur lesquels je serai ravie d’avoir votre retour !

## Le dilemme Vision/Stratégie

Beaucoup d’entreprises ont du mal à différencier Vision et Stratégie, mélangeant parfois ces deux notions essentielles. Or, cette confusion peut créer un manque de clarté et de cohérence dans les actions menées au quotidien. La Vision inspire et donne un cap à long terme, tandis que la Stratégie en est la feuille de route opérationnelle. Par exemple, imaginez un capitaine de navire qui veut atteindre une île lointaine (Vision), mais qui change de cap constamment sans plan clair (Stratégie) : le voyage devient incertain. Dans cet article, nous vous expliquons comment distinguer et articuler ces deux piliers pour maximiser l’impact de vos décisions.

{{< figure  src="productvisionvspoductstrategy.png" alt="Illustration issue de Christian Strunk" caption="Illustration issue de christianstrunk.com" class="center-small">}}

Prêt à clarifier vos idées ? C’est parti !

## La Vision : Rêver l’avenir

### Qu’est-ce que la Vision ?

La Vision d’une entreprise est une déclaration ambitieuse qui décrit l’avenir souhaité. Une bonne Vision va au-delà du profit ou du résultat quantifié : elle reflète un *pourquoi* profond et appelle à une notion d’objectif infini dans le temps. Elle doit être claire, inspirante et donner un sens à toutes les actions de l’organisation, comme le rappelle Simon Sinek dans son ouvrage “Start with Why”. (Retrouvez son TEDx Talk dans [nos vidéos préférées](https://aqoba.fr/video))

Prenons l’exemple de Tesla : « Accélérer la transition mondiale vers une énergie durable », ou de Netflix “ Devenir le meilleur service mondial de distribution de divertissement ». Ces phrases captent une ambition audacieuse et mobilise employés, partenaires et clients autour d’une cause commune sans définir de date d’atteinte. À l’époque ces idées semblaient irréalistes, mais elle ont été utilisées pour galvaniser les équipes et attirer des investisseurs. 

### Pourquoi une Vision forte est essentielle ?

Une Vision claire agit comme un phare dans le brouillard, alignant les efforts des équipes et renforçant leur engagement. Une Vision forte sert aussi de boussole en temps de crise : quand tout semble incertain, elle rappelle pourquoi l’entreprise existe, vers où elle doit aller, même si parfois la réponse est de se réinventer ou de rendre possible l’impossible ( comme vous le montre l’autruche de Samsung :) )

<iframe width="560" height="315" src="https://www.youtube.com/embed/hq3deG4foaY?si=dROhxpGvfOHu2GA2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Dans les années 2000, Starbucks faisait face à une crise majeure : perte de clients, dégradation de la qualité perçue, et concurrence accrue. Lorsqu’Howard Schultz est revenu en tant que PDG en 2008, il a rappelé la Vision fondatrice de Starbucks : *« Inspirer et nourrir l’esprit humain – une personne, une tasse et un quartier à la fois. »*

Plutôt que de se concentrer uniquement sur la rentabilité immédiate, Schultz a recentré l’entreprise sur l’expérience client et la qualité. Cela s’est traduit par des actions concrètes comme la fermeture temporaire de 7 100 magasins pour re-former les baristas à l’art de préparer un espresso parfait. Cette réaffirmation de la Vision a permis à Starbucks de renouer avec son essence et de regagner la confiance de ses clients.

Le fil conducteur qui mène vers ces actions concrètes, parlons-en !

## La Stratégie : L’art de planifier le chemin

### Qu’est-ce que la Stratégie ?

La Stratégie, c’est l’art de transformer la Vision en réalité par un ensemble de choix, d’actions et de priorités. Si la Vision fixe  le *pourquoi*, la Stratégie se concentre sur le *comment*. 

 La Stratégie reposant sur des plans concrets qui permettent de progresser étape par étape, elle doit être suffisamment agile pour s’adapter aux défis.

### Vision et Stratégie, une relation complémentaire

Vision et Stratégie ne sont pas opposées, elles sont interdépendantes.

 La Vision fixe un cap ambitieux, tandis que la Stratégie traduit cette ambition en actions concrètes. L’équilibre entre les deux garantit une exécution cohérente et efficace. Ensemble, elles créent une vraie dynamique qui donne un sens et une direction à l’ensemble de l’organisation.

Prenons l’exemple d’IKEA : sa Vision est de « créer un quotidien meilleur pour le plus grand nombre ».La Stratégie pour atteindre cette Vision implique alors de penser un design de meubles fonctionnels, de mettre en place des prix plus compétitifs que ses concurrents directs et de développer une distribution mondiale efficace avec un système d’implantation stratégique.Cet équilibre entre Vision et Stratégie leur a permis de transformer une idée simple en un empire mondial.

### De la Vision et de la Stratégie aux objectifs opérationnels

Une fois la Vision définie et traduite en une Stratégie claire, il est essentiel de décliner ces éléments en objectifs opérationnels concrets définis dans le temps.

Ces objectifs représentent les actions spécifiques et mesurables que les équipes vont réaliser pour exécuter la Stratégie et progresser vers la Vision. Par exemple, si la Vision d’une entreprise est de « révolutionner l’alimentation durable » et que sa Stratégie inclut la démocratisation des produits à base de protéines végétales, un objectif opérationnel pourrait être « développer et lancer trois nouveaux produits végétaliens d’ici 12 mois. » 

Ces objectifs servent de jalons intermédiaires, permettent de mesurer les progrès, et garantissent que chaque membre de l’organisation comprend comment son travail contribue à l’ambition globale.

## Les erreurs courantes et comment les éviter

### Confondre Vision et Stratégie

Une erreur fréquente consiste à présenter une Stratégie comme une Vision, ce qui réduit son pouvoir d’inspiration. Par exemple, imaginez une entreprise qui déclare : « Nous voulons augmenter nos parts de marché de 20 % dans les trois prochaines années. » Déjà, cela sonne plus comme un objectif stratégique, cela n’a rien d’inspirant ( what’s in it for me?) et surtout présente une date de fin ( et après?).Ces éléments peuvent désorienter les équipes et limiter leur engagement. Pour éviter cela, il est indispensable de garder en tête que **la Vision est intemporelle, tandis que la Stratégie évolue avec le contexte.**

La Vision de Patagonia, « Nous sommes en affaires pour sauver notre planète »,elle, montre bien qu’une Vision se doit de transcender les objectifs financiers de l’entreprise seule et toucher un enjeu plus grand, qui peut résonner en chacun de ses membres.

Il est donc essentiel de toujours bien dissocier Vision et Stratégie pour éviter la confusion.

### Construire une Stratégie sans Vision

Une Stratégie bâtie sans Vision ressemble à une maison sans fondations : elle manque de sens et risque de s’écrouler.

Sans un cap clair, les décisions stratégiques peuvent paraître incohérentes ou opportunistes, démotivant les collaborateurs. L’entreprise risque alors de courir après des résultats immédiats, sans perspective d’avenir. 

Un exemple marquant est celui de Kodak, qui, malgré sa domination initiale sur le marché de la photographie, a échoué à se réinventer. Faute de Vision claire sur l’avenir de l’image numérique, l’entreprise a élaboré des Stratégies court-termistes qui n’ont pas su répondre aux évolutions du marché. Résultat : une faillite, qui aurait pû être évitée. 

Il est important de prendre le temps de formaliser une Vision forte avant d’élaborer une feuille de route desquelles découleront les objectifs opérationnels. C’est un investissement qui porte ses fruits à long terme.

Il existe de surcroît de très bon outils pour poser sa vision tels que :

* le Vision Canvas de David Sibbet : [https://www.designabetterbusiness.tools/tools/5-bold-steps-canvas](https://www.designabetterbusiness.tools/tools/5-bold-steps-canvas)
* le Product Vision Board, pour aligner les équipe sur la Vision Produit : [https://draft.io/fr/example/product-vision-board](https://draft.io/fr/example/product-vision-board)

{{< figure  src="productvision.png" alt="Illustration issue de Christian Strunk" caption="Illustration issue de christianstrunk.com" class="center-small">}}




## Conclusion

Vision et Stratégie sont deux piliers indissociables d’une organisation performante : l’un inspire, l’autre guide.  

Une Vision sans Stratégie est une utopie ; une Stratégie sans Vision est une dérive.

En clarifiant ces notions, vous posez les bases d’une entreprise alignée et tournée vers l’avenir. Par exemple, SpaceX aurait pu se limiter à une stratégie d’amélioration des technologies spatiales existantes, mais sa vision très  ambitieuse de « rendre l’humanité multi planétaire » lui impose de repousser les limites de l’innovation. 

Maintenant, posez-vous cette question : votre Vision est-elle suffisamment claire et motivante pour guider votre Stratégie ? Si vous hésitez, il est temps de prendre un moment pour y réfléchir avec vos équipes

Après tout, un grand rêve n’a de sens que s’il peut être réalisé 🙂.
