---
draft: false
title: Scrum au secours du train
authors:
  - arnaud-bracchetti
values:
  - conviction
  - partage
tags:
  - safe
  - agile
  - train
  - scrum
offers:
  - unlock-delivery
date: 2023-01-31T11:15:11.994Z
thumbnail: trainvsscrum.png
subtitle: Le principe des poupées russes appliqué aux trains SAFe
---
Il y a quelque temps, j'ai eu l’occasion de faire une intervention sur le thème “Comment expliquer rapidement un train SAFe a des agilistes ?”. Pour répondre à cette question, je suis parti sur l’analogie suivante :  Un train SAFe, c’est une équipe d'équipes qui fonctionne en Scrum. C’est en creusant un peu ce postulat que je me suis rendu compte qu'il était beaucoup plus riche qu’il ne le paraissait. Dans cet article, je vous propose de pousser un peu cette comparaison et ainsi mettre en évidence des erreurs d’implémentation que l’on trouve assez régulièrement dans la mise en place de trains SAFe.

## Une équipe Scrum, qu'est-ce que c'est ?

Scrum est un framework bien connu, je vais sûrement énoncer ici des points qui vous sembleront être des évidences. Cependant, vous verrez plus tard que cette partie a son importance. En effet, si les postulats énoncés ici pour une équipe Scrum, paraissent aujourd’hui comme évidents pour bon nombre d’entre nous, il n’en est pas de même quand on les transpose au train SAFe.

Ainsi, une équipe Scrum peut se caractériser par les points suivants : 

* Elle est constituée d’un maximum 10 personnes (avec une préférence de 5 à 8 personnes)
* Dans une équipe Scrum, on recherche les profils en T (c.-à-d. des personnes ayant une spécialité, mais qui sont aussi capables d’être polyvalentes et d’intervenir sur d’autres domaines)
* Les équipiers d’une équipe Scrum sont toujours prêts à s'entraider pour maximiser la valeur produite
* Au sein d’une équipe Scrum, tout le monde est engagé sur un même objectif

## Une équipe d'équipes qui fonctionne en Scrum

Avant de rentrer plus en détails sur la comparaison entre une équipe Scrum et un train SAFe, revenons sur la signification de cette analogie.

### Un train SAFe est une équipe

Notre analogie nous dit qu’un train SAFe est une “méta” équipe dont les équipiers sont remplacés par des équipes. En ce sens, comme pour une équipe Scrum, un train doit rassembler des équipes qui doivent partager ensemble pour construire quelque chose de grand.

> Tout doit donc être dans un train. C’est faux 

Cela peut paraître évident, mais il arrive parfois que dans certaines organisations on rassemble dans un train “fourre tout” des équipes qui n’ont rien à voir entre elles. La raison principale à cela, est que l’entreprise passe en SAFe, Tout doit donc être dans un train. C’est faux. SAFe le précise bien avec sa notion de “dual operation system”, une entreprise ne doit utiliser le modèle SAFe que sur le sous-ensemble de son activité sur lequel, c'est pertinent.

### Maximum 10 personnes

Si une équipe Scrum doit être composée d’un maximum de 10 personnes, un train SAFe devrait donc être constitué d’un maximum de 10 équipes. 10 équipes de 10 personnes, auxquelles on ajoute une “system team”, un product management, un RTE,… on arrive bien à la limite du [nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar), limite posée par SAFe à 125.

> La taille idéale d’un train : 5 à 8 équipes, comme les membres d’une équipe Scrum

Toutefois, les 20 ans de recul que nous avons sur la pratique de Scrum nous ont montré que des équipes de 5 à 8 personnes étaient plus efficaces. Il en est de même pour un train. 5 à 8 équipes semblent être la taille idéale. Au-delà, la synchronisation et la collaboration des équipes deviennent rapidement très complexes, et obligent à perdre de l’agilité dans les processus.

### Des profils en T pour favoriser l’entraide

Dans une équipe Scrum, on va chercher à la fois spécialité et polyvalence dans le choix des équipiers. Dans un train SAFe, c’est la même chose : on va chercher à avoir des équipes qui ont leurs sujets de prédilection, mais qui sont aussi capables de sortir de leur zone de confort pour adapter leurs activités aux besoins du train.

Ce point est un défaut que l'on retrouve dans bon nombre d'implémentations de SAFe. Il n'est en effet pas rare de rencontrer des trains SAFe constitués d’équipes ultra-spécialisées qui n’ont pas la polyvalence nécessaire pour aider les autres équipes du train qui pourraient être en difficulté ou tout simplement pour maximiser la valeur produite.

### L’entraide pour maximiser la valeur produite

Grâce au profil en T et à une synchronisation très régulière, les équipiers d’une équipe Scrum sont capables de s’adapter et de s’entraider dès que le besoin s’en fait sentir. Par analogie, les équipes d’un train doivent se synchroniser régulièrement, et ne pas hésiter à revoir leur plan d’équipe pour aider d’autres équipes si cela permet de livrer plus de valeur.

> Rester flexible sur le plan, c’est vrai dans un train comme dans une équipe Scrum

Ce point est aussi un défaut courant. Les équipes d’un train (ou le RTE) attachent souvent beaucoup trop d’importance aux objectifs d’équipes pris en PI Planning. Il est important tout au long du PI de se poser les bonnes questions sur la valeur produite par le train, et de ne pas hésiter à changer les plans du PI planning si cela est nécessaire, comme le fait naturellement une équipe Scrum.

### Un objectif commun

Dans une équipe Scrum, à la fin d’un Sprint Planning, l’ensemble des équipiers s’entendent pour définir un objectif commun. On ne pense même pas à se définir des objectifs individuels. 

En SAFe, il se passe très souvent le phénomène inverse. Pendant le PI Planning, les équipes passent du temps à se définir des objectifs d’équipe (objectifs individuels) qui sont valorisés par le Business Owner. Très peu voire pas du tout de temps est alors passé à définir les objectifs du train qui engagent et alignent tout le monde. Les équipes quittent alors le PI Planning avec une version individuelle des engagements, ce qui pose souvent des problèmes de silotage des équipes au sein du train, et un manque d’entraide pour atteindre les objectifs à forte valeur.

{{< figure  src="illustration-train.png" alt="Illustration de train généré par MidJourney" class="center-large">}}

## Conclusion

En conclusion, on l'oublie trop fréquemment, mais un train SAFe doit rester agile. Ce n’est pas parce qu’on change d’échelle que les principes de base qui ont fait la réussite de l’agilité ne doivent plus être respectés. Étudier cette analogie m’a permis de me rendre compte de beaucoup d’erreurs, et me donne les moyens de donner du sens aux actions de transformation que je propose aux entreprises que j’accompagne. 
Afin de poursuivre cette analogie, je vous proposerai peut  être une analogie entre les rituels d’une équipe Scrum et celle d’un train SAFe. Vous êtes preneurs ?

<small>Les images de cet article ont été générées via [MidJourney](https://midjourney.com)</small>
