---
title: Une nouvelle aventure
authors:
  - thomas-clavier
  - antoine-marcou
  - arnaud-bracchetti
  - olivier-marquet
  - laurent-dussault
values:
  - altruisme
  - conviction
  - humanite
  - partage
  - transparence
date: 2022-03-16T18:43:48.175Z
thumbnail: team.jpg
subtitle: Faire part de naissance d'aqoba
---
Nous sommes heureux et fiers de vous faire part de la naissance d'aqoba.

## aqoba, c'est quoi ?

C'est le nouveau partenaire Conseil des entreprises qui ont l'ambition de transformer en profondeur leurs modes de travail.
Qui ont besoin de pivoter vers un modèle opérationnel simple, solide et capable de s'adapter au contexte du marché. 
Qui veulent faire évoluer leur culture de travail pour remettre l'humain au centre.

<!--more-->

## Notre ambition ?

**Changer radicalement la manière d'apporter du conseil aux entreprises**

Pour nous, le conseil, ce n'est pas se rendre indispensable, au contraire : c'est rendre nos clients autonomes.
Ce n'est pas appliquer une recette générique. Au contraire : c'est adapter nos réponses à l'ADN de nos clients.
Ce n'est pas produire des slides. Au contraire : c'est ancrer des pratiques et des outils dans le quotidien des équipes et du management pour renforcer l'entreprise et améliorer 
l'expérience des collaborateurs.
Ce n'est pas s'adresser qu'aux grands groupes, habitués des consultants. Au contraire : c'est ouvrir notre boîte à outils aux ETI et PME françaises qui en ont besoin en leur offrant un mode d'accompagnement à leur mesure.

Vous l'avez compris, nous sommes là pour diffuser en France une culture du travail en laquelle nous croyons sincèrement. Et nous espérons vous avoir à nos côtés dans cette aventure.

Pour en savoir plus, rendez-vous sur notre site : <https://www.aqoba.fr/>, sur [twitter](https://twitter.com/aqoba_fr), sur [mastodon](https://pouet.chapril.org/@aqoba) ou contactez l'un des aqobiens directement ([Antoine](https://www.linkedin.com/in/antoine-marcou-7613734/), [Thomas](https://www.linkedin.com/in/thomasclavier/), [Olivier](https://www.linkedin.com/in/omarquet/), [Laurent](https://www.linkedin.com/in/laurent-dussault-53b82a116/), [Arnaud](https://www.linkedin.com/in/arnaud-bracchetti-9a8a392/)).
