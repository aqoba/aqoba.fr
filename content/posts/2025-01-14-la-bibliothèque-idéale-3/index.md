---
draft: false
title: La bibliothèque idéale 3
authors:
  - antoine-marcou
  - benjamin-feireisen
values:
  - partage
  - conviction
tags:
  - leadership
  - agile
  - coaching
  - bibliothèque
offers:
  - unlock-transformation
date: 2025-01-14T13:55:00.000Z
thumbnail: janko-ferlic-sfl_qonmy00-unsplash.jpg
subtitle: 5 nouveaux livres à partager autour de vous !
---
Pour démarrer cette année, nous vous proposons cinq livres qui nous ont plus et nous ont fait réagir.
Et n’oubliez pas : 1 jour, 1 chapitre !

<!--more-->

Retrouvez tous nos conseils lecture ici : [\#Bibliothèque](../../tags/biblioth%C3%A8que/)

{{<toc>}}

## Livre 1 : Réussir vos transformations

{{< figure  src="reussirvostransfo.jpg" alt="RÉUSSIR VOS TRANSFORMATIONS" class="center-small">}}

**Thèmes** : Coaching d’organisation, Conduite du changement

**L’auteur** : Arnaud Tonnelé

**Pourquoi on aime** : Une vision pragmatique et réaliste des transformations d’entreprise, à contre-courant des méthodes traditionnelles, qui vous donne des idées sur comment mener une grande transformation. Ses sept incontournables valent le détour et sont un guide solide pour tout agent du changement !

**À lire si ...** : Vous êtes responsable de transformation, directeur ou manager, coach ou consultant, et êtes actuellement dans une grande transformation d’entreprise. 

**Niveau de difficulté** : 2/5, le livre vulgarise des concepts haut niveaux mais demande une deuxième relecture plus précise pour l’appréhender totalement.

**Edition** : Eyrolles.

**Publié en** : dernier tirage en 2024.

## Livre 2 : Transformed

{{< figure  src="transformed.jpg" alt="Transformed" class="center-small">}}

**Thèmes** : Product Management, Conduite du changement

**L’auteur** : Marty Cagan

**Pourquoi on aime** : Ce livre donne des clés pour transformer son organisation en mode produit, mais avec un twist : il rappelle surtout ce qui les empêche de se transformer, et du rôle clé du CEO.

De plus, les bases du “Product Operating Model”, ou comment se comporte une organisation réellement “produit”, sont expliquées clairement et cela peut vous donner une base de réflexion commune.

**À lire si ...** : Vous êtes CEO, coach produit, ou responsable de transformation dans une entreprise souhaitant passer en mode produit ou se posant des questions par rapport à ce type d’organisation.

**Niveau de difficulté** : 3/5. Marty Cagan aborde des concepts liés à ses précédents livres Inspired et Empowered, qu’il vaut mieux avoir lus avant.

**Edition** : Wiley (en anglais uniquement, mais sera sûrement traduit)

**Publié en** : 2024

## Livre 3 : Joy of agility

{{< figure  src="joyofagility.jpg" alt="Joy of agility" class="center-small">}}

**Thèmes** : Agilité

**L’auteur** : Joshua Kerievsky

**Pourquoi on aime** : Une bouchée d’air frais dans le monde de l’agilité, qui donne plusieurs exemples concrets : des contrats sécurisés de Van Halen avec des M&M's, à réussir à discuter quotidiennement avec des utilisateurs dans une grande entreprise bancaire… Et qui démontre que quand c’est fait avec intelligence et écoute, l’agilité est une joie au quotidien !

**À lire si ...** : Vous êtes intéressés par l’agilité, quel que soit votre poste ou métier !

**Niveau de difficulté** : 1 / 5.

**Edition** : Matt Holt (en anglais uniquement)

**Publié en** : 2023

## Livre 4 : La règle ? Pas de règle !

{{< figure  src="netflix.jpg" alt="LA RÈGLE ? PAS DE RÈGLE !" class="center-small">}}

**Thèmes** : Culture d’entreprise, Innovation, Leadership

**L’auteur** : Reed Hastings & Erin Meyer

**Pourquoi on aime** : Parce que ce livre nous fait découvrir les coulisses de Netflix, son fondateur si particulier et une culture d’entreprise audacieuse. Sans être (trop) laudatif, il livre des clés concrètes pour :

* optimiser la transparence avec le fameux “« Ne dites de quelqu’un que ce que vous lui diriez en face »,
* densifier le niveau de talents,
* diminuer le niveau de contrôle pour libérer la responsabilisation.

Cette organisation va avec sa part d’ombres : la pression et une culture managériale parfois violente.

Mais elle regorge d’anecdotes inspirantes et d’outils managériaux qui tranchent avec l’approche managériale dominante en France. Bref, un livre rafraîchissant.

**À lire si ...** : Vous êtes dirigeant, manager, coach ou RH et vous cherchez à transformer votre culture d’entreprise, ou si vous êtes simplement curieux de découvrir les secrets du succès de Netflix.

**Niveau de difficulté** : 3/5, le livre mélange des concepts organisationnels et des anecdotes concrètes. Il demande une lecture attentive pour en saisir les subtilités.

**Édition** : De Boeck Supérieur. 

**Publié en** : 2021.

## Livre 5 : Co-intelligence : Living and working with AI

{{< figure  src="co-intelligence.jpg" alt="CO-INTELLIGENCE : Living and working with AI" class="center-small">}}

**Thèmes** : Intelligence artificielle, Collaboration homme-machine, Transformation du travail 

**L’auteur** : Ethan Mollick

**Pourquoi on aime** : Parce qu’on n’est pas tous des experts de l’IA et que tout le monde n’a pas conscience des conséquences concrètes que ses applications vont avoir sur notre quotidien. Et ce livre nous explique simplement et sans jouer sur les peurs comment nos manières de vivre et de travailler vont se transformer.

Pour des professionnels de l’organisation des systèmes, ce livre est une pépite : car nous devons déjà être capables de construire des organisations qui intègrent la puissance de l’IA sans oublier l’humain. 

Ethan Mollick propose une approche humaine et collaborative, où l’IA devient un véritable partenaire. Il regorge d’exemples concrets et d’outils pratiques pour tirer le meilleur de cette révolution technologique, tout en conservant une réflexion éthique et stratégique.

**À lire si ...** : Vous souhaitez démystifier l’IA et ses implications dans votre vie, celle de vos voisins, celle de vos collègues ou de vos clients.

**Niveau de difficulté** : 2/5, le style est clair et accessible, avec quelques notions techniques à approfondir pour en maîtriser les applications. Attention, ce livre n’est pour le moment disponible qu’en anglais.

**Édition** : Penguin Random House.

**Publié en** : 2023
