---
draft: false
title: Le Coach Agile Magazine est dans les kiosques
authors:
  - antoine-marcou
  - arnaud-bracchetti
  - laurent-dussault
  - marion-bialecki
  - olivier-marquet
  - thomas-clavier
values:
  - conviction
  - humanite
  - partage
tags:
  - 1erAvril
  - CAM
date: 2023-04-01T00:23:00.000Z
thumbnail: cam-dans-les-kisoques.png
subtitle: Et comme on est sympa, on vous met gratuitement à disposition sa
  version numérique !
aliases:
  - /posts/20230331-le-coach-agile-magazine-est-dans-les-kiosques/
  - /posts/20230401-le-coach-agile-magazine-est-dans-les-kiosques/
---
Le nouveau numéro de **Coach Agile Magazine** vient de sortir **en kiosques** et, comme on est sympa, on vous met gratuitement à disposition sa version numérique !

Parce que le 1er avril, on fait quoi, habituellement ?
Ben, on fait des poissons d’avril ! 🐟

Mais chez aqoba, des poissons d’avril, on vous en fait déjà tous les mois avec les couvertures parodiques de notre mensuel-pour-de-rire : Coach Agile Magazine.

Et puis, il faut qu’on vous dise : chaque fois qu’une couverture paraît, nous recevons des messages de personnes qui veulent se procurer le magazine (𝘭𝘦 𝘷𝘳𝘢𝘪). Et chaque mois, nous devons les décevoir.

Alors, pourquoi ne pas sortir un **vrai** Coach Agile Magazine pour le 1er avril ? Avec des **vrais** articles dedans ? Qui portent de **vraies** convictions ? Qui partagent de **vraies** expériences ?

Ce 1er (et probablement dernier) Coach Agile Magazine, le voilà ! Nous espérons sincèrement que vous prendrez plaisir à le parcourir.

Bonne lecture ! Et joyeux 1er avril !

<div style="font-size:3rem;text-align:center" >
➡ <a href="CAM_1_avril_2023.pdf">Coach Agile Magazine en PDF</a> ⬅
</div>
