---
draft: false
title: Combattre les dynamiques de sabotage au sein d'une équipe
authors:
  - marion-bialecki
values:
  - partage
  - humanite
  - conviction
tags:
  - sabotage
  - équipe
  - performance
offers:
  - unlock-management
date: 2024-11-12T10:59:00.000Z
thumbnail: dynamiquesabotage.png
carousels:
  "1":
    - Carrousel - ADKAR.jpg
    - Carrousel - ADKAR (1).jpg
    - Carrousel - ADKAR (2).jpg
    - Carrousel - ADKAR (3).jpg
    - Carrousel - ADKAR (4).jpg
    - Carrousel - ADKAR (5).jpg
    - Carrousel - ADKAR (6).jpg
    - Carrousel - ADKAR (7).jpg
    - Carrousel - ADKAR (8).jpg
    - Carrousel - ADKAR (9).jpg
subtitle: IDENTIFIE TON MODE DE JEU
---
Avant de rentrer dans le vif du sujet, permettez moi de vous poser un peu de contexte.

Il y a quelques temps, j’accompagnais dans son amélioration continue, une équipe en charge d’un des plus gros projets de la DSI, pour lequel il y avait énormément d’attente et de pression sur le delivery. Alors que l’équipe était stabilisée depuis plusieurs mois et composée d'éléments compétents, le delivery n’était pas au rendez-vous et les différents membres se questionnaient sur le périmètre de chacun..

 Dans une des réunions d’équipes, qui avait pour but d’aider l’équipe à reclarifier et comprendre les rôles des membres, beaucoup de désaccords avaient ressurgi et un des membres avait prononcé cette phrase en parlant d’un autre membre “ Tu sabotes encore la réunion”. Ce mot, à ce moment-là,  a eu une véritable résonance. J’ai alors réalisé que malgré tous les efforts portés pour aider l’équipe, si on ne revenait pas sur son fonctionnement et les dynamiques existantes entre ses membres, il y aurait toujours des objections, des refus de s’écouter ou de s’accorder, on ne pourrait pas avancer..

J’ai alors procédé à quelques recherches sur le sujet et suis tombée sur un article de la Harvard Business Review , rédigé par N. Anand, Professeur de leadership global, titulaire de la chaire Shell, et doyen responsable de recherche à l'IMD, et Jean-Louis Baroux Enseignant-chercheur à l'IMD Business School de Lausanne, Fixing a Self-Sabotaging Team “[How to spot and counter dysfunctional group behavior](https://hbr.org/2023/03/fixing-a-self-sabotaging-team)”

Dans cet article, les auteurs racontent une intervention auprès de la responsable des ressources humaines (RH) d'une autorité de transport public en Europe, qui avait du mal à s'intégrer à l'équipe dirigeante. En surface, son comportement était perçu comme un problème, mais après enquête, il apparaît que l'équipe entière souffrait de tensions liées à une crise stratégique : elle devait choisir entre étendre les infrastructures de transport ou rendre le système plus écologique, sans les ressources nécessaires pour accomplir les deux objectifs. Cette incapacité à décider avait généré de l'anxiété, et l'équipe avait inconsciemment désigné la nouvelle recrue comme bouc émissaire.

Derrière cette histoire, il nous est montré que dans certaines situations, les équipes sous pression adoptent des comportements de sabotage inconscients, souvent dus à des mécanismes de défense profondément enracinés dans la psychologie humaine. Elles peuvent régresser vers des comportements de survie, où un membre est assigné à un rôle spécifique pour contenir l'anxiété collective. Ce mécanisme peut mener à des comportements destructeurs, comme le fait de blâmer une personne ou de fuir les vrais problèmes. Ce type de dynamique sabote à long terme la performance de l'équipe.

Cet article m’a captivée, car les anti-patterns décrits ressemblaient énormément à ce que l’équipe vivait au quotidien.

## Quatre modèles pathologiques destructeurs

Les auteurs mettent en lumière quatre modèles pathologiques destructeurs que les équipes peuvent rencontrer lorsqu'elles sont sous pression :

* **Le Sauveur Unique** (the sole savior): Une personne est chargée de "sauver" l'équipe, tandis que les autres membres se déresponsabilisent.
* **Le Duo Dynamique** (the dynamic duo): Deux personnes prennent le contrôle, créant une dépendance qui isole le reste de l'équipe.
* **Mode Combat** (fight mode) : L’équipe se soude autour d'un ennemi commun perçu (le management, le coach, le métier, etc.), mais évite de traiter ses problèmes internes.
* **Mode Fuite** (flight mode): L'équipe élude les dysfonctionnements identifiés tout en blâmant des facteurs externes.

La clé pour surmonter ces dynamiques est de reconnaître les forces inconscientes qui influencent l’équipe et de les aborder en toute transparence. En exposant ces dynamiques, l’équipe peut ajuster ses interactions et travailler de manière plus productive

J’ai alors suivi les préconisations des auteurs. Je me suis d’abord posé quelques questions clés pour savoir si comme je le soupçonnais mon équipe sabotait sa propre performance et si oui, quelles dynamiques dysfonctionnelles pouvaient être en place.

### **1. Dans mon équipe, qui parle, et de quoi ?**

* Est-ce qu'une seule personne domine constamment les discussions ? (Indicateur de **sauveur unique**)
* Les mêmes deux personnes dirigent-elles toujours les discussions ? (Indicateur de **duo dynamique**)
* Tout le monde parle-t-il en même temps, rendant les échanges chaotiques ? (Indicateur de **mode combat**)
* Les discussions évitent-elles les sujets importants et stratégiques ? (Indicateur de **mode fuite**)

### **2. À qui s’adressent les orateurs ?**

* Se concentrent-ils toujours sur la même personne ? (Indicateur de **sauveur unique**)
* Le leader s'adresse-t-il principalement à une personne spécifique ? (Indicateur de **duo dynamique**)
* Est-ce que les orateurs fixent intensément les autres membres ? (Indicateur de **mode combat**)
* Évitent-ils le contact visuel avec d'autres membres ? (Indicateur de **mode fuite**)

### **3. Qui interrompt ou remet en question les autres ?**

* Est-ce qu'une seule personne interrompt les autres de manière récurrente ? (Indicateur de **sauveur unique**)
* Une personne seulement est-elle autorisée à interrompre le leader ? (Indicateur de **duo dynamique**)
* Les membres interrompent-ils constamment les uns les autres ? (Indicateur de **mode combat**)
* Les membres évitent-ils de se remettre en question ? (Indicateur de **mode fuite**)

### **4. Comment l'équipe aborde-t-elle les grands problèmes ?**

* Une seule personne semble-t-elle être responsable des problèmes de l’équipe ? (Indicateur de **sauveur unique**)
* Deux personnes portent-elles seules les défis de l'équipe ? (Indicateur de **duo dynamique**)
* L'équipe évite-t-elle les problèmes et focalise-t-elle son agressivité sur un ennemi commun ? (Indicateur de **mode combat**)
* Est-ce que l’équipe blâme des facteurs externes mais évite la confrontation directe ? (Indicateur de **mode fuite**)

Après ce simple assessment il m’est clairement apparu que mon équipe oscillait entre des phases de sauveur unique, de combat et de fuite. Pour sortir de ces schémas, il va falloir adopter de nouvelles méthodes de travail et de pensée, plutôt que d’effectuer de simples interventions temporaires.

Pour cela, il m’appartenait donc de faire prendre conscience à l’équipe des forces en jeu, des tensions cachées et des rôles inconscients qui se forment au fil du temps. Quoi de mieux que l’auto-diagnostique pour accepter concrètement nos fragilités?

## Prise de conscience et Sociogrammes

Les auteurs préconisent l'utilisation de sociogrammes, des diagrammes représentant les relations et interactions entre membres d'une équipe, et les intervenants extérieurs potentiels. Il convient, pour cela, d’animer un atelier dans lequel chaque membre dessine sa perception des autres et des relations dans l’équipe. On identifie alors rapidement les sous-groupes, les leaders informels, les personnes isolées, et les rivalités en cours.

Pour cela, un atelier de 1h suffit. On expose les règles puis chacun dresse son diagramme, individuellement et en silence, afin de ne pas subir d’influence ou de perturbation. Chaque membre est représenté par une bulle nominative. On rapproche ou regroupe les individus les plus proches, et on trace des liens correspondant à l’intensité des échanges et des relations entre les “bulles”.

Je laisse 15 mn à chacun pour dresser son sociogramme et on les passe rapidement en revue. Le but ici est d’observer si les perception sont globales, si des schémas spécifiques ressortent ( une bulle ou deux bulles isolées, une qui communique avec tous, des relations simples ou tendues..). Un sociogramme qui présente sur le papier une équipe fonctionnelle mais qui présente des symptômes de sabotage pourra par exemple être une alerte de comportement de fuite (flight mode).

{{< figure  src="sociogramme.png" alt="Exemple de Sociagramme" class="center-large">}}

Une fois les comportements principaux identifiés, il est possible d'en discuter ouvertement, de redistribuer les responsabilités et de créer une culture de travail plus saine, où chacun peut s’exprimer et contribuer de manière équitable, ce que l’on a fait sur la seconde moitié de l’atelier.

<span style="text-decoration:underline;">Par exemple :</span> 

**Le sauveur unique** : si identifié par une grande partie de l’équipe pourra prendre conscience qu’il assume ce rôle sciemment ou parfois malgré lui. Il pourra alors modifier sa façon de donner les directions et encourager son équipe à prendre plus de responsabilités.

**Le duo dynamique** : Les acteurs identifiés pourront prendre conscience de l’existence d’une relation de codépendance, coupée du reste de l'équipe. Lorsque les choix de stratégies se font au détriment des préoccupations de l'équipe, elles sont souvent inefficaces. Il s’agira ici pour y remédier de s' aligner avec le groupe au complet. 

**Mode combat** : Une équipe anxieuse peut aussi s'unir contre un ennemi commun (réel ou imaginaire) au lieu de s'attaquer à ses propres problèmes internes. Un exemple est une banque européenne où l'équipe dirigeante a dirigé son hostilité contre les facilitateurs de formation, détournant l'attention des vrais problèmes de l'équipe.

**Mode fuite** : Lorsqu’il est identifié que l'équipe évite ses problèmes en blâmant des facteurs extérieurs, l’arrivée d’un nouvel acteur neutre ( nouveau membre ou coach externe) peut permettre à l'équipe à rompre avec ses propres mécanismes d'évitement en se recentrant sur les éléments sur lesquels l’équipe à la main, et non les événements externes.

Dans mon équipe, ce processus a permis de libérer la parole et a largement atténué les dynamiques destructrices.

Notre stratégie de cadrage des besoins qui était la propriété unique du “sauveur” devient de plus en plus l’affaire de toute l’équipe. ( On amorce enfin des “example mappings”, des “3 amigos”...)

Les choix techniques qui étaient portés par le “duo”, sont dorénavant discutés avec l’ensemble des développeurs et des architectes avant d’être validés. Les décisions sont parfois un peu plus longue mais on a pu éviter quelques pièges de faisabilité technique, en s’appuyant sur la veille et l'expérience de chacun. 

L’équipe, consciente des pressions business, commence à changer de regard sur le Métier ( ce bourreau) et réclame de passer du temps avec les keys users pour améliorer la compréhension des besoins et pour réaliser des formations à l’outil.

L’équipe se responsabilise . Des retards persistent mais on commence à pointer notre sur-engagement plutôt que des causes liées à des facteurs externes.

Il reste un long chemin à parcourir mais ces prises de conscience ont un véritable effet bénéfique, qui illustre bien le principe de ADKAR... mais ça c’est une autre histoire. 

{{< carousel duration="5000" ordinal="1">}}

Et vous, quelles dynamiques observez-vous dans vos équipes?