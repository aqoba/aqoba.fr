---
draft: false
title: La force des réunions où on ne prend pas de décision
authors:
  - laurent-dussault
values:
  - partage
  - conviction
  - altruisme
date: 2023-01-12T16:38:24.489Z
thumbnail: reunions_sans_decision.jpg
subtitle: Et si toutes les réunions ne devaient pas être faites pour trancher !
aliases:
  - 20221118-la-force-des-réunions-où-on-ne-prend-pas-de-décision
---
Peter Senge dans son livre **la 5ème Discipline** parle de l’intelligence collective. Il y décrit, entre autres, une bonne pratique pour que l’écoute soit optimum et que la logique de compétition laisse place à une logique de collaboration. Il propose d’avoir des temps très distincts entre l’échange d’idées ou de propositions nouvelles et la prise de décision.

## Décision = un seul vainqueur

Lors d’une réunion où une prise de décision doit être faite, il n’est pas rare de voir se mettre en place une **logique de compétition**.

On voit s’affronter des idées, ou pire des égos. Dans cette situation, les personnes titulaires de l’idée ou de l’ego en question vont chercher à se renforcer pour dominer le débat. Le moyen le plus rapide, malheureusement, pour doubler l’autre est de la ralentir, en dénigrant son point de vue et en pointant ses faiblesses.

Et comme dans ce genre de « jeu », il faut être plusieurs, bien entendu, ses rivaux vont faire de même.

Les ressources des participants sont donc accaparées sur les défauts et les failles des arguments de chacun.

En guise d’exemple, je pourrais citer de nombreux votes au sein de notre Assemblée nationale, où des députés votent contre une proposition qui, bien qu’elle soit un pas vers un attendu partagé, elle ne va pas assez loin selon eux. On finit ainsi parfois par renoncer à une amélioration qui va dans le sens souhaité, parce qu’elle a des défauts.

Regardons le vocabulaire qu’on utilise pour décrire les postures ainsi observées :

* On vend son projet
* On remporte la décision finale
* On « benchmark » des propositions
* On fait la promotion de ses idées

Tout souligne un contexte mercantile, une application d’une **logique de marché**, autrement dit, la loi du plus fort. 

![Illustration d'un débat sans fin, ou 2 groupes affrontent leurs arguments sans s'ecouter mutuellement](debat-sans-fin.jpg "Débat sans fin")

Plus simplement, on a coutume de dire que choisir c’est renoncer, ici, on renonce au mieux, en entendant le meilleur (qui parfois n’existe pas encore et n’existera jamais).

## Et si on se contentait de s’écouter ?

Quels mécanismes sont mis en œuvre dans des contextes différents : prenons l’exemple des conférences ou des communautés de pratiques.

Quand on participe à un tel événement, même si on est un orateur, on écoute les autres, dans l’espoir d’y trouver une ressource inconnue dont on va pouvoir **s’enrichir**.

Toute l’attention est donc portée sur les forces et les qualités des points de vue partagés.

Il n’est d’ailleurs pas rare qu’un auditeur de ce genre d’évènement finisse pas vouloir exprimer un agrégat de ce qu’il a compris/crée avec ses savoirs et compétences ainsi acquis.

Tout ceci n’est qu’un exemple de mécanisme de collaboration.

Dans ces circonstances, on quitte les lois du marché pour pratiquer **l’économie du savoir** :

* Quand je donne un savoir, un point de vue, je ne le perds pas (1 -1 = 1)
* Quand je combine 2 points de vue, j’en crée parfois un 3ème (1+1 = 3)

![Illiustration de la force de l'écoute comme étape de la co-construction](collaboration.jpg "Ouverture d'esprit et co-construction")

Ici, on cumule les mieux pour co-construire le meilleur.

## Pas de décisions, mais des échanges

Pour provoquer ces comportements, créons des espaces d’échanges où l’expression n’est pas jugée car les acteurs ne sont pas confrontés.

Donc oui, bloquons des temps de réunions qui ont cet objectif. Car oui, l’échange d'idées prend du temps et l’intelligence collective a besoin d’un espace pour se mettre en œuvre. Il n’y a pas de magie.\
Ainsi des **communautés de pratique** internes aux entreprises sont un moyen de créer cet espace.

Écoutons nous, mais ne tombons pas dans l’extrémité d’un monologue et rien d’autre.

Même si un livre, un article ou une vidéo peuvent déjà permettre d’émettre ses idées, l’intérêt du face à face est de pouvoir interagir et donc suivre le message qu’on émet : Vérifier qu’il est bien reçu, compris et qu’il suscite un intérêt (autrement que par des pouces bleus).

Il existe des outils pour tout de même apporter du feedback sur un point de vue exposé et engager des discussions positives.

S’il ne fallait en citer qu’un, qui permet de rester dans l’optique de valoriser ce qu’il y a de bien, d’enrichissant dans le discours proposé, regardons le **PERFECTION GAME**.

Au delà de donner une note à ce qui est exposé, il s’agit de justifier sa note par ce qu’il y a de positif à retenir.

Cela incite des retours qui ressemble à ceci :

« Sur ce qui est proposé je mettrais la  note de X/10, car il y a déjà telle ou telle valeur. Pour l’améliorer je te propose de creuser par là… »

Pour terminer, il faut individuellement apprendre à écouter, et voici quelques pistes :

* **Limiter les interruptions** : si des questions vous viennent, notez-les pour plus tard, quand l’orateur aura fini son raisonnement. Il se peut qu’il vous amène quelque part où vous n’imaginiez pas. Suivez son chemin, ne le ramenez pas sur le votre.
* **Bannir les jugements** : les « à mon mon avis », « selon moi », « je sais où tu veux en venir »….
* **Accueillir les imperfections** : remplacer le *NON* par des *OUI ET*…
* N**e pas inciter à des postures défensives** : un *POURQUOI* amener une justification... essayez de formuler un *COMMENT GÈRES-TU CECI*, qui prolongera la réflexion.

## Pour résumer

Bien distinguer 2 types de réunions qui auront 2 dynamiques très différentes :

* **Des instances de décision** (avec un protocole de décision clair et connu de tous, mais c’est un autre sujet)
* **Des instances de partage**, où l’ouverture d’esprit est de mise, sans autre enjeu que de comprendre le point de vue de chacun.

Bons échanges !
