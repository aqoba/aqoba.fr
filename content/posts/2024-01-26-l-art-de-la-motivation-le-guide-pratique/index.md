---
draft: false
title: "L’art de la motivation : Le guide pratique"
authors:
  - arnaud-bracchetti
  - thomas-clavier
values:
  - partage
  - conviction
  - humanite
tags:
  - motivation
offers:
  - unlock-management
date: 2024-01-29T23:05:11.018Z
thumbnail: escaliers.jpg
subtitle: De la prise de conscience à l’action !
---
## Pourquoi connaître les besoins de ses équipiers ?

Chaque individu a des besoins distincts qui évoluent dans le temps. Par exemple, besoin de manger, besoin de reconnaissance ou de pouvoir. Notre motivation principale, c'est de combler ces besoins. Vous avez faim, vous allez alors chercher à vous rassasier, ça passera probablement par une étape intermédiaire de gagner de l'argent pour acheter à manger. Une fois un besoin comblé, notre motivation peut changer pour se concentrer sur le besoin suivant ou au contraire avoir commencé à combler un besoin peut le décupler. L'intrication entre besoins et motivations est donc très forte. L'intérêt de se concentrer sur les besoins, c'est d'aller chercher la racine de la motivation. Je n'ai pas besoin de gagner de l'argent pour gagner de l'argent, j'ai besoin de manger, de me loger, de m'habiller, de me divertir, etc. Une fois ces besoins comblés, je n'aurais pas besoin de plus d'argent[^remuneration]. En quelques mots : ajouter un babyfoot n'est probablement pas l’action la plus urgente pour motiver les équipes.

Tout l'art de la motivation commence par apprendre à bien connaître les besoins de chacun.

Nous passons 8h par jour dans un environnement professionnel. Chez aqoba, il nous paraît important que ce tiers de notre vie soit le plus épanouissant possible. Répondre aux besoins des collaborateurs est probablement le premier élément à considérer pour construire des équipes et des organisations florissantes.

De nombreuses études et de nombreux modèles ont été publiés autour de ces sujets. De notre côté nous en avons sélectionné 3 :

* [Moving Motivator](https://management30.com/practice/moving-motivators/) de Management 3.0 mis en avant par Jurgen Appelo, apporte un aspect ludique à l'identification de ses propres besoins.
* [Michaël Aguilar](https://www.michaelaguilar.fr/) dans son livre de 2009 : [l'art de motiver, Les secrets pour booster son équipe](https://www.dunod.com/entreprise-et-economie/art-motiver-secrets-pour-booster-son-equipe)
* [Steven Reiss](https://en.wikipedia.org/wiki/Steven_Reiss), la référence la plus unisersitaire, qui a travaillé une grande partie de sa vie sur les motivations intrinsèques et à publié dans son article de 1998 : [Toward a comprehensive assessment of fundamental motivation: Factor structure of the Reiss Profiles.](https://psycnet.apa.org/doiLanding?doi=10.1037%2F1040-3590.10.2.97) un modèle complet.

À défaut de rentrer dans les détails de chacun, nous étaierons nos propos dessus.

## À qui parlons-nous ?

Cet article s'adresse aux leaders, aux managers, et à toute personne impliquée dans la gestion d'équipes. Que vous dirigiez une petite équipe ou une grande organisation, la compréhension des besoins individuels est une compétence fondamentale. En explorant des cas pratiques, nous visons à démontrer comment intégrer ces axes de motivations dans la vie quotidienne d'une équipe, créant ainsi un environnement où chaque collaborateur peut s'épanouir.

## Mise en application d’un modèle

### Comment avons-nous revitalisé la motivation de nos Scrum Masters ?

Il y a quelque temps, au sein du train que j’accompagnais, avec le RTE, nous avons été confrontés à un problème de manque d’énergie et d’envie de construire quelque chose ensemble de la part des Scrum Masters. Cette situation était devenue préoccupante, car la baisse de motivation semblait s'installer et se propager. Pour remédier à cette situation, nous avons décidé de mener un travail sur la motivation de ce groupe et sortir ainsi de ce cercle vicieux. 

### 1. Comprendre la motivation

La première étape de notre démarche a été de nous aligner sur ce qu’est la motivation et sur les moyens d’agir. Pour arriver à cet alignement, nous avons profité d’un point d’échange régulier que nous avions entre managers, coachs et RTE pour organiser une session d’[Extreme Reading](/posts/20221017-nous-avons-tenté-lexpérience-extrem-reading/). Lors de cette séance, nous avons pris 45mn pour lire le livre "L'art de motiver" de Michaël Aguilar, puis 45mn pour débriefer. 
Lors de notre débriefing, nous nous sommes tous alignés sur le fait que la motivation est un processus complexe et personnel. Ainsi, il est vain d’essayer de motiver les autres de la même façon que nous souhaiterions être motivés. Il existe de nombreux facteurs de motivation, les besoins de chacuns, et l’ordre de ces facteurs varie d'un individu à l'autre. Pour motiver, il est donc indispensable de commencer par comprendre quels sont les besoins de l’autre.

### 2. Choix du modèle et de la méthode

Après avoir clarifié notre compréhension de la motivation, nous avons réfléchi au modèle que nous souhaitions utiliser pour aborder ce sujet. Nous avons rapidement opté pour le modèle SACRÉ[^sacre], décrit dans le livre que nous venions de lire. Ce modèle a l'avantage de sa simplicité, ce qui le rend facile à utiliser et à intégrer dans des discussions informelles.

Pour notre démarche, nous avons décidé de rencontrer individuellement chaque Scrum Master, puis d’organiser un débriefing collectif et anonyme avec l'ensemble du groupe.

Pour animer ces entretiens, nous avons construit un jeu en partant des 10 cartes du Moving Motivator de Management 3.0. Nous avons choisi de partir de ce modèle, car il était déjà connu dans le groupe aussi bien les cartes que la mécanique de jeu. Nous avons ajouté 3 cartes pour couvrir plus uniformément les 5 catégories SACRÉ.

{{< figure  src="mm-et-sacre.png" alt="Répartition des cartes Moving Motivator dans le SACRE" title="Répartition des cartes Moving Motivator dans le SACRE" class="center-small">}}

Pour être cohérent entre le moving motivator et le modèle SACRÉ, nous avons effectué une table de correspondance entre les 2 modèles. Cette table a mis en évidence que les principes SACRÉ de Sécurité et de Confort était peu ou pas représenté dans le modèle du Moving Motivator. Nous avons donc décidé de rajouter 3 cartes pour équilibrer les correspondances.

{{< figure  src="trois-cartes-complementaires.png" alt="ÉVALUATION | Je connais précisément la façon dont je suis évalué | Sécurité |  | CONDITION | Mon poste de travail est fonctionnel et agréable | Confort |  | TRANSPORT | Les temps de transport domicile travail son acceptable, ou le télétravail et suffisamment développé | Confort |" title="3 cartes pour équilibrer les correspondances" class="center-small">}}

Notre objectif était de sortir de cet exercice avec un plan d'action validé par tous les membres de l'équipe.

#### 3. Les entretiens individuels

Chaque entretien était composé de deux parties :

Une première partie formelle, organisée autour du jeu de cartes que nous avions complété. Par son côté ludique, cette étape a grandement favorisé les personnes à s’exprimer, ce qui a facilité la 2nd partie.

La seconde partie était plus informelle. Pour nous aider à piloter cette partie nous avions préparé ces quelques questions :  

* En dehors de la mission actuelle, donne-nous des situations que tu as particulièrement appréciées, ou détestées, et explique-nous pourquoi.
* Pourquoi as-tu choisi ce métier ?
* Qu'attends-tu de ton RTE, ou de ton manager ?

Derrière les réponses à ces questions, nous sommes resté très attentifs aux thèmes SACRÉ que chaque partie de la réponse mettait en avant.

Après chaque entretien, nous avons fait un débriefing avec le RTE. Nous en avons profité pour classer les thèmes SACRÉ pour chaque personne.

#### 4. Synthèse et débriefing collectif

Une fois tous les entretiens individuels terminés, nous avons compilé les résultats pour mettre en évidence les facteurs de motivation partagés au sein de l’équipe des Scrum Master et ceux qui étaient plus individuels.

{{< figure  src="repartition.png" alt="Répartition des axes de motivations des équipoers dans le SACRE" class="center-small">}}

Suite à cela, nous avons présenté ces résultats à l’ensemble des Scrum Masters et nous nous sommes alignés sur des facteurs de motivation suffisamment communs aux différents membres de l’équipe pour pouvoir être abordés lors des ateliers de réflexion de groupe. Les facteurs plus individuels nécessitaient, quant à eux, une approche plus personnalisée, avec des discussions individuelles entre les Scrum Masters et le RTE.
Nous sommes sortis de là avec 2 choses :

* Une série d’ateliers planifiés pour identifier ensemble un plan d’action pour mettre en avant les facteurs de motivation commun au groupe.
* Des entretiens individuels entre Scrum Master et RTE. Avec, pour chacun d’eux, une liste de thèmes à aborder.

En conclusion, notre démarche visant à re-dynamiser la motivation de notre équipe de Scrum Masters a été fructueuse. En utilisant le modèle SACRÉ et en menant des entretiens individuels, nous avons pu identifier les facteurs de motivation essentiels pour chaque membre de l'équipe. Cette approche nous a permis de mettre en place des actions concrètes pour renforcer la motivation collective tout en tenant compte des besoins individuels. La motivation est un élément clé de la réussite de toute équipe, et investir du temps et de l'effort pour la cultiver en vaut la peine.

## À vous de jouer

Si vous voulez aller plus loin, nos références :

{{< figure  src="art-de-motiver.png" alt="L'Art de motiver - 2e éd. - Les secrets pour booster son équipe de Michaël Aguilar" title="L'Art de motiver - 2e éd. - Les secrets pour booster son équipe de Michaël Aguilar" link="https://www.amazon.fr/LArt-motiver-secrets-booster-%C3%A9quipe/dp/2100746049" class="float-left-tiny">}}

{{< figure  src="moving-motivator.png" alt="Moving Motivators : Motivation Game - Management 3.0 " title="Moving Motivators : Motivation Game - Management 3.0 " link="https://management30.com/practice/moving-motivators/" class="float-left-tiny">}}

{{< figure  src="PsycArticles.gif" alt="Reiss Model, The Science of Motivation® " title="Reiss Model, The Science of Motivation® " link="https://www.reissmotivationprofile.com/motivation" class="float-left-tiny">}}

3 modèles plus ou moins scientifiques, plus ou moins empiriques qui méritent votre attention. Notre plongée dans les méandres des besoins et de la motivation nous a conduit à des débats animés. Entre les trois modèles, les axes pour les regrouper, nous avons tenté de tisser des liens et de créer une toile harmonieuse.

### Le résultat ?

* Des cartes standardisées conçues pour mélanger les approches en mixant les decks,  Vous pouvez jeter un œil à nos [cartes](https://gitlab.com/aqoba/outils/art-de-motiver/-/releases/1.0/downloads/besoins-recto-verso.pdf).  N’oubliez pas de nous raconter ce que cette graine a produit chez vous…  Quelle "carte" allez vous ajouter ?
* Et que dire de Dan Pink dans tout ça ? Ici, juste une vidéo pour vous inspirer : [RSA ANIMATE: Drive: The surprising truth about what motivates us](https://www.youtube.com/watch?v=u6XAPnuFjJc)
* Mais attendez, il y a plus ! Motivations, besoins, aspirations – on dirait le début d'un bon roman (ou pas). Et que serait une discussion sur la motivation sans un clin d'œil à [Maslow](https://fr.wikipedia.org/wiki/Abraham_Maslow) et à la [spirale dynamique](https://fr.wikipedia.org/wiki/Spirale_dynamique) ?

Ces sujets semblent sans fin, une invitation constante à explorer de nouveaux horizons. Alors, à bientôt pour de nouvelles aventures motivantes !

[^remuneration]: Voir l’étude [Show them the money? The role of pay, managerial need support, and justice in a self-determination theory model of intrinsic work motivation](https://pubmed.ncbi.nlm.nih.gov/25810152/)
[^sacre]: Dans son livre, Michaël Aguilar, définit 15 besoins fondamentaux  qu’il regroupe dans 5 thèmes représentés par l’acronyme SACRÉ. La ***Sécurité*** désigne tout ce qui participe à une réduction des peurs et du risque. L’***Appartenance*** désigne les besoins sociaux, ainsi que l’ambiance de travail qui favorise le sentiment d’appartenance. Le ***Confort*** désigne tout ce qui concourt à la simplification de l’existence. La ***Reconnaissance*** désigne la quête d’affection et de feed-back positif. L’***Épanouissement*** regroupe le développement de soi, le dépassement de soi, la réalisation de soi mais aussi l’aventure et la nouveauté.
