---
draft: false
title: Comment Google a-t-il façonné sa culture d’entreprise ?
authors:
  - antoine-marcou
  - olivier-marquet
values:
  - partage
  - transparence
tags:
  - google
offers:
  - unlock-transformation
date: 2022-06-09T11:19:29.057Z
thumbnail: reza-rostampisheh-me4q1wherkc-unsplash.jpg
subtitle: Et doit-on s’en inspirer dans les entreprises traditionnelles françaises ?
---
« Des outils de management étourdissants », selon The Economist. « Un véritable Chef d’œuvre », selon Forbes. Le livre de Laszlo BOCK, “Work Rules!” (2015, Editions *John MURRAY*) que nous vous avons déjà conseillé dans la [bibliothèque agile idéale](https://aqoba.fr/posts/20220426-la-biblioth%C3%A8que-agile-id%C3%A9ale/) déroule sur plus de 350 pages les rouages de la culture d’entreprise Google. Il se présente aussi comme un guide pratique pour les managers et équipes RH qui voudraient transformer leur culture d’entreprise.

#### Mais les outils et pratiques que partage Work Rules! peuvent-ils vraiment être imités et produire les mêmes effets dans une entreprise française traditionnelle ?

Un mot sur Laszlo BOCK, d’abord : ancien de *McKinsey* et *General Electrics*, il a été recruté par Google comme *Head of People Operations* en 2006, soit 8 ans après la création de l’entreprise et 2 ans après son introduction en bourse. Il ne fait donc pas partie des pionniers de l’entreprise mais de celles et ceux qui ont contribué à structurer sa culture d’entreprise et accompagné son hyper-croissance. Il a quitté l’entreprise en 2016.

## Premier enseignement : La culture d’entreprise Google repose sur quelques principes simples et connus.

**La culture d’entreprise de Google n’a rien de secret. Elle s’est d’abord construite par tâtonnements, en faisant des erreurs et en les corrigeant. Elle est sans cesse en train d’évoluer. Elle repose sur 9 principes simples que les dirigeants, les managers et les équipes RH ne perdent jamais de vue :**

1. Ancrez une raison d’être de l’entreprise pour donner du sens au travail de chaque collaborateur
2. **Faites sincèrement confiance aux individus et aux équipes avec lesquelles vous travaillez**
3. Embauchez des personnes qui sont meilleures que vous-mêmes
4. **Ne mélangez pas l’évolution des compétences et le management de la performance**
5. Mobilisez votre énergie sur les 5% les moins performants et étudiez au microscope les 5% les plus performants
6. Soyez frugal tout le temps mais généreux lorsque vos collaborateurs en ont réellement besoin
7. **Payez vos collaborateurs de manière injuste**
8. Utilisez le Nudge ou et provoquez de grands changements par de petites actions bien ciblées
9. Face aux attentes croissantes, n’ayez jamais peur d’expérimenter, de vous tromper et, quand c’est le cas, de revenir en arrière

*Notez qu'ils’agit de notre traduction française des principes énoncés dans Work Rules! : nous assumons sa part de subjectivité.*

Nous avons indiqué en gras les 3 principes que nous allons détailler dans cet article. Nous avons choisi ceux qui sont le plus susceptibles de surprendre nos cerveaux français habitués à un management “traditionnel”.

## Focus #1 : Faites confiance aux individus et aux équipes avec lesquelles vous travaillez

Mouais… A priori, pas le principe le plus révolutionnaire de la planète. Mais chez Google, le curseur de la confiance est poussé à fond.

#### **Transparence, responsabilisation et absence de tabous**

Confiance signifie transparence. Par conséquent, un manager ne se demande plus quelles sont les informations qu’il/elle peut partager avec ses équipes : il/elle part du présupposé que toute information peut et doit être partagée avec elles. Restreindre l’accès à une information doit devenir un effort conscient et un manager a intérêt à avoir une excellente raison pour agir ainsi. Cette culture de la transparence est héritée du monde du logiciel Open Source, où cacher de l’information s’avère contre-productif.

Quatre exemples de pratiques en cours chez Google, partagées par Laszlo BOCK :

* Une nouvelle recrue chez Google aura, dès son premier jour, accès à l’immense majorité de son code source. Autrement dit aux secrets des algorithmes et produits du géant
* Chaque collaborateur, via l’intranet, a accès aux roadmaps Produit et aux OKRs (*Objectives, Key Results*) trimestriels de chaque équipe. Ainsi, s’il le souhaite, chaque collaborateur sait sur quoi travaillent les autres
* Des collaborateurs juniors sont régulièrement invités à assister à des comités de direction pour faciliter la diffusion des décisions qui y sont prises
* Chaque semaine, la direction et les fondateurs répondent aux questions des collaborateurs, sans modérateur : les questions dérangeantes y sont encouragées

Alors, oui : on peut toujours objecter que Google n'a fait que ré-inventer la boîte à suggestion. Mais soyons honnêtes : dans combien de nos entreprises françaises traditionnelles partageons-nous la manière dont les décisions sont prises ou invitons-nous à poser les questions qui fâchent ?

## Focus #2 : Payez vos collaborateurs de manière injuste

Ce principe est rédigé de manière provocante. Pourtant, il ne dit qu’une chose : Ne vous sentez pas obligé de payer 2 personnes le même salaire simplement parce qu’ils occupent le même poste.

Pour Laszlo BOCK, les meilleurs talents d’une entreprise sont meilleurs que ce que leur manager le pensent, et valent plus que ce qu’ils sont payés. Google a donc tourné le dos au système de rémunération traditionnel qui conduit les meilleurs à voir leur salaire plafonner au bout de quelques années : après 2 à 3 augmentations annuelles de 10%, ces personnes ont atteint le plafond salarial de leur poste.

Cela se voit régulièrement dans nos entreprises traditionnelles : à l’approche de ce plafond salarial, on donne à la personne un autre poste ou des galons de manager. Alors que cette personne n’a pas forcément l’envie ni les compétences pour cela. Cette pratique mène bien souvent à transformer des experts super performants en managers-superviseurs au mieux mal à l’aise dans leur travail et au pire toxiques pour leurs équipes.

Google prend cette pratique à contre-pied et s’appuie sur un fait simple :

#### **Les 1% les plus performants génèrent 10 fois plus d’impact positif que la moyenne. Les 5% les plus performants génèrent 4 fois plus l’impact positif que la moyenne.**

Par conséquent, l’entreprise applique un modèle de bonus où chacun est récompensé en fonction de son impact. Et sans réelle limite d’écart entre les montants des récompenses. Et tant pis si des personnes seniors se retrouvent fréquemment avec un package salarial très inférieur à celui de collaborateurs bien plus juniors.

Mais attention, cette pratique ne marche qu’à deux conditions : 

* Être précis sur la notion “d’impact positif” : comment on le définit et comment on l’évalue
* Et avoir des managers capables d’expliquer cette mécanique à l’ensemble des collaborateurs, y compris et surtout à ceux qui ne font pas partie des meilleurs performers

En définitive, ce principe “injuste” ne fonctionne que s’il est appliqué dans un cadre clair. Pour Bock, cette pratique permet d’éviter le départ des meilleurs collaborateurs dans un marché hyper-concurrentiel.

## Ne mélangez pas l’évolution des compétences et le management de la performance

#### Même les meilleurs d’entre nous ont des zones de progression, échouent ou ratent des choses. Et pour que les gens puissent apprendre il faut que le feedback fait par leur manager soit le plus constructif possible et surtout dénué de conséquences professionnelles ou financières. Dans le cas contraire, la personne visée se défendra et argumentera, allant à l’encontre d’une approche ouverte et d’une volonté de progresser.

C’est en cela que Google préconise de ne pas mélanger l'évolution des compétences, c'est-à-dire les discussions de feedback qui permettent au collaborateur de grandir, du management de la performance qui va sanctionner, généralement annuellement, la performance d’un individu et l’atteinte ou non des objectifs fixés.

Pour Google l’évolution des compétences s’appuie sur des conversations récurrentes, dans un environnement psychologiquement sûr, qui vont permettre au collaborateur de progresser et grandir, sans sanctionner l’erreur, ni qu’elle porte préjudice. Elles sont séparées en termes d'état d’esprit, mais aussi de lieux et de temps de la conversation pour l'évaluation de la performance, qui en France correspondrait à l’évaluation de fin d’année, et qui elle sera centrée sur l’atteinte ou non d’objectifs. 

Pour garantir que l’avis du manager soit le moins biaisé possible, Google préconise :

* Pour l’évaluation de la compétence, s’appuyer sur l’avis de pairs, par une discussion ou même juste un questionnaire
* Pour l’évaluation de la performance, calibrer ses évaluations avec d’autres managers, en groupe, afin de s’assurer de la plus grande équité

Qu’est ce que l’on observe aujourd’hui dans les entreprises françaises ?

A priori cette séparation entre évaluation des compétences et management de la performance est là. Mais l’évaluation des compétences est souvent malmenée, les principales causes :

* Un manager avec beaucoup trop de personnes à accompagner, à plus de 7 il devient très difficile d’assurer une suivi de qualité pour faire grandir les collaborateurs
* Un manager beaucoup trop occupé sur l'opérationnel, et objectivé sur des résultats opérationnels plus que pour le management des personnes
* Des discussion régulières, voir hebdomadaires, mais orientées sur l’actualité de la semaine et pas sur le développement des compétences

Nous avons ici parcouru 3 des principes qui fondent la culture d’entreprise Google, selon BOCK. Si vous souhaitez que nous creusions d’autres des principes de la liste partagée ci-dessus, n’hésitez pas à le mentionner en commentaire sur LinkedIn. Ce sera avec plaisir !

Et surtout, nous espérons que ces principes vous inspireront pour faire évoluer les pratiques de votre propre organisation.
