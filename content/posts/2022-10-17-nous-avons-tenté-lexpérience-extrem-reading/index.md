---
draft: false
title: Nous avons tenté l'expérience Extrem Reading
values:
  - partage
summary: >
  Chaque mois, nous nous retrouvons pour prendre du recul sur notre travail et
  notre manière de fonctionner chez aqoba. Pour nous, c’est aussi un moment
  privilégié pour échanger sur le fond : nos convictions, nos expériences, les
  nouvelles pratiques que nous expérimentons ou avons envie d’utiliser.
authors:
  - antoine-marcou
  - olivier-marquet
  - laurent-dussault
tags:
  - xtrem reading
  - livre
offers:
  - unlock-transformation
date: 2022-10-17T19:31:29.005Z
thumbnail: apres-lecture.jpg
subtitle: Assimiler Team Topologies en 1h15, c’est jouable ?
---
**Disclaimer** : Cet article n’a pas vocation à commenter l'ouvrage *Team Topologies* (ce sera l’objet d’un article prochain, combiné à unFix), mais à vous présenter la pratique de l'*Extrem Reading* telle que nous l'avons expérimentée. Nous ne revendiquons d’ailleurs pas la paternité de la méthode que nous avons employée, mais n’avons pas trouvé d’article ou de site la décrivant.

Chaque mois, nous nous retrouvons pour prendre du recul sur notre travail et notre manière de fonctionner chez aqoba. Pour nous, c’est aussi un moment privilégié pour échanger sur le fond : nos convictions, nos expériences, les nouvelles pratiques que nous expérimentons ou avons envie d’utiliser.

Pour l’aqoday d’octobre, nous avons décidé d’écouter [Thomas](https://aqoba.fr/team/thomas-clavier/) qui nous exhortait à tenter l’expérience *Extrem Reading* : choisir un bouquin et le lire de manière collaborative en quelques dizaines de minutes.

> 1h pour lire Team Topologies, chiche ?

Nous avons choisi Team Topologies car nous avons tous lu des articles à son sujet mais aucun de nous ne l’avait lu en entier. Et nous avions tous très envie de le décortiquer. Et décortiquer, c’est le mot. Vous allez voir !

## L’Extrem Reading, en 7 étapes (et 1 grosse heure)

{{< figure src="xtrem-reading-scribing.png" alt="Scribing de laurent">}}

### Étape #1 : Choisissez et commandez

Commencez par commander le livre que vous souhaitez lire en format papier broché. Pourquoi en format papier ? Parce que vous allez devoir le déchirer.

### Étape #2 : Découpez (sans vous faire mal)

Munissez-vous d’un cutter et découpez le livre en autant de parties qu’il y a de participants à l’atelier (voir notre photo). Ça fend un peu le cœur, mais c’est indispensable pour réussir l’atelier. Et puis rassurez-vous, vous pourrez le rafistoler après l’atelier.

{{< figure src="img_5960.jpeg" alt="Découpage du livre">}}

### Étape #3 : Lisez / Tour de lecture #1 - 15’

* Chaque participant récupère un bout du livre et s’isole
* On désigne un maître du temps qui lance un minuteur
* Et pendant 10’, chacun lit son bout de livre au calme. Lire est un bien grand mot, vu le grand nombre de pages il vous  faudra les parcourir stratégiquement pour en tirer la substantifique moelle. N'hésitez pas à prendre des notes directement sur le livre pour faciliter la mémorisation

### Etape #4 : Débriefez / Échange #1 - 15’

Au bout de 10’, le maître du temps rappelle tout le monde autour de lui/elle et chacun, à tour de rôle, résume ce qu’il a lu pendant 2 à 3 minutes. On prend bien évidemment le livre, et les pages, dans l’ordre de lecture. et chacun partage: 

* Ce qu’il/elle a retenu et ce qui l’a interpellé
* Comment cela résonne avec ses propres expériences, ses propres connaissances / lectures
* Ce qu’il/elle n’a pas compris ou pas eu le temps d’approfondir
* À la fin de son tour de parole, chacun dépose son bout de livre sur la table

### Étape #5 : Lisez / Tour de lecture #2 - 15’

Chacun prend un nouveau bout de livre et nous repartons pour un deuxième tour de lecture. À ce moment vous aurez probablement décidé d’approfondir un sujet en particulier sur la partie dont vous avez hérité, ou de zoomer un sujet qui n’a pas été vu au tout précédent. Il n’y a pas de règles. Faites comme vous le sentez.

### Étape #6 : Débriefez / Échange #2 - 15’

De la même manière, chaque participant débriefe la partie qu’il a lue. 

A ce stade, vous avez le choix de vous arrêter là (passez à l’étape 7) ou de lancer un troisième tour de lecture. 

### Etape #7 : Concluez - 5’

À la fin de l’atelier, posez-vous 2 questions 

* Avez-vous envie de lire ce livre dans son intégralité ?
* Et recommandez-vous sa lecture ?

{{< figure src="img_5974.jpeg" alt="Le livre découpé">}}

## Questions / Réponses

Bon, maintenant que vous avez saisi la démarche, quelques questions / réponses et indices pour faciliter votre propre *Extrem Reading*.

### Mais, attends… C’est impossible de lire 40 ou 50 pages en 15 minutes !

Effectivement, à moins d’être un lecteur particulièrement rapide, impossible de lire aussi rapidement. Pour plusieurs d’entre nous, la contrainte de la lecture rapide est plus synonyme de souffrance que de plaisir !

En 15’, l’enjeu n’est pas de lire en profondeur le texte. Mais de saisir :

* comment il est structuré
* quels en sont les principaux messages
* quel rôle cette partie du texte jour dans le cadre plus global du livre

Une fois que vous avez saisi ces grandes lignes, il vous reste quelques minutes pour lire en détail une partie du texte qui attire particulièrement votre attention.

N'hésitez pas à écrire sur vos pages lors de votre lecture, ça fixera la mémoire et laissera une information précieuse à celui qui les reprendra derrière vous.

### Pourquoi un débrief de 15’ ? Et pas plus ? Ou pas moins ?

Pour nous, l’intérêt de l’*Extrem Reading* consiste à aborder un texte au travers de différents regards. Les expériences, références, culture générale de chacun vont l’amener à lire le texte sous un angle particulier qu’il va faire partager aux autres.

C’est le croisement de ces différents angles qui fait toute la richesse de l’atelier. Nous vous conseillons donc de bien prendre le temps de débriefer entre vous. Sans courir après la montre.

### Est-ce qu’il faut vraiment acheter le livre en format broché ? Pourquoi pas en numérique ?

On s’est posé cette question qui a fait débat parmi nous. Et finalement, les adeptes de la lecture sur tablette / liseuse se sont rangés à l’avis des autres : pour cet exercice, mieux vaut un livre papier, pour deux raisons :

* D’abord parce qu’il est beaucoup plus facile de faire des allers-retours sur un texte papier vu le temps limité
* Ensuite, parce qu’on bénéficie des annotations des lecteurs précédents : on voit ce qu’ils ont souligné, ce qui a attiré leur attention. Cela éclaire la lecture

Et le bilan carbone, vous me direz ? Et bien, on s’est aussi posé la question, mais avons conclu que le jeu en valait la chandelle, d’autant plus que nous n’avons eu à nous procurer qu’un exemplaire pour 5 personnes. Et que nous avons, in fine, reconstitué l’ouvrage pour qu’il continue à servir.

### Bon, et en vrai, est-ce qu’à la fin de l’atelier, j’ai vraiment lu le livre ?

Définitivement non. A la fin d’un *Extrem Reading*, vous n’aurez pas l’impression d’avoir lu le livre que vous avez eu entre les mains. Vous n’aurez pas suivi l’argumentaire et les exemples de l’auteur. Vous n’aurez pas pris le temps de vous attarder sur un paragraphe difficile que vous aurez lu et relu pour vous en imprégner. Il se peut aussi que vous ratiez un détail structurant et ce malgré 2 lectures par 2 personnes différentes.

En revanche, vous connaîtrez les intentions de l’auteur, la structure de sa pensée, ses principales références et les messages clés qu’il veut passer. Et vous saurez s’il mérite d’être lu dans le détail.

Et si vous en reprenez la lecture seul, vous le ferez enrichi du regard de toutes celles et ceux avec lesquels vous avez réalisé l’*Extrem Reading* et avec des clefs de lectures qui vous aideront très probablement à rentrer plus rapidement dans le livre.

### Ça marche avec tous les livres cette méthode ?

Nous n’avons essayé pour le moment qu'avec le livre Team Topologies. A nous 5 nous avons lu 30 à 40 pages par personne et les parcourir en 15 minutes pour les résumer s’est avéré être déjà un un bon challenge.

À priori avec plus de pages par personnes il faudra soit accepter la difficulté, soit faire plus de rounds de lecture en ne découpant que des parties de 40 pages maximum.

Et bien sûr, inutile de vous lancer dans un roman avec cette méthode, vu la nature décousue de la lecture.

## Concluons : l'*Extrem Reading*, stop ou encore ?

D’un commun accord, nous avons décidé de retenter l’expérience de l’Extrem Reading tous les mois. Parce que nous pensons que c’est un moyen efficace et innovant de découvrir dans un ouvrage. Cette démarche ne remplace pas la lecture “traditionnelle” du livre, mais elle la complète, l’enrichit.

Mais assez parlé : si ça vous botte, tentez l’expérience !
