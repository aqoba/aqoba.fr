---
draft: false
title: "Approche systémique : Utiliser un système pour obtenir le changement souhaité"
authors:
  - laurent-dussault
values:
  - partage
tags:
  - systemique
offers:
  - unlock-transformation
date: 2022-08-30T14:12:41.586Z
thumbnail: 4559773791_c5c28440e2_o.jpg
subtitle: '"Casser" un système en place est souvent une tâche difficile...
  pourquoi pas utiliser celui-ci pour obtenir un "mieux" ?'
---
Vous avez sûrement déjà résumé des constats à : on est dans un cercle vicieux ou c’est une spirale infernale…
Vous constatez des impacts négatifs, produit par un système qui ne fait que renforcer le problème.
on se demande comment casser ce système… et bien avant de tout casser, si on se demandait si on ne pourrait pas utiliser la mécanique en place pour la rendre positive. C’est a priori beaucoup moins difficile que de changer le logiciel de votre environnement.
Intéressons nous ici au principe de modélisation de ces boucles et à l’étude des changements du 1er ordre.

## La pensée linéaire

Avant d’introduire la pensée circulaire, revenons sur la pensée linéaire par un exemple issue d’une histoire que j’ai mainte fois entendue :
Laissez moi vous conter la réponse à cette question : Pourquoi les réservoirs de la navette Challenger étaient-ils fichus ainsi ?
Pour acheminer ces réservoirs depuis leur lieu de construction, ils voyageaient par train. Or, sur le trajet, un tunnel devait être traversé.
La taille du tunnel a donc directement impacté la taille de nos réservoirs.
Maintenant,  interrogeons nous sur la taille de ce tunnel. Il est évident qu’un tunnel ferroviaire dépend de la taille du train, qui lui même dépend de l’espacement entre les rails.
Mais d’ailleurs, d’où vient cet espacement entre les rails ? Aux US, comme en Angleterre, il est la réplique du carrosse royal, qui était la norme des chariots.
Cette norme fait sens afin de limiter les perturbations pour le passage des roues dans les empreintes des 2 chevaux qui tractaient les engins. D’ailleurs les romains déjà avaient construit des routes tenant compte de cette dimension…
Pour vulgariser cette dimension : un peu plus que deux culs de cheval.

La pensée linéaire nous permet donc d’expliquer qu’un des engins les plus évolués du vingtaine siècle intègre une contrainte équine.

![pensée lineaire](pensee-lineaire.png)

J’aurai aussi pu vous parler de la défaite de Waterloo provoquée par les fraises de Grouchy ou encore de la faute originelle d’Eve, cause de nombreux maux …
Tout ça permet de raconter de jolies histoires, mais on sent bien que c’est un peu tiré par les cheveux et que les mécanismes que nous observons dans notre environnement nécessitent un regard différent.

## Les Boucles de causalité et leurs propriétés

Cette intuition qu’il y a un truc qui tourne en rond, qu’on se mord la queue… vous voyez de quoi je parle ?
Vous exprimez ainsi une intuition de ce que l’approche systémique nomme une boucle de causalité ou bien une boucle systémique. 

Commençons par LA boucle qui fascine tant :

![l'oeuf ou la poule](oeufoupoule.png)

L’intérêt de cet exemple n’est pas d’essayer de répondre à ce grand classique de l'œuf ou la poule, mais d’observer la boucle : elle est relativement stable et c’est d’ailleurs une propriété émergente de cette boucle.

Continuons à illustrer cette approche sur un exemple qui m’est plus personnel :
* J’aime la guitare
* Plus j’aime
* Plus j’y passe du temps
* Plus je progresse
* Plus je prend de plaisir
* Plus j’aime la guitare…..

![Schema simple](1.png)

Maintenant si je fais abstraction de ma personne et que je tente de généraliser cette boucle avec les concepts dont elle n’est qu’une instance. J’obtiens la boucle suivante :

![abstraction](2.png)

Maintenant vérifions la stabilité de cette boucle en renversant les termes positifs en terme négatifs :

* Moins on consacre du temps à jouer d’un instrument
* Moins on progresse
* Moins on prend de plaisir à jouer
* Moins on consacre de temps à jouer….

Ma boucle reste vraie et le renforcement du système reste vrai. Mon fils cadet en fait d’ailleurs l’expérience.

![boucle de renforcement d'émotion négative'](3.png)

## Comment agir sur un tel système

La première chose à faire est de prendre du recul. Il est intéressant de nommer la boucle ainsi obtenue. Ici par exemple, ce dernier schéma pourrait être titré : Logique pour abandonner la guitare. C’est un principe de dissociation simple en PNL. Ce simple fait de nommer les choses lui donne plus de consistance et est une première étape majeure pour identifier, reconnaître (au sens d’accepter) la problématique.

Ensuite, il faut s’abstraire du contexte local pour ne manier que des concepts, déchargés le plus possible d’émotions.

Enfin identifions dans ce schéma quelle zone d’influence nous pouvons avoir :

Je ne peux clairement pas décider de progresser en claquant des doigts.
Je ne peux pas contrôler mes émotions et décider que ça me plait si ce n’est pas le cas (on peut toujours tenter de tromper 1000 fois une personne….. mais ce n’est pas le sens de cet article)
Je peux par contre décider de consacrer du temps, et maintenir mon effort, c'est-à-dire passer plus de temps, le temps que notre système (qui est stable) s’auto-alimente…

Et c’est le principe de tout apprentissage, ça demande un effort initial.

Au passage si vous vous interrogez sur la durée raisonnable de cet effort, je ne peux que vous conseiller ce talk de Josh Kaufmann - *[YouTube | TEDx Talks
The first 20 hours -- how to learn anything | Josh Kaufman | TEDxCSU](https://youtu.be/5MgBikgcWnY)*

## Conclusion

Il est donc possible d’améliorer un système en utilisant ses propriétés et donc sa force, c’est un peu le judo de la systémique.
Cet article ne présente qu’une première approche, il existe bien sûr des systèmes trop complexes pour tenir dans une boucle unique, et des résultats significatifs nécessitent de casser le système en place pour introduire un nouveau système, mais nous en reparlerons une autre fois….

## Ça vous intéresse ?

Entraînez vous à repérer des boucles :

* Je repère là où quelque chose se passe
* Je cherche la boucle
* Je donne un nom à cette boucle
* Je fais apparaître la boucle “neutre” avec des concepts
* J’imagine les points de levier dans ma zone d‘influence

Cours gratuits sur la [chaîne de Luc RAMBALDI](https://www.youtube.com/watch?v=jsZ8OFnVlRw&list=PLYJr_BvPM0cvNnnW5E3LJuPy_xXF9X7pE)

Un jeu de carte pour résoudre des boucles simples : chez [Bloculus](https://bloculus.com/resolvia/)
