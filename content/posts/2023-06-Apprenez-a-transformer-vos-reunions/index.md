---
draft: false
title: Apprenez à transformer (enfin) vos réunions d'équipe sans coach
authors:
  - laurent-dussault
  - antoine-marcou
values:
  - conviction
  - partage
tags:
  - réunion
  - toptop
  - astuce
offers:
  - unlock-transformation
date: 2023-06-19T00:24:00.000Z
thumbnail: tips réunions illustration.jpeg
carousels:
  "1":
    - Résonnances en réunion.jpg
    - Résonnances en réunion (1).jpg
    - Résonnances en réunion (2).jpg
    - Résonnances en réunion (3).jpg
    - Résonnances en réunion (4).jpg
    - Résonnances en réunion (5).jpg
    - Résonnances en réunion (6).jpg
    - Résonnances en réunion (7).jpg
  "2":
    - DECIDER - a core protocol.jpg
    - DECIDER - a core protocol (1).jpg
    - DECIDER - a core protocol (2).jpg
    - DECIDER - a core protocol (3).jpg
    - DECIDER - a core protocol (4).jpg
    - DECIDER - a core protocol (5).jpg
    - DECIDER - a core protocol (6).jpg
    - DECIDER - a core protocol (7).jpg
subtitle: inclus notre méthode TIPTOP
---
> "J'ai passé 7 heures en réunion aujourd'hui, chuis lessivé !"

> "Un créneau pour se voir ? Bien sûr, attends... Dans 10 jours je te cale 30' entre 2 meetings, ok ?"

> "Ah ! Là j'ai 3 rendez-vous en parallèle. Pas mal. Bon, lequel je choisis ?"

La vie d'entreprise et la vie d'équipe vont de paire avec les réunions. Inutile de se casser la tête pour les faire disparaître (des générations entières de spécialistes ont essayé) : les réunions existent car elles sont (la plupart du temps) essentielles.

Aligner des individus, réfléchir en commun, prendre une décision, rendre compte, féliciter une équipe, analyser une situation, cadrer un nouveau projet, etc. Rassembler les bonnes personnes dans une même pièce est essentiel pour faire vivre une entreprise.

Le tout est de ***bien*** le faire.

[Jean-François Hélie](https://www.linkedin.com/in/jean-fran%C3%A7ois-helie-4240411ba/), spécialiste de la facilitation, souligne que plusieurs aspects des réunions sont en résonance directe avec la réalité d'un projet, tels que :

* La gestion du temps révèle le respect des délais, un élément crucial dans la réussite d'un projet.
* La relation avec l'animateur révèle la relation à la hiérarchie, et l'efficacité des échanges peut influencer la communication ascendante et descendante.
* La gestion de l'espace en réunion révèle des stratégies territoriales, comme l'occupation de l'espace physique ou la prise de parole.
* Les processus de communication en réunion révèlent les circuits d'information, et leur fluidité peut impacter la diffusion des connaissances au sein de l'équipe.
* Les résultats obtenus en réunion sont révélateurs de la réalisation des objectifs de l'équipe, et peuvent fournir des indications sur les progrès accomplis.
* L'énergie déployée en réunion est révélatrice de la motivation et de l'engagement des personnes impliquées dans le projet.

{{< carousel duration="5000" ordinal="1">}}

Dans cet article, nous explorerons des conseils et astuces pour des réunions plus efficaces. Préparez-vous à booster votre productivité et à rendre vos réunions plus percutantes !

## Déléguez 3 rôles clés à chaque réunion

Lors des réunions, nous sommes convaincus qu’il est essentiel de déléguer des rôles spécifiques à chaque participant. 

La nomination d'un facilitateur pour gérer le déroulement de la réunion, d'un secrétaire pour prendre des notes et d'un timekeeper pour veiller au respect du temps alloué sont des exemples de rôles clés. Ces responsabilités bien définies permettent une meilleure organisation et une plus grande efficacité lors des échanges. \
Les rôles délégués peuvent jouer un rôle dans la gestion efficace des réunions. 

Voici quelques rôles clés à considérer (il en existe d’autres):

### Secrétaire (Scribe) :

Le secrétaire/scribe est responsable de la prise de notes pendant la réunion. 

### Gardien du temps (TimeKeeper) :

Ce “chronométreur” joue un rôle crucial dans la gestion du temps alloué à chaque point de discussion. En respectant l’agenda et les délais fixés, vous maintenez la dynamique de la réunion et évitez de vous perdre dans des discussions interminables. 

Le Gardien du temps est explicitement autorisé à interrompre les débats, pour recadrer. recentrer… \
Les outils du timekeeper sont : un chronomètre ET un agenda bien préparé.

### Pousse Décision (push decision maker):

En tant que "pousse décision", votre rôle est de vous  concentrer sur l’émergence de décisions et d’actions concrètes. 

Le pousse décision veille à ce que des décisions soient prises, de manière définitive, avec le “commitment” de tous les participants.  Si des actions sont définies, il veille à ce qu’elle soit affectée en séance. 

Il est plus aisé d’être “pousse décision” si votre groupe a un protocole de décision défini et connu de tous, comme le [decider des core protocols](#appliquez-les-core-protocols)

## Prenez les notes de la réunion en transparence

Dans le cadre d'une réunion, le secrétaire peut utiliser un tableau blanc,un paperboard ou  bien partager son écran, le tout étant d’être visible de tous les participants, pour prendre des notes en temps réel. Cette approche favorise la transparence et permet à chacun de suivre et de contribuer à la documentation de la réunion. Les participants peuvent intervenir pour apporter des précisions, corriger des erreurs ou ajouter des informations importantes pendant que le secrétaire prend les notes. Ainsi, la prise de notes devient un processus interactif où les participants sont actifs et concernés par ce qui est consigné. 

Pour consigner une décision,faut-il encore qu’elle ait été énoncée. Ainsi notre scribe pourra interpeller la salle pour clarifier ou provoquer une décision. Il devient naturellement “pousse décision”

Les notes prises à la vue de tous sont à considérer comme un compte rendu préliminaire, et leur validation se fait directement lors de la réunion.

Cela encourage la collaboration et responsabilise les participants. Chacun est directement impliqué dans la production du compte rendu et de la documentation de la réunion. Les participants ont la possibilité de contribuer, clarifier et valider les informations en temps réel, ce qui renforce leur engagement et assure la cohérence des décisions prises. 

Cette approche favorise la confiance et la transparence au sein de l'équipe, en évitant une étape ultérieure de validation des notes et en garantissant que tous les participants sont actifs et concernés par ce qui est produit pendant la réunion.

## Appliquez la loi des 2 pieds

La loi des 2 pieds, popularisée par la méthode [Open Space Technology](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert), propose un principe simple mais puissant : si vous ne contribuez ni n'apprenez rien lors d'une réunion, il est préférable de quitter celle-ci. Encouragez les participants à utiliser leurs "deux pieds" pour se déplacer librement et rejoindre les discussions qui les intéressent le plus. Cela permet d'optimiser l'implication et l'engagement de chacun, en évitant les réunions inutiles ou où l'on se sent passif.

J'ai trop de réunions/je perds trop de temps en réunion :

Si vous constatez que vous avez trop de réunions ou que vous perdez trop de temps en réunion, la loi des 2 pieds vous encourage à agir de manière proactive. Voici quelques comportements à adopter :

{{< figure  src="lois des 2 pieds.png" alt="illustration de la loi des 2 pieds" class="center-small">}}

* Évaluez l'importance de votre présence : Interrogez-vous sur l'impact de votre absence à certaines réunions. Identifiez celles qui sont essentielles pour votre rôle et celles qui pourraient être moins prioritaires. N'hésitez pas à décliner les invitations aux réunions qui ne sont pas directement liées à vos objectifs ou où votre contribution est limitée.
* Priorisez et optimisez votre temps : Si vous devez assister à plusieurs réunions, assurez-vous de planifier votre emploi du temps de manière efficace. Identifiez les moments où vous êtes le plus productif et réservez ces plages horaires pour les tâches qui nécessitent votre concentration. Essayez également de négocier des durées plus courtes pour les réunions, en proposant des agendas précis et en limitant les discussions non essentielles.

## Mesurez systématiquement l'efficacité de vos réunions - Le ROTI

La mesure de l'efficacité des réunions est essentielle pour garantir une utilisation optimale du temps et des ressources. Le ROTI, ou Return on Time Invested, est une méthode simple et pratique pour évaluer la valeur et l'impact d'une réunion. Voici comment vous pouvez l'appliquer :

1. Définir les objectifs : Avant la réunion, clarifiez les objectifs spécifiques que vous souhaitez atteindre. Cela peut inclure la résolution d'un problème, la prise d'une décision, la communication d'informations importantes, etc.
2. Évaluer le temps investi : À la fin de la réunion, prenez quelques instants pour réfléchir au temps passé. Était-il utilisé de manière efficace ? Avez-vous pu atteindre les objectifs fixés ? Évaluez si le temps investi a été justifié par les résultats obtenus.
3. Mesurer le ROTI : Posez-vous la question suivante : "Quelle a été la valeur ajoutée de cette réunion par rapport au temps investi ?". Vous pouvez utiliser une échelle de mesure simple, par exemple de 1 à 5, pour évaluer le ROTI. Un ROTI élevé signifie que le temps investi a été pleinement rentabilisé, tandis qu'un ROTI faible indique que le temps aurait pu être mieux utilisé autrement.
4. Identifier les points d'amélioration : Utilisez les résultats du ROTI pour identifier les aspects à améliorer lors des prochaines réunions. Cela peut inclure une meilleure gestion du temps, une clarification des objectifs, une optimisation de la participation des membres, etc.

<figure class="center-small">
    <table style="text-align: left">
        <tr>
            <td><img  src="hand-01.svg" alt="illustration ROTI" style="height:70px"></td> 
            <td> J'ai complétement perdu mon temps </td> 
        </tr> 
        <tr>
            <td> <img  src="hand-02.svg" alt="illustration ROTI" style="height:70px"></td>
            <td> Améliorable, mon temps aurait pu être mieux investi </td> 
        </tr> 
        <tr>
            <td> <img  src="hand-03.svg" alt="illustration ROTI" style="height:70px"></td>
            <td> Je n'ai pas perdu mon temps mais sans plus </td> 
        </tr> 
        <tr>
            <td> <img  src="hand-04.svg" alt="illustration ROTI" style="height:70px"></td> 
            <td> J'en ai retiré plus que ce que j'attendais. J’ai gagné du temps </td> 
        </tr> 
        <tr>
            <td> <img  src="hand-05.svg" alt="illustration ROTI" style="height:70px"></td>
            <td> C'était génial, on remet ça quand ?  </td> 
        </tr> 
    </table>
</figure>

## Allégez votre agenda et celui de vos collègues

Lorsque vous êtes confronté à l'organisation d'une réunion, il est essentiel de vous poser la question : "Dois-je maintenir cette réunion ?". Pour vous aider à prendre cette décision de manière éclairée, vous pouvez utiliser un algorithme simple basé sur l'article "Hacker ses réunions" de "[Makestorming](http://makestorming.com/outils/freemium/fiches-exos)" :

### 1. Définir l'objectif de la réunion :

Identifiez clairement l'objectif spécifique que vous souhaitez atteindre avec cette réunion. Demandez-vous si cette réunion est vraiment nécessaire pour atteindre cet objectif.

### 2. Évaluer l'importance des participants :

Considérez les personnes qui seront présentes à la réunion. Sont-elles réellement indispensables pour atteindre l'objectif défini ? Si certaines personnes peuvent être exclues de la réunion sans compromettre les résultats, envisagez des alternatives telles que la transmission d'informations par email ou un bref point de suivi individuel.

### 3. Examiner les alternatives :

Explorez les différentes options alternatives à la réunion. Est-il possible d'utiliser des outils de collaboration en ligne pour échanger des informations et prendre des décisions ? Pourriez-vous organiser une réunion plus courte ou plus ciblée pour répondre spécifiquement à l'objectif défini ?

### 4. Évaluer le coût-avantage :

Pesez les avantages potentiels de la réunion par rapport aux coûts en temps, ressources et productivité. Est-ce que les bénéfices attendus de la réunion justifient réellement ces coûts ? Comparez également ces coûts avec les alternatives identifiées à l'étape précédente.

### 5. Prendre une décision :

Sur la base de votre évaluation, prenez une décision éclairée. Si vous constatez que les avantages de la réunion surpassent les coûts et que les alternatives ne sont pas suffisantes, maintenez la réunion. Sinon, envisagez d'ajuster la forme ou le format de la réunion ou de l'annuler complètement.

{{< figure  src="HACKER SES REUNION.png" alt="algorithme complet issu de Makestorming" link="http://makestorming.com/outils/freemium/fiches-exos">}}

Commençons par nous demander de quelles réunions on a ***vraiment*** besoin. En utilisant cet algorithme de décision, vous pourrez évaluer de manière plus objective la pertinence de maintenir une réunion. Cela vous permettra d'optimiser l'utilisation du temps et des ressources de votre équipe, tout en favorisant des réunions plus efficaces et plus ciblées.

## Utilisation d'outils et de techniques

#### L’ESVP (Explorateur, Shopper, Vacancier, Prisonnier) :

L'ESVP est une technique simple de “check in” qui permet aux participants de s'exprimer ouvertement sur leur niveau d'engagement dans une réunion. Encouragez les participants à se positionner en tant qu'Explorateur (ouvert à de nouvelles idées), Shopper (à la recherche de solutions), Vacancier (détaché de la réunion) ou Prisonnier (présent par obligation). Cette technique favorise la transparence et permet de mieux comprendre les attentes de chacun.

<figure class="center-large">
 <table>
  <tr>
   <td>

**Explorateurs**

J’ai envie d’en croquer. Je suis vraiment curieux de ce qu’on va découvrir, et il y a toujours de bonnes choses à prendre.

**Vacanciers**

Il y avait de la lumière, ça a l’air sympa, et surtout ça me sort du quotidien, alors je viens passer un bon moment.
   </td>
   <td>

![illustration ESVP](./ESVP.png)
   </td>
   <td>

**Shoppers**

Je viens jeter un œil et j’achèterai si ça me plait.

**Prisonniers**

Je n’ai pas eu le choix, on m’a forcé à venir ici, et à la limite, je ne sais pas pourquoi.
   </td> 
  </tr> 
 </table>
</figure>

#### Appliquez les "core protocols"

Les "core protocols" sont un ensemble de règles et de pratiques qui favorisent la collaboration et la communication efficace lors des réunions. Par exemple, le "Check-in" permet aux participants de partager leur état d'esprit et de se synchroniser, tandis que le "Decider" facilite le processus de prise de décision. En utilisant ces protocoles, vous pouvez créer un environnement propice à la participation active et à la productivité.

{{< carousel duration="5000" ordinal="2">}}

Pour en apprendre davantage sur ces protocoles et leur utilisation, je vous recommande de vous référer à leurs sources officielles :

* Site officiel des core protocols : <https://liveingreatness.com/>
* Livre "Software for Your Head: Core Protocols for Creating and Maintaining Shared Vision" par Jim and Michele McCarthy 

#### Une boite à outils : Les liberating structures

Il existe de nombreux formats d’animation, de facilitation d’une réunion à adapter en fonction de votre objectif. Nous publions il ya quelques mois un article sur les liberating structures qui vous offre une boite à outils de 33 formats d’interactions collectives

[Voir l'article dédié](../20230516-des-r%C3%A9unions-plus-efficaces-gr%C3%A2ce-aux-liberating-structures/)

## <span>6 clés que vous devez appliquer ***sans aucune exception*** <br> et qui vont changer votre vie et celle de votre équipe (faites-nous confiance)</span>

Notre méthode **TIPTOP** :


#### <span> **T** pour TITRE</span>

Soyez spécifique dans le titre du meeting que vous créez. Vous me voyez venir ? On oublie les "Point Arnaud - Laurent" ou "Réunion projet". Utilisez un verbe ou une proposition qui décrit le but de la réunion. "Atelier de cadrage du projet X", "Prise de décision sur...", "Brainstorming sur le Besoin Y", etc.

#### <span>**I** pour Intention</span>

Indiquez systématiquement l'intention de la réunion dans les notes de l'évènement. Ex : "Objectif : Prendre une décision concernant la solution à appliquer pour déployer la solution X dans les équipes". Cela permet aux participants invités de décider en toute connaissance de cause d'assister ou non à la réunion.


#### <span>**P** pour participant</span>

Choisissez avec soin les participants et ne cherchez jamais à "faire plaisir". Astreignez-vous à une liste stricte des personnes clés pour atteindre l'objectif de la réunion. Et abandonnez le réflexe d'inviter des personnes "pour leur les brosser dans le sens du poil : "Si je n'invite pas Michel, il va mal le prendre." Vous leur rendrez un plus grand service en n'alourdissant pas leur agenda.

#### <span>**T** pour temps</span>

Adaptez la durée des réunions que vous créez : Le créneau d'1h, c'est Sooooo 1995 ! Vous devez aligner des individus pour leur faire part d'une décision ? Ça vous prendra 20'. En revanche, vous devez réfléchir et brainstormer ? Prévoyez 1h20

#### <span>**O** pour "Outlook a tort"</span>

Ne choisissez ***jamais*** le créneau que vous propose Outlook / Google Calendar : Vos réunions ne démarrent jamais à l'heure pile ! Et ne se terminent jamais à l'heure pile ! Vous prenez soin de vos collaborateurs et vous leur permettez de souffler entre 2 meetings > vos réunions démarrent donc à 10h05 / 14h35 / 16h05. Et elles se terminent à 11h55 / 14h55 / 16h25

#### <span>**P** pour Pièce Jointe</span>

Si votre réunion s'appuie sur un support ou une note dont il est important que les participants prennent connaissance, joignez-le document en pièce jointe du créneau de réunion. Les participants ayant lu le document, le temps passé en meeting sera bien pus efficace. Par ex : Lorsque vous montez l'entretien annuel d'un collaborateur, mettez le support d'entretien en PJ. Cela lui permettra de préparer l'entretien sereinement.

6 pratiques simples quand vous montez une réunion
1 acronyme à retenir : **TIPTOP**
Faites-nous confiance : ça va changer la vie de votre équipe

## CONCLUSION

En adoptant quelques bons comportements et en promouvant une culture d'efficacité dans la gestion du temps et des réunions, vous pouvez contribuer à réduire la surcharge de réunions et à optimiser l'utilisation du temps de travail au sein de votre entreprise.

Il est temps de libérer le potentiel de vos réunions et de stimuler l'efficacité de votre équipe. 

Bonnes réunions !
