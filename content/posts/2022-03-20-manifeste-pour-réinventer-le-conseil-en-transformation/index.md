---
title: Manifeste pour réinventer le conseil en transformation
authors:
  - antoine-marcou
values:
  - conviction
tags:
  - conseil
  - transformation
  - agile
  - safe
  - management
date: 2022-03-18T20:59:53.422Z
thumbnail: pexels-pixabay-326055.jpg
subtitle: "Imposer une nouvelle approche du conseil : sincère, opérationnelle et
  efficace"
---
Avec mes camarades d’aqoba, cela fait 15 ans que nous travaillons dans le conseil aux organisations en France, principalement auprès de directions IT /  digitales ou auprès de directions générales.

Notre mission, comme celle des cabinets de conseil traditionnels, est claire : Transformer la manière dont travaillent les entreprises françaises pour les rendre plus performantes et en faire des organisations où il fait bon travailler.

En revanche, pour y parvenir, notre approche est radicalement différente de celle des équipes de conseil traditionnelles. Cette approche, nous l’avons synthétisée dans un manifeste de 4 valeurs et de 10 principes, en s’inspirant du manifeste agile.

Ces valeurs et ces principes sont notre boussole dans le choix de nos clients comme dans le pilotage de nos accompagnements, au quotidien. Nous sommes heureux de les partager avec vous : 

## 4 valeurs

* Nous ne nous rendons pas indispensables. Au contraire, nous mettons nos clients en autonomie
* Nous n’appliquons pas une recette générique. Au contraire, nous nous adaptons à l’ADN de nos clients
* Nous ne produisons pas de slides à usage unique. Au contraire, nous focalisons notre énergie à ancrer de nouvelles pratiques et outils
* Nous ne nous considérons pas comme un fournisseur complaisant de nos clients. Au contraire, nous nous positionnons comme un partenaire sincère

## 10 Principes

1. Notre objectif est de **faire évoluer la culture de l’entreprise** que nous accompagnons. Pas de remplacer la culture de l’organisation par une nouvelle culture normée. Nous construisons dans le respect de l’ADN de nos clients.
2. Nous pensons que **les pratiques et les outils tirent la culture de travail**. Nous transformons concrètement le modèle opérationnel de l’entreprise pour renouveler la manière de travailler : privilégier la transparence, repenser la relation managériale, optimiser l’engagement des équipes opérationnelles.
3. **Nous ne connaîtrons jamais mieux l’entreprise que nous accompagnons que celles et ceux qui y travaillent depuis des années**. C’est pourquoi nous les mettons aux manettes de la transformation dès le début de notre accompagnement.
4. Nous embarquons **tous les services clés** pour enraciner la transformation, **en particulier les fonctions support** : Direction Générale, Direction Administrative et Financière, Direction des Ressources Humaines. Sans elles, il n’est pas de transformation pérenne.
5. Nous formons les individus à maîtriser notre boîte à outils et la culture de travail associée. **Nous les mettons en autonomie en 3 étapes** : nous faisons à leurs côtés > nous faisons ensemble > ils/elles font à nos côtés.
6. **La transformation ne vaut que si elle produit des bénéfices concrets** à la fois pour les clients de l’entreprise, pour sa position de marché et pour ses collaborateurs.
7. **Nous mesurons en permanence les impacts de la transformation** et mettons à disposition de toutes et tous les résultats de nos mesures, qu’ils soient bons ou non.
8. Le nouveau cadre de travail comme ses résultats sont **formalisés en continu dans une documentation vivante et accessible à toutes et tous**. Nous limitons le temps passé à produire des documents « à usage unique », type Power Point.
9. **Il n’y a pas de sujet tabou** : nous sommes sincères et transparents avec les personnes que nous accompagnons. Nous attendons la même transparence de la part de nos clients et nous tirons les leçons des critiques que nous recevons.
10. **Nous acceptons que la transformation se poursuive après notre départ**, car nous avons appris à l’entreprise à s’adapter en permanence à son environnement et à répondre à ses difficultés.

**Pourquoi ce manifeste ?** Parce que  nous considérons que l’approche traditionnelle des cabinets de conseil en transformation est **dangereuse pour les entreprises françaises**.

* Elle **tend mécaniquement vers l’implémentation de la même recette « Marmiton »** dans toutes les organisations. Un exemple avec le framework agile à l’échelle de la Scaled Agile : SAFe. Le considérant comme un nouveau standard, ces cabinets forment quelques-uns de leurs consultants et le déploient « by-the-book ». Résultat : ils tentent de faire rentrer un rond dans un carré et dégradent involontairement la performance des équipes et le bien-être des collaborateurs.
* Un **Target Operating Model gravé dans le marbre est une perte de temps et d’argent** : généralement formalisé sous forme d’un lourd document Power Point, le « TOM » est déjà obsolète au moment où il est livré en grande pompe au management du client. Entendons-nous bien : Documenter un cadre de travail homogène est essentiel. Mais ce cadre doit rester vivant pour s’adapter en permanence aux nouvelles réalités de l’organisation. Les consultants qui travaillent à un TOM sont souvent de bonne foi : livrer un TOM a quelque chose de satisfaisant. Mais inconsciemment, ils abusent leur client en dédiant des semaines de travail précieuses à autre chose qu’à déployer sur le terrain  le nouveau cadre de travail
* Le nouveau modèle opérationnel, **s’il n’a pas été conçu et déployé avec les collaborateurs de l’entreprise** s’effrite mécaniquement après le départ des consultants. Et c’est naturel : comment pourraient-ils le maintenir s’ils n’en comprennent pas le sens et s’ils ne le maîtrisent pas sur le bout des doigts ? Or, c’est la manière de faire des cabinets de conseil traditionnels : concevoir un modèle en chambre formalisé et le déployer à grand renfort de consultants juniors.

Parce que nous pensons que le conseil doit faire sa révolution, nous nous battons pour une approche en rupture. Nous espérons que nombreuses et nombreux d’entre vous, grands et petits, s’y retrouveront. Et se trouveront à nos côtés pour la défendre.

Si vous souhaitez en discuter plus encore **n'hésitez pas à nous contacter sur [contact@aqoba.fr](mailto:contact@aqoba.fr)**
