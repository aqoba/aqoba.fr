---
draft: false
title: "Les rôles délégués : impactez vos réunions"
authors:
  - laurent-dussault
  - benjamin-feireisen
values:
  - partage
  - conviction
tags:
  - réunion
offers:
  - unlock-delivery
date: 2024-11-03T08:26:00.000Z
thumbnail: roles-delegues.png
carousels:
  "1":
    - DECIDER - a core protocol.jpg
    - DECIDER - a core protocol (1).jpg
    - DECIDER - a core protocol (2).jpg
    - DECIDER - a core protocol (3).jpg
    - DECIDER - a core protocol (4).jpg
    - DECIDER - a core protocol (5).jpg
    - DECIDER - a core protocol (6).jpg
    - DECIDER - a core protocol (7).jpg
subtitle: ET NON, ON NE RÉSOUT PAS TOUT AVEC UN TIMEKEEPER !
---
Quand je questionne des managers, des dirigeants et même certains Scrum Masters sur leur connaissance des rôles délégués, la réponse revient souvent au seul Timekeeper. Ce rôle, qui consiste à gérer le respect du temps lors des réunions, semble être le plus connu et utilisé. Pourtant, il existe bien d’autres rôles délégués qui peuvent apporter une grande valeur.

L’objectif de cet article est de vous offrir un éclairage plus large sur ces rôles, au-delà du Timekeeper. Je vous présenterai non seulement un tour d’horizon des rôles délégués les plus importants, mais aussi des conseils pratiques pour les utiliser de manière efficace dans vos équipes. Nous aborderons également les pièges à éviter et les bonnes pratiques pour que ces rôles contribuent véritablement à l’autonomisation et à la performance de votre équipe

Pour finir, Benjamin vous propose un [exemple de mise en œuvre](#un-exemple-dun-atelier-avec-une-équipe-utilisant-les-rôles-délégués-depuis-quelques-mois) des rôles délégués dans une situation concrète. 

## Origine, Intention et Application

### Premières apparitions

{{< figure  src="livre-cardon.png" alt="livre Coaching d'équipe - Alain Cardon" class="float-right-tiny">}}

Il n'existe pas de source unique et exhaustive retraçant l'origine précise des Rôles Délégués. Ce concept, bien qu'ayant gagné en popularité ces dernières décennies, notamment grâce à des auteurs comme Alain Cardon (“Coaching d’équipe” - Editions Eyrolles), semble s'être développé de manière progressive et s'inspirer de différentes approches.

Les cadres agiles ont toujours cherché à favoriser l'auto-organisation des équipes, permettant ainsi à chaque membre de s’investir activement. Cependant, dans un environnement complexe et souvent contraignant en termes de temps, il est apparu nécessaire de définir des rôles spécifiques pour éviter que certaines responsabilités ne retombent uniquement sur un ScrumMaster ou un facilitateur.

### Intention Globale des Rôles Délégués

Les rôles délégués ont une intention claire : répartir les responsabilités afin d’engager plus de membres de l’équipe dans le succès des réunions, des cérémonies, et des projets. En déléguant certains rôles, on permet à chacun de jouer un rôle actif dans le bon déroulement des échanges, évitant ainsi que tout repose sur une seule personne, souvent le ScrumMaster ou le coach.

Les rôles délégués permettent également :

Clarté et focus : Chaque membre de l’équipe sait qui est responsable de quoi, ce qui réduit les ambiguïtés et améliore la collaboration.
Engagement collectif : En intégrant des rôles délégués, on encourage la participation active et responsabilisée de tous. Les décisions et actions ne reposent plus uniquement sur une ou deux personnes, mais sur l’ensemble de l’équipe.
Répartition de la charge cognitive : En attribuant des rôles spécifiques, on réduit la charge cognitive du facilitateur ou du ScrumMaster, qui peut se concentrer sur quelques tâches ou bien simplement participer comme un membre de l’équipe (qu’il est censé être !!). Cela garantit aussi une fluidité dans les processus sans dépendre d'une seule personne.

Le principe est donc de maximiser l’autonomie de l’équipe en garantissant que chaque rôle contribue au bon fonctionnement du groupe sans surcharger un individu.

Comme il s’agit d’un apprentissage collectif, ça sera particulièrement pertinent pour des réunions cycliques, ou a minima , des points qui réunissent régulièrement les mêmes personnes.

### Survol des Rôles Délégués

Voici un tableau présentant les rôles délégués les plus courants :

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Rôle     | Description | Objectif | Exemple d’action |
| :------: | ----------- | -------- | ---------------- |
| **Gardien du Temps** | Assure le respect du temps durant les réunions et les cérémonies pour éviter les débordements. | Permet de maintenir la réunion dans le temps imparti, en veillant à ce que chaque sujet soit traité. | Il peut, par exemple, annoncer les temps restants pour chaque point de l’ordre du jour ou rappeler les priorités en cas de débordement. |
| **Facilitateur** | Guide les réunions et ateliers en apportant une structure et une dynamique participative. | Garantit que chacun puisse s’exprimer et recentre les échanges quand ils dévient du sujet principal.| Par exemple, il peut poser des questions ouvertes pour clarifier un point, rappeler le but de la réunion, ou synthétiser avant de passer à autre chose. |
| **Pousse-Décision** | Encourage l’équipe à prendre des décisions lorsque nécessaire pour éviter les blocages ou les indécisions. | Accélère les prises de décision pour maintenir le rythme de la réunion et garantir son efficacité. | Il peut inviter un membre de l’équipe à exprimer son avis ou à trancher, ou encore rappeler les options pour aboutir à une décision. |
| **Coach** | Apporte un regard “méta” et propose des axes d’amélioration pour la suite, en aidant l’équipe à prendre du recul sur son fonctionnement. | Aide à l’amélioration continue en relevant des points de progrès pour les prochaines réunions. | Par exemple, il peut proposer une rétrospective sur les points qui ont bien fonctionné ou qui doivent être ajustés. |
| **Participant** | Chacun est invité à prendre part activement aux discussions et à contribuer aux décisions collectives. | Favorise l’inclusion et l’expression de tous les points de vue. | Les participants peuvent/doivent poser des questions, émettre des idées ou exprimer leurs préoccupations pour enrichir les échanges.|
{{</ table >}}

Rôles supplémentaires :

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Rôle     | Description | Objectif | Exemple d’action |
| :------: | ----------- | -------- | ---------------- |
| **Scribe** | Prend en charge la prise de notes et veille à rendre visibles les informations pour tous. | Garantir la traçabilité des échanges et décisions, en rendant les informations accessibles à tous. | Par exemple, il peut résumer les décisions prises et les afficher en direct sur un support partagé, ou envoyer un compte-rendu après la réunion. |
| **Hôte** | Responsable de l’intendance et de l’accueil des participants, ce rôle agit également en amont de la réunion. | Faciliter le bon déroulement de la réunion en assurant que l’accueil et la logistique sont bien organisés. | Il peut par exemple préparer la salle, offrir des rafraîchissements ou accueillir les participants en début de séance. |
| **Technicien** | S’occupe des aspects techniques, notamment dans les environnements de réunion avec connectiques complexes ou défaillantes. | Assurer une expérience fluide en résolvant les problèmes techniques pour ne pas interrompre les échanges. | Par exemple, il peut tester et préparer les équipements en avance ou intervenir en cas de problème de connexion ou de son. |
{{</ table >}}

## Conseils pour Utiliser les Rôles Délégués de Manière Optimale

### Conseils d'ordre général

**Mise en œuvre** 
L’introduction des rôles délégués doit se faire de manière progressive. Commencez avec deux rôles clés, comme le Gardien du temps et le Scribe, puis observez les bénéfices sur l’efficacité des réunions. Il est important d’expliquer clairement ces rôles à l’équipe, car leur nombre et leur nature dépendent souvent de la taille du groupe. Par exemple, dans une équipe de trois personnes, les responsabilités seront réparties différemment que dans un groupe de dix.

Les rôles peuvent également être adaptés et choisis en fonction des besoins spécifiques de chaque réunion. Au fur et à mesure que l'équipe devient plus mature et que les membres s'impliquent naturellement dans la dynamique de groupe, il devient moins nécessaire de formaliser ces rôles. La vigilance et la répartition des responsabilités deviennent alors presque automatiques, et la distribution des rôles peut être moins explicite sans que cela nuise à l'efficacité collective.

**Les rôles travaillent ensembles** 
Inutile de tenir un rôle de manière mécanique et intraitable. Le contre exemple qui me vient en tête est le gardien du temps qui annonce la fin du temps, en s’opposant au pousse-décision qui questionne la conclusion de la séquence…

Autre exemple, si le facilitateur délègue la fonction d’hôte, il sera explicite sur l’intendance nécessaire à son animation (taille et disposition de la salle etc…).

Ces rôles faisant “équipe”, il peut être pertinent d’inclure de temps en temps une rétrospective de l’usage des rôles délégués.

**Rotation des rôles**
Les rôles délégués doivent tourner régulièrement pour éviter la cristallisation de certains pouvoirs ou jeux psychologiques. Laisser constamment un rôle à une seule personne peut entraîner des tensions ou des enjeux politiques dans l’équipe.

Cette rotation n’est pas nécessaire à chaque réunion, une période de temps peut également être un repère : Je serai scribe pour un mois, puis on changera…

**Participants :  Ce n’est pas réservé aux autres.**
Même quand on a un rôle attribué, on doit rester un acteur de la réunion. Toutefois il est bon de préciser, quand vous prenez la parole, que vous intervenez en tant que participant ou scribe ou etc…

### Pièges à éviter

{{< figure  src="time-keeper.png" alt="Time Keeper" class="float-right-tiny">}}

**Gardien du temps (Time Keeper)**

*Pièges courants*\
Certains participants peuvent se “jeter” sur ce rôle pour éviter de prendre des responsabilités plus engageantes. Ils se cachent derrière la "gestion du temps" avec enthousiasme pour ne pas contribuer au fond des discussions.

*Conseil*\
Veillez à ce que le Gardien du temps participe aussi activement au contenu de la réunion.
Soyez explicite sur le fait que ce rôle est “facile” et attribuez le en dernier !

**Scribe**

{{< figure  src="scribe.png" alt="Scribe" class="float-right-tiny">}}

*Ce qu’on entend souvent :*\
Il est en charge d’envoyer en CR !! (Compte Rendu)
Écrire un CR que personne ne lira, ou (pire) que tout le monde va contester, ne remplit personne de joie.

*Conseil*\
 Rendre les notes visibles, pendant la réunion, en direct, à tous les membres de l’équipe (par exemple via un tableau blanc,  un écran partagé ou un outil collaboratif en ligne). Cela permet une transparence immédiate et évite des malentendus ou des oublis.
En annonçant que ces notes seront “le” CR (incontestable parce que vous l’avez tous vu !)… vous obtiendrez probablement l’attention de tous sur ce qui est écrit.
Et votre CR aura au moins été lu cette fois ci.

**Pousse décision (Push Decision)**

{{< figure  src="push-decision.png" alt="push decision" class="float-right-tiny">}}

Ce rôle est là pour inciter l’équipe à prendre des décisions à des moments clés, sans trop retarder les actions à cause d'hésitations ou de discussions prolongées.

*Conseil*\
Ce rôle fonctionne bien en tandem avec le Scribe (même personne ou duo de choc), car la personne qui pousse à la décision peut s’appuyer sur les notes prises en direct pour clarifier les points et encourager la conclusion des discussions. Il insistera sur le fait que la décision est bien présente dans les notes, dans une formulation claire pour tous : 

* "Quelle décision prenons-nous à ce sujet ?"
* "Qui sera responsable de cette action ?"

Il doit s’appuyer aussi sur le Gardien du temps, et rappeler qu’il ne s’agit pas de “changer de sujet, parce qu’il est l’heure” mais d’arriver à une conclusion/décision.

Avoir un protocole de décision peut également aider. Si vous pratiquez le “DECIDER”, le pousse-décision peut arrêter les débats et faire une proposition qu’il soumet au vote. 

{{< carousel duration="5000" ordinal="1">}}

**Coach**

*Pièges courants*\
Attribuer ce rôle à la personne dont c’est la fonction : LE coach ou LE Scrummaster.
Cela détache l’équipe de la gestion de sa propre amélioration continue.

*Conseil*\
Donnez à ce rôle une seule mission : Veillez à ce que les rôles soient tenus. Par exemple, il servira de backup au Gardien du temps, soutiendra le pousse-décision…
Il pourra aussi proposer un unique point d’amélioration à l’issue de la réunion (lors des 5 dernières minutes)
Eviter les constats génériques (“on” , fatigue générale… pourquoi pas la météo ?)...
Pointer des choses factuelles, en exprimant le ressenti personnel que ça provoque (voir notre article sur la [Communication Non Violente](../20230828-communication-nonviolente-en-entreprise-la-clé-pour-des-relations-harmonieuses-et-une-collaboration-efficace/) )...
Le scribe inscrit l’axe d’amélioration dans les notes !

## A vous !

Moi aussi je me suis retrouvée dans des réunions ou j'étais : 

* hôte : qui planifie, lance les invitations, ouvre la salle, arrive le premier, part le dernier…
* technicien : qui checke que la video va marcher, que le matériel est là et opérationnel (micro, stylo etc…)  et qui le restitue à la fin.
* facilitateur, qui structure la réunion
* scribe (aka faciltateur graphique)
* gardien du temps, pousse-decision… 

... et c’est bien normal qu’un coach donne l’exemple, par contre, TOUS ces rôles doivent être pris en main par l’équipe pour que les dynamiques misent œuvre perdurent.

Prenez quelques secondes pour repenser à vos réunions d’équipe, vos PI Planning etc…\
Ces rôles sont-ils clairement répartis ? Tournent-il ? Quelqu’un compense-t-il en coulisse ? 

Alors saisissez-vous de ces conseils, expérimentez, 
et n’hésitez pas à nous faire un feedback !

## Un exemple d’un atelier avec une équipe utilisant les rôles délégués depuis quelques mois


La réunion a commencé depuis 5 minutes, les personnes sont arrivées dans la salle, les rituels classiques s’enclenchent,  “Bonjour, tu vas bien ?” “Oui, et toi ?”. Puis l’atelier commence…

*Benoit (Manager) :* “ Nous avons plusieurs sujets importants en ce moment, et en priorité je vous propose de faire maintenant un atelier sur la délégation de certains sujets entre vous et moi. J’ai bien vu qu’il y avait des points de divergences sur ce sujet… Et même si c’est notre second point sans notre coach d’équipe, restons fidèles à notre logique de rôles délégués, ça marchait bien ! Je propose donc que l’un d’entre vous soit le facilitateur. Qui souahite endosser ce rôle ?”

*Manu (Facilitateur) :* “Allez, ça faisait un moment que je voulais tenter. Je le prends, et la délégation je connais un peu les concepts ! Je vais avoir besoin d’abord d’un pousse-décision, qui nous recentre sur la prise de décision et les notes. Qui veut m’aider ?”

*Vincent (Pousse-décision) :* “Moi, je suis d’accord, je m‘en charge…”

*Manu (Facilitateur) :* “Top ! Maintenant il me faut un coach, qui nous prend 15 minutes à la fin de la réunion pour qu’on puisse améliorer nos points, et nous proposer des pistes d’améliorations sur nos rôles et notre participation.”

Après quelques secondes de silence…

*Françoise (Coach) :* “Je veux bien le faire; Même si  je ne suis pas toujours à l’aise, je sais maintenant que c’est un rôle crucial !”

*Manu (Facilitateur) :* “Merci, je me rends compte que ce n’est pas toujours facile… Et il me reste un rôle : le cadenceur, qui nous rappelle le temps régulièrement et nous challenge sur le temps restant !”

*Anais (Gardienne du temps) :* “Je prends le rôle.”

*Manu (Facilitateur) :* “Merci Anais ! Nous avons tous nos rôles. Juste, vous pouvez me laisser 3 minutes, le temps que je crée l’atelier. Benoit, peux-tu m’aider ? On le fait ensemble ! Le reste des participant speuvent en profiter pour aller prendre un café rapidement ou discuter en attendant.

*Anais (Gardienne du temps) :* “Ok pour 3 minutes, tu es sûr que tu ne veux pas plus de temps ?”

*Manu (Facilitateur) :* “T’inquiète pas, je pense avoir l’idée… 3 minutes c’est pour l’écrire.”

3 minutes plus tard…

*Benoit (Manager) :* “ Cela me semble très bien !”

*Manu (Facilitateur) :* (se tourne vers les participants) “Voici l’agenda pour l’atelier : tout d’abord nous regarderons quels sont les sujets importants de délégation dans l’équipe pendant 10 minutes par une idéation individuelle. Puis nous prendrons 10 minutes ensemble pour les proposer et les réduire ensemble de manière collective, et nous ferons un essai de delegation poker pendant 1h, ce qui j’espère sera assez large, puis nous laisserons pour notre coach les 15 dernières minutes pour nous faire réfléchir sur nos postures et sur comment améliorer ce point régulier. Cela vous convient ?

*Virginie (Participante) :* “C’est clair pour moi !”

*Anais (Gardienne du temps) :* “Si ça vous va, je regarde le timing. Je vous rappellerai le temps des ateliers, mais aussi l’heure à chaque fois. Actuellement, il est 14h15, le temps total prévu étant jusqu’à 16h, ça semble parfait”

*Manu (Facilitateur) :* “Parfait. On commence : on lance l’idéation !”

La réunion avance… Nous sommes au moment des discussions sur les points clés à déléguer.

*Benoit (Manager) :* “Ok, je commence à avoir les différents points, c’est plus clair, par exemple suivre le budget, je n’ai même pas pensé que ça pourrait vous intéresser… !”

*Vincent (Pousse-décision) :* “ça va donc faire 8 minutes qu’on en parle, niveau timing c’est serré, donc quelle décision on prend maintenant pour savoir lesquels on garde ?”

*Manu (Facilitateur) :* “On en a 12 là. Ce que je propose c’est de tenter d’en prendre que 8 pour commencer, on en enlève 4 ?”

*Anais (Gardienne du temps) :* “Compliqué niveau temps, je sens que ça va nous prendre bien plus que 5 minutes ça…”

*Vincent (Pousse-décision) :* “Je propose de les garder comme ça, en 1h on devrait pouvoir sélectionner les zones à déléguer.”

*Virginie (Participante) :* “On essaie plutôt de les prioriser, peut-être par un vote ? Et si on a pas le temps de tout faire, on finira lors du début du prochain atelier ?”

*Manu (Facilitateur) :* “Hmm… Ok, on fait un ‘dot-voting’, chacun a 3 votes !”

Avançons dans la réunion… Pour arriver à la toute fin.

*Manu (Facilitateur) :* “Franchement bravo, nous avons défini ensemble les niveaux de délégation ! On a encore quelques désaccords, mais globalement nous sommes alignés sur les points importants comme le budget, les congés…”

*Vincent (Pousse-décision) :* “J’ai tout noté sur notre tableau visuel. Et aussi, les actions qu’on peut se prendre, par exemple pour Benoit sur le budget de nous donner les accès aux outils de budgétisation en lecture pour qu’on commence à le regarder, et j'ai pris l’engagement de le consulter demain après-midi, j'ai bloqué un créneau exprès. Manu a aussi pris comme action de partager notre tableau de délégation à l’équipe de transformation lors d’un atelier, pour leur montrer notre travail, qui pourra je l’espère être utile“.

*Anais (Gardienne du temps) :* “Il est 15h45 exactement. On laisse la place au coach de séance ?”

*Françoise (Coach) :* “Merci ! Comment avez-vous vécu la réunion ?”

*Virginie (Participante) :* “Mieux que ce que j’aurais cru… C’est pas un exercice facile, et pourtant on s’en est bien sorti !”

*Françoise (Coach) :* “Je me permets une remarque, si c’est OK ?”

(Les participants font oui de la tête.)

*Françoise (Coach) :* “Je me suis rendu compte qu’à un moment, je me suis déconcentré. C’est au moment où Manu et Benoit, vous avez discuté sur la question du budget pendant au moins 10 minutes. J’ai eu l’impression aussi que les autres participants ont décroché à ce moment-là. Manu, ce que je te propose, ce serait de ne pas hésiter à ouvrir le débat aux autres.”

*Manu (Facilitateur) :* “Franchement, tu as raison. C’est noté.

*Virginie (Participante) :* “J’aurais pu essayer aussi d’intervenir, mais c’est compliqué… Je me sentais pas forcément légitime.”

*Benoit (Manager) :* “Je l’entends aussi, vos remarques et vos retours. J’essaierai de faire attention la prochaine fois.

*Françoise (Coach) :* “Vincent, tu peux le mettre sur notre tableau d’amélioration ? Comme principe ? Par exemple ce serait, hmm… “Facilitateur : Ouvrir le débat aux autres quand 2 personnes débattent”

*Manu (Facilitateur) :* “C’est pas facile quand on est soi même dans le débat… Mais bon, c’est un axe à prendre en compte, vraiment. Merci à tous et toutes, on arrive au bout du temps. On se voit dans 2 semaines pour un autre atelier !”

*Vincent (Pousse-décision) :* “Attends, je regarde justement notre tableau d’amélioration, et on avait mis “Nommer un facilitateur à la fin du point pour le prochain atelier. Je veux bien le faire la prochaine fois, si ça vous convient ?”

*Manu (Facilitateur) :* “Merci à toi ! C’est noté.”
