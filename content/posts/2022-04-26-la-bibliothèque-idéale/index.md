---
title: La bibliothèque agile idéale
authors:
  - antoine-marcou
  - olivier-marquet
  - laurent-dussault
  - thomas-clavier
values:
  - partage
  - conviction
tags:
  - transformation
  - peopleops
  - rh
  - delivery
  - autoorganisation
  - bibliothèque
offers:
  - unlock-transformation
  - unlock-delivery
  - unlock-management
  - unlock-project
date: 2022-04-26T18:47:52.864Z
thumbnail: janko-ferlic-sfl_qonmy00-unsplash.jpg
subtitle: 7 livres références de la transformation d'entreprise à mettre sur
  votre table de chevet
aliases:
  - /posts/20220426-la-bibliothèque-idéale
---
Dans la communauté des coachs en transformation, vous trouverez de gros lecteurs. Nous en faisons partie.

Nos lectures nous ouvrent de nouvelles démarches, de nouveaux outils, de nouvelles pratiques : elles nous permettent d’enraciner la transformation plus profondément dans les organisations que nous accompagnons.

Dans cet article, pas de bla-bla. Juste 7 livres que nous avons sélectionnés car ils nous inspirent et ont largement influencé les dernières transformations que nous avons menées. Juste 7 livres que nous vous recommandons d’avoir sur votre table de chevet, s’ils n’y sont pas déjà !

Si vous n'avez pas déjà lu l'un des livres suivants, prenez quelques minutes pour parcourir cet article : 

Retrouvez tous nos conseils lecture ici : [#Bibliothèque](../../tags/biblioth%C3%A8que/)

***

{{<toc>}}

***

## Livre #1 : Work Rules ! 

{{<figure  src="workrules.png" alt="Couverture du livre Work Rules" class="center-small">}}

**Thèmes** : People Ops, Culture Agile

**L’auteur** : Laszlo Bock, ancien Senior Vice Président People Ops chez Google Inc

**Pourquoi on aime** : Le témoignage d'un personnage clé de l'organisation Google qui nous éclaire sur la culture et les pratiques de l'entreprise. Une introduction au concept de PeopleOps et une somme de retours d’expérience concrets qui amènent à repenser en profondeur le rôle des RH dans nos organisations. Éclairant.

**À lire si...** : Vous cherchez à faire évoluer la culture de travail au sein de vos équipes ou vous réfléchissez au futur du métier des RH dans votre organisation

**Niveau de difficulté** : 3 / 5. Ce livre n’a pas encore été traduit en français mais reste d'une langue accessible   

**Edition** : John Murray

**Publié en** : 2016

***

## Livre #2 : Agile HR, Deliver value in a changing world of work 

{{<figure  src="agileRH.png" alt="Couverture du livre Agile HR, Deliver value in a changing world of work" class="center-small">}}

**Thèmes** : RH, Transformation Agile

**Les autrices** : Natal Dank et Riina Hellström, 2 ex-RH devenues consultantes et coachs, fondatrices de l'Agile HR Community

**Pourquoi on aime** : Un livre d'agilité qui s'adresse directement aux RH, dans leur langage. Et qui développe le double concept *Agile for HR* + *HR for Agile* : 

* quels outils et pratiques les directions RH peuvent piocher dans la base de connaissances agile pour traiter leurs propres points de douleur ?
* et que peuvent-elles faire pour rendre possible la transformation de leur entreprise vers une culture agile ?

Un travail qui fait écho à notre approche : nous pensons qu’il est contre-productif de demander à une direction d’aller vers une culture de travail agile si elle n’en tire pas d'abord des bénéfices pour elle-même.

**À lire si...** : Vous devez mener une transformation d’entreprise car vous il est alors clé d’embarquer tout ou partie de la direction RH

**Niveau de difficulté** : 2/5. Un livre en langue anglaise qui revient sur les fondamentaux agiles avant de rentrer dans le concret des équipes RH

**Edition** : Kogan Page

**Publié en** : 2020


***

## Livre #3 : Le thérapeute et le philosophe

{{<figure  src="therapeute.jpg" alt="Couverture du livre Le thérapeute et le philosophe" class="center-small">}}

**Thèmes** : Psycho & Philo / Coaching

**L’auteur** : Dany Gerbinet, thérapeute et systémicien

**Pourquoi on aime** : Plus nous élaborons des plans, plus, en les suivant, nous négligeons les effets néfastes qu'ils peuvent avoir. Or, ces effets néfastes sont indissociables du plan que nous avons conçu. Ce livre explore d’autres façons de penser, d’autres façons de percevoir le monde. À travers différents regards, l’auteur nous invite à lâcher prise sur notre volonté de changer l’autre pour se concentrer sur ce qu’il appelle le non-agir. Une approche très intéressante du changement.

**À lire si...** : Vous êtes coach et vous souhaitez découvrir un mode de pensée disruptif

**Niveau de difficulté** : 3/5. Un livre qui fait appel à des notions de systémique 

**Edition** : Enrick B. Editions

**Publié en** : 2020


***

## Livre #4 : Reinventing organizations 

{{<figure  src="reinventing-org.png" alt="Couverture du livre Reinventing organizations" class="center-small">}}

**Thèmes** : Auto-organisation, Culture Agile

**L’auteur** : Frédéric Laloux, coach et facilitateur

**Pourquoi on aime** : Un ouvrage référence pour comprendre les bases de l'auto-organisation et de l'entreprise libérée. Ses limites et ses perspectives. Il nous est particulièrement utile dans nos échanges avec le management exécutif des entreprises que nous accompagnons. 

**À lire si...** : Vous êtes dirigeant ou manager et cherchez à faire évoluer votre culture d'entreprise en vous appuyant sur des expériences concrètes.

**Niveau de difficulté** : 2/5. Nous conseillons l’excellente version illustrée par Etienne APPERT, simple et agréable.

**Edition** : Diateino

**Publié en** : 2017


***

## Livre #5 : Apprendre à apprendre avec le Lean, accélérateur d'intelligence collective

{{<figure  src="apprendre-a-apprendre.png" alt="Couverture du livre Apprendre à apprendre avec le Lean" class="center-small">}}

**Thèmes** : Transformation des organisations, Lean

**L’auteur** : Michael Ballé, Jacques Chaize, Régis Medina, Anne-Lise Seltzer

**Pourquoi on aime** : Chez aqoba, notre moto c’est "Apprenez à changer”. Alors forcément ce livre nous parle. Il nous donne des pistes pour mettre en place une entreprise apprenante ainsi que les clés théoriques qui entrent en jeu. Il aborde aussi les difficultés que l'on rencontre dans la mise en place d'une démarche Lean sur le long-terme.

**À lire si...** : Si vous voulez comprendre les mécanismes d’apprentissage et transformer le concept d'intelligence collective en réalité. Pour les managers ou les leaders qui veulent prendre du recul.

**Niveau de difficulté** : 3/5. Accessible bien que parfois pointu.

**Edition** : Eyrolles

**Publié en** : 2021


***

## Livre #6 : Accelerate, The Science Behind Devops: Building and Scaling High Performing Technology Organizations

{{<figure  src="accelerate-book.png" alt="Couverture du livre Accelerate" class="center-small">}}

**Thèmes** : Performance du Delivery

**L’auteur** : Nicole Forsgren PhD, Jez Humble, Gene Kim

**Pourquoi on aime** : Nous y avons dédié un [article complet](https://aqoba.fr/posts/20220319-accelerate-la-boussole-indispensable-de-votre-transformation/), que vous trouverez sur notre blog. Accelerate offre une approche radicalement nouvelle pour optimiser le delivery des équipes de développement logiciel. Nous nous appuyons sur elle pour construire des transformations qui améliorent directement la vitesse et la stabilité du delivery.

**À lire si...** : Vous cherchez des clés d’organisation pour améliorer le delivery de vos équipes de développement tout en cherchant à remettre le client au centre.

**Niveau de difficulté** : 4/5. Un ouvrage relativement technique mais indispensable.

**Edition** : IT Revolution Press

**Publié en** : 2018


***

## Livre #7 : Empowered : Ordinary People, Extraordinary Products

{{<figure  src="empowered-book.png" alt="Couverture du livre Empowered : Ordinary People, Extraordinary Products" class="center-small">}}

**Thèmes** : Organisation Produit

**L’auteur** : Marty Cagan, Chris Jones

**Pourquoi on aime** : Quand le gourou du produit Marty Cagan et Chris Jones nous parlent de produit, on a envie de savoir. Après *Inspired* voici *Empowered*, ou comment créer un environnement qui responsabilise, mobilise et motive les équipes pour leur redonner responsabilité, autonomie et impact.

**À lire si...** : Si vous adorez les *Feature Teams* et que vous voulez ouvrir vos chakras. Si vous êtes un leader qui a envie de trouver une solution pour rebooster ses équipes et remettre l’approche produit au centre.

**Niveau de difficulté** : 2/5. Ce livre n’a pas encore été traduit en français mais reste facile d'accès

**Edition** : Wiley

**Publié en** : 2020

***

Bonne lecture !
