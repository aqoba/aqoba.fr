---
title: Comment utiliser la vélocité pour piloter ses activités ?
authors:
  - arnaud-bracchetti
  - olivier-marquet
values:
  - conviction
  - partage
  - transparence
tags:
  - agile
  - évaluation
  - vélocité
  - planification
offers:
  - unlock-delivery
date: 2022-05-03T06:00:00.603Z
thumbnail: estimation-voyage.jpg
subtitle: " "
---
Dans nos organisations actuelles, nous cherchons sans cesse à prévoir, à planifier, à mesurer ce que nous pouvons et allons faire. Dans [un précédent article](/posts/20220420-comprendre-les-%C3%A9valuations-agiles/), nous avons tenté de comprendre pourquoi l’agilité amène de nouvelles façons de réaliser ces évaluations, en mettant en avant les évaluations relatives. Aujourd’hui nous allons nous intéresser à l’utilisation de ces évaluations relatives afin de réussir à se projeter et à piloter nos activités en connaissance de cause.

{{< toc >}}

## Qu'est ce que la vélocité ?

Maintenant que nous avons vu que l’évaluation d’un backlog revenait à positionner ses éléments les uns par rapport aux autres (évaluations relatives),  la question initiale reste entière, comment pouvons nous nous projeter dans le temps pour pouvoir planifier et piloter notre travail ?

Nous devons trouver un moyen de convertir le poids relatif des éléments les uns par rapport aux autres en une durée.  Pour cela nous avons besoin d’introduire une notion supplémentaire, la vélocité. La vélocité représente le nombre de points de backlog qu'une équipe est en mesure de réaliser dans un intervalle de temps déterminé. Pour pouvoir calculer cet indicateur, il faut avoir un backlog évalué relativement (pour avoir le nombre de points associé à chaque élément), ainsi qu'un intervalle de temps fixe et propre à l'équipe, que nous appellerons itération. 

Si on considère que notre backlog est constitué d’éléments porteurs de valeur, on peut voir cette notion de vélocité comme la capacité de l’équipe à produire de la valeur. 

## Pourquoi calculer une vélocité ?

L’objectif principal de la vélocité est de pouvoir **se projeter sur notre backlog** pour estimer ce qui pourrait être fait pour une date donnée. Ceci nous permet de **faire des hypothèses ou de prendre des décisions**.

Le backlog sur lequel nous voulons nous projeter, peut être à court terme, constitué de différents types d’éléments, des éléments porteurs de valeur et d’autres qui sont nécessaires mais ne portent pas de valeur particulière. Par contre, à moyen ou long terme, le backlog est principalement constitué d'éléments plus ou moins précis et qui sont porteurs de valeur pour nos utilisateurs. Par exemple, un backlog long terme ne va pas contenir de bugs, qui eux arrivent au fil de l’eau. Ceci à un impact sur la façon dont nous allons calculer la vélocité pour nous projeter.

Afin de pouvoir nous projeter dans le temps, il convient d’avoir **2 prérequis**:

1. **Une vélocité calculée à partir d’éléments de même nature** que celle de la partie du backlog sur laquelle vous souhaitez vous projeter.
2. **Un backlog au minimum macro-estimé** sur la partie sur laquelle vous souhaitez vous projeter (en faisant un atelier d'[Extreme Quotation](https://www.google.com/search?q=extreme+quotation) ou [Magic Estimation](https://www.google.com/search?q=magic+estimation) par exemple).

## Comment calculer la vélocité ?

Pour nous projeter sur un backlog, nous avons besoin que la vélocité soit **calculée sur le même type d'éléments que ceux que l’on retrouve dans le backlog**. Comme nous l’avons vu précédemment, pour des projections à moyen ou à long terme, ce sont principalement des éléments qui portent de la valeur. Il est donc important de ne pas prendre en compte les autres types d’éléments dans notre calcule de la vélocité.

Ceci étant dit, si nous calculons notre vélocité uniquement sur un seul type d’éléments, ne risque-t-on pas de fausser nos projections ? En effet, les équipes ne travaillent pas exclusivement sur des éléments porteurs de valeur, certaines autres activités sont nécessaires au bon fonctionnement de l’équipe, et ces activités prennent du temps. 

![Le cube de la vélocité](cube-velocite.png "Le cube de la vélocité")

C’est justement là que l’utilisation d’une vélocité constatée est un élément important.  Le diagramme ci-dessus, présente les différents constituants de cette vélocité. Ce qui est présenté ici, c’est que lorsqu'une équipe mesure sa vélocité, même si cette vélocité n’est calculée que sur les éléments porteurs de valeur (en bleu clair), celle-ci prend en compte toutes les activités de l'équipe. En d’autre termes, **la vélocité prend en compte et s’adapte automatiquement aux impondérables et aux activités non planifiées réalisées par l’équipe**.

Si lors d’une itération nous avons beaucoup d'imprévus, l’équipe aura moins de temps à consacrer aux éléments planifiés, la vélocité sera donc en baisse. Cela sans que nous ayons eu à calculer le temps passé sur ces imprévus. C’est automatique.  

Si ce taux de perturbation devait se maintenir, notre capacité à créer de la valeur, notre vélocité, prendra automatiquement en compte cela. nous créerons moins de valeur à chaque itération et lorsque nous utiliserons cette vélocité pour nous projeter sur notre backlog, nous saurons en estimer les impacts.

Inversement, si nous améliorons la qualité de notre travail ou nos processus pour avoir moins de perturbations, notre équipe devient plus performante et la vélocité augmente.

![Historique de vélocité](histo-velo.png "Historique de vélocité")

**<p style="text-align: center;">Exemple: adaptabilité de la vélocité pour s’adapter à l'augmentation du nombre de perturbations lors du sprint 4</p>**

Comme cela à était dit dans l’[article sur les estimations](https://aqoba.fr/posts/20220420-comprendre-les-%C3%A9valuations-agiles/), pas besoin d’une précision d’horloger dans les estimations pour pouvoir se projeter. En effet, qu’essaye t-on de savoir en projetant la vélocité ? 

* Quel périmètre pourra-t-on faire pour une date donnée ?
* A quelle date pouvons-nous  réaliser ce périmètre ?

Ce que nous cherchons ici, c’est avoir les grandes masses, pour être en mesure de prendre des décisions stratégiques:

* Faut-il prévenir nos partenaires ou adapter le plan ?
* Quand doit-on prévoir de faire notre plan de communication marketing ?
* Serons nous prêt pour la saison de Noël ?
* Notre MVP fini quand ? Faut revoir notre approche afin de  vérifier nos hypothèses au plus tôt ?
* etc…

## Gérer la variabilité de la vélocité 

La vélocité est un indicateur constaté, c'est-à-dire qu’il représente la réalité des choses. De ce fait, il est tout à fait normal d’avoir une vélocité qui varie d’une itération à l’autre. Il est important de bien avoir conscience de cette variabilité pour la prendre en compte dans notre utilisation de la vélocité.

**Quels sont les grands facteurs de variabilité sur la Vélocité ?**

* **La précision de nos évaluations relatives.** En effet, si nous utilisons la suite de fibonacci pour réaliser nos évaluations, nous avons une erreur moyenne de +/- 50%. Ce qui à mécaniquement un impact sur la vélocité.
* **Les absences dans l'équipe ou les jours fériés.** En effet le nombre de points produits par une équipe sur une itération sera fortement dépendant du nombre de jours effectivement travaillés sur cette période.
* **L’organisation du travail au sein de l’équipe.** En effet, si l'équipe parallélise trop le travail et ne s'attache pas à terminer les choses commencées, il est possible, qu’en fin d'itération, beaucoup d'éléments du backlog soient presque terminés sans l’ être réellement. Dans ce cas, on ne compte pas dans la vélocité les éléments presque terminés, ce qui peut avoir un impact fort sur la vélocité de l’équipe.

Mais alors, comment peut-on utiliser la vélocité si elle est aussi imprévisible ?

La première chose à faire est d'en avoir conscience, et de ne pas vouloir lui faire dire ce qu'elle ne dit pas. 

* **Il ne faut pas considérer la vélocité comme une valeur unique**, mais plutôt comme un intervalle de valeurs entre une valeur optimiste et une valeur pessimiste. 
* **Toutes les projections qui utilisent la vélocité ne doivent pas être prises pour un résultat engageant** mais plutôt comme un outil permettant de mieux anticiper les risques, et de prendre le plus tôt possible des décisions pour les réduire. 

Ceci étant dit, et sans oublier que la vélocité intègre une forte variabilité, il est possible de lisser celle-ci de façon à améliorer la précision de nos projections.

*  La première chose à faire est de considérer la moyenne glissante des vélocité de l’équipe sur les 3 ou 4 dernières itérations. Ceci à pour effet de lisser les variations accidentelles, sans pour autant écraser les tendances à la hausse ou à la baisse de la vélocité de l’équipe. 
* Calculer une Vélocité équipe pleine. Pour limiter les variations de vélocité dues aux absences dans l’équipe, on peut introduire la notion de vélocité équipe pleine. Pour calculer cette vélocité, nous appliquons une simple règle de trois à la vélocité constatée, pour déterminer la vélocité que l’équipe aurait pu avoir si tout le monde avait été présent.
* Éviter de travailler avec les évaluations d’un élément isolé de notre backlog. Plus nous travaillons avec un nombre important d’éléments, plus les erreurs d’évaluation tendent à diminuer (cf. paragraphe suivant)

## Les évaluations sont localement fausses, mais globalement justes 

Les évaluations sont localement fausses, mais globalement justes. Cette phrase nous dit que vu l’erreur que nous pouvons nous attendre à avoir dans les évaluations (+/-50%), si nous prenons un  élément isolé du backlog il sera sûrement faux. Par contre, **si nous prenons un ensemble d’éléments dans notre backlog, les erreurs positives et négatives vont s’annuler**, l’évaluation résultante sera donc d’autant plus juste que le nombre d'éléments est important. 

Le diagramme ci-dessous illustre le dernier point. Il représente les résultats de 4 simulations de l’erreur sur les évaluations en fonction du nombre d’éléments du backlog pris en compte. Ce que nous voyons, c’est que pour un petit nombre d’éléments (moins de 5) l’erreur peut être très importante. pour tomber à moins de 10% si on considère plus de 5 éléments. Passé la vingtaine d’éléments, l’erreur tend à diminuer rapidement sur toutes les simulations, pour passer en dessous des 5%. 

![Diagrammes des erreurs d'évaluation](simul-erreur.png "Diagrammes des erreurs d'évaluation")

Cela amène 2 remarques importantes si nous souhaitons **réduire la variabilité de la vélocité** due aux erreurs sur les évaluations: 

* Il faut **découper finement notre backlog** pour maximiser le nombre d’éléments réalisés dans une itération.
* Il faut **éviter de vouloir corriger le poids de certains éléments** du backlog en les re-estimant “après coup”. Cela à tendance à déséquilibrer l’erreur.

## Se projeter avec les Burnups 

Le burnup est un graphique qui permet de visualiser la valeur qu’on peut espérer livrer pour une date donnée ou alors a quelle date un périmètre donné peut être livré, tout dépendra de votre approche. 

Sur l’axe vertical on retrouve des points, ils correspondent au périmètre du backlog, sur lequel nous allons nous projeter. L’axe horizontal est une axe temporel exprimé en itération (sprint).

**Les différentes courbes (actual, worst et best) projettent notre vélocité moyenne actuelle, ainsi qu’une vélocité pessimiste et optimiste**. On obtient alors un cône d’incertitude qui se réduira au fur et à mesure que nous clarifierons notre compréhension du périmètre et que nous stabiliserons notre vélocité.

Nous pourrons alors **nous servir de ces prédictions pour prendre des décisions stratégiques**.

![Burnup: quelle date pour un périmètre donné ?](burnup1.png "Burnup: quelle date pour un périmètre donné ?")

**<p style="text-align: center;">Burnup: quelle date pour un périmètre donné ?</p>**

Dans cet exemple, nous cherchons à savoir à quelle date nous pouvons nous attendre à réaliser notre périmètre cible (représenté par le ligne horizontal en pointillés). Dans cet exemple le cône d'incertitude est grand, on peut presque aller du simple au double sur la date prédite.

Toutefois, sans plus connaître le contexte, Nous pouvons déjà en tirer certaines décisions :

* Des actions doivent être prises pour commencer à resserrer ce cône d’incertitude.
* Une date d'atterrissage commence à émerger et sera certainement entre le sprint 8 et 18, en étant conservateur le sprint 14 peut sembler un bon candidat.

![Burnup: quel périmètre pour une date donnée ?](burnup2.png "Burnup: quel périmètre pour une date donnée ?")

**<p style="text-align: center;">Burnup: quel périmètre pour une date donnée ?</p>**

Dans cet exemple l’approche est un peu différente, Nous avons ici un délai fixé et nous cherchons  à savoir à quel périmètre s’attendre pour cette date.

La encore, sans connaître le contexte, nous pouvons rapidement en tirer quelques conclusions :

* Comme dans le graphique précédent, des actions peuvent être prises pour commencer à resserrer le cône d’incertitude.
* A priori les 100 premiers points seront là au sprint 10, les 100 autres points très probables et les 100 derniers pas si sûr (vous reconnaîtrez presque ici une sorte de MoSCoW: Must, Should, Could, Would). 

Bien évidemment, l'interprétation et les décisions prises sur ces graphiques sont très dépendantes du contexte.

## Conclusion

Nous espérons qu'au travers de cet article vous avez pu y voir plus clair sur le pourquoi de la vélocité et comment s'en servir. Pour aller (encore) plus loin, nous vous proposerons dans un prochain article un serious game basé sur la vélocité ou vous pourrez mettre en œuvre cet exercice de planification sur un burnup mais aussi diagnostiquer votre historique de vélocité.
