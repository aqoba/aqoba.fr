---
title: Comprendre les évaluations agiles
authors:
  - arnaud-bracchetti
values:
  - partage
  - conviction
  - transparence
tags:
  - evaluation
  - agile
offers:
  - unlock-delivery
date: 2022-04-20T06:42:01.279Z
thumbnail: balance-roberval-cuivre-5-kg_01.jpg
subtitle: Donner du sens aux pratiques agiles
---
Dans nos organisations, nous cherchons sans cesse à prévoir, à planifier, à mesurer ce que nous pouvons et allons faire. Pour cela, nous devons évaluer la taille, la durée,  le budget des travaux que notre équipe doit accomplir. La culture agile, que nous défendons, propose des outils et pratiques d'évaluation très spécifiques et qui commencent à s'imposer dans de nombreuses organisations. Aujourd'hui, je vous propose de tenter de donner du sens à ces pratiques, en se posant non pas la question du COMMENT, mais celle du POURQUOI qui nous permettra de comprendre ces outils et pratiques d'évaluation agile.

Ce que nous appelons ici évaluations agiles, rassemble l’ensemble des pratiques qui permettent de prendre un backlog (une liste de choses à faire) et d’associer à chacun de ses éléments les informations nécessaires pour répondre aux questions de budget, de temps ou de périmètre réalisable. 

# Pourquoi fait-on des évaluations ?

Pour comprendre la problématique des évaluations agiles, la première question à se poser est : pourquoi fait-on des évaluations dans un contexte agile ? 

Pour tenter de répondre à cette question, nous pouvons regarder un outil classique du pilotage de projet, le triangle de fer. 

{{< figure  src="waterfall-vs-agile.png" alt="Waterfall vs agile" class="center-small">}}

Ce que nous apprend l'observation de ces deux triangles, c'est que de façon traditionnelle, nous travaillons avec un scope fixe, à partir de là nous tentons d'évaluer le temps et l'argent nécessaire à la réalisation du projet. A contrario, en agilité nous partons d'un délai et d’un budget fixés à l’avance, puis nous cherchons à évaluer quelles sont les fonctionnalités que nous serons en mesure d'embarquer avec cette contrainte.

Ainsi, de façon traditionnelle, les évaluations portent sur le temps et l'argent qui sont deux ressources extrêmement sensibles dans l'entreprise.  Les évaluations devront donc avoir  une précision importante et seront très engageantes.

Dans le cas d’un contexte agile, les évaluations portent sur le scope, qui est un élément beaucoup plus flou et adaptable.  De plus, l’agilité prône l’adaptation au changement. Ceci veut dire que le scope évalué à un instant T, peut être amené à changer pour s'adapter aux besoins réels de nos utilisateurs.  Dans ce cadre, les évaluations ne sont donc plus un engagement, mais un outil de projection nous permettant de prendre les bonnes décisions pour maximiser la livraison de valeur. 

> **En résumé, une équipe agile doit pouvoir réaliser des évaluations de façon régulière pour prendre en compte les évolutions du backlog. Les évaluations doivent donc pouvoir se faire rapidement avec un niveau de précision juste nécessaire pour pouvoir se projeter.** 

# Pourquoi faire des évaluations en relatif plutôt qu’en absolu

Une des particularités des évaluations Agiles est qu'on évalue plus en absolu (en Euro ou en jour.homme) mais en relatif (les éléments du backlog les uns par rapport aux autres). Ici encore, nous allons essayer de comprendre le pourquoi de cette bazarie contre intuitive qu’introduit l’Agilité. 

Pour cela, essayons un petit exercice :

{{< figure  src="aquarium.jpg" alt="Aquarium" class="center-small">}}

Si nous devions classer les questions suivantes dans l’ordre croissant de leur difficulté.

1. Quel est l’aquarium le plus lourd ?
2. Quel est le poids de l’aquarium de droite ?
3. Combien de fois l’aquarium de gauche rentre-t-il dans celui de droite ?

Nous aurions surement les réponses suivantes :

1 - **La question A** : Il est en effet très simple de voir que l’aquarium de droite est plus grand et donc plus lourd que celui de gauche.

2 - **La question C** : Ici, il est moins facile de savoir combien de fois l'aquarium de gauche rentre dans celui de droite, mais en réfléchissant un peu et avec un peu d'imagination nous pouvons évaluer cette quantité à quatre ou cinq.

3 - **La question B** : Par contre ici, il n'est clairement pas possible de répondre à la question.  En effet, sans informations supplémentaires pour connaître l'échelle de la photo,  nous ne pouvons pas faire d'estimation absolue du poids de l'aquarium de droite. De plus, même si nous avions les informations d'échelle, nous aurions besoin d’un bon niveau d’expertise pour répondre à la question (calcul des volumes, masse volumique de l’eau, …). Ce qui limite le nombre de personnes qui peuvent participer..

Les questions A et C amènent à faire des évaluations relatives d'un aquarium par rapport à l'autre. L’exercice montre que ce sont les évaluations qui sont les plus simples à réaliser. Besoin de moins d’information, niveau d’expertise nécessaire plus faible. À l’opposé, la question B nous demande de faire une évaluation absolue. Ce type d'estimation demande des données beaucoup plus précises sur les éléments à évaluer, ce qui demande plus de  travail en amont pour pouvoir effectuer cette évaluation. Ce qui rentre en opposition avec un des principes agiles, qui est de travailler juste à temps.

> **En suivant les conclusions du chapitre précédent, si nous voulons faire des évaluations simplement, rapidement et souvent, il est plus intéressant de travailler en relatif.**

# Pourquoi impliquer toute l’équipe dans les évaluations plutôt qu’uniquement les experts du domaine ?

Nous venons de voir qu’en utilisant des évaluations relatives, nous avons besoin de moins d'expertise qu’avec des évaluations absolues.  Une question légitime peut donc se poser, doit-on réserver les évaluations aux experts du domaine ou toute l'équipe peut-elle y participer ?

En permettant à un panel plus large de personnes de faire des évaluations, nous apportons de la diversité dans notre façon de raisonner. Nous favorisons ainsi un principe cher à l’Agilité, l’utilisation de l'intelligence collective de l’équipe. Ceci permet de confronter les points de vue et d'identifier des incompréhensions ou des manques d’alignement de l’équipe sur le contenu des éléments du backlog. 

Dans la grande majorité des cas, quand on constate un écart important d'évaluation entre des profils d’origines et de cultures différentes, on se rend vite compte que cette différence est due à une compréhension différente du périmètre du travail à réaliser. Ne pas limiter l’exercice d’évaluation aux seuls experts du domaine peut permettre d'identifier très tôt des divergences de compréhension et ainsi d’aligner l’équipe sur le travail à réaliser. 

> **La recherche de l’intelligence collective met en avant un point supplémentaire dont nous n'avons pas encore parlé, c'est qu’un atelier d’évaluation collective entraîne des discussions qui alignent l'équipe sur le travail réel à réaliser.** 

# Pourquoi évaluer l’effort plutôt que le temps de travail 

Maintenant que nous savons que les évaluations doivent être faites en relatifs, la question suivante à se poser est, que doit-on réellement évaluer ?

En effet, se dire que nous allons faire des évaluations relatives n'est pas suffisant, nous devons aussi nous aligner sur les critères que nous devons utiliser pour comparer les éléments du backlog les uns par rapport aux autres. De façon traditionnelle, nous aurions tendance à utiliser le temps nécessaire pour réaliser les éléments du backlog.  Malheureusement, réfléchir de cette façon amène un certain nombre de contraintes dans un cadre agile. C'est ce que nous allons essayer d'expliquer dans les paragraphes suivants. 

{{< figure  src="clock.png" alt="Clock" class="center-small">}}

Le problème avec l'évaluation du temps c'est que celui-ci n'est pas stable. Les deux exemples suivants mettent ceci en évidence :

* Le temps de réalisation d'un élément est dépendant de l'expérience de la personne qui va le réaliser. Information que nous n’avons généralement pas au moment de l'estimation, et qui peut être amenée à changer par la suite.
* Le temps de réalisation peut varier si le moment de l’estimation est éloigné du moment de la réalisation. Dans ce cas, il est fort probable que l'équipe ait gagné en expérience et que le temps de réalisation ait évolué entre le moment de l'estimation et le moment de la réalisation. 

Les deux exemples précédents montrent qu’utiliser le temps comme critère de comparaison des éléments du backlog nous conduit à devoir réévaluer régulièrement celui-ci, ce qui n’est pas efficace.

Dans un contexte agile, nous sommes souvent amenés à évaluer un nombre important de petits éléments constituant le backlog. Dans ce cadre, il est souhaitable de trouver un critère de comparaison qui soit le plus stable possible, pour éviter de devoir revenir trop souvent sur les évaluations déjà effectuées.

Pour cela il est préférable d’utiliser la notion d’effort pour effectuer nos évaluations. Pour évaluer l’effort, nous pouvons nous poser les questions suivantes :

* Quelle est la quantité de travail nécessaire pour réaliser l’élément ? 
* Le travail est-il sensible et nécessite-t-il beaucoup de vérifications ?
* …

La notion d’effort rassemble les caractéristiques intrinsèques de l'élément à évaluer qui sont donc stables dans le temps.

> **Ce qu’il est important de retenir ici, c’est que pour avoir un travail d’évaluation efficace les données qui servent de point de comparaison dans l’évaluation d’un élément, doivent être les plus stables possibles dans le temps.**

Maintenant que nous comprenons qu’il est préférable d'évaluer l’effort de façon relative, et de la façon la plus collaborative possible, il faut que nous soyons en mesure d’utiliser ce travail pour nous projeter sur le périmètre. Pour cela que nous devons introduire une nouvelle donnée, la vélocité (nombre de points d'efforts que produit une équipe sur un intervalle de temps fixé). Il y a beaucoup de choses à dire sur la vélocité, nous y consacrerons donc un prochain article.
