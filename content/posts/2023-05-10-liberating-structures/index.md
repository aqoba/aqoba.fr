---
draft: false
values:
  - partage
  - conviction
toc_min_heading_level: 2
title: Des réunions plus efficaces grâce aux Liberating Structures
authors:
  - olivier-marquet
  - laurent-dussault
tags:
  - équipe
date: 2023-05-16T04:15:00.000Z
thumbnail: LS.jpg
carousels:
  "1":
    - Carrousel - LS 124ALL.jpg
    - Carrousel - LS 124ALL (1).jpg
    - Carrousel - LS 124ALL (2).jpg
    - Carrousel - LS 124ALL (3).jpg
    - Carrousel - LS 124ALL (4).jpg
    - Carrousel - LS 124ALL (5).jpg
    - Carrousel - LS 124ALL (6).jpg
    - Carrousel - LS 124ALL (7).jpg
  "2":
    - Carrousel - LS 25 10.jpg
    - Carrousel - LS 25 10 (1).jpg
    - Carrousel - LS 25 10 (2).jpg
    - Carrousel - LS 25 10 (3).jpg
    - Carrousel - LS 25 10 (4).jpg
    - Carrousel - LS 25 10 (5).jpg
    - Carrousel - LS 25 10 (6).jpg
  "3":
    - Carrousel - LS TRIZ.jpg
    - Carrousel - LS TRIZ (1).jpg
    - Carrousel - LS TRIZ (2).jpg
    - Carrousel - LS TRIZ (3).jpg
    - Carrousel - LS TRIZ (4).jpg
    - Carrousel - LS TRIZ (5).jpg
    - Carrousel - LS TRIZ (6).jpg
    - Carrousel - LS TRIZ (7).jpg
  "4":
    - Carrousel - LS TROIKA CONSULTING.jpg
    - Carrousel - LS TROIKA CONSULTING (1).jpg
    - Carrousel - LS TROIKA CONSULTING (2).jpg
    - Carrousel - LS TROIKA CONSULTING (3).jpg
    - Carrousel - LS TROIKA CONSULTING (4).jpg
    - Carrousel - LS TROIKA CONSULTING (5).jpg
    - Carrousel - LS TROIKA CONSULTING (6).jpg
toc_max_heading_level: 5
subtitle: UNE BOÎTE À OUTILS DE LA FACILITATION DE GROUPES
---
Les Liberating Structures ou "Structures Libérantes" en français, sont un ensemble de méthodes et d'outils qui ont été développés pour aider les groupes de toutes tailles à travailler plus efficacement et de manière plus collaborative. Créées par Keith McCandless et Henri Lipmanowicz en 2014, les Liberating Structures ont été conçues pour donner une voix à chacun et encourager la participation active de tous les membres d'un groupe. Elles sont sous [licence Creative Commons](https://creativecommons.org/licenses/by-nc/3.0/deed.fr)

{{< toc >}}

## LES LIBERATING STRUCTURES

Les Liberating Structures (LS) ont été créées en réponse à une observation commune dans les environnements de travail : les réunions sont souvent ennuyeuses, inefficaces et peuvent être dominées par quelques personnes qui parlent beaucoup, tandis que d'autres restent silencieuses. Ce sont des microstructures conventionnelles dans les entreprises dont voici les types les plus courants :

* **La présentation:** cette structure implique une personne qui présente le contenu à un public qui écoute sans participer activement.
* **Les comptes-rendus d'activité (ou rapport de situation):** cette structure implique une personne qui présente l'état d'avancement ou le bilan d'une activité à un public qui écoute sans participer activement.
* **Les réunions standards sous forme de discussions dirigées:** cette structure implique une personne qui dirige la discussion, qui peut inclure un petit groupe de personnes, avec un ordre du jour préétabli.
* **Les séances de brainstorming:** cette structure implique un groupe de personnes qui génèrent des idées sur un sujet spécifique, sans contrainte.
* **Les échanges informels:** cette structure se produit spontanément lorsque les gens échangent des idées et des informations sans cadre formel ni structure prédéfinie.

Ces microstructures conventionnelles peuvent avoir leurs limites en termes d'inclusion et d'implication de tous les participants, et peuvent être complétées par des structures plus libératrices pour encourager une participation active et une prise de décision plus efficace et collaborative.

{{< figure  src="LS principes.png" alt="Matrice des types de réunion" class="center-small">}}

Les Liberating Structures visent donc à casser ces modèles en donnant à tous les participants des moyens égaux de s'exprimer et de participer à la conversation. Ces structures libérantes sont conçues pour être simples à utiliser et faciles à adapter à diverses situations, parfois seules, souvent dans une séquence logique. Après, pour les plus “facilitateurs/facilitatrices” d’entre vous, vous allez rapidement vous apercevoir que certaines d’entre elles sont des formats que vous avez déjà utilisés ou que vous connaissez déjà sous un autre nom, néanmoins les Liberating Structures forment un recueil intéressant et pertinent.

### Les principes

Les LS sont guidées par 10 principes:

* Inclure tous les participants et débrider les idées
* Respecter réellement les gens et les solutions locales
* Bâtir la confiance dans le changement
* Apprendre de ses erreurs
* Accroître les libertés et valoriser la responsabilité
* Mettre l’accent sur les possibilités : croire avant de voir
* Pratiquer l’auto-découverte des solutions au sein des groupes
* Encourager la destruction créatrice et permettre l’innovation
* Inciter à une curiosité sérieuse et ludique
* Toujours commencer avec un objectif clair

En utilisant une combinaison de structures en séquence, vous pouvez créer des ateliers de travail plus dynamiques et plus collaboratifs, où chacun peut contribuer pleinement à la conversation et libérer son potentiel créatif.

Chaque Liberating Structure est une méthode pratique qui vise à structurer des groupes de travail pour des durées allant de 15 minutes à 2 heures. Elles sont construites selon les mêmes 5 éléments de conception, qui comprennent:

1. une invitation structurante pour guider les participants
2. des directives sur l'aménagement de l'espace et le matériel nécessaire
3. la répartition de la participation entre les participants et le facilitateur
4. la configuration des groupes
5. une séquence d'étapes réparties dans des temps définis

Avec en plus les objectifs que peut couvrir la Liberating Structure ainsi que des astuces, les pièges à éviter et quelques exemples.

Voici la liste de des 33 Liberating Structures:

{{< figure  src="LS liste.jpg" alt="liste des 33 liberating structures">}}

###### [Ref : https://www.liberatingstructures.fr/menu-des-ls/](https://www.liberatingstructures.fr/menu-des-ls/)

### Construire un atelier

Vous pouvez facilement choisir les Liberating Structure les plus pertinentes pour construire votre réunion/atelier en fonction des objectifs que vous souhaitez atteindre, et ce, grâce à la Matching Matrix et le Matchmaker que vous trouverez sur le site officiel.

{{< figure  src="LS Matrix.png" alt="Construire un atelier à base de Liberating Structures">}}

Son fonctionnement est le suivant, commencez par cocher sur la 1ere page du Matchmaker chaque objectif que vous souhaitez atteindre, puis:

1. Regroupez les objectifs dans une séquence logique de début, milieu et fin.
2. Réduisez votre liste en retirant les objectifs les moins critiques ; gardez-les pour plus tard.
3. Si vous avez entre 3 et 7 objectifs, vous avez votre première séquence de LS
4. Développez une ou deux séquences alternatives, plus courtes, plus longues ou différentes.
5. Partagez avec les autres, comparez, modifiez et choisissez celle qui a le plus de sens.
6. Faites correspondre votre séquence d'objectifs avec sa séquence de Liberating Structure (en regardant la 2nd page qui fait correspondre objectif et LS) et vérifiez le timing.
7. Conservez des objectifs alternatifs pour improviser au besoin pendant la mise en œuvre.

###### [Ref : https://www.liberatingstructures.com/matching-matrix/](https://www.liberatingstructures.com/matching-matrix/)

A l’arrivée vous pourrez construire votre déroulé complet sous cette forme:

{{< figure  src="LS Storyboard.png" alt="Lancement d'une initiative">}}

###### [Ref : https://www.liberatingstructures.fr/design-storyboards-basic/](https://www.liberatingstructures.fr/design-storyboards-basic/)

Pour allez plus loin vous trouverez des exemples de séquences (appellées Strings) sur cette page: 
<https://www.liberatingstructures.com/sample-strings/>

## NOS LIBERATING STRUCTURES PRÉFÉRÉES

Zoomons sur 4 Liberating Structures que nous utilisons très régulièrement.

### Un exemple de séquence

Voici tout d’abord, 3 Liberating Structures, qui peuvent en plus former une séquence efficace :

1. **[TRIZ](https://www.liberatingstructures.fr/triz/):** cette structure consiste à aider un groupe à abandonner des pratiques limitant leur succès en posant la question "Que devons-nous arrêter de faire pour faire des progrès sur notre but le plus profond ?", afin de permettre l'émergence de sujets peut être tabous et de favoriser des conversations courageuses.

{{< carousel duration="3000" ordinal="3">}}

2. **[1-2-4-ALL](https://www.liberatingstructures.fr/1-2-4-tous/):** cette structure est utilisée pour encourager la réflexion individuelle et la participation active. Les participants commencent par réfléchir individuellement à une question ou un sujet, puis ils partagent leurs réflexions avec un partenaire en binôme. Ensuite, les deux binômes se regroupent pour discuter ensemble de leurs idées et retenir l’idée maîtresse qui ressort de leur conversation. Enfin, tous les groupes se rassemblent pour partager et discuter des idées qui ont été générées.

{{< carousel duration="5000" ordinal="1">}}

3. **[Troika Consulting](https://www.liberatingstructures.fr/troika-consulting/):** cette structure est une méthode qui permet aux participants de travailler en petits groupes de trois personnes pour offrir des conseils mutuels sur un problème ou une question donnée. Chaque membre du groupe a une chance de présenter leur situation à ses deux partenaires, qui écoutent attentivement et posent des questions de clarification. Ensuite, les deux partenaires donnent des conseils ou des suggestions en se basant sur leurs propres expériences ou leur expertise. Le présentateur peut ensuite réfléchir à ces conseils et partager ses réflexions avec le groupe. Cette structure favorise l'échange d'idées, la réflexion en profondeur et l'engagement actif de tous les membres du groupe.

{{< carousel duration="3000" ordinal="4">}}

**Exemple de séquencement pour améliorer la collaboration dans l’équipe pendant un Sprint:**

1. **TRIZ** pour identifier quelles activités ou comportements arrêter, “Le prochain Sprint est une catastrophe, qu’a t’on fait pour en arriver là?”
2. Ensuite invitez tout le monde à générer de nouvelles idées avec **1-2-4-All,** “Comment faire pour améliorer la collaboration dans l’équipe pendant le Sprint?”
3. Demandez à tous les participants d'identifier un irritant en rapport a la “collaboration” qui leur arrive fréquemment pendant un sprint puis invitez-les à décrire cette situation pour une session **Troïka Consulting**.

### Une rétrospective rapide et efficace

De plus en plus d’entreprises s’organisent en trains SAFe. Ce framework ne propose qu’une unique technique d’animation de sa grande “rétrospective” trimestrielle, l’Inspect & Adapt.

La liberationg structure **25/10 Crowd sourcing** vous permet en 30 min :

* L'expression individuelle de tous les participants
* un tri express de des meilleures propositions

Alors pour booster cette recherche d’axes d’amélioration, éviter de longues discussions sur ce qui ne va pas… cherchez directement des solutions avec le 25/10.

{{< carousel duration="3000" ordinal="2">}}

## A RETENIR

Toutes ces Liberating Structures et leur utilisation sont répertoriés sur le site officiel <https://www.liberatingstructures.com/> ainsi que sur un site en francais: <https://www.liberatingstructures.fr/> 

Il existe aussi une application mobile officielle somme toute indispensable: <https://www.liberatingstructures.app/en/> (français disponible dans l’application) et vous pouvez aussi vous référer au livre officiel, en francais, qui résume tout ça: “[Le pouvoir surprenant des Liberating Structures: Des méthodes simples pour libérer une culture de l’innovation”](https://www.amazon.fr/pouvoir-surprenant-Liberating-Structures-linnovation/dp/B0B6L6WL8S).

Les Liberating Structures sont donc une série d’ateliers accessibles à tous, sans nécessité d'expertise préalable, axés sur les résultats, amusants et responsabilisants, avec des cycles rapides d'itération, adaptés à différentes échelles et contextes, et qui favorisent l'innovation et la créativité. Notez que vous ne devez pas vous limiter à ces Liberating Structures pour vos ateliers ou réunions, il existe beaucoup d’autres façons de structurer des conversations et des réflexions tout aussi efficacement voir parfois même plus efficacement (jetez un coup d’oeil ici par exemple: <https://untools.co/>). Les Liberating Structures sont néanmoins indispensables à avoir dans votre boîte à outils 🙂

Il ne vous reste plus qu'à expérimenter !

### ANNEXE

<span style="text-decoration:underline;">Descriptif court en français des 33 Liberating Structures:</span>

1. Impromptu Networking : Échanger rapidement les attentes et les challenges. Établir de nouveaux contacts (5 à 20 min.)
2. 9 Whys : Rendre clair l’objectif de votre travail commun (20 min.)
3. What, So What, Now What : Examiner tous ensemble notre progression et décider quels ajustements sont nécessaires (15 à 45 min.)
4. TRIZ : Arrêtez les activités et les comportements contre-productifs pour faire de la place à l’innovation (30 à 45 min.)
5. Appreciative Interviews : Découvrir les causes premières du succès pour s’en inspirer (30 à 60 min.)
6. 1-2-4-All : Impliquer simultanément tous les participants d’un groupe pour faire éclore Questions, Idées & Suggestions (12 min.)
7. User Experience Fishbowl : Partager le savoir-faire issu de l’expérience avec une large assemblée (25 à 70 min.)
8. 15% Solutions : Découvrir et se concentrer sur ce que chacun a la liberté et les ressources de faire immédiatement (15 à 20 min.)
9. 25-to-10 Crowd Sourcing : Générer et sélectionner rapidement les idées les plus puissantes et exploitables d’un groupe (20 à 30 min.)
10. Troika Consulting : Obtenir de ses collègues et sur l’instant une aide pratique et imaginative (15 à 30 min.)
11. Conversation Café : Donner du sens à un défi majeur (35 à 60 mins)
12. Min Specs : Définir uniquement les actions qui sont absolument « À faire » et « À ne pas faire » pour atteindre un objectif (35 à 50 min.)
13. Wise Crowds : Exploiter la sagesse d’un groupe en cycles rapides (15 min. par personne)
14. Wicked Questions : Mettre en perspective les défis paradoxaux auxquels un groupe est confronté pour réussir (25 min.)
15. Drawing Together : Révéler idées et pistes de travail au travers de l’expression graphique (40 minutes)
16. Improv Prototyping : Développer des solutions efficaces à des problèmes chroniques en jouant sérieusement (15 à 20 min. par tour)
17. Agreement-Certainty Matrix : Classer les défis et problèmes en quatre catégories : simple, compliqué, complexe et chaotique (30 à 45 min.)
18. Shift & Share : Diffuser de bonnes idées et créer des contacts informels entre innovateurs (35 à 90 min.)
19. Heard, Seen, Respected : Pratiquer l’écoute active et l’empathie avec ses collègues (35 min.)
20. Social Network Webbing : Schématiser les connexions informelles et décider comment renforcer le réseau relationnel commun pour atteindre un but (45 à 60 min)
21. Design Storyboards Basic : Définir les conditions et étapes d’une réunion pour obtenir des résultats productifs (25 à 70 min.)
22. Open Space Technology : Libérez les potentialités et le leadership dans des groupes de toutes tailles. (de 90 min à 3 jours)
23. Discovery & Action Dialogue : Découvrir, inventer et déclencher des solutions locales à des problèmes chroniques (25 à 70 min.)
24. Integrated~Autonomy : Passer de solutions en forme d’alternatives « l’un ou l’autre » à des solutions solides qui incluent « l’un et l’autre » (80 min.)
25. Generative Relationships STAR : Révéler les tendances relationnelles qui créent des dysfonctionnements ou de la valeur insoupçonnée (25 min.)
26. Critical Uncertainties : Développer des stratégies pour rester opérationnel dans une variété de situations futures, plausibles mais imprévisibles (100 min.)
27. Purpose-to-Practice : Définir les cinq éléments essentiels pour qu’une initiative soit pérenne et résistante (25 à 120 min.)
28. Ecocycle Planning : Analyser l’intégralité des activités et des rapports d’un groupe pour identifier les obstacles au progrès et les opportunités d’évolution (60 à 95 min.)
29. Panarchy : Comprendre comment des systèmes intégrés interagissent, évoluent, diffusent l’innovation et se transforment (2h.)
30. What I Need From You : Révéler les besoins réciproques et essentiels de différents départements. Accepter ou décliner les demandes d’aide (55 à 70 minutes)
31. Celebrity Interview : Relayer l’expérience des leaders et des experts auprès des personnes les plus proches des défis locaux (30 à 60 min.)
32. Helping Heuristics : Pratiquer des méthodes progressives pour aider les autres, recevoir de l’aide et demander de l’aide (15 min.)
33. Simple Ethnography : Rendre clair l’objectif de votre travail commun (75 min. à 7h)
