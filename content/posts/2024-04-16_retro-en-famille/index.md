---
draft: false
title: "Agilité en famille : Rétrospective"
authors:
  - thomas-clavier
values:
  - partage
  - humanite
  - transparence
tags:
  - rétrospective
  - amélioration continue
  - agilité en famille
offers:
  - unlock-delivery
date: 2024-04-16T10:54:00.000Z
thumbnail: ducks.jpg
subtitle: Le rétroviseur de la famille
---
Cette série d'articles offre un retour sur quelques histoires d’agiliste qui se sont déroulées chez moi durant ces dernières années. Ce nouvel épisode met en valeur la force de la rétrospective dans une famille.

## Des rétrospectives en famille
À la maison, tous les dimanches soir c’est rétro … enfin presque tous les dimanches … enfin c'est régulier mais peut-être pas autant que nous l’aimerions, pour l'article on va dire que c'est toutes les semaines.

### Pourquoi ?
Le principe de base d'une rétrospective c'est de regarder la période qui vient de s'écouler et de prendre conscience de ce qui a bien fonctionné, ce qui a moins bien fonctionné et de prendre des décisions d'actions pour amplifier ce qui fonctionne bien et améliorer ce qui peut l'être.

Dans un cadre professionnel, on va se concentrer sur le flux de production et la manière de travailler ensemble. Dans un cadre familial, il n'y a pas de flux de production ni de volonté de travailler ensemble. En  revanche, il y a  besoin de gérer le bien-être du collectif familial, ces tâches qui prennent une charge mentale colossale, comme gérer le linge, les courses, les billets de train pour les vacances, la logistique pour les goûter d'anniversaire, etc, vous voyez ? Il y a aussi le vivre ensemble : quelles sont les règles de vie que l'on souhaite avoir ? Est-ce acceptable que l'ado se lève à 14h le dimanche ? Tous les dimanches ? Et dans le cadre d’une famille séparée, il y a tout plein de non-dits qu’il est indispensable de clarifier pour dégonfler «les histoires que l’on se raconte», ces histoires qui commencent par «Je suis sur qu’il à fait ça pour …»

### Comment ?

{{< figure src="helping-families-to-change.png" alt="Couverture du livre : helping-families-to-change" class="float-left-tiny">}}
Quand j’ai cherché un format de rétrospective adaptée à la famille, je me suis naturellement tourné vers les travaux de [Virginia Satir](https://fr.wikipedia.org/wiki/Virginia_Satir).  Je la connaissais pour ses travaux sur la thérapie familiale. Elle proposait aux familles qu'elle accompagnait de faire un atelier de [temperature reading](http://www.satirworkshops.com/wp-content/uploads/2009/06/SatirTemperatureReading.pdf). C’est une réunion en cercle où les membres d’une famille se parlent et font l’expérience individuelle d’appartenir à un groupe. Mes amis d’[/ut7](https://ut7.fr/) et en particulier [Emmanuel Gaillot](https://changer-grandir.org/authors/manu.html) qui a fait quelques traductions de ses articles l'appellent le [conseil](https://ut7.fr/blog/2015/11/18/animer-vos-retrospectives-avec-le-conseil.html).

Dans l’atelier original, il y a 2 rôles et 5 étapes. Les participants, l'ensemble de la famille, et un guide qui va s'assurer que les règles sont respectées, il annonce les étapes dans l’ordre et peut aussi jouer le gardien du temps.
Les 5 étapes sont les suivantes :

#### Les appréciations

Chacun peut exprimer une appréciation à une autre personne du groupe, un merci pour une action particulière et le pourquoi on a apprécié cette action de cette personne. 

{{< icon-block icon="home" >}}
À la maison, nous avons changé cette étape pour commencer par un tour de gratitude, de [chaudoudou](https://apprendreaeduquer.fr/conte-chaud-doux-chaudoudoux/) comme diraient d’autres. Chacun exprime une marque d’affection et/ou d’amour pour tous les autres membres du groupe. Ça va d’un simple «merci d’être aller chercher le pain» à «Je t’aime très fort»
{{< /icon-block >}}

#### Les plaintes et les craintes

Décrire ce qui ne va pas, ses plaintes, ses colères ou ses peurs. Il est important de penser à associer sa plainte à une recommandation que l’on se fait à soi-même, ceci afin de décharger les autres de l’envie de nous trouver des solutions.

{{< icon-block icon="home" >}}
À la maison, c’est cette étape qui est probablement la plus compliquée à gérer, il y a 2 éléments importants à assurer :
* Faire en sorte que les plaintes soient des observations et pas des jugements. Une technique consiste à s’exprimer à la première personne «je me sens abandonné quand personne ne m’aide à mettre la table» et pas à la seconde personne «tu ne viens jamais m’aider»
* Accompagner sa plainte d’une action personnelle et pas d’une injonction à changer. Par exemple «je viendrais dans ton bureau avant de partir au judo pour savoir si tu peux m’accompagner» et pas «tu dois arrêter de travailler plus tôt pour t’occuper de nous !»
{{< /icon-block >}}

#### Les puzzles

Expliciter ce que l’on ne comprend pas dans le groupe, les mystères. 

{{< icon-block icon="home" >}}
À la maison, cette étape a pris une importance toute particulière depuis notre séparation. On y retrouve des «Papa, pourquoi tu n’étais pas là mon match de samedi ?» ou des «Pourquoi le week-end de la fête des pères c’est avec maman ?»
{{< /icon-block >}}

#### Les informations et les nouvelles

Partager les faits nouveaux. Parfois, les faits nouveaux ont été dévoilés à l’étape précédente pour expliquer un mystère. 

{{< icon-block icon="home" >}}
À la maison, nous mettons aussi dans cette étape les questions de logistiques comme «Maman part en déplacement la seconde semaine des vacances, papa, tu seras bien là dès vendredi matin ?» en plus des nouvelles relatives à nos familles respectives du genre «Es-tu au courant que ma soeur à eu son bébé ?»
{{< /icon-block >}}

#### Les souhaits, les espoirs et les rêves

Exprimer ce que l’on aimerait, c’est l’occasion de partager sa vision, tout en sachant qu’elle ne va pas forcément s’incarner. 

{{< icon-block icon="home" >}}
À la maison, cette étape n’est pas très utilisée.
{{< /icon-block >}}

De la même façon que la rétrospective n'est pas le seul rituel clé d'une équipe performante et sereine, Virginia Satir proposait d'autres outils dans le cadre de ses thérapies familiales.

{{< icon-block icon="home" >}}
À la maison, nous ajoutons un peu de management visuel pour visualiser les actions et les chaudoudoux. Ça se traduit par un grand tableau blanc avec dessus :
* des chaudoudoux : ces petits messages d’amour et d’affection qui font que l’on se sent tout chaud et tout doux.
* et les actions : «N'oubliez pas de descendre le bac à linge salle tous les matins»
{{< /icon-block >}}

### Le coin du coach

On retrouve dans cette rétrospective familiale toutes les recettes d’une bonne rétrospective.
* Un cadre avec de la bienveillance, sans jugement. Ce n’est pas la faute de l’autre et je peux influencer le système.
* La remise en question personnelle : que puis-je changer chez moi pour que le groupe fonctionne différemment. 
* Le conditionnement positif avec les chaudoudoux de la première étape.
* La segmentation du mode de pensée : le positif dans le groupe, les éléments à améliorer dans le groupe, les mystères du groupe, les infos du groupe et de l’extérieur avant de revenir au groupe avec une expression du futur désiré.
* Garder la concentration des participants
* Permettre à chacun de s’exprimer pleinement.
* Des actions personnelles.

Ce que l’on ne retrouve pas forcément bien dans cette rétrospective c’est le déroulé : collecter, filtrer, décider. Enfin, et c’est pas le rôle d’une rétrospective, il est nécessaire de rajouter quelque chose pour suivre l’avancée des actions.

## Conclusion

La rétrospective, rituel clé de l’amélioration continue, est maintenant utilisée chez nous pour garantir la communication dans une famille fonctionnelle.  Ce n’est certes pas suffisant pour garantir la meilleure des familles, mais si aujourd’hui nous pouvons dire fièrement que malgré la séparation nous avons une famille fonctionnelle, notre rétrospective régulière n’y est pas pour rien.

