---
draft: false
title: Les six chapeaux de Bono
authors:
  - thomas-clavier
values:
  - conviction
  - partage
tags:
  - rétrospective
  - agile
  - astuce
offers:
  - unlock-management
date: 2023-06-14T00:23:00.000Z
thumbnail: chapeaux.jpg
subtitle: Ou comment réfléchir couvert ?
---
On devrait plutôt dire l’histoire des 6 chapeaux de “de Bono” car cette méthode de structuration de la pensée a été inventée par Edward de Bono. Mais revenons en arrière pour comprendre d'où vient cette méthode et comment elle fonctionne avant de la mettre en pratique en rétrospective.

Quand en 1985, Edward de Bono, spécialiste en sciences cognitives, publiait un livre intitulé *Six Thinking Hats*, il ne se doutait pas qu’il révolutionnerait nos rétrospectives 30 ans plus tard.

## Théorie

### 6 chapeaux

{{< figure  src="six-chapeaux-de-bono.png" alt="Les six chapeaux de Bono" class="center-small">}}

Ce qu’Edward de Bono souligne dans ses écrits c’est que dans un groupe, chacun est différent. On trouve des esprits plus enclins à une pensée critique quand d'autres sont plus créatifs. Le problème c’est qu’en mélangeant ces esprits il est fort possible qu’ils se brident mutuellement. Le créatif va proposer une idée et l’objecteur va tout de suite lui montrer le danger. Pour éviter ça, il propose de segmenter dans le temps les modes de pensées. Et pour que tout le monde soit bien conscient du mode de pensée du moment, il va utiliser des chapeaux de couleurs.
* **Blanc** : La neutralité, des faits purement et simplement.
* **Rouge** : Les émotions, les sentiments, sans avoir besoin de se justifier.
* **Noir** : Les objections, les risques et les dangers.
* **Jaune** : L'optimisme, on accepte tout même les rêves.
* **Vert** : La créativité, la proposition d’idées sans se limiter.
* **Bleu** : L’organisation, la méthode et les décisions.

### 6 types de personnalités
Dans l’article sur le [modèle de la process communication](/posts/20230419-process-communication-model/), il nous parlait de 6 types de personnalités. Et si les 2 modèles se rejoignaient ?
* **Travaillomane** (analyseur) : on parle d’une personalité analytique, très neutre, comme le chapeau blanc, mais qui peut aussi s’épanouir avec le chapeau bleu.
* **Empathique** : une personnalité compatissante et sensible qui se nourrit des émotions, ce qui rappelle le chapeau rouge.
* **Persévérant** : une personnalité engagée et consciencieuse qui se rapprocher du chapeau jaune.
* **Rebelle** : une personnalité créative et spontanée souvent dans la réaction parfaitement à l'aise avec les chapeaux noir et vert.
* **Promoteur** : désigne les personnes très directives et qui s'épanouissent dans l’action, proches du chapeau bleu.
* **Rêveur** : une personnalité calme et imaginative, très chapeau vert.

Si on résume :

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Type de personnalité      | Chapeau                   |
| -----------               | ------------------------- |
| Travaillomane (analyseur) | **blanc** / bleu          |
| Empathique                | **rouge**                 |
| Persévérant               | **jaune**                 |
| Rebelle                   | **noir** / vert           |
| Promoteur                 | **bleu**                  |
| Rêveur                    | **vert**                  |
{{</ table >}}

Il n’y a pas de correspondance parfaite, mais malgré tout, la similitude entre les 2 modèles est troublante.

### Tout n’est pas blanc ou noir

Le modèle de process communication empile les types de personnalité, nous ne sommes pas définis par un seul de ces types. De la même manière, Edward de Bono souligne que chacun est capable de respecter tous les chapeaux. Nous avons naturellement une affinité pour l’un ou l’autre qui nous pousse à régulièrement réagir de la même façon, mais si les règles du jeu sont clairement définies, tout le monde est capable de suivre le chapeau en cours. Cela coûtera plus d’énergie quand le chapeau force un moyen d’expression qui est haut dans sa pyramide de personnalité.

Reste à l’animateur de judicieusement orchestrer ces chapeaux pour atteindre le but de son atelier. Et c’est là tout l’art de préparer une rétrospective.

## Utilisation en rétrospective

Je ne suis pas fan des rétrospectives à thème genre Star Wars ou Orient Express. Je trouve plus de biais cognitifs que d’avantage à changer de thème toutes les 2 semaines. Je me rappelle en particulier d’un scrum master super fier de me partager sa dernière rétro sur le thème des pirates, me disant : nous avons décrit ce que l’on aimerait voir de différent sur le dessin du trésor, mais c’est pénible, ils n’ont pensé qu’à des outils. Je lui ai alors posé la question : le trésor de pirate qui est dessiné là, c’est de l’or et des diamants ? Penses-tu que le dessin puisse avoir biaisé la pensée de l’équipe au point de leur faire oublier que leur futur préféré n’est pas uniquement matériel ?

Je vous livre là, le plan d’une rétrospective utilisant les chapeaux de Bono. Libre à vous de l’adapter en fonction de votre contexte. N’oubliez pas de rappeler l’objectif du chapeau en cours.

* Chapeau bleu : 
    * Présentation du cadre, Bienveillance, Participation, Confidentialité et Humour. 
    * Suivi du rappel sur les chapeaux de Bono : on respecte le chapeau en cours 
    * puis présentation des objectifs de la réunion : 
* Chapeau rouge : Comment vous sentez-vous maintenant ?
* Chapeau blanc : Quel sont les faits depuis la dernière fois ?. 
* Chapeau Jaune : De tous les faits énnoncés précédement, quels sont les éléments que l’on souhaite garder ?
* Chapeau noir : De tous les faits énoncés précédement, quels sont les élements que l’on souhaite améliorer ?
* Chapeau bleu :  Parmi tous les éléments à garder ou améliorer quels sont les 3 plus importants à travailler ? 
* Chapeau vert : Pour chacun des 3 éléments sélectionnés précédemment, quelles sont toutes les actions possibles et imaginables ?
* Chapeau rouge : Comment vous sentez-vous maintenant ?
* Chapeau bleu :  Parmi toutes les actions, qui s’engage à faire quoi d’ici à la prochaine fois ?
* Chapeau rouge :  Rappel des objectifs, les a-t-on atteints ?

Voici donc une trame de rétrospective toute simple, en double-diamant et relativement facile à faire évoluer. Certains remplacent le dernier chapeau par un ROTI (Return Of Time Investment, à paraittre dans un prochain article) d'autres s'arrêtent au chapeau bleu et repartent avec la liste des actions et les porteurs identifiés.

Je vous livre aussi le [svg à utiliser en fond de board klaxoon](./retro-bono.svg).

## Conclusion

Pour terminer, si vous devez préparer une rétrospective d'équipe, je vous recommande sans hésitation d'abandonner les Rétros "à thème", souvent chronophages dans leur préparation et qui biaisent la réflexion des participants. Et de vous lancer dans l’utilisation des Chapeaux de Bono : un format qui, lorsqu'il est maîtrisé, conduit l'équipe à structurer son intelligence collective.

