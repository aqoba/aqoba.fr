---
draft: false
title: À la découverte d’unFIX
authors:
  - olivier-marquet
  - thomas-clavier
values:
  - partage
tags:
  - unfix
  - scaled agile
  - agilité à l'échelle
  - organisation
  - entreprise
  - transformation
offers:
  - unlock-transformation
date: 2022-11-29T20:39:19.309Z
thumbnail: versatile-organization-design.jpg
subtitle: L'anti framework d'agilité à l'échelle
---
Avez-vous entendu parler d'unFIX ?

Les 20 et 21 septembre derniers, lors de l’édition 2022 d’Agile En Seine, [Clément de Scaleway](https://www.linkedin.com/in/crochas/) et [Thomas](/team/thomas-clavier/), l’un des fondateurs d’aqoba, ont présenté cet outil lors de leur conférence : [“unFIX, l’anti-framework d’agilité à l’échelle ?”](https://www.agileenseine.com/events/unfix-lanti-framework-dagilite-a-lechelle-rex-scaleway/)

À cette occasion, nous nous sommes aperçus que peu de personnes, autour de nous, ont pris le temps de lire le contenu du site unFIX publié par J. Appelo, son créateur, au début de l’année 2022. Et pour les rares qui l’avaient lu, peu avaient tenté de manipuler “l’objet” unFIX dans leur propre contexte.

C’est pour toutes ces personnes que nous partageons cet article. Il a pour ambition de présenter, en français, les principaux éléments du modèle unFIX pour permettre au lecteur de se l’approprier pleinement et d’être capable de l’expérimenter dans la vie réelle. Mais attention, cet article n’est pas qu'une traduction du site unFIX : c’est à la fois notre analyse et notre retour d’expérience sur les différents objets qui le composent.

Ce partage nous tient à coeur car, bien qu’encore imparfait, nous sommes convaincus du potentiel de ce nouvel outil pour trouver des modèles d’organisation optimisés en termes de :

* performance du delivery, 
* de culture apprenante, 
* et d’autonomie / responsabilisation des équipes opérationnelles.

Alors, accrochez vos ceintures et entrons ensemble dans le détail d’unFIX !

**Avec un accès rapide aux grande parties:**
{{< toc >}}

## PRÉAMBULE - Qu’est ce qu’unFIX ?

{{< figure  src="unfix-logo.png" alt="unFIX logo" class="center-large">}}

unFIX est un modèle inventé par Jurgen Appelo. Il a officiellement été publié en janvier 2022. Ce dernier cherchait une alternative flexible et beaucoup plus minimaliste que les représentations poussées par SAFe, LeSS, Scrum@Scale, Holacracy, ou le modèle Spotify.

Sur le [site officiel d’unFIX](https://unFIX.com), en guise de définition nous trouvons ce qu'unFIX n’est pas:

* **Ce n'est PAS un framework** \
  La définition de « framework » et donc de “cadre” suggère un ensemble d’éléments structurants et essentiels à mettre en place mais dans unFIX tout est facultatif. 
* **Ce n’est pas un ensemble de processus** \
  Le but d'unFIX est de couvrir **uniquement** les modèles de conception d'organisation et la structure organisationnelle. Il faudra aller chercher ailleurs la partie process.
* **Ce n'est pas  réservé à l'informatique** \
  Il n’y a rien de spécifique à ce domaine, il peut être utilisé dans tout contexte.
* **Le modèle unFIX n'est pas  descendant** \
  La modélisation de votre structure organisationnelle se fait même plutôt de bas-en-haut en commençant par un périmètre de taille petite ou moyenne.
* **Ce  n'est pas un substitut aux autres modèles et framework** \
  Il y a beaucoup de bonnes choses dans d'autres modèles et frameworks qui ne doivent pas être jetés et surtout il y a des choses qui ne sont de toute façon pas couvertes par unFIX.

### Donc en résumé c’est quoi unFIX ?

D’abord, arrêtons-nous un moment sur le nom choisi par l’auteur d’unFIX. “unFIX” peut se traduire en français par “non-FIGÉ”. Cette appellation n’est pas anodine : J. Appelo projette un outil permettant de chercher, en permanence, à faire évoluer et à optimiser une organisation et son fonctionnement.

unFIX est une bibliothèque de patterns et de briques organisationnelles. Une façon de modéliser des organisations, de les penser, et de les faire évoluer. Avec l’ambition de s’abstraire des représentations que proposent les frameworks du marché et, peut-être, permettre d’éviter certains écueils. 

Il ne décrit pas une organisation cible idéale, ni comment les équipes interagissent. Pour autant, bien que le modèle se veuille non-prescriptif, il contient quand même un ensemble de parti-pris qui biaisent sa neutralité, mais nous y reviendrons.

## Chapitre I - Les fondamentaux qui structurent unFIX

### I - 1 : À quoi ressemble le modèle unFIX ?

La première illustration d’unFIX que l’on trouve représente l’organisation d’une entreprise dans son ensemble. Ce qui nous apprend plusieurs choses sur l’outil :

{{< figure  src="unfix-overview.png" alt="unFIX overview" class="center-large">}}

* Il est conçu pour modéliser **une organisation de taille moyenne** dans son ensemble.
* On trouve le ou les flux de valeur de l’entreprise au centre à l’horizontal et en jaune,
* Et tout autour, au-dessus et en dessous, les équipes “transverses” qui contribuent à ces flux de valeurs,
* C’est coloré comme un jouet d’enfant.

Dans les briques de bases qui composent unFIX on retrouve **les équipages** (Crews) autrement dit les équipes ou les squads. Ces équipages sont **regroupés dans des secteurs** (Turfs) autrement appelés domaines ou territoires, qui sont **eux-mêmes regroupés dans une base** ce que d'autres appellent tribu ou Business Unit. unFIX définit **7 types d’équipages différents**.

On trouve aussi en transverse, **les forums** souvent nommés communautés de pratique, chapters ou guildes.

Le premier réflexe peut être de se dire qu’unFIX n’est rien d’autre qu’un nouveau langage pour représenter les mêmes choses. Et bien c’est en partie le cas, oui : car, en adoptant une nouvelle langue pour décrire un objet connu, on change de perspective. On met en lumière des forces ou des défauts de l’organisation avec lesquels on vit depuis trop longtemps pour les considérer.

### I - 2 : Les principes d’unFIX

Pourquoi certains patterns ou modèles sont-ils considérés meilleurs que les autres dans unFIX ? Comment comprendre les choix qui ont été fait ?

La réponse à ces deux questions se trouve dans les 7 principes qui ont prévalu à l’écriture d’unFIX. Ces principes sont importants car ils soulignent les partis pris de l’équipe de J. Appelo. Prenons le temps de nous y attarder : 

#### 1. Cela dépend et tout est facultatif

Il serait peut-être préférable que les frameworks agiles soient décomposés en bibliothèques de modèles voire même en briques de bases similaires à unFIX. Tout dépend du contexte, et tout est facultatif.

#### 2. N’échouez pas, essayez : bon marché, sûr et rapide

Vous échouez uniquement lorsque vous gaspillez votre argent, votre santé ou votre temps. Et vous échouez lorsque vous arrêtez d'essayer et que vous n'apprenez rien.

#### 3. L'expérience bat le produit et le service

Un focus mis sur les produits est encore une sous-optimisation. Le client se soucie plus de son expérience que de la qualité de votre produit ou service.

#### 4. Où est le client ? Partout !

L'organisation existe pour offrir un échange de valeur avec toutes les parties prenantes, pas seulement les clients. Toutes ces parties prenantes s'attendent à tirer profit d’une organisation optimisée.

#### 5. Équilibrer une cohésion élevée avec un faible couplage

Il n'est pas nécessaire que chaque personne, au sein de l'organisation, maîtrise la Big Picture de l'organisation. Si chacun maîtrise les besoins de son environnement "local", alors l'organisation est déjà optimisée.

#### 6. Augmentez la simplicité, adoptez la variété

Nous devrions garder les choses aussi simples que possible. Une complexité inutile n'est pas souhaitable. Mais réduire la variété n'est pas une option.

#### 7. Manager le système, leader les gens

Les modèles d'organisation sont bons lorsqu'ils favorisent le leadership plutôt que le management. Et quand ils donnent la priorité à la gestion des systèmes plutôt qu’à la gestion des personnes.

### I - 3 : Des partis-pris qui limitent unFIX

Nous insistons sur le fait qu'unFIX est construit sur des parti-pris assumés. Ses principes et ses inspirations différencient ce qui est "bon" (les patterns) de ce qui est "mauvais" (les anti-patterns) dans une organisation, selon J. Appelo. 

C'est la raison pour laquelle aucun des objets d'unFIX ne permet entre autres de représenter :

* les couches de management intermédiaire (ce qui est cohérent avec le principe #7),
* ou encore les équipes composants (cohérent avec le principe #4).

Vous pourriez donc avoir des difficultés à modéliser votre organisation actuelle, si elle en comporte. En un mot comme en cent, soyons simplement conscients qu'unFIX n'est pas à 100% "objectif" ou "neutre".

Maintenant que les bases sont posées et qu’on sait dans quoi on a mis les pieds, il est temps de faire le grand saut et de plonger plus en détails dans unFIX.

### I - 4 : Les inspirations d’unFIX et leur impact sur les modèles d’équipe

#### Team Topologies

Du livre sur Team Topologies, unFIX en reprend 3 éléments principaux : 

* la modélisation colorée des différents types d’équipes
* le concept de “charge mentale” (cognitive load). Les équipes doivent toujours comprendre chaque partie du travail qui leur appartient, sinon le réapprentissage constant les ralentit énormément.
* Il reprend aussi globalement les 4 types d’équipe: 

  * **L'équipe alignée sur le flux** qui devient la **Value Stream Crew**
  * **L’équipe de facilitation** qui devient la **Facilitation Crew**
  * **L’équipe de sous-système complexe** qui devient, dans une version un peu différente, la **Capability Crew**. 
  * **L’équipe plateforme** qui devient la **Platform Crew**

Les méthodes d’interactions (Facilitation, Collaboration, X as a service) ne sont pas reprises ici : actuellement, il n’y pas de possibilité de représenter celles-ci dans le modèle unFIX.

#### Dynamic Reteaming

Une autre influence notable d’unFIX est le livre Dynamic Reteaming de Hedi HELFAND.

Pour Jürgen Appelo, les équipes statiques ne sont pas vraiment agiles. En lien avec Dynamic Reteaming, il considère qu’il y a au moins quatre raisons pour lesquelles les managers doivent permettre aux collaborateurs de se réorganiser facilement et sans douleur en différentes équipes :

1. **Pouvoir réagir rapidement**, en effet monter une task force pour régler un gros problème et avoir cette équipe efficace prend du temps. Si vous avez déjà cette habitude d’équipe flexible, vous réagirez plus rapidement à votre environnement.
2. La loi de Conway étant incontournable, votre organisation fabriquera des produits qui ressembleront à votre structure organisationnelle. **Des équipes flexibles (changeantes) générent des architectures elles aussi flexibles** et donc cela évite de scléroser votre système.
3. Avoir des équipes flexibles implique que les collaborateurs pourront travailler sur une grande variété de sujets, stimulant ainsi la **curiosité, la créativité et la montée en compétence**. Cela jouera sur la motivation.
4. Avoir des équipes flexibles remet en cause le principe d’équipe fixe dans le temps (et donc d’évaluation de la performance de l’équipe plus que de l'individu) ce qui permet d’avoir des individus qui vont moins **se focaliser sur l'optimisation locale de leur équipe et plus sur l’expérience client** au global.

Attention, unFIX ne prône pas le changement des équipes chaque mois : il y a clairement des avantages à avoir des équipes stables, **mais stable ne veut pas dire statique** ! 

Démarrer, modifier ou stopper des équipes doit être vu comme une opportunité, pas un risque. 

Dans la lignée de *Dynamic Reteaming* où l’auteure est dans l’acceptation que la composition des équipes change inévitablement avec le temps et qu’il faut la piloter, unFIX prend une approche un peu plus radicale en conceptualisant dès le début 4 catégories d’équipes: l’équipe stable, l’équipe flexible, l’équipe à mission ou l’équipe liquide (nous y reviendrons dans le détails plus bas).

#### La virtualisation

Ici l’inspiration vient du principe de Virtualisation dans l’informatique. 

* Une machine virtuelle est un logiciel qui imite une machine physique.
* Une équipe virtuelle est un groupe de personnes agissant comme si elles étaient physiquement assises les unes à côté des autres.

Si vous cherchez à obtenir une structure organisationnelle pérenne, l’approche d’unFIX est de ne faire aucune différence entre une équipe physique et une équipe virtuelle. Vous devez être capable de créer des équipes virtuelles presque instantanément, avec des membres d'équipe dédiés travaillant de n'importe où.

## Chapitre II - Les briques de base d'unFIX

### II - 1 : À la base il y a la base (la tribu, le clan, la BU)

{{< figure  src="unfix-base.png" alt="unFIX base" class="center-small">}}

Une Base est le foyer dans lequel est regroupé un ensemble de personnes.

Elle peut être vue comme une entreprise entièrement autonome et peut externaliser certaines de ses activités non essentielles vers d'autres bases, mais son cœur de métier est la seule chose qu'elle ne peut pas déléguer. 

L'activité principale de la Base est axée sur la valeur pour les clients.

La taille d'une base peut se situer entre une seule équipe et une petite centaine de personnes, on parle régulièrement de 150 personnes sur le forum [d’unFIX](https://unFIX.circle.so/), en référence au nombre de Dumbar. 

Au sein de la Base, les gens travaillent dans une ou plusieurs équipes (Crew), et ils peuvent participer à un ou plusieurs Forums.

Il n'y a pas de management intermédiaire dans une Base, les seuls postes de direction sont dans l'équipe de gouvernance.

Il existe 4 types de Base:

1. **Fully Integrated Base** (à représenter par un cercle avec un trait plein)

   Une base entièrement intégrée offre un grand produit ou service qui est déployé et proposé comme une solution unique.
2. **Strongly Aligned Base** (à représenter avec un cercle avec des pointillés longs)

   Une base fortement alignée offre un ensemble cohérent de produits ou de services avec de nombreuses dépendances entre eux.
3. **Loosely Aligned Base** (à représenter avec un cercle avec des pointillés courts)

   Une base faiblement alignée propose différents produits ou services avec peu de dépendances entre eux.
4. **Fully Segregated Base** (à représenter avec un cercle avec des points)

   Une base entièrement ségréguée offre plusieurs produits ou services indépendants et potentiellement concurrents.

Définir le type de base n’a à priori pas d’impact particulier dans unFIX, mais permet de gérer l’évolution de cette Base si vous souhaitez la faire évoluer de Fully Integrated à Strongly Aligned par exemple.

#### Et quand ça scale il y a la League (la ligue) et la Crowd (la foule)

{{< figure  src="unfix-scaled.png" alt="unFIX scaled" class="center-large">}}

La League est un ensemble de Bases, elle réunit des personnes (jusqu'à plusieurs milliers) qui ont un point commun ou qui partagent une expérience commune. 

La Crowd est un ensemble de Leagues et peut réunir jusqu'à plusieurs dizaines de milliers de personnes.

### II - 2 : Les Crews (les équipes, les squads, les pods, les cellules)

Dans le modèle unFIX, une équipe ou crew en anglais, est une équipe composée généralement de trois à sept personnes. Les membres de l’équipe sont principalement dédiés à leur équipe mais peuvent aussi dédier du temps à un Forum. 

Il y a 7 types d’équipes: la **Value Stream** Crew, la **Facilitation** Crew, la **Capability** Crew, la **Governance** Crew, la **Platform** Crew, la **Experience** Crew, la **Partnership** Crew.

La plupart des équipes devraient être des *Value Stream Crews*. Même si unFIX nous encourage à essentiellement appuyer notre organisation sur des Value Stream Crews, il autorise donc 6 autres types d'équipes qui ne sont pas alignées sur un flux de valeur.

En tant qu'équipes autonomes, ces Crews ont la responsabilité de leurs rôles, des méthodes qu'elles utilisent, des objectifs qu'elles visent, de leur cadence ou flux de production, et de leurs dépendances vis-à-vis des autres Crews. Tant qu'une équipe assume l'entière responsabilité de la valeur qu'elle fournit, elle décide de la façon dont son travail est effectué. 

#### Le rôle clé dans chaque crew : Le Captain

{{< figure  src="la-croisiere-s-amuse.png" alt="La croisière s'amuse" class="center-small">}}

Chaque équipe a un et un seul **Captain** (Capitaine, ou Team Lead, Squad Leader, Champion). Cette personne est le contact principal avec le monde extérieur.

Malgré l’utilisation du champ lexical militaire, le Captain ne donne pas des ordres à tout le monde : dans unFIX, une équipe performante doit savoir s'auto-organiser, et chacun de ses membres d'équipage doit comprendre ses responsabilités. Le Captain a néanmoins le vote final en cas de besoin. 

Il est possible dans unFIX de répartir les responsabilités du Captain sur plusieurs autres membres de l’équipe. Par exemple, une personne peut être Captain pour les discussions Produit et un autre membre de l'équipe peut être Captain pour les discussions techniques. Cette configuration rend le rôle plus flexible - moins ‘fix” ! - car, selon les sujets,  la meilleure personne pour agir en tant que représentant de l’équipe n’est pas forcément la même.

Enfin une Crew peut avoir des rôles supplémentaires, tels que Product Lead (PM, PO, …), Tech Lead, etc. 

Si on essaye de faire le parallèle avec des frameworks connus, allez au hasard Scrum ou SAFe, à quoi ce rôle de Captain correspondrait-il ? Selon la modélisation de Scrum ou SAFe que fait unFIX sur son site, ([https://unFIX.work/models-and-frameworks](https://unfix.work/models-and-frameworks)) c’est **le Product Owner** qui serait le plus à même d'être considéré comme le Captain d’une Crew.

Maintenant que nous avons compris ce qu’était la Base et le rôle du Captain,  rentrons dans le détails des différents types de Crews : 

#### Type #1 : Le Value Stream Crew (aka Agile Team, Scrum Team)

Ses membres sont responsables de tout, depuis le moment où ils reçoivent une demande (utilisateur) jusqu'au moment où ils fournissent de la valeur au client (utilisateur). Il n'y a pas de transfert de travail entre/vers d'autres équipes.

L'équipe possède toutes les compétences nécessaires pour accomplir l’essentiel de son travail.

Dans le cas de plusieurs Value Stream Crew pour un même domaine fonctionnel il faudra probablement mettre en place des équipes de facilitation pour coordonner le travail des équipes entre elles.

#### Type #2 : Facilitation Crew (aka Support Team, Coordination Team)

L'équipe de facilitation à pour but d'aider les autres équipes à faire un excellent travail. Elle n'a pas sa propre chaîne de valeur. Elle veille à ce que les équipes Value Stream de la base puissent fonctionner sans heurts. Un exemple de Facilitation Crew serait une équipe de Scrum Masters ou de coachs agiles.

#### Type #3 : Capability Crew (aka ~~Component Team~~, Specialist Team)

Parfois, vous avez quelques personnes avec des compétences spéciales que vous ne pouvez pas répartir sur toutes les équipes Value Stream. Dans ce cas, la Capability Crew regroupe ces **compétences rares**. Leur objectif est de développer une expertise unique et de la rendre facilement accessible à toutes les autres équipes. 

Cette expertise peut être: cybersécurité, Data Analyst, etc.

D’une façon générale si vous avez assez de ces compétences pour les répartir dans les différentes équipes Value Stream, unFIX recommande de le faire.

Dans ce cas, les experts travaillent (temporairement) sur les Value Stream Crews ou ils sont considérés comme des membres invités. 

Dans unFIX, c’est écrit : l’objectif d’une Capability Crew n'est pas de gérer un composant technique, contrairement à ce qu’on peut comprendre d’une complex-subsystem team dans *Team Topologies.* 

Nous sommes là encore sur un parti-pris d'unFIX qui, s'il est suivi à la lettre, rendra difficile la modélisation de certaines organisations actuelles. La capability crew reste malgré tout la brique la plus à même de représenter une équipe composant.

#### Type #4 : Platform Crew (aka Service Team, Infrastructure Team)

Les équipes Value Stream partagent souvent une architecture et une infrastructure communes. Pendant un certain temps, vous pouvez parvenir à maintenir cette base commune avec une équipe de facilitation mais à un moment donné il sera nécessaire de créer une Platform Crew.

Le but d'une Platform Crew est d'offrir des services partagés aux autres Crews. Souvent, ces services seront de nature technique comme pour un service de CI/CD ou d'infrastructure et seront généralement fournis via des API.

#### Type #5 : Governance Crew (aka Management Team)

C’est l’équipe des “chefs à plume”. dans une Base il n’y a qu’une et une seule Gouvernance Crew (n’oubliez pas: il n’y a pas de middle management dans unFIX).

L’équipe de gouvernance est composée d’un ensemble de **Chiefs** qui sont les managers de tous les membres de la Base. Vous l’aurez compris, dans le cas où une Base représente l’entreprise entière, la Governance Crew est l'équipe de direction.

Cette équipe porte le leadership de la Base : elle définit la vision et ses objectifs; elle décide du business model et de la stratégie. Elle fait en sorte de créer un environnement favorisant la motivation des collaborateurs.

Il est dans l'intérêt de la Base que le Governance Crew délègue le plus possible.

Le Governance Crew est redevable envers les parties prenantes externes de la Base.

#### Type #6 : Experience Crew (aka Customer Journey Team)

L'objectif de cette équipe est de s'assurer que l'expérience du client et de l'utilisateur soit la meilleure possible. Les membres de l’équipe Experience réalisent un travail qui impacte les différentes équipes Value Stream, mais ils se focalisent sur l’ensemble de l’expérience ce qui est in-fine plus large que juste une vue produit ou service que pourraient avec les équipes Value Stream.

#### Type #7 : Partnership Crew

L’équipe Partnership a un rôle presque identique à celui de l’équipe Experience. Alors que l'Experience Crew se concentre sur les clients et les utilisateurs, le Partnership Crew se concentre sur les fournisseurs, les partenaires, les indépendants, les employés et les sous-traitants. Toute personne ou entité avec laquelle les équipes de la Base signent des contrats, par le biais des achats ou des ressources humaines.

Voilà, nous avons passé en revue les 7 différents types de Crews. Mais accrochez-vous car unFIX va plus loin et différencie aussi les équipes selon leurs degrés de stabilité et de perméabilité. 

### II - 3 : Les 4 catégories d’équipes

unFIX définit en effet 4 catégories d’équipes qui ont comme particularité d’être plus ou moins stables dans le temps ou plus ou moins perméables à des nouveaux membres

{{< figure  src="unfix-teams.png" alt="unFIX teams" class="center-large">}}

#### Catégorie #1 : L’équipe stable (ou Steady Team)

L'équipe “stable” a une longue durée de vie et la composition de l'équipe change rarement.

Un exemple d'équipe stable est une équipe “Produit” dans le développement logiciels. L'équipe "possède" le code ; il y a peu ou pas de changements dans la composition de l'équipe, et l'équipe existe aussi longtemps que le produit existe. Il s'agit d'un modèle typique que nous retrouvons dans différents frameworks.

#### Catégorie #2 : L’équipe flexible (ou Dynamic Team)

L'équipe “flexible” en elle-même a une longue durée de vie, mais sa composition change fréquemment.

Un exemple d'équipe “flexible” est une équipe de service dans un centre d'appels où l'équipe "possède" le service. Mais dans ce cas, les membres de l'équipe peuvent aller et venir à plusieurs reprises pendant que l'équipe (avec des membres entrant et sortant) existe aussi longtemps que l'équipe offre le service.

#### Catégorie #3 : L’équipe à mission (ou Mission Team)

La mission de l’équipe en elle-même a une existence de courte durée, mais la composition de l'équipe change rarement.

Un exemple d'équipe à mission serait un équipage de compagnie aérienne ou une équipe de sauvetage. Ce sont des personnes qui travaillent ensemble pendant une courte période pour atteindre un objectif précis ou pour réaliser un projet. Aucun membre de l'équipe n'entre ou ne sort pendant la durée de la mission.

#### Catégorie #4 : L’équipe liquide (ou Liquid Team)

L'équipe “liquide” elle-même est de courte durée et les membres de l'équipe changent également fréquemment.

Enfin, un exemple d'équipe liquide serait une équipe de tournage ou une équipe de construction. Une sorte de task force. Ces équipes travaillent ensemble pendant une courte période pour atteindre un objectif spécifique, mais les membres de l'équipe (généralement des professionnels et des spécialistes) peuvent effectuer une rotation continue.

Bien que ces modèles fassent partie d'unFIX, il n’y a aucune façon de les représenter actuellement sur ses schémas organisationnels.

### II - 4 : Les Turfs (les domaines ou territoires)

Le Turf est une domaine entretenu et protégé par les mêmes personnes. Il peut s'agir d'une base de code, un domaine fonctionnel, ou un quartier dont un groupe de personnes s'occupe collectivement.

Le Turf doit avoir une taille qui lui permette de maintenir son domaine.

Le Turf joue également un rôle dans le concept d’équipes qui se reconstituent selon les besoins (ex: équipes Liquides), ce concept sera plus facilement mis en œuvre sur un domaine ou le nombre de personnes sera plus réduit et ou le domaine fonctionnel sera connu de tous.

### II - 5 : Les Forums (les communauté de pratiques, les guildes, les chapitres)

Les Forums assurent une certaine coordination entre les équipes.

Les chapitres, les guildes, les groupes d'utilisateurs, les communautés de pratique, les comités de technologie et les comités de collaborateurs au sens large sont des exemples de collaboration au-delà des frontières de l'équipe. 

Dans les forums, les gens échangent des connaissances, s’alignent, partagent leurs bonnes pratiques et en standardisent parfois. Il n'y a pas de supérieurs hiérarchiques d’un Forum car il doit être facile de créer et de dissoudre des Forums quand nécessaire.

## Chapitre III - Comment construire son modèle d’organisation avec ces briques ?

Il n’y a plus qu'à jouer aux légos. Vous trouverez même un template Miro pour pouvoir commencer à vous amuser avec.

{{< figure  src="unfix-miro.png" alt="Miro unFIX template" link="https://unfix.circle.so/c/downloads/unfix-miro-template-version-2" class="center-large">}}

et aussi des case studies ([https://unFIX.com/case-studies](https://unfix.com/case-studies)) qui sont des retours d'expérience de modélisation d'organisation avec unFIX.

Nous avons pu nous essayer à l'exercice pour certains de nos clients.

### III - REX #1 : Une startup en hyper croissance

Nous sommes arrivés chez l’un de nos clients avec la mission suivante : aider les équipes à construire une nouvelle organisation plus alignée avec le business pour faire face à leur croissance. Après avoir identifié une [coalition](/posts/20221005-une-coalition-pour-les-guider-tous/), le petit groupe qui allait travailler sur cette nouvelle organisation, nous leur avons donné la mission de modéliser l’organisation actuelle de l’entreprise en utilisant unFIX. Après une courte montée en compétence, le résultat a été extraordinaire. En quelques jours, ils avaient modélisé la totalité de l’organisation, ses flux de communications et la répartition des gens dans les différentes briques.

Voici une version sans détail de ce qu’ils ont construit :

{{< figure  src="rex1-orga.png" alt="Rex 1 orga" class="center-small">}}

Après ça, nous leur avons dit : êtes vous capable de construire l’organisation du futur ? la réponse a été sans appel : Oui.

Nous avons été très agréablement surpris par l’influence d’unFIX sur l’organisation qu’ils ont construite. En effet la lecture du site unFIX dans ses détails durant la première phase a largement influencé leur vision de l’organisation parfaite. Les choix faits par le modèle ont permis de dessiner une organisation cible relativement bien alignée avec la demande de la direction sans que celle-ci ait eu besoin d’exprimer le moindre mot.

{{< figure  src="rex1-unfix.png" alt="Rex 1 unfix" class="center-small">}}

Le modèle de nouvelle organisation a été accompagné d’un certain nombre d’éléments complémentaires auquel unFIX ne répondait pas : 

* Le mode de fonctionnement des équipes (kanban, scrum, scrumban)
* La liste des rituels types d’un équipage
* La liste des outils standards pour la communication orale, la communication écrite, la documentation, la gestion des tickets, les métriques, etc.
* Et un ensemble de questions à adresser à la direction ou aux coachs.

En synthèse : 2 mois pour dessiner une nouvelle organisation, embarquer la majorité des salariés et lancer le mouvement. À la fin des 2 mois, l’organisation avait commencé à changer.

### III - REX #2 : Une organisation de plus de 60 ans qui cherche à se transformer

Une autre histoire, une autre organisation : cette fois, la direction a déjà identifié une coalition interne d’agents du changement. Ce petit groupe motivé a cependant besoin d’aide. La contrainte  : tout le monde est sous l’eau, pressé par le système pour livrer des engagements toujours plus gros.

Cette fois, la coalition avait déjà proposé une organisation pour l’année en cours. La direction a utilisé unFIX pour construire la version suivante de l’organisation. 

{{< figure  src="rex2-unfix.png" alt="Rex 2 unfix" class="center-small">}}

La mission de la coalition : remplir ou faire remplir les cases avec les noms des collaborateurs, les modes de communications entre équipes, le mode d’organisation interne de chacune d’elles. 

unFIX a eu un double apport dans ce contexte : 

* Un nouveau vocabulaire qui permet d’éviter les crispations liées aux nombreux changements d'organisation précédents, 
* Et encore une fois, les partis pris d’unFIX ont influencé dans le bon sens la construction de l’organisation cible.

En synthèse : unFIX nous a permis de construire une nouvelle version de l’organisation en s'appuyant sur les victoires et les forces des équipes en place.

## CONCLUSION - Finalement on en retient quoi d’unFIX ?

Si vous ne devez retenir qu’une seule chose, c’est qu’unFIX est un nouveau langage, un nouvel outil pour modéliser des organisations. Ce n’est donc pas un framework et encore moins le nouveau SAFe.

Ce nouveau langage, ce nouvel outil de modélisation, n’est pas adapté à n’importe qui, selon nous. Il s’adresse :

* à celles et ceux qui ont déjà tenté de transformer leur organisation, qui ont appris de leurs expérimentations et qui maîtrisent les concepts clés du design d’organisations,
* à des personnes qui cherchent à adopter une nouvelle perspective pour analyser et améliorer leur organisation, car unFIX est un outil d’analyse plus qu’un outil de communication.

Mais pour celles et ceux qui sauront s’y intéresser et le manipuler, ce nouveau langage est une double opportunité : 

* celle de représenter sa propre organisation, telle qu’elle est, pour mieux  déceler ses forces et ses faiblesses. C’est comme regarder une personne que vous n’avez jamais vue qu’en costume au bureau et que vous voyez pour la première fois en short sur un terrain de sport. Vous la regardez dans un nouvel environnement et la découvrez sous un autre jour, 
* et celle de dessiner une organisation cible en vous affranchissant des outils de design classiques. Utiliser unFIX, c’est proposer une méthode nouvelle et donc se donner la possibilité de trouver des solutions très différentes de celles déjà existantes ou déjà utilisées par le passé.

Pour vous rendre compte de son potentiel, nous vous suggérons sincèrement de modéliser votre propre organisation en l’utilisant. En revanche, vous devez savoir que ce nouveau langage comporte ses contraintes : 

* Même s’il peut modéliser des organisations de centaines de milliers de personnes, il est conçu pour des périmètres de petite et moyenne taille. Il faut donc savoir maîtriser ses concepts pour les abstraire et les penser à l’échelle
* Par ailleurs, ce langage ne permet de représenter (entre autre):
  * ni le middle-management
  * ni les component teams

Enfin, retenez aussi qu’unFIX est un langage récent : J. Appelo l’a publié début 2022. Malgré son potentiel, il reste un outil incomplet et perfectible, un outil qui va donc évoluer dès les prochains mois, surtout que Jurgen Appelo ne travaille plus seul dessus, un groupe de 7 personnes forme maintenant l’entreprise unFIX. Nous vous ferons suivre ses probables modifications lorsqu’elles seront disponibles.

Désormais, c’est à vous d’essayer !
