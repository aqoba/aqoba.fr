---
title: Comment une future licorne de la tech est en train de renouveler son approche RH
authors:
  - thomas-clavier
values: [humanite]
tags:
  - peopleops
  - rh
offers:
  - unlock-management
date: 2022-06-01
thumbnail: 2205782794_72ed455e74_k.jpg
subtitle: Et si on remplaçait des documents par des ateliers
---
C'est l'histoire d’une société que j’ai accompagnée et de son département RH. On est dans une société en hyper croissance qui cherche constamment à s'améliorer. Une des pratiques de ce département ressources humaines, c'est de demander un rapport d'étonnement aux derniers arrivés. Cette pratique, en place depuis la création de l'entreprise, nous l'avons totalement remise à plat en collaboration avec cette équipe RH. Laissez-moi vous raconter ici pourquoi et comment nous nous y sommes pris.

<!--more-->

## Contexte
L'histoire se déroule donc dans une société en pleine croissance, leur objectif est d'embaucher près de 300 personnes par an pendant 3 ans. Ce qui représente presque une nouvelle personne par jour. De son côté, le département RH souhaite rester très attentif au bien-être au travail. Pour ce faire, chaque nouvel embauché est invité à rédiger un <<rapport d'étonnement>>. Pas un gros rapport : juste 2 ou 3 pages regroupant ses principales surprises. On entend par “surprise” les choses auxquelles le nouveau salarié ne s'attendait pas et qu'il a soit trouvé hyper chouette, soit au contraire, qu'il a trouvé améliorable. À la suite de ce rapport, une personne des RH va le lire et classer les surprises en 4 grandes catégories :

* C’est à garder et c’est le département RH qui a la main dessus
* C’est à garder et il faut prévenir les bonnes personnes
* C’est à changer ou améliorer et c’est le département RH qui gère
* C’est à changer ou améliorer et il faut prévenir les bonnes personnes pour que le changement ait lieu

Enfin, la personne ayant lu le rapport prend rendez-vous avec le jeune embauché pour lui expliquer en tête-à-tête les actions qui vont être menées suite à son rapport d'étonnement.

Maintenant faisons un simple calcul, pour chaque nouvel embauché il faut prévoir le temps de lecture et d’analyse du rapport ainsi que le temps de l’entretien individuel. Il faut compter aussi le temps d’agréger les données de tous ces rapports enfin tout ça est multiplié par 300 et il faut faire tout ça en moins de 220 jours travaillés avec une équipe de 5 personnes. On est d’accord : c’est une énorme charge de travail.

## Déroulé

En voyant ça, j’ai proposé de remplacer ce processus post-embauche par un simple atelier : un [world café](https://en.wikipedia.org/wiki/World_caf%C3%A9_(conversation)). Embarquer l’équipe RH dans une expérience de 3h pour remplacer la lecture et l’analyse d’une cinquantaine de rapports n’a pas été très compliqué. C’est l’avantage d’accompagner une entreprise apprenante : ils ont l’habitude de tester des choses, une expérience de plus avec un tel ROI ça se tente. L’atelier s’est donc déroulé de la façon suivante :

Réunion non obligatoire de l’ensemble des jeunes embauchés avec une ancienneté comprise entre 30 et 60 jours sur une après-midi dans un lieu suffisamment grand pour accueillir tout le monde et former des petites tables de 4 à 5 personnes.
Leur donner du papier et des crayons, puis dérouler le World Café avec les questions suivantes[^1] :

[^1]:je mets aussi la version traduite en anglais au cas ou vous ayez besoin de les dérouler dans un contexte anglophone)

* Racontez entre vous des histoires que vous avez vécues depuis votre arrivée et qui évoquent chez vous un sentiment négatif (Tell the group at your table your stories that you've had that are negative / left you with feelings of anger, loneliness, frustration or other negative feelings).
* En vous inspirant des histoires de votre nouvelle table, construisez la liste des choses à améliorer. Priorisez les éléments à améliorer (With these stories in mind, what needs to be changed so that these experiences do not happen again or to other salaries?).
* Racontez entre vous des histoires que vous avez vécues depuis votre arrivée qui évoquent chez vous un sentiment positif (In the group, tell your stories of positive, happy experiences you've had so far)
* En vous inspirant des histoires de votre nouvelle table, identifiez les éléments sur lesquels nous pouvons nous appuyer pour faire encore mieux avec les prochains embauchés et construisez ainsi la liste priorisées des choses à garder (With these stories in mind, what should we keep, encourage to ensure these experiences continue to be shared by other salaries?).
* Imaginez-vous demain : que commencez-vous à faire pour améliorer l'expérience des nouveaux salariés ? (Starting tomorrow, what can YOU as an individual do to bring a positive experience to salaries and the community?)

Enfin nous avons terminé par un bâton de parole pour répondre à la question : Comment avez-vous trouvé cet atelier ? Pensez-vous qu’il faut le refaire avec les autres nouveaux embauchés ? (How did you find the workshop today? Do you think we should continue with these types of workshops?)

## Apprentissages

Que s'est-il passé ? Les participants ont adoré l’expérience pour les raisons suivantes :
* C’est moins de travail pour eux, car rédiger un rapport c’est long et fastidieux.
* C’était l’occasion de rencontrer pleins de nouveaux embauchés dans d’autres équipes.
* Ils ont eu le sentiment d’être écoutés et entendus.
* Ils ont pris des actions concrètes pour accueillir encore mieux les futurs embauchés

C’est plus rapide et plus impliquant que le rapport écrit pour plusieurs raisons :
* Tout d’abord, en moins de 3 heures nous avons agrégé le retour d’une trentaine de nouveaux embauchés, nous avons défini les actions les plus importantes, identifié les équipes qui pouvaient agir, identifié des personnes pour porter ces sujets (majoritairement des RH présents).
* Enfin, le nouvel embauché est maintenant lui aussi acteur de l'accueil des nouveaux

Les RH présentes à cet atelier (oui ce ne sont que des filles) ont été très surprises par l’efficacité de l’intelligence collective. Elles ont donc décidé de généraliser ces ateliers et de les rendre mensuels.

En résumé, en plus d’avoir fait gagner du temps à tout le monde, nous avons créé du lien et rendu les nouveaux embauchés acteurs de l’accueil des nouveaux.

Et vous, dans vos départements RH avez-vous pensé à changer vos processus et vos habitudes en vous inspirant des pratiques des autres départements ? Chez aqoba, c’est notre ADN de piocher dans nos pratiques d’ingénieurs pour inspirer des changements dans vos métiers. Et puis, pour être honnêtes, nous sommes convaincus que le mouvement  People Ops c’est l’avenir. Mais ça ce sera le sujet d’un autre article.

