---
draft: false
title: Une coalition pour les guider tous !
authors:
  - antoine-marcou
values:
  - conviction
  - partage
tags:
  - agile en seine
offers:
  - unlock-transformation
date: 2022-10-05T11:43:48.175Z
thumbnail: 1663834778821.jpeg
subtitle: Construisez une coalition de transformation puissante et formez vos
  leaders du changement
---
Monter une équipe de transformation efficace et impactante n’a rien de trivial. C’est pourtant un prérequis à toute transformation d’organisation réussie.

Lors de la conférence Agile en Seine 2022, le 21 septembre 2022, nous avons eu la chance de partager le retour d’expérience de la Guiding Coalition SACEM. 3 membres de cette équipe sont venus décrire la manière dont fonctionnait leur équipe d’internes chargée de la transformation de son organisation.

Leur retour d’expérience sert de base à cet article. S’y ajoutent ceux de plusieurs autres coalitions auxquelles nous avons contribué : nous en retenons 10 clés de succès (illustrés de leurs bonnes pratiques et de leurs anti-patterns) pour mettre une coalition aux manettes de sa transformation d’entreprise. En espérant qu’elle nourrira vos propres démarches de transformation.

## Une coalition de transformation, c’est quoi ?

Avant de rentrer dans le vif de ce retour d’expérience, prenons une minute pour définir ce qu’est une coalition.

Le terme de “coalition” est utilisé par [John Kotter](https://www.kotterinc.com/methodology/8-steps/) dans son modèle “*8-Step Process to Leading Change*”. Ce modèle identifie 8 étapes pour créer une dynamique de transformation à l’échelle d’une organisation. Construire une “*Guiding Coalition*” est un prérequis à un changement réussi, selon ce modèle : elle est composée de personnes engagées et motivées qui vont guider, coordonner et communiquer autour des activités de transformation.

![8-Step Process to Leading Change](kotter.png)

## Au fait, pourquoi créer une coalition d’internes ?

Chez aqoba, cela fait une dizaine d’années que nous menons des transformations d’entreprises. Et nous savons d’expérience que ces transformations ne s’enracinent pas si elles ne s’ancrent pas dans une envie profonde de changement de la part des collaborateurs eux-mêmes. Même s’ils sont sincères dans leur désir de se fondre dans l’entreprise, des consultants ne maîtriseront jamais aussi bien que les internes l’ADN, la culture et l’histoire de l’organisation.

> Une transformation d’entreprise confisquée par une équipe de consultants externes est vouée à l’échec

C’est pourquoi, lorsqu’on s’apprête à faire évoluer les modes de travail, les outils ou  l’organisation d’un groupe d’équipes, nous suggérons fortement de rassembler une coalition de transformation. Bien sûr, inutile de vous lancer ce défi si votre périmètre de transformation concerne moins de 30 personnes. Mais au-delà de ce nombre, une coalition est clé.

Maintenant, ce n’est pas le tout de décider de lancer une coalition. Encore faut-il réussir sa composition, son lancement et sa mise en autonomie. Et nous en savons quelque chose car il nous est arrivé de faire des erreurs lors de ces 3 étapes. Voici ce que nous en avons retenu.

## Coalition de transformation : 10 clés de réussite et autant d’anti-patterns à éviter

Voici les 10 bonnes pratiques que nous respectons systématiquement lorsque nous faisons émerger une coalition de transformation. Ne pas les appliquer, c’est freiner mécaniquement la dynamique de l’équipe.

![10 clés de réussite](unnamed.png)

### 1. Une coalition officielle, connue et reconnue, dès le début de la transformation

Il est important de faire, mais aussi et surtout de faire savoir : toute l’organisation doit savoir qu’une équipe d’internes va guider, coordonner et communiquer autour de la transformation. 

**Notre bonne pratique** : La coalition est présentée lors du kick-off de la transformation, lors d’une réunion plénière rassemblant tous les collaborateurs de l’organisation. Pour ces collaborateurs, elle est ainsi personnifiée. Et pour les membres de la coalition, c’est un moment d’adoubement.

**L’anti-pattern** : Ne lancez pas une coalition en catimini pour tester l’équipe. Cela ne lui donne pas les moyens de lancer une dynamique de changement : c’est donc la meilleure manière de saboter la transformation.

### 2. Un pouvoir de décision et des sponsors forts

La coalition ne peut pas être une équipe fantoche qui se bornerait à appliquer les décisions d’un top-management omniprésent. Chacun son rôle :

* Au top-management de poser la cible,
* À la coalition de tracer la trajectoire de transformation.

**Notre bonne pratique** : favoriser confiance et transparence entre la coalition et la direction. Et s’assurer que cette dernière joue le jeu en rappelant régulièrement son mandat à la coalition.

**L’anti-pattern** : Ne lancez pas une coalition qui n’a pas un mandat clair et des appuis forts dans l’organisation. Elle sera freinée dans ses actions et ne pourra réaliser des victoires rapides.

### 3. Pas de hiérarchie, pas de statut à l’intérieur de la coalition

L’équipe de transformation peut être composée de personnes ayant des grades hiérarchiques différents dans l’organisation. Mais ces grades sont laissés à la porte de la coalition.

**Notre bonne pratique** : 1 personne = 1 voix au sein de la coalition

**L’anti-pattern** : Ne laissez pas le/la HIPPO (Highest Paid Person Opinion) imposer sa volonté au reste de la coalition. Au contraire, encouragez les autres dans leurs prises de position.

![SGT-Hartman](SGT-Hartman-2.jpeg)

### 4. Un rituel par semaine hyper efficace : on y décide et on y fait

La ressource rare d’une coalition de transformation, c’est le temps. Il est donc indispensable d’optimiser les moments qu’elle passe ensemble afin de lancer et conserver sa dynamique.

**Notre bonne pratique** : réunir la coalition chaque semaine, sur le même créneau, pendant 2h30 à 3h. Ce rituel se découpe ainsi : 

* 20’ d’alignement
* 2h à 2h30 de travail en 2 ou 3 petits groupes sur les sujets en cours : il s’agit d’ateliers où l’on réfléchit, on décide et on produit. Avec une règle : on ne sort pas de ces ateliers avec des actions à réaliser, on les traite en séance
* 10’ de débrief

Dans l’idéal, ce rituel se tient en présentiel pour souder l’équipe. 

**L’anti-pattern** : Ne promettez pas aux membres de la coalition que vous leur dégagerez du temps en leur permettant de déléguer certaines tâches. Cela n’arrivera pas. Soyez simplement conscient que la coalition est composée de personnes ayant déjà un agenda chargé.

### 5. 25% de l’énergie dépensée sur la conduite du changement, en continu

Nous sommes convaincus que faire évoluer une culture de travail n’est possible qu’en embarquant tout le monde et en étant transparent sur la démarche de transformation. Il est donc clé que la coalition dédie une grande partie de son énergie à conduire le changement et à donner de la visibilité sur ce qu’elle fait.

**Notre bonne pratique** : Avoir en permanence une action de conduite du changement correspondant à 25% de la capacité à faire de l’équipe de transformation. Ces actions doivent varier dans le temps : diffusion d’une newsletter, sessions de sensibilisation ou de formation, petits-déjeuners thématiques, rencontres face-à-face, etc.

**L’anti-pattern** : Ne faites pas de la conduite du changement la cinquième roue du carrosse, autrement dit le sujet que vous traitez quand vous en avez le temps / quand vous y pensez.

### 6. Une équipe Scrum de moins de 10 membres, appuyée sur un noyau dur de 3 à 4 personnes

Dans un monde idéal, nous aimerions rassembler une équipe où tous les membres sont super-engagés dans la démarche de transformation. Dans la réalité, créer un noyau dur de 3 à 4 personnes très impliquées suffit pour lancer et maintenir la dynamique. Les autres membres “flottent” autour de ce noyau en fonction de leur intérêt pour les sujets du moment.

**Notre bonne pratique** : Rassembler une équipe de moins de 10 personnes qui sera facile à animer, sur le modèle d’une équipe SCRUM. Et identifier les quelques-uns qui sont les plus présents, impliqués et actifs pour faire émerger le noyau dur.

![noyau d'avocat](noyau-avocat-1200x.jpeg)

**L’anti-pattern** : N’acceptez pas qu’une coalition devienne trop grosse (au-delà de 10 personnes), elle deviendrait impilotable. Et ne vous accrochez pas à l’idée que tout le monde doit avoir le même niveau d’engagement.

### 7. Des internes, des internes, des internes et uniquement des volontaires

L’un des atouts d’une coalition composée d’internes, c’est sa légitimité pour expliquer la transformation et les y faire adhérer. Et pour s’appuyer sur des gens engagés (qu’ils soient promoteurs ou détracteurs de la transformation), on les laisse se positionner eux-mêmes : ne participent que des volontaires.  

**Notre bonne pratique** : Privilégier une majorité d’internes dans la composition de la coalition. Et ne leur mettez aucune pression pour participer à l’équipe.

**L’anti-pattern** : Ne montez pas une équipe composée à plus d’un tiers par des externes. La coalition perdrait en légitimité. Et bien sûr, ne manipulez personne pour qu’elle y participe. Et tranquillisez-vous : si une personne n’est pas partante à un moment T, elle le sera peut-être plus tard.

### 8. Deux accélérateurs clés dans la coalition : l’administrateur JIRA et un membre de l’équipe Comm’

Dans deux tiers des transformations que nous menons, JIRA est un outil essentiel de transparence et de pilotage. Embarquer l’administrateur JIRA dans la coalition nous fait gagner un temps précieux car nous adaptons l’outil en séance, avec lui/elle.

Et, on l’a déjà partagé plus haut, la conduite du changement est clé : elle est d’autant plus efficace si un membre de l’équipe de Communication Interne accepte de dégager du temps pour aider la coalition à communiquer efficacement.

**Notre bonne pratique** : Appuyez-vous sur les sponsors de la coalition pour obtenir que ces personnes participent à la coalition. Même si cela ne peut être fait qu’avec l’approbation des personnes en question.

**L’anti-pattern** : Ne dépendez d’aucune personne clé extérieure à la coalition.

### 9. Une rotation de l’équipe tous les semestres

La plupart du temps, les membres de la coalition prennent ce mandat en plus de leurs autres responsabilités. Pour certains, il est difficile de maintenir un rythme soutenable en participant à une coalition. C’est pourquoi nous inscrivons dans les principes de la coalition une rotation tous les semestres, voire tous les trimestres lorsque nous le pensons nécessaire. Cela permet aux personnes qui le souhaitent de se mettre en retrait sans douleur.

Par ailleurs, accueillir régulièrement de nouveaux membres de la coalition permet de multiplier le nombre d’ambassadeurs de la transformation.

**Notre bonne pratique** : Instaurer, dès le démarrage de la coalition, le principe d’un renouvellement partiel de l’équipe tous les semestres.

**L’anti-pattern** : Ne laissez personne se sentir coupable pour avoir envie de quitter la coalition. Au contraire, faites de la rotation de l’équipe un non-évènement.

![Passage de relai]()

### 10. Une coalition représentative : diverse par ses profils et mixte

Des jeunes & des seniors, des nouveaux & des anciens, des femmes & des hommes, des profils techniques & des profils métier, des managers & des opérationnels. Dans l’idéal, chaque membre de l’organisation doit se sentir représenté au sein de la coalition. Et inutile de faire de la discrimination positive pour arriver à un résultat satisfaisant.

**Notre bonne pratique** : Au moment de la composition de l’équipe, faites un pas de côté et assurez-vous que sa composition est équilibrée.

**L’anti-pattern** : Faites en sorte qu’aucune équipe / aucun profil ne soit sur-représenté.

## Pour conclure : allez-y !

On ne monte jamais une coalition de transformation parfaite (en tout cas, nous n’y sommes jamais arrivés). Mais nous sommes convaincus que l’existence même d’une coalition composée d’internes engagés dans la transformation de leur propre organisation est un accélérateur du changement et la manière la plus efficace de l’enraciner.

Et si vous souhaitez qu’on vienne en parler avec vous, faites-nous signe : nous échangerons avec plaisir. 

Pour conclure, je tiens, en mon nom et en celui de mes camarades, à remercier sincèrement les membres de la coalition SACEM qui ont accepté de partager leur retour d’expérience lors de la conférence Agile en Seine. Ils nous inspirent par leur engagement, leur transparence et leur envie constante de s’améliorer !

Et merci à [Gautier Lesieur](https://www.linkedin.com/in/gautier-lesieur-3a31244/) pour sa facilitation graphique pendant la conférence et dont le résultat sert d'illustration à cet article.

<figure>\
<iframe title="AES22 - Guiding Coalition SACEM" width="560" height="315" src="https://video.ploud.fr/videos/embed/5c1883a8-5e99-4ea6-bd1e-8cda1ce58197" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

\    <figcaption>La vidéo de l'intervention à Agile en Seine 2022</figcaption>

</figure>
