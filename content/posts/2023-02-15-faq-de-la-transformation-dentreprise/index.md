---
draft: false
title: FAQ de la transformation d'entreprise
authors:
  - thomas-clavier
values:
  - conviction
tags:
  - FAQ
  - transformation
  - lean
  - entreprise
  - organisation
offers:
  - unlock-transformation
date: 2023-02-15T17:13:48.544Z
thumbnail: illustration-article-transfo.jpg
subtitle: Apprenez à changer !
---

Notre métier c’est la transformation d’organisations, une bien belle expression qui n’est pas forcément facile à cerner voire même totalement dénuée de sens pour d'autres. Afin de répondre à tout un ensemble de questions qu'évoque cette expression, nous allons dérouler cet article comme une interview.

## Ça veut dire quoi transformer une entreprise ou une organisation ?

Une entreprise est un système complexe qui produit de la valeur pour des clients. Il peut arriver que ce système complexe ait besoin de changer son organisation, sa structure interne et sa façon de penser pour continuer son évolution. Transformer une entreprise c’est aider ce système complexe à bouger de l’intérieur. Prenons un exemple, la société BIC qui fabrique les fameux stylos à billes, en 1972, le comité de direction reformule la raison d’être de la société de : <<créateur d'instruments d'écriture>> en <<faciliter la vie de nos utilisateurs avec des produits en plastique fiables, simples d’utilisation et très peu onéreux>>
À partir de là, ils se sont mis en capacité de produire des briquets et du matériel de sport. Alors que leurs bureaux d’études, leurs chaînes de productions, leurs filières de ventes, leur système d’information étaient tous optimisés pour produire et vendre des stylos, ils ont transformé l’entreprise pour s’adapter à ces nouveaux marchés.

## Pourquoi vouloir transformer une entreprise ?

{{< figure  src="illustration-transfo-avant-après.png" alt="Illustration d'une entreprise, avant, après" class="center-small">}}

Les raisons qui poussent une entreprise à se transformer sont nombreuses. Celles que nous croisons régulièrement sont : 
* Le rachat la transmission ou l'absorption d’une société
* La fusion de plusieurs entreprises
* La levée de fonds pour le passage à l’échelle de l’activité
* Un changement de taille d’entreprise grosse croissance ou réduction drastiques d’effectifs
* Un changement de marché, pivot ou changement de la taille du marché
* Un changement de direction stratégique
* Une demande de la direction, par exemple un nouveau directeur du système d’information qui arrive avec des idées neuves
* Mais le plus souvent, c'est la nécessité, pour défendre la position de l'entreprise sur son marché, d'améliorer sa capacité de production : produire plus, plus vite, de manière plus qualitative ou plus sereinement. Nous parlerons alors de réduire le Time To Market, d’augmenter la valeur, d’être plus orienté produit, d’automatiser, d’industrialiser, de remettre le client au centre, etc.

En bref, les facteurs déclencheurs qui poussent une entreprise à se transformer sont variées et très nombreux.

## À quels moments de sa vie une entreprise doit-elle se transformer ?

{{< figure  src="evolution_of_a_company.png" alt="Illustration de la transformation sur le thème du papillon" class="center-small">}}

Une entreprise n’est pas un pokémon avec un cycle d’évolutions standard. Il y a de très nombreux événements qui peuvent déclencher une transformation, ces événements sont souvent rapprochés les uns des autres et contrairement à une idée préconçue qu’il y a un début et une fin de transformation, l’entreprise doit accepter de se transformer en permanence il devient alors indispensable d’apprendre à changer.
Une autre réponse à cette question serait de partir de la réponse de Taiichi Ono à Darwin : "les entreprises qui survivent ne sont pas les entreprises les plus fortes ni les plus innovantes, mais celles qui s'adaptent le mieux à leur environnement" le lean management comme le mouvement de l'entreprise apprenante prône l'idée d'une organisation en perpétuel adaptation, à l'organisation mouvante qui s'adapte en permanence. Notre environnement change de plus en plus vite, et il est plus difficile de stopper l'évolution du monde que de s'adapter. Ces entreprises font le choix d'être en transformation perpétuelle.

## Comment ça marche une transformation ?

Les êtres humains étant tous différents, il n'y a pas de recette magique. Aux deux extrêmes de la transformation, il y a :

* La stratégie rapide et brutale, comme licencier tout le monde pour reconstruire. Stratégie qui se détecte dans les expressions : “C'est tout pourri, faut tout refaire” ou “Tu t'adaptes ou tu sors”.
* La stratégie tout en douceur, dans laquelle on garde tout le monde, on garde l’existant et on soutient, on accompagne chaque homme et femme en les tenants par la main.

Entre les deux, il y a toute une variété de stratégies possibles. Nos valeurs nous obligent à nous éloigner le plus possible de la première extrémité pour garder l'humain au centre de vos entreprises.  Ce n'est pas toujours simple, on travaille beaucoup en s'appuyant sur les travaux de chercheurs en leadership et en management. Je pense en particulier à John Kotter et à mes collègues du [LUMEN](https://lumen.univ-lille.fr/).

## À la base vous êtes des informaticiens, c'est quoi le rapport ?

D'une part la façon de produire du logiciel est source d'inspiration pour bon nombre d'entreprises et d'autre part l'informatique est maintenant le cœur de nos sociétés.

Détaillons, pendant longtemps on a considéré l'informatique comme un centre de coût pour l'entreprise. Un coût devenu obligatoire comme un comptable. Aujourd'hui ce n'est plus le cas, maintenant nous consommons du logiciel quasiment en permanence, l'informatique est devenu un centre d'innovation.
L'ensemble de l'entreprise est modélisé dans son système d'information (SI) et toute modification dans ce SI a des impacts grandissants sur l'activité de l'entreprise. À tel point que le principal concurrent d'Auchan, Amazon, se définit comme éditeur de logiciel et pas comme vendeur. Tesla conçoit et vend des voitures comme si c'était du logiciel avec des mises à jour hardware presque aussi transparentes que les mises à jour de google chrome ou Firefox. Pour s'adapter à cette concurrence qui pense les produits et les services comme des logiciels il est nécessaire d'avoir des spécialistes du logiciel.
Notons aussi que le mouvement agile est né dans le giron du logiciel, et qu’il s'étend progressivement à l’ensemble de l’entreprise (RH, finance, gestion de budget dynamique, etc.). Nos expériences dans l’informatique, le fait que nous soyons des précurseurs de l’agilité, nous positionnent de facto comme des experts du sujet.
En gros, les équipes informatiques ont inventé, dans la fin des années 90, un cadre de travail révolutionnaire qui s'est enrichi et structuré pendant 30 ans. C'est cette culture et cette boîte à outils que nous maîtrisons et avons adapté pour qu'elle transforme toute l'entreprise.

## Les transformations ont-elles un lien avec le lean ?

Pour faire court, oui, le Lean et tous ses outils ont une place de choix dans notre boîte à outils.

[Michael Ballé](https://www.institut-lean-france.fr/portfolio-item/michael-balle-2/) donne la définition suivante du Lean : 

> Le Lean est une méthode de management qui vise l'amélioration des performances de l'entreprise par le développement de tous les employés. 

Pris sous cet angle, il nous paraît indispensable d’utiliser au moins une partie du Lean. L’approche très flux de production de valeur, trouvant ses origines dans l'industrie automobile, centrée sur le client et les employés parle bien à certaines personnes. Avec d’autres personnes, l’approche sera plus axée sur l'excellence opérationnelle, l’art de bien faire son travail. Les individus sont tous différents, leurs affinités aussi. Trouver le bon vecteur de communication fait aussi partie de notre métier. Chez Renault par exemple, juste après le drame du technocentre, les syndicats définissaient le lean comme une méthode pour produire sans gaspillage, au plus juste au détriment des humains. Difficile alors d’utiliser le vocabulaire du lean management pour leur parler.

## Les transformations ont-elles un rapport avec avec l'entreprise apprenante ou l'entreprise libérée ?

{{< figure  src="sketch_learning_organization.png" alt="Illustration de l'intelligence collective" class="center-small">}}

L'expression “entreprise apprenante” fait référence aux travaux de Chris Argyris et Peter Senge qui expliquent qu'une organisation est comme un être vivant dans un écosystème : chaque membre doit être attentif à cet écosystème pour assurer le développement durable de l'organisation. C’est effectivement une des racines de nos actions. Je conseille d'ailleurs régulièrement la lecture du livre “La 5e discipline” aux personnes que j’accompagne.

L’entreprise libérée fait référence aux travaux d’Isaac Getz qui souligne que les organisations qui laissent aux employés une liberté et une responsabilité totales pour prendre les mesures qu'ils jugent les meilleures sont les plus performantes. Nous sommes nous aussi persuadés que la performance n'est pas un objectif en soit et que ce sont les femmes et les hommes qui composent l'entreprise qui doivent rester le centre de l'attention afin de collectivement prendre soin des clients. 

Pour répondre à la question : oui, les transformations que nous menons considèrent l'entreprise apprenante et l'entreprise libérée comme des sources d’inspiration. Nous encourageons et accompagnons  les organisations qui, pour accélérer les prises de décisions, décident de décentraliser la décision. Ceci déclenche un cycle vertueux et permanent de montée en compétence de leurs membres.

## En fait, vous faites de l’innovation ?

Wikipedia définit l’innovation comme la recherche constante d'améliorations de l'existant, par contraste avec l'invention, qui vise à créer du nouveau. 

Donc oui, nous faisons de l’innovation d’organisation, de l’innovation managériale. En piochant dans les pratiques et les outils tirés de notre culture. En utilisant des pratiques et des outils qui ont fait leurs preuves autre part. En mélangeant ces pratiques et ces outils aux contraintes de nos clients, nous participons à l’amélioration constante des organisations existantes qui nous appellent. 

Notez que certains voient de l’invention dans des pratiques anciennes mais peu répandues. Pour rebondir sur l’actualité, des chercheurs ont récemment découvert [le secret de la longévité du béton romain](https://www.science.org/doi/10.1126/sciadv.add1602) l’utiliser aujourd’hui serait une innovation ou pas ?

## Vous appliquez toujours les mêmes modèles ?

Nous accompagnons les changements intrinsèques de l’entreprise. Pour ce faire nous sommes à l’écoute de ses volontés de changement, à l'affût de ses points de désagréments et d’irritations, mais nous avons aussi des convictions. Et c’est le mariage de ces éléments qui produit la nouvelle organisation. Une nouvelle organisation qui réduit les points de frictions, qui est adaptée à l’avenir et qui remet l’humain au centre. 
Donc non, nous n'avons pas de modèle type que nous appliquons systématiquement, en revanche notre culture prend ses racines dans de nombreuses recherches, apprentissages et expériences dans lesquelles nous piochons les éléments les plus adaptés à la culture de nos clients. 
Nous maîtrisons les principaux frameworks d'agilité à l'échelle et nous nous en servons de source d'inspiration pour coller à la culture et aux enjeux de l'organisation que nous accompagnons. In fine, nous n'appliquons jamais 2 fois les mêmes recettes à deux entreprises différentes. Et nous suggérons à nos clients de fuir les équipes de conseil qui clament avoir trouvé la "recette Marmiton" qui fonctionnerait partout.

## Un mot de conclusion ?

Regrouper ici ces questions que nous avons croisées de nombreuses fois nous a permis de clarifier notre pensée. Merci à vous toutes et tous, vous que nous avons croisé autour d'événements d'entreprise, de soirées d'entrepreneurs, vous créateurs d'entreprise, startuppeurs, managers, directeurs ou repreneurs qui avaient cherché à comprendre ce que nous faisons. Peut-être que grâce à vous, d'autres comprendront mieux notre activité et qui sait, nous aurons peut-être le plaisir de collaborer.



<small>Illustration principale par [pbrad1974](https://www.flickr.com/photos/pbrad1974/), les autres images ont été générées avec [MidJourney](https://midjourney.com)</small>
