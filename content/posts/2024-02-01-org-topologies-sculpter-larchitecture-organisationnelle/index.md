---
draft: false
title: "Org Topologies™ : Sculpter l'Architecture Organisationnelle"
authors:
  - benjamin-feireisen
values:
  - conviction
  - partage
tags:
  - Org Topologies
  - Organisation
  - Changement
offers:
  - unlock-transformation
date: 2024-02-01T08:59:02.883Z
thumbnail: ot-illustration.jpg
subtitle: Explorer les Modèles de Changement et Optimiser la Cartographie des Équipes
---
Naviguer à travers les évolutions constantes des modèles organisationnels et des approches de gestion du changement devient une nécessité impérative dans le monde des entreprises. Depuis l'avènement de l'agilité, la diversification des modèles s'est intensifiée, chaque nouvelle approche apportant son propre langage, sa propre simplification et une perspective unique sur la manière d'appréhender les dynamiques organisationnelles. : avoir plus de cartes, pour enrichir notre vision du monde.

Certains modèles sont sortis du lot, comme par exemple  **[Team Topologies](https://teamtopologies.com/)[^teamtopologies]** [dont on a parlé suite à un Xtream reading](/posts/20221017-nous-avons-tent%C3%A9-lexp%C3%A9rience-extrem-reading/), qui permet de définir des équipes à travers leurs buts : “Suis-je une équipe qui livre directement de la valeur ou qui est en soutien pour réduire la charge mentale ?”,  et leurs interactions : “Quelle méthode j’utilise pour interagir avec autrui ? Synchrone ? Asynchrone ?”.

Le nouveau modèle que je vous présente, celui de **[Org Topologies™](https://www.orgtopologies.com/)** créé par [Roland Flemm](https://nl.linkedin.com/in/rolandflemm) et [Alexey Krivitsky](https://www.krivitsky.com/), vient étendre notre boîte à outils organisationnelle. Son point fort ? Permettre de cartographier les organisations, les équipes, et les personnes au travers de deux axes : 

* **La complémentarité des compétences** : créer de l’autonomie par la pluridisciplinarité afin d’obtenir un flux de travail fluide. Plus une équipe possède cette complémentarité, moins il existe de gaspillage dans son processus de livraison, de l’idée à la mise en production.
* **Le périmètre du travail** : étendre le périmètre de compréhension du produit des équipes, afin d’améliorer l’adaptation aux besoins des clients. Plus une équipe étend son périmètre de travail, plus elle est capable de s’adapter rapidement aux changements de priorités et aux besoins des clients. C’est la différence entre des component teams, qui se passent le travail (une équipe back qui passe à une équipe front qui passe aux testeurs…), et des features teams qui peuvent travailler sur des parties du produit en autonomie, voire des équipes produit holistique capables de travailler sur un produit dans son entièreté !

## Vos équipes ont une histoire

Vous avez généralement pu croiser certaines de ces équipes, car Org Topologies propose 7 archétypes particulièrement communs : toute ressemblance avec des équipes déjà existantes est, bien évidemment, totalement fortuite ! ;)

Imaginons… Vous êtes dans une entreprise d’e-commerce, vous vendez des instruments de musique, et bien sûr, vous avez un site web. Vous avez un pôle métier, qui assigne des projets par l’intermédiaire d’un pôle PMO. Coté IT,  un manager / chef de projet / responsable technique / super héros qui assigne les projets à des “ressources” en découpant fonctionnellement et techniquement le travail. Chaque “ressource” a des compétences techniques bien spécifiques et sont réunies dans une équipe, ou plutôt des groupes de travail, avec toutes les mêmes compétences (une équipe back, une équipe front…). L’informel n’existe pas chez eux, d’ailleurs les managers évitent de créer des moments pour apprendre à se connaître, même en dehors des heures de travail…

Bienvenue dans le monde des équipes **“Projets et tâches” (Y1)** ! Tout est **optimisé pour maximiser l’utilisation des ressources et réduire les coûts**. Ce qui amène à avoir des personnes qui sont au centre de tous les projets, et quand ils ne sont pas là… Plus rien ne marche !

Après quelques années de bons et loyaux services, un nouveau DSI arrive et découvre le fonctionnement actuel. En discutant avec les managers et quelques développeurs, il se rend compte que toutes les équipes fonctionnent comme ça : l’individualisme règne. Il se dit que ce serait bien d’instaurer un peu d’esprit d’équipe, de mettre en place des team buildings, et aussi de créer une équipe d’automatisation des tests et une équipe DevOps pour accélérer le travail.

Cependant, elles sont encore sur des “tâches”... “Mettre en place Selenium pour automatiser tous nos smoke tests”, “Créer la pipeline de CI/CD pour l’équipe front”... Chaque personne dans ces équipes travaille sur ses tâches à lui, même s’ils s’entraident parfois un peu : ce sont les équipes **Y2 : “équipes étroitement spécialisées”**. Elles sont **optimisées pour avoir une responsabilité bien précise, étroite.** Ce sont les component teams par excellente, développant chacun son composant, sa petite partie à l’édifice. Ils ont beaucoup de compétences en interne, certes… Mais il manque la vision : 

Pourquoi faire cela ? Quel est le gain final pour l’utilisateur ? Le concept de “Product Discovery” leur est totalement inconnu…

{{< figure  src="org-topo-7-archetypes-majeurs.png" alt="Org Topologies propose 7 archétypes particulièrement communs" caption="Image tirée du primer Org Topologies" class="center-large">}}

Voyant ces problématiques, le DSI propose une restructuration. Il souhaite tester Scrum avec une équipe mobile, ayant une grande partie des compétences nécessaires, et propose le recrutement d’un premier Product Owner, et d’un Scrum Master expérimenté. Dès les premiers jours, les membres de l’équipe apprennent à se connaître, à faire dès le début du pair programming ensemble. En quelques semaines, l’équipe démontre qu’elle peut livrer bien plus vite que les autres, et avec qualité grâce à un tech lead qui est à fond sur la Clean Architecture. C’est un succès !

Mais… Une frustration grandit dans l’équipe après quelques mois : cette équipe devient malgré tout extrêmement dépendante des équipes du site web pour le contenu, et doivent toujours attendre que des modules externes soient prêts…

Ce sont les équipes **A2 : “pleines d’espoirs mais enchevêtrées”**. Elles sont **optimisées pour des victoires rapides, et surtout pour éviter les conflits** : on évite de supprimer les dépendances, ça créerait des conflits avec les autres ! L’organisation n’est pas prête pour briser ces barrières… Malgré tout, les autres équipes de l’organisation s'essayent aussi à Scrum. Elles livrent plus vite, ont leur backlog produit avec leurs listes de fonctionnalités, recrutent des personnes ayant des compétences différentes et commencent à discuter avec leurs parties prenantes plus souvent !

Avec toutes ces frustrations, qui durent depuis des mois, l’équipe mobile en a marre, elle prend son indépendance : elle devient l’équipe “Androidator” ! Elle se crée sa propre CI/CD sans suivre les préconisations de l’organisation, avec le mantra “you build it, you maintain it”, tout est en feature flag, les traductions sont automatisées… Elle livre extraordinairement vite par rapport aux autres, avec des déploiements journaliers ! Ce sont les équipes **A3 : “fières et autonomes”**. Elles sont **optimisées pour être responsable de leurs fonctionnalités uniques et pour aller très vite !** 

Ce doux rêve peut avoir un prix avec ces équipes ultra-rapides qui n’ont pas la vue des utilisateurs finaux : ça devient “eux contre le reste” ! Car toutes ces équipes A2 ou A3 ont chacune leur backlog de fonctionnalité. Et malgré tout, le problème des dépendances reste… Et le delivery global de l’entreprise reste donc moyen. L’entreprise regarde le “temps de livraison d’un changement”, soit le temps entre le commit d’une partie d’un code et sa livraison, et celui-ci reste désespérément supérieur à 2 semaines (ou bien plus) pour la quasi totalité des équipes… Cela convient néanmoins au business : passer de plusieurs mois à quelques semaines, c’est déjà une victoire !

Quelques années passent. Et le marché de l’e-commerce devient de plus en plus difficile, de nombreuses start-up commencent à prendre des parts de marché. 

Le DSI cherche néanmoins à passer cette étape difficile : que faire pour créer une différence parmi tous ces compétiteurs ? Comment gérer nos dépendances qui augmentent de plus en plus ? Comment innover rapidement dans un marché aussi complexe ?

Il décide de faire appel à un ami, qui est CPO, soit Chief Product Officer, le responsable du Product Management et des Product Owners dans une scale-up en pleine croissance, et lui demande ce qu’ils font pour rester compétitifs et s’inspirer. Ils ont créé leur propre cadre de travail maison, inspiré de Spotify : chaque tribu est reliée à une partie du produit qui a chacun son propre parcours pour ses utilisateurs. Ils ont des tribus “paiement”, “livraison”, “commandes”, “inventaire”, “gestion client”, “catalogue des produits” et “authentification”... Liées à des parties claires de leur business ! Ce sont les équipes **B2 : “dépendantes liées à la valeur métier”**, **optimisées pour gérer les dépendances.**

Récemment, ils tentent une expérimentation sur une des tribus, celle de l’inventaire, ayant 3 équipes pluri-disciplinaires qui travaillaient déjà beaucoup ensembles. Désormais, il n’y aura plus qu’un seul backlog produit et un Product Manager dans cette tribu. Les équipes vont “tirer” le travail, et n’importe quelle équipe peut prendre un besoin dès qu’elle en a la capacité. Elles sont à 80% en distanciel, avec un outil de visioconférence gamifié mis en continue où les équipes vont et viennent pour discuter entre elles. Plutôt que de simples backlog, ce sont des problèmes à impact direct sur l’utilisateur, des objectifs en somme. Ce sont les équipes **B3 : “interdépendants liées à la valeur métier”, optimisées pour le contrôle du parcours client liées aux objectifs du business.**

Une équipe est à part, d’après le CPO. Après un moment d’ennui, les co-créateurs originels de la scale-up ont décidé de créer un nouveau produit innovant, et de s’y mettre à fond. Elle se crée ses propres objectifs, gère son budget elle-même, et n’a aucune dépendance à l’extérieure directe : uniquement via des APIs pour les quelques dépendances internes aux autres équipes. C’est l’équipe **C3 :** “**Développement holistique du produit**.”

## Pour se servir du modèle, créons des cartes !

À quoi cela peut-il servir de cartographier ?

Je me souviens que dans une formation de coaching d’équipe, notre mentor nous disait que le coach organisationnel est un cartographe : c’est exactement ce à quoi aide ce modèle, à cartographier ses équipes, ses services, son organisation entière. Je vous propose 3 manières de l’utiliser !

### L’auto-diagnostic : où sommes-nous et où allons-nous ?

La manière la plus simple et efficace de l’utiliser, c’est de laisser les équipes se diagnostiquer : 

* Quel est votre état actuel ?
* Vers où voulez-vous aller ?
* Quel est le premier petit pas pour aller vers ce changement ?
* Pour améliorer la pluri-disciplinarité : quels sont les prochains pair-programming que vous allez tester ? Comment prendre du temps pour vous former ?
* Pour améliorer le périmètre de travail : Qui sont nos utilisateurs ? Comment peut-on se réorganiser pour se concentrer sur nos utilisateurs ?

{{< figure  src="org-topo-auto-diagnostic.png" alt="Exemple d'auto-diagnostic" caption="En rouge, l’état initial des équipes, en vert où elles souhaitent arriver !" class="center-large">}}

### Modéliser les interactions

Une technique que j’utilise lors d’un accompagnement individuel ou d’une équipe, c’est de réaliser une socio-map : une carte des interactions sociales. Cela permet de faire prendre conscience des soucis d’interactions. : Prenez un service, un train SAFe, une tribe… Un système, donc ! Et mettez les différentes parties : un architecte solution qui travaille sur tout le produit ? Il est en C0. Il communique beaucoup avec une équipe API qui est en A3, et cette équipe API travaille avec une équipe back office en A2 et une équipe front-end en B2… Petit à petit, vous pourrez avoir une cartographie des interactions des équipes et des individus, ce qui vous permettra de voir des manques d’interactions à peut-être combler ?

{{< figure  src="org-topo-interactions.png" alt="Un potentiel exemple d’un système d’équipes IT et des principales interactions" caption="Un potentiel exemple d’un système d’équipes IT et des principales interactions" class="center-large">}}

### Décalage d’un membre dans une équipe

Lorsqu’on accompagne une équipe, certaines personnes peuvent se sentir en décalage, être isolées, ou être trop présentes. Modéliser cela permet aux équipes de se rendre compte du problème : une équipe B2 pourrait avoir un individu qui est en Y0, c’est-à-dire qu’il bosse seul sur des tâches sans s’intéresser à l’utilisateur… Ce serait une opportunité pour l’accompagner, peut-être ? C’est une autre manière d’observer les dysfonctionnements possibles.

On notera ainsi que ce modèle est fractal.

{{< figure  src="org-topo-zoom-équipe.png" alt="Observation des écarts dans une équipe de type B2" caption="Observation des écarts dans une équipe de type B2" class="center-large">}}

## Alors, Org Topologies : évolution ou révolution ?

Ce modèle permet d’aider à cartographier et à donner une vision sur deux axes clés de l’agilité : se rapprocher des besoins et du parcours des utilisateurs du produit, et s’améliorer ensemble pour étendre nos compétences pour fluidifier notre flux de valeur. Cela donne un objectif clair vers où aller : en haut à droite ! Il permet de manière simple d’aider à faire prendre conscience des changements à effectuer pour être plus efficient et rapide.

De plus, si vous souhaitez utiliser LeSS comme cadre de travail agile à l’échelle, Org Topologies s’intègre parfaitement dans le cadre pour aider à cette transformation ! Cela se voit d’ailleurs que les créateurs sont des fans de LeSS, et pourrait potentiellement relancer ce cadre de travail dans nos organisations.

Cependant, il y a quelques choix structurels qui semblent surprenants dans ce modèle. Les équipes C3 sont considérées par Org Topologies comme un idéal semblant impossible à atteindre. Les équipes C3, c’est comme si le produit était l’organisation : toutes les équipes ne forment qu’une seule et même équipe. Cela semble extrême et peut démotiver les équipes et organisation à même faire un premier pas vers cet idéal !

L’autre choix est de totalement s’abstraire de la technique, contrairement à Team Topologies qui prend en compte l’architecture technique comme cruciale dans l’organisation des équipes, là où Org Topologies n’en parle pas vraiment… Cela pourrait être vraiment intéressant de trouver une manière d’utiliser ce modèle et de le comparer à son architecture technique !

N’oublions pas que le modèle est très jeune, il sera sûrement amené à évoluer pour répondre à ces questions, et pour ça il faut l’essayer ! ;)

[^teamtopologies]: Team Topologies: Organizing Business and Technology Teams for Fast Flow, By Matthew Skelton and Manuel Pais, 2019, IT Revolution
