---
draft: false
title: "Agilité en famille : Voyages agiles"
authors:
  - thomas-clavier
values:
  - conviction
  - partage
tags:
  - rétrospective
  - agile
  - astuce
  - Agilité en famille
offers:
  - unlock-delivery
date: 2023-08-07T20:37:15.772Z
thumbnail: dunes.jpg
subtitle: La force du management visuel pour rendre autonomes les grands et les petits.
---
Cette série d'articles est un retour sur quelques histoires d’agiliste qui se sont déroulées chez moi durant ces dernières années. Ce nouvel épisode met en valeur la force du management visuel pour rendre autonome et favoriser l'entraide.
## Faire les valises en famille


Ça vous est déjà arrivé le premier soir des vacances, de chercher partout le doudou et de se rendre compte qu’il avait été oublié ?
Ben moi, oui...
Et comme j'aime pas quand mes enfants sont malheureux, j'ai cherché dans ma boîte à outils agile ce qui pourrait nous aider à résoudre cette situation.
Et c'est comme ça qu'on en est arrivés à utiliser le management visuel à la maison pour préparer nos valises avant les grands départs en vacances.
Je vous raconte.
Imaginez un grand tableau kanban avec 3 colonnes (À faire, En cours et Fini), quand je dis grand, c’est que les 3 colonnes sont les portes de la baie vitrée du jardin. Dans la première colonne de haut en bas, tous les éléments à mettre dans les valises, du plus important au moins important. On trouve par exemple vers le haut des éléments comme “les brosses à dents” et vers le bas des choses moins importantes comme “les pantoufles” ou “mon oreiller préféré”.

{{< figure  src="baie-vitree.jpg" alt="Ma fille Céleste devant la fameuse baie vitrée, mais sans les post-its." class="center-small" caption="Ma fille Céleste devant la fameuse baie vitrée, mais sans les post-its.">}}

Je vous passe les étapes de création, d’enrichissement puis de priorisation de ce backlog pour vous partager l’étape que je trouve la plus impressionnante, le moment ou une heure avant le départ, toute la maison s’active comme une ruche pour remplir les valises. Un des parents prend le rôle de rangeur, il va prendre ce que tout le monde apporte pour le ranger dans les valises, et les autres prennent les post’its dans l’ordre de priorité. Ça court dans la maison pour trouver les éléments en question, ça crie, ça rigole, et là, il se passe plusieurs choses fabuleuses :
* Les petits prennent tous seuls un post-it et demandent à ceux qui savent lire ce qu’il faut faire puis sont autonomes pour, par exemple, collecter les bottes en caoutchouc de toute la famille
* Chaque fois que quelqu’un finit un post’it, le premier réflexe c’est d’aller aider le reste de la famille sur les post-its en cours.
* Enfin une fois les valises pleines ou le temps écoulé, on s’arrête pour aller prendre le train. La fin de la liste est encore dans la colonne à faire et “la peluche pour faire rire les cousins” ou “les pantoufles en forme de pattes d’ours” restent dans la chambre.

Une contrainte pour que ça fonctionne, le “rangeur” doit être plus rapide que les collecteurs. Il va aussi devoir se souvenir des lieux de rangement pour pouvoir répondre aux questions comme : “Mais ils sont où, les pyjamas ?”

L'une des grandes forces du management visuel c'est de rendre autonomes l'ensemble de celles et ceux à qui il s’adresse.

## Préparer un voyage

{{< figure  src="board.jpg" alt="Vision partiel du board" class="center-small">}}

Collecter, trier, prioriser puis organiser transport, logement et quelques activités clés. Le tout en étant certains de faire plaisir à tout le monde.

Je ne sais pas vous, mais moi je n’aime pas les voyages organisés, encore moins quand c’est quelqu’un d’autre qui l’organise pour moi.

Il y a quelques années nous avons décidé de faire un voyage au Maroc : prendre l’avion pour les vacances c’est pas écologique mais ce n’est pas le sujet de cet article.  Madame avait très envie de visiter les villes impériales du nord du maroc, nous devions passer voir des amis à Casablanca et j’avais très envie d’aller dans le désert pour marcher dans les dunes. En plus nous partions avec 3 enfants en bas âge, il nous semblait important de réserver les hôtels à l’avance. Comment faire pour organiser les grandes lignes du voyage, en étant sûr de faire plaisir à tout le monde ?

La petite voix de l’agiliste me criait : **Rendre visible !** Alors j’ai pris des feuilles A4 et j’ai sorti les post-it. Une feuille A4 égale une journée, j’ai noté en titre les jours et j’ai étalé dans le salon tous les jours de notre futur voyage comme une grande fresque.
Puis j’ai déroulé l’atelier suivant :
* **Partager ses voeux** : Chacun écrit sur des post-its tout ce qu’il veut faire, 1 post-it par idée
* **Ordonnancer** : Chacun les priorise, pas d'ex-aequo, si je ne devais faire qu’une chose se serait quoi ?
* **Partager & regrouper** : À tour de rôle, on pose 1 post-it sur la fresque en l'expliquant, on essaye de regrouper au fur et à mesure les post-its par zone géographique en laissant  1 journée de voyage entre chaque point de chute
* **Identifier les actions à mener** : Une fois la totalité des jours remplis, on note les dates et les lieux des hôtels à trouver et réserver, des trains à prendre et les copains à appeler

On se répartit les tâches de réservation et les jours qui suivent voient apparaître les noms des hôtels, les horaires de trains et autres réservations. De temps en temps il faut prendre une décision : le guide n’est pas dispo le lundi mais le mardi on décale ? Pour prendre ces décisions, rien de tel que des points d’étapes réguliers, mais ça j’en parle dans un autre article sur les rétrospectives de famille.

Pour dérouler cet atelier il faut au préalable avoir compulsé les guides pour être capable de lister les choses que l’on veut vraiment faire. Madame aime beaucoup les guides papier comme le guide vert ou le guide michelin, de mon côté je prépare cette étape avec les forums du guide du routard.

Mais vous avez probablement très envie de savoir ce que nous avons fait durant ce voyage, alors je vous raconte.
Tout d’abord le périple :


{{< figure  src="periple.jpg" alt="Périple sur une carte" class="center-small">}}

Comme vous pouvez le voir, arrivée et départ de Casablanca. Le début du périple était consacré aux villes impériales du nord (Fès, Meknès, Moulay Idriss, le site gallo-romain de Volubilis et Rabat), puis retour à Casablanca pour profiter de nos amis et enfin, direction le désert du Sahara avec quelques visites sur la route : Marrakech, Tamegroute, Ouarzazate et enfin Zagora (les portes du désert) avant de faire quelques jours dans les dunes de Chegaga.

{{< figure  src="direction-volubilis.jpg" alt="À dos d'âne, direction Volubilis." class="center-small" caption="En chemin pour Volubilis avec notre guide.">}}

{{< figure  src="dromadaires.jpg" alt="Dans les dunes de Chegaga à dromadaires." class="center-small" caption="Balade dans les dunes de Chegaga à dos de dromadaires.">}}

C’était vraiment un super voyage et je recommande chaudement l’usage du train pour les déplacement locaux, c'est à mon avis le meilleurs moyen d'apprendre à connaître les habitants durant les trajets.

## Conclusion
Un manageur chez Toyota disait que le Lean se résume à 3 principes :
* faire simple
* rendre visible
* et faire confiance

Je trouve que ce format papier avec des post-its est justement très simple et permet d’avoir une visualisation rapide de l’ensemble des contraintes. J’ai reproduit plusieurs fois cet atelier avec toujours autant de succès et, cerise sur le gâteau, ça permet d’impliquer les enfants dans la préparation du voyage.

