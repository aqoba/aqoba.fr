---
draft: false
title: Êtes-vous LE bon manager pour transformer vos équipes ?
authors:
  - antoine-marcou
values:
  - partage
  - conviction
tags:
  - Culture manageriale
offers:
  - unlock-transformation
  - unlock-management
date: 2024-02-14T22:21:36.006Z
thumbnail: La-culture-manageriale-et-transfo.png
subtitle: Quelle est la culture managériale la mieux adaptée pour réussir une
  transformation d’organisation ?
---
Avec Valentine Ferreol, nous faisons face aux mêmes challenges lorsque nous rejoignons une nouvelle organisation. Valentine est Directeur des SI et du Numérique et intervient en management de transition. De mon côté, j’interviens en tant que consultant en transformation auprès d’entreprises de toutes tailles. Elles font appel à nous pour mener des projets de transformation opérationnelle et culturelle.


Lorsque nous intervenons, nos premiers interlocuteurs sont les membres de la direction de l’entreprise. Ce sont eux qui ont perçu la nécessité de transformer tout ou partie de l'entreprise. Et ces leaders incarnent une **culture managériale** propre. Cette culture, ce mode de leadership, c’est la clé de voûte de la transformation à venir.


Car **une transformation d’entreprise**, c’est un moment de **mise en tension des individus, des équipes et de l’organisation dans son ensemble**. Pendant une transformation, on se remet en cause, on appuie là où ça fait mal, on ne met aucun sujet sous le tapis et on met temporairement l’organisation en déséquilibre. C’est un épisode qui peut être stressant et où le management joue un rôle incontournable pour motiver les individus à adopter de nouvelles manières de travailler.


Après plus de 15 ans à mener des transformations pour des organisations de toutes tailles et dans des secteurs économiques variés, nous faisons tous les deux les mêmes constats : 






* d’abord, on ne démarre pas et on ne pilote pas une transformation de la même manière dans une entreprise de culture directive et dans une organisation de culture délégative
* ensuite, une direction incapable d’adapter son mode de leadership au cours de la transformation conduira celle-ci à l’échec




### La culture managériale dominante est le principal facteur clé de succès ou d’échec de transformation d’une entreprise


Attention : il n’existe pas un unique type de leader qui soit capable d’amorcer et de mener à bien une transformation. Oublions tout de suite l’idée d’un super-héros type capable de faire grandir une organisation de manière rapide et fluide.


Mais en tant que manager ou membre d’un groupe de managers, que votre périmètre soit petit ou très large, vous incarnez un certain type de leadership. Le connaître, savoir quel impact il a sur les équipes, identifier dans quel contexte il est efficace, et savoir le faire évoluer : ce sont des clés pour amorcer et réussir la transformation de l’édifice dont vous avez la responsabilité.




## 4 modes de management


Pour commencer, tentons de poser le cadre. Les études sur les différents types de culture managériale sont légions, depuis le début du XXème siècle. Nous vous proposons de nous inspirer du modèle posé par Ken H. Blanchard et Paul Hersey qui ont publié ensemble depuis les années 60 jusque dans les années 2000. Ensemble, ils ont théorisé 4 grands types de managements.


En tant que leader, ou que membre d’une équipe de leaders, vous vous retrouverez forcément dans un ou plusieurs des 4 comportements managériaux suivants : 


{{< figure  src="4-types-de-management.png" alt="4 comportements managériaux" class="center-large">}}


Dans chaque organisation, **l’un de ces comportements domine** et définit ce que l’on appelle la **culture managériale de l’entreprise**.


Alors, quel type de culture managériale se rapproche le plus de celui de votre organisation ? Et comment vous en servir pour réussir  à la transformer ?


## Management directif : “Je suis seul maître à bord”


{{< figure  src="directif.png" alt="Comportement directif" class="float-left-tiny">}}


Dans mon périmètre, il y a un chef et un seul. C’est lui (ou elle) qui prend les décisions et les communique via la voie hiérarchique.


Les personnes qui travaillent avec le manager sont là pour les appliquer. Elles sont évaluées sur leur loyauté, leur capacité à faire appliquer les décisions efficacement.


Le chef est généralement respecté et craint. D’autant plus qu’il n’hésite pas à brandir la menace de sanctions pour parvenir aux résultats qu’il souhaite.


Avouons-le : ce mode de management directif n’est pas en odeur de sainteté. Il est perçu comme à contre-courant de la culture de l’empathie et du bien-être au travail. Pourtant, il est particulièrement efficace en situation de crise, de chaos, de survie : il permet de prendre des décisions intuitives, rapides et suivies par toutes et tous.


> On n’a jamais vu un bateau dans la tempête se sauver grâce à l’action d’un comité


La communication se fait donc de manière top-down uniquement. Pour ces situations ou types de décisions, l’avis des membres des équipes ne compte, pour ainsi dire, pas.


Exemples de secteur d’activité où nous avons observé ce type de culture managériale : 


* Cyber-risque 
* Activités dans lesquelles la sécurité des personnes est en jeu comme le BTP
* Certaines start-ups dont le succès tourne autour du charisme du fondateur et sa capacité à prendre des décisions rapides


{{< figure  src="chirac-cheffer.png" alt="un chef c'est fait pour cheffer - J.Chirac" class="center-small">}}


Une culture managériale directive est-elle adaptée pour faire pivoter votre organisation et réussir sa transformation ?


* Oui, dans un certain sens, car le chef est seul à décider qu’il est temps de se transformer. Il donne le nouveau cap, pose le nouveau cadre et ordonne la mise en mouvement et le calendrier de transformation.
* Oui également, car les équipes peuvent se reposer sur leur capitaine qui prend la responsabilité de déstabiliser l’organisation existante. Si elles lui font confiance, chacune se met en action sur son périmètre pour permettre à l’organisation de pivoter efficacement.
* Mais le management directif a une faiblesse de taille, dans une situation de transformation : il rend difficile d’enraciner la transformation. Dans un cadre directif, une transformation n’est pas forcément efficace ni pérenne.


  * Pas forcément efficace parce que les équipes appliquent sans adhérer aux nouveaux modes de fonctionnement. Elles obéissent sans s’investir réellement, et donc sans chercher par elles-mêmes à optimiser réellement le système
  * Et pas forcément pérenne, car lorsque le chef s’en va, il n’est pas rare que les équipes reviennent à leurs anciennes pratiques. Et c’est logique : si j’applique un système que j’ai l’impression de ne pas avoir choisi, en cas de difficulté importante dans mon travail, je reviendrai par réflexe à mes pratiques anciennes, celles que je maîtrise le mieux et avec lesquelles je me suis construit professionnellement. C’est pourquoi les transformations d’organisation s’enchaînent souvent au rythme où  les managers se suivent dans de nombreuses entreprises


### Notre recommandation


Une culture managériale directive n’a généralement pas bonne presse. 

Néanmoins, si vous incarnez ce type de leadership ou qu’il s’agit de la culture managériale dominante dans votre organisation, tirez-en parti : servez-vous en pour enclencher la transformation des équipes avec efficacité. Cette posture vous aidera à fixer le cap et lancer la dynamique.


En revanche, ne vous y enfermez pas. Un manager uniquement directif, incapable de faire confiance ou de déléguer se cognera rapidement à un plafond de verre dans sa volonté de transformation : après avoir déclenché la démarche de changement, activez rapidement un type de leadership consultatif. Il sera indispensable pour :  


* faire adhérer les équipes, 
* les inviter à identifier les impacts sur leurs métiers, 
* pour définir et mettre en œuvre les nouvelles pratiques / rôles / outils. 


Elles pourront ainsi être pérennisées, car ancrées au plus près des expertises opérationnelles. Les bénéfices que vous tirerez de la transformation seront bien plus grands si vos collaborateurs se sentent autorisés à s’exprimer pour le bien du système. 


{{< figure  src="directif-consultatif.png" alt="Directif - Consultatif" class="center-large">}}


## Management persuasif ou consultatif : “je décide en m’appuyant sur les autres”


{{< figure  src="persuasif-consultatif.png" alt="Comportements persuasifs - consultatifs" class="float-left-tiny">}}


Dans une culture managériale persuasive ou consultative, le manager reste le chef. Mais il encourage ses collaborateurs à donner de la voix et être des relais efficaces : 


* Le manager persuasif montre la cible et cherche à motiver ses équipes pour l’atteindre,
* Le manager consultatif est même prêt à déléguer une partie de ses responsabilités à certains de ses collaborateurs les plus compétents et engagés.


Mais attention, dans une culture managériale persuasive ou délégative, le cadre hiérarchique reste strict. Simplement, contrairement à une culture directive qui tient par la crainte et la peur du bâton, le manager persuasif ou délégatif distribue récompenses et sanctions : il peut ainsi distinguer les membres des équipes qu’il souhaite valoriser ou, au contraire, avertir.


La communication est principalement top-down même si les membres des équipes sont encouragés à s’exprimer et donner leur avis :


* S’il consulte ses équipes, les objectifs de l’organisation sont bel et bien fixés par le manager qui reste le responsable : c’est lui/elle qui est exposé(e) aux conséquences des décisions prises.
* Par ailleurs, les équipes ne se sentent pas toujours libres d’aborder leur statut ou de donner un avis qui pourrait contredire la position de la direction.


Ces modes de management sont particulièrement utiles pour que l’entreprise garde son cap initial : le manager et son premier cercle sont les garants de l’esprit dans lequel le projet s’est monté, il incarne ses valeurs. Il permet une grande stabilité de l’organisation dans le temps.


Exemples de secteur d’activité où nous avons observé ce type de culture managériale : 


* Entreprises familiales où le clan familial est le seul à avoir un réel pouvoir de décision et reste le garant de la résilience de l’entreprise
* Certaines start-ups structurées autour d’un petit groupe de fondateurs, souvent très proches les uns des autres
* L’armée : si le chef du GIGN est celui/celle qui donne son GO à une opération, ce sont les membres de l’équipe opérationnelle qui sont en autonomie et doivent s’adapter à la situation qu’ils rencontrent. Le cadre de travail est structuré avec des process de référence, tout en laissant une place à l’expertise, l’expérience et au libre-arbitre des membres de l’équipe.


## Une culture managériale persuasive ou consultative est-elle adaptée pour faire pivoter votre organisation ?


Oui, car la transformation sera initiée avec force par le manager et bénéficiera de sa force d’entraînement. Mais elle s’appuiera aussi sur une dynamique plus collective car ce manager cherchera à impliquer, au moins en partie, les représentants des équipes. 


Cela a plusieurs implication : 


* La transformation démarrera moins rapidement, car :


  * d’une part, elle doit être d’abord expliquée, les équipes doivent y adhérer
  * d’autre part, les équipes chercheront à influer sur la cible : par exemple, au lieu de calquer un framework du marché, elles chercheront à construire leur propre modèle maison
* Cependant, son déploiement sera rapide car les équipes adhèrent et avancent dans le même sens
* Et la transformation donnera des résultats positifs et efficaces sur le terrain : en effet, la cible ayant été construite en consultant le terrain, elle a des chances de répondre aux difficultés concrètes des équipes, donc d’améliorer la performance de l’ensemble du système


Le risque : l’enracinement de la transformation n’est pas garanti. Dans un management persuasif ou délégatif, le manager reste celui/celle qui trace la voie. S’il quitte l’organisation ou décide de changer de priorité, c’est tout le collectif qui se détournera et la dynamique de transformation disparaîtra comme elle est apparue.


### Notre recommandation


A l’heure de transformer votre organisation, si vous incarnez une culture managériale persuasive ou consultative, servez-vous en pour enclencher la transformation. À vous de désigner la cible et de l’affirmer haut et fort : elle n’est pas négociable.


> La cible n’est pas négociable. La trajectoire, elle, est co-construite.


En revanche, une fois la dynamique lancée, identifiez rapidement des équipiers compétents et engagés capables de porter la dynamique à vos côtés : 
* Assumez qu’on prend un peu plus de temps pour construire la trajectoire de transformation
* Évitez que cette construction prenne trop de temps en vous dotant d’une mécanique de prise de décision et d’un tempo / calendrier pour chaque étape 
* Sachez revenir à un management plus directif lorsqu’il faut trancher ou réaffirmer la transformation comme un projet d’entreprise (éviter les retours arrières en cours de démarche)


Ainsi, le succès de la transformation ne reposera pas sur vos seules épaules. Sa dynamique ne reposera pas sur votre seule énergie. 


Vous pouvez par exemple décider de construire une [coalition de transformation](https://aqoba.fr/posts/20221005-une-coalition-pour-les-guider-tous/) composée de représentants des équipes et dégager 1 journée par semaine de leur temps pour piloter le déploiement de la transformation.


## Management participatif : “j’épaule mes équipes qui décident seules le plus souvent”


{{< figure  src="participatif.png" alt="Participatif" class="float-left-tiny">}}


Ce type de management[^type] se caractérise notamment par ce principe clé : toute difficulté doit être traitée au niveau où elle se situe afin d’optimiser les solutions trouvées et responsabiliser l’ensemble des membres de l’organisation.


Cela ne signifie pas l’absence de liens hiérarchiques dans l’organisation. Mais ce type de culture managériale fonctionne sur la base :


* d’une très grande confiance entre tous les collaborateurs, 
* d’une communication pluri-directionnelle et transparente,
* d’une culture non-feinte du droit à l’erreur.


Dans ce type de culture, le dirigeant use avec parcimonie de son pouvoir décisionnaire et se pose généralement en coordinateur des débats pour faire émerger la solution la plus adéquate de la part des équipes.


D’ailleurs, qu’il/elle soit absent(e) ou présent(e), le système fonctionne exactement de la même manière.


Cette culture managériale est particulièrement efficace dans une entreprise dont le projet est construit autour de l’innovation : les membres opérationnels ont l’autonomie suffisante pour lancer et industrialiser des solutions auxquelles une équipe managériale n’aurait pas pensé, car trop éloignée du terrain. Par exemple : 


* l’entreprise Buurtzorg (soin à domicile), aux Pays-Bas, fonctionne en petites équipes autonomes d’une dizaine de personnes, réparties par quartier : chacune gère son recrutement, son planning, les clients qu’elle accepte ou non, etc.
* les 20% de Google : les équipes d’ingénieurs décident de ce sur quoi elles travaillent le vendredi, chaque semaine
* on peut également observer ce type de dynamique pour adresser une problématique particulièrement complexe qui va nécessiter des expertises pointues réunies dans un collège d’experts mobilisés vers le même objectif : comprendre et résoudre cette problématique


Mais nous avons constaté qu’un des challenges d’un management participatif consiste à donner suffisamment de visibilité au reste de l’organisation concernant les décisions prises sur le terrain, alors que celles-ci peuvent avoir des impacts non-anticipés ou induire des dépendances sur d’autres pans de l’organisation.


## Une culture managériale participative est-elle adaptée pour faire pivoter votre organisation ?


Lorsque l’organisation repose sur un management participatif, on ne parle pas de faire pivoter l’ensemble de l’organisation : celle-ci est déjà sans cesse en mouvement. Les équipes opérationnelles sont autonomes et se reconfigurent de manière dynamique.

Ce mouvement continu et permanent s’inscrit dans une transformation incrémentale : il n’y a pas de changement du modèle initial. Le cadre de travail de chaque cellule et les règles de fonctionnement et des adhérences entre elles sont stables : cela pour éviter qu’elles ne se fassent concurrence les unes aux autres.



### Nos recommandations


* Vous vous trouvez déjà dans un modèle de management idéal pour une organisation en amélioration continue
* Mais n’oubliez pas d’assumer votre rôle en tant que manager / dirigeant :


   * fixer et communiquer un but commun qui permet à l’ensemble de l’organisation de s’aligner et d’avancer dans le même sens,
   * et faire respecter la culture de l’entreprise et ses règles du jeu. Elles sont la condition nécessaire à l’harmonie entre les différents périmètres de l’organisation. Lorsque ces règles du jeu sont menacées, assumez un retour par un court épisode de management directif : il est indispensable pour maintenir la confiance au sein de l’édifice.



{{< figure  src="blabla.png" alt="Echanges" class="center-small">}}


## En synthèse, qu’est-ce qu’on peut retenir de tout cela ?
* Qu’il n’existe pas de mauvais type de leadership pour transformer une organisation : un manager plutôt directif comme un manager plutôt délégatif peuvent y parvenir. Personne ne doit se censurer. Bien au contraire.
* En revanche, il est impossible de piloter efficacement et d’ancrer une transformation d’entreprise dans le temps si vous restez enfermé dans un type de management unique : 
   * restez purement directif et vous n’arriverez qu’à changer les choses en surface : une fois que vous relacherez la pression, les équipes rebasculeront dans leurs schémas traditionnels
   * restez purement délégatif et vous n’arriverez pas à maintenir une cohérence d’ensemble dans l’organisation : chacun poursuivra son propre but et les équipes finiront par ne plus savoir travailler les unes avec les autres


La clé réside dans le Leadership Situationnel, également théorisé par Ken H. Blanchard et Paul Hersey dont nous vous avons parlé plus haut dans cet article : **être capable d’adapter son type de management** aux personnes, aux équipes et aux situations de transformation que nous rencontrons


Et nous sommes toutes et tous capables d’y parvenir. Nous devons simplement être conscients de là où nous partons et de comment passer d’un mode de leadership à l’autre. Nous vous décrirons cela dans un article à venir, dédié au Leadership situationnel.




[^type]:  Type de management promu par R. Likert dans ses publications des années 60


