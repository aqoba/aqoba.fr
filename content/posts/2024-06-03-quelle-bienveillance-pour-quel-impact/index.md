---
draft: false
authors:
  - laurent-dussault
values:
  - conviction
tags:
  - management
  - bienveillance
offers:
  - unlock-management
date: 2024-06-03T09:15:00.000Z
thumbnail: bienveillance.jpg
carousels:
  "1":
    - Lencioni.jpg
    - Lencioni-2.jpg
    - Lencioni-3.jpg
    - Lencioni-4.jpg
    - Lencioni-5.jpg
    - Lencioni-6.jpg
    - Lencioni-7.jpg
title: Quelle bienveillance pour quel impact ?
subtitle: Bisounours ou sécurité psychologique
---
Vous avez sûrement déjà vu ces vidéos hilarantes qui se moquent de la bienveillance en entreprise. Prenez par exemple "Et tout le monde s'en fout #19 - La bienveillance" sur YouTube : 

{{< figure  src="tout_le_monde-la_bienveillance.png" alt="Et tout le monde s'en fout #19 - La bienveillance" link="https://youtu.be/pCBcuVitjKo?si" class="center-small">}}

Avec beaucoup de dérision, cette vidéo met en lumière les absurdités de cette fameuse bienveillance dont tout le monde parle mais que peu comprennent vraiment. Derrière les rires, il y a une réalité bien plus piquante.

Aujourd'hui, la bienveillance, c'est un peu comme la licorne du management : tout le monde en parle, mais personne ne sait vraiment à quoi ça ressemble. Selon le Larousse, la bienveillance, c'est "une disposition d'esprit inclinant à la compréhension, à l'indulgence envers autrui". Ça a l'air sympa sur le papier, mais est-ce vraiment ce dont nos équipes ont besoin ?

Le hic, c'est que cette version édulcorée de la bienveillance peut rapidement se transformer en une tyrannie douce, où l'envie de faire plaisir et d'éviter les vagues prend le dessus sur le besoin de poser des limites claires et de dire les choses comme elles sont. Cette bienveillance mal comprise devient alors un bouclier contre le changement, empêchant les débats nécessaires et maintenant le statu quo. Alors, comment pratiquer une véritable bienveillance sans tomber dans le piège de la complaisance ?

Dans cet article, on va creuser un peu : d'où vient vraiment ce mot de bienveillance ? Comment en est-on arrivé à cette version aseptisée qu'on connaît aujourd'hui ? Et surtout, comment peut-on remettre les pendules à l'heure pour que la bienveillance serve vraiment le bien collectif ? On va aussi jeter un œil au Triangle de Karpman pour comprendre comment la bienveillance mal utilisée peut alimenter des jeux psychologiques pas très sains.

{{< figure  src="meme_bienveillance.png" alt="meme Bienveillance" class="center-small">}}

## La Bienveillance : Origines et Étymologie

**Étymologie :**

Alors, d'où vient ce mot "bienveillance" qui semble si gentil au premier abord ? Surprise ! Contrairement à ce qu'on pourrait penser, bienveillance ne vient pas de benevolentia (qui signifie littéralement "vouloir le bien"). Non, non, non. En fait, le mot vient de bona vigilantia, qui signifie "bonne vigilance". Oui, vous avez bien lu, il s'agit bien de vigilance, comme dans "être attentif" ou "garder un œil sur".

{{< figure  src="les_mots_eetr_bienveillance.png" alt="Bienveillance Eric et Razmi" class="center-small">}}

**Déviation sémantique :**

Mais comment a-t-on pu passer de "vigilance" à cette version sucrée de la bienveillance qu'on connaît aujourd'hui ? C'est ce qu'on appelle une déviation sémantique. Au fil du temps, le sens original de "veiller attentivement" s'est transformé en quelque chose de plus doux, de plus gentil, de plus... indulgent. On a troqué notre chapeau de gardien vigilant pour une casquette de bisounours. Et voilà comment la bonne vigilance s'est vue reléguée au rang de simple gentillesse.

**Retour aux sources :**

Revenir à l'étymologie, c'est un peu comme faire un voyage dans le temps. C'est se rappeler que veiller au bien, ce n'est pas seulement être gentil et compréhensif. C'est aussi savoir poser des limites, recadrer quand c'est nécessaire, et garder un œil vigilant sur ce qui se passe autour de nous. La véritable bienveillance, c'est être un gardien du bien. Ce n'est pas toujours agréable, ni pour celui qui recadre, ni pour celui qui est recadré, mais c'est essentiel pour le bien collectif.

## 2. La Bienveillance Moderne : Une Nouvelle Tyrannie ?

**Bouclier de l'immobilisme**

Bien que la bienveillance soit généralement présentée comme une vertu louable, elle peut parfois devenir un bouclier contre le changement et la confrontation nécessaire. Dans de nombreuses organisations, la bienveillance est souvent invoquée pour éviter les discussions difficiles ou les remises en question. On préfère alors se réfugier derrière un voile de gentillesse plutôt que d'affronter les défis de front.

**Faire le bien vs. Faire plaisir**

C'est là que réside le piège de la bienveillance moderne. On confond trop souvent le fait de "faire le bien" avec celui de "faire plaisir" ou de "ne pas contrarier". On cherche à éviter les vagues à tout prix, même si cela signifie ignorer des problèmes importants ou maintenir un statu quo néfaste. Comme le souligne Julia de Funès, "Il ne faut pas confondre bienveillance et complaisance."

**Une tyrannie douce**

Cette version édulcorée de la bienveillance peut rapidement se transformer en une tyrannie douce. Sous couvert de gentillesse, on étouffe les voix discordantes et on empêche tout véritable débat. Les critiques constructives sont étouffées au nom de la bienveillance, et les problèmes sont balayés sous le tapis pour ne froisser personne. Cette tyrannie sympathique maintient une apparence lisse en surface, mais engendre de la stagnation et de la frustration en profondeur.

**L'illusion du consensus**

De plus, cette bienveillance mal comprise crée une illusion trompeuse de consensus. Comme personne n'ose vraiment exprimer ses désaccords, on en vient à croire que tout le monde est d'accord. Pourtant, ce prétendu consensus n'est qu'une façade fragile, qui s'effondrera dès que quelqu'un osera briser le silence. Une véritable bienveillance doit encourager un dialogue ouvert et honnête, plutôt que d'étouffer les voix divergentes.

En définitive, la bienveillance moderne, lorsqu'elle est mal interprétée, peut devenir une nouvelle forme de tyrannie. Une tyrannie douce, certes, mais une tyrannie néanmoins. Il est donc crucial de retrouver l'essence véritable de la bienveillance, celle d'une vigilance attentive au bien collectif, plutôt que d'une complaisance aveugle.

## 3. Veiller au Bien : Le Rôle du Gardien du Cadre

Être bienveillant, ce n'est pas seulement sourire et dire "oui" à tout va. Non, non, non ! La véritable bienveillance implique parfois de jouer les rabat-joie et de remettre les pendules à l'heure. Bienvenue dans le rôle ingrat mais ô combien crucial du "Gardien du Cadre".

**Gardien du cadre**

Imaginez la scène : vous êtes dans une réunion d'équipe où tout le monde papillonne joyeusement. On plaisante, on rit, on dévie du sujet principal. Une ambiance détendue, presque trop détendue... Soudain, vous réalisez que le temps file et qu'on n'a rien avancé sur les points importants à l'ordre du jour. C'est le moment de jouer les Gardiens du Cadre !

En bon Gardien, votre rôle est de recadrer la situation, de rappeler les règles du jeu à vos joyeux lurons d'équipiers. "Ok les gars, c'était sympa cette petite récré, mais revenons au sujet si on veut avancer !" Bien que nécessaire, ce recadrage n'est jamais agréable à entendre. Vous devenez l'emmerdeur de service, le rabat-joie officiel. Mais c'est un mal pour un bien !

**Conséquences de l'absence de recadrage**

Car que se passerait-il si personne ne jouait ce rôle ingrat ? Un joyeux bordel, voilà ce qui arriverait ! Sans Gardien pour veiller au respect du cadre, les réunions deviendraient d'interminables fiestas sans queue ni tête. Les projets s'éterniseraient à l'infini, les deadlines seraient systématiquement ratées. Un véritable gâchis de temps et d'énergie, le tout sous une apparence de bonne humeur.

C'est un peu comme si une société décidait de bonnes règles pour son bien-être collectif, mais qu'ensuite personne ne veillait à leur application. À quoi bon avoir un code de la route si personne ne verbalise les excès de vitesse ? La bienveillance exige parfois d'être le méchant de service pour que les choses avancent.

**Bienveillance vs complaisance**

Bien sûr, il ne s'agit pas d'être un tyran insensible qui passe son temps à crier sur les autres. Non, le Gardien du Cadre doit savoir allier fermeté et empathie. Recadrer, oui, mais avec bienveillance et dans un esprit de progrès collectif. C'est la différence entre la bienveillance véritable et la simple complaisance.

La complaisance, c'est laisser tout le monde faire n'importe quoi par pure indulgence. La bienveillance, c'est parfois devoir dire "non" pour mieux avancer ensemble. Comme le dit si bien Julia de Funès : "La bienveillance n'est pas l'indulgence systématique, c'est savoir dire non quand c'est nécessaire."

Alors la prochaine fois que vous serez tenté de laisser filer pour avoir la paix, rappelez-vous : un peu de Gardien du Cadre, c'est parfois la dose de bienveillance dont votre équipe a vraiment besoin !

> "Let the police do the job" - Commissaire Bialès

{{< figure  src="polemiques-sur-nice-laissons-les-services-faire-leur-travail-demande-benoist-apparu.gif" alt="Commissaire Biales - La Cité de la Peur" class="center-small">}}

## 4. Bienveillance et Équipes Performantes

Lorsqu'on parle de bienveillance au sein des équipes, on a souvent tendance à penser qu'elle rime avec indulgence et laisser-aller. Pourtant, rien n'est plus éloigné de la vérité ! Une véritable bienveillance est en fait essentielle pour construire des équipes performantes et soudées.

**Indulgence vs. Performance**

Commençons par dissiper un mythe tenace : l'indulgence excessive n'a rien à voir avec la bienveillance. Être constamment dans le laxisme et le "tout passe" n'aide personne à long terme. Au contraire, cela crée un environnement mou où les standards s'effritent et où la médiocrité devient la norme.

Une équipe performante a besoin de challenges, de remises en question constructives et d'une saine pression pour se dépasser. La bienveillance n'est pas l'ennemi de l'excellence, elle en est un catalyseur.

**Le Modèle de Tuckman**

Prenons l'exemple du modèle de développement des équipes de Tuckman. Selon ce modèle, toute équipe passe par quatre phases : Forming (formation), Storming (confrontation), Norming (normalisation) et enfin Performing (performance).

C'est justement pendant la phase de Storming, celle des conflits et des remises en question, qu'une bienveillance mal comprise peut faire le plus de dégâts. Si on cherche à étouffer tous les désaccords au nom d'une fausse bienveillance, on empêche l'équipe d'avancer vers les phases suivantes de normalisation et de haute performance.

Une véritable bienveillance reconnaît que les conflits font partie intégrante du développement d'une équipe saine. Elle permet d'aborder ces conflits de manière constructive, en gardant à l'esprit le bien collectif.

**Les Dysfonctionnements selon Lencioni**

De même, les 5 dysfonctionnements d'une équipe décrits par Patrick Lencioni (manque de confiance, peur du conflit, manque d'implication, évitement de la responsabilité, inattention aux résultats) peuvent tous être exacerbés par une mauvaise interprétation de la bienveillance.

Imaginez une équipe où, par bienveillance mal placée, on évite soigneusement tout conflit ouvert. Les problèmes et les tensions s'accumuleraient alors en silence, minant la confiance et l'engagement de chacun.

Au contraire, une bienveillance authentique favorise un dialogue franc et honnête, permettant de résoudre les problèmes à la source. Elle crée un espace sûr où les gens peuvent s'exprimer sans crainte, dans un esprit de progrès collectif.

{{< carousel duration="5000" ordinal="1">}}


En définitive, loin d'être un frein à la performance, la bienveillance bien comprise en est un puissant accélérateur. Elle permet de relever les défis avec lucidité et courage, tout en préservant la cohésion et le respect mutuel au sein de l'équipe.

## 5. Le Triangle de Karpman et le Bouclier de la Bienveillance

Une des façons les plus insidieuses dont la bienveillance peut être détournée est à travers le célèbre "Triangle Dramatique" de Karpman. Ce modèle décrit comment les individus peuvent se retrouver pris dans des jeux psychologiques malsains, en endossant l'un des trois rôles suivants : la Victime, le Persécuteur ou le Sauveur.

**Introduction au Triangle de Karpman**

Le Triangle de Karpman illustre comment ces trois rôles interagissent de manière toxique.

La Victime se plaint et rejette la responsabilité, le Persécuteur blâme et critique, tandis que le Sauveur tente de résoudre les problèmes des autres, souvent au détriment de ses propres besoins.

{{< figure  src="process-com-stress-karp.png" alt="Triangle de Karpman : Persécuteur - Sauveur - Victime" class="center-small">}}

Ces rôles sont fluides, les gens pouvant passer de l'un à l'autre selon la situation. Mais le piège est que plus on reste coincé dans ce triangle, plus les comportements dysfonctionnels s'enracinent.

**Bienveillance et Rôle de Sauveur**

C'est là qu'intervient la bienveillance mal interprétée. Sous couvert de gentillesse, on peut facilement se retrouver à endosser le rôle de Sauveur, cherchant constamment à "protéger" les autres de tout conflit ou malaise.

On évite les discussions difficiles, on minimise les problèmes, on fait passer les besoins des autres avant les siens. Une attitude apparemment bienveillante, mais qui en réalité alimente un cercle vicieux de dépendance et d'irresponsabilité.

**Bienveillance et Rôle de Victime**

De même, certains n'hésitent pas à se draper dans le manteau de la Victime, utilisant la bienveillance comme un bouclier contre toute remise en question. "Comment peux-tu me critiquer, je ne fais que vouloir être bienveillant !"

En jouant ainsi la carte de la Victime incomprise, on évite d'avoir à prendre ses responsabilités ou à remettre en question ses comportements.

**Bienveillance et Rôle de Persécuteur**

Mais la bienveillance peut aussi cacher un Persécuteur, une forme de contrôle déguisée sous une apparence de sollicitude. "Je ne fais que vouloir ton bien, c'est pour ça que je te dis ce que tu dois faire."

Derrière un masque de bienveillance se cache alors une tentative d'imposer sa propre vision des choses, de dicter ce qui est "bien" pour les autres.

**Rompre le Triangle par une Bienveillance Authentique**

Pour sortir de ce triangle toxique, la clé est de pratiquer une bienveillance authentique, qui n'a pas peur d'affronter les conflits de manière saine et constructive.

Une bienveillance qui encourage la responsabilité personnelle plutôt que la victimisation. Qui pose des limites claires plutôt que de sauver les autres à ses propres dépens. Et qui accepte les différences d'opinion sans chercher à persécuter ou contrôler.

En somme, une bienveillance qui vise le bien collectif à long terme, plutôt que des solutions de facilité à court terme. Une bienveillance vigilante, qui n'hésite pas à secouer un peu les choses quand c'est nécessaire.

## Conclusion

Au terme de cet examen, une chose est claire : la bienveillance n'est pas ce petit nuage rose et doux qu'on nous vend trop souvent. Non, la véritable bienveillance a des crocs, elle a du mordant ! C'est une attitude de vigilance attentive, prête à recadrer quand c'est nécessaire pour le bien collectif.

Revenir aux sources étymologiques du mot "bienveillance" nous rappelle ses racines de "bonne vigilance". Être bienveillant, ce n'est pas simplement être gentil et faire plaisir à tout va. C'est savoir poser des limites claires, affronter les conflits de manière constructive, et garder un œil vigilant sur ce qui se passe autour de nous.

Trop souvent, on tombe dans le piège d'une bienveillance dévoyée, devenue un bouclier contre le changement et la confrontation pourtant nécessaires. On se réfugie derrière une façade de gentillesse pour éviter d'affronter les défis de front. Une tyrannie douce s'installe alors, étouffant les voix discordantes au nom de la bienveillance.

Pour la forme, bien entendu, il ne faudra pas confondre sincérité et fermeté avec violence et agression. A ce sujet je vous renvoie vers notre article sur [la Communication Non Violente](../20230828-communication-nonviolente-en-entreprise-la-clé-pour-des-relations-harmonieuses-et-une-collaboration-efficace/).

Toutefois, il est grand temps de remettre les pendules à l'heure ! Une équipe performante a besoin de challenges et de remises en question constructives, pas d'indulgence excessive. Un véritable gardien du cadre doit savoir allier fermeté et empathie pour faire avancer les choses. Et surtout, ne tombons pas dans les jeux psychologiques malsains du Triangle de Karpman, où la bienveillance devient un prétexte pour la victimisation, le contrôle ou le déni de responsabilité.

Alors oui, pratiquons la bienveillance, mais une bienveillance authentique ! Une bienveillance qui n'a pas peur d'affronter les conflits et les défis, dans un esprit de progrès collectif. Une bienveillance vigilante, qui sait dire "non" quand c'est nécessaire, pour mieux avancer ensemble vers un bien commun durable.

La route sera parfois chaotique, les discussions animées. Mais c'est le prix à payer pour une véritable bienveillance, celle qui vise l'excellence plutôt que la simple complaisance. Alors remettons la bienveillance sur ses rails, et avançons d'un pas vigilant : Soyons bienveillant bordel !

## Références

**Étymologie :**

"Dictionnaire étymologique de la langue françoise", par B. de Roquefort (1829)  \
Bienveillance ne vient pas de benevolentia mais bien de bona vigilantia, la bonne vigilance ou le fait de bien veiller. Même racine que veille, vigie, vigilance etc. Le sens de soin attentif, d'attention soigneuse qui fait veiller (veillée des morts, veillée des gardes, des heures de nuit, etc). Cette fausse étymologie a ainsi permis un détournement du sens qui aujourd'hui est synonyme de gentillesse, alors qu'il s'agit plutôt d'exercer une bonne vigilance, d'être attentif.

**Modèle de développement des équipes :**

Tuckman, B.W. (1965). Developmental sequence in small groups.

**Les 5 dysfonctionnements d'une équipe :**

Lencioni, P. (2002). The Five Dysfunctions of a Team.

**Le Triangle Dramatique de Karpman :**

Karpman, S. (1968). Fairy tales and script drama analysis. Transactional Analysis Bulletin

**Autres sources :**

De Funès, J. (2019). La Bienveillance n'est pas l'indulgence systématique. Harvard Business Review France.

Larousse - Définition de "bienveillance" : nom féminin, Disposition d'esprit inclinant à la compréhension, à l'indulgence envers autrui
