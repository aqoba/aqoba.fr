---
draft: true
authors:
  - thomas-clavier
  - marion-bialecki
values:
  - partage
  - conviction
date: 2023-02-01T16:15:11.994Z
thumbnail: Marion.png
carousels:
  "1":
    - welcome-marion_1.jpg
    - welcome-marion_2.jpg
    - welcome-marion_3.jpg
    - welcome-marion_4.jpg
    - welcome-marion_5.jpg
  "2":
    - welcome-marion_2.jpg
    - welcome-marion_4.jpg
    - welcome-marion_1.jpg
title: Bienvenue Marion !
tags:
  - équipe
subtitle: aqoba fait +1 !
---
aqoba fait plus 1 !!

1 an de plus, car très bientôt nous fêterons le premier anniversaire de l'équipe.

Mais surtout +1 dans l’équipe car nous accueillons aujourd’hui Marion Bialecki qui nous rejoint comme #ScrumMaster pour accompagner les transformations de nos clients.

Bienvenue Marion !
Nous sommes très heureux de te compter parmi nous.

{{< carousel duration="5000" ordinal="1">}}

{{< carousel duration="3000" ordinal="2">}}