---
draft: false
title: Nos valeurs
authors:
  - arnaud-bracchetti
values:
  - altruisme
  - conviction
  - humanite
  - partage
  - transparence
tags:
  - Nos valeurs
  - valeurs
date: 2022-07-05T07:25:57.801Z
thumbnail: eyes-g77b3d248f_640.jpg
subtitle: Comment les valeurs guident notre façon d'être
---
Pour nous la notion de valeur est extrêmement importante. Dès les prémices de la création d’aqoba, nous avons passé beaucoup de temps sur l’identification et la définition de celles-ci. Nous ne voulions pas en faire un élément marketing, mais un guide, un outil qui pourrait permettre à chaque aqobien de bien comprendre ce qu’on attend de lui, et ainsi de le guider dans ses choix et ses prises de décisions. 
 
Nous souhaitons décrire ici chacune de ces valeurs : nous nous adressons à toutes celles et ceux avec lesquels nous aurions l'occasion de collaborer dans un futur proche ou lointain. Car ces 5 valeurs,  **d’Humanité, de Transparence, de Conviction, d’Altruisme et de Partage**, ne sont pas simplement des mots : elles guident toutes nos actions et nos comportements en interne et avec nos clients.

# Humanité

Malgré la tendance forte à la digitalisation de ces deux dernières décennies, les organisations ne peuvent se passer des femmes et des hommes qui les composent. De l’engagement et de la motivation de ces personnes va dépendre une grande partie de la performance de ces entreprises. 

Aujourd’hui, les nouvelles générations ne trouvent plus leur motivation de la même manière que leurs parents. Là où, pour nos parents, les entreprises pouvaient retenir leurs employés uniquement sur la base d’une promesse de sécurité et un salaire attractif, ceci n’est plus suffisant. Fort des changements culturels et sociétaux de ces dernières décennies, l’appartenance à un groupe, l'autonomie, et le sens du travail fourni sont des éléments clés de motivation au sein des entreprises. Nous ne voulons plus perdre notre vie à la gagner.

En interne, tout est mis en œuvre pour permettre à chaque aqobien de travailler dans le respect de ses valeurs et de se constituer une identité de groupe forte, qui lui permettra de proposer et d’agir pour le bien de la structure. 

Chez nos clients, c’est penser nos missions non pas comme l'accompagnement d’une structure, mais plutôt des personnes qui la composent.

# Transparence

Chez aqoba, la transparence c’est la capacité de chacun de pouvoir collaborer de façon honnête et sans arrière pensée avec ses différents interlocuteurs. C’est dire clairement les choses, même si elles ne sont pas à notre avantage, du moment qu’elles permettent de faire avancer la réflexion autour du problème que nous cherchons à résoudre ensemble. En ce sens, la transparence est la base de la collaboration.

En interne, la transparence appelle la tolérance, la bienveillance et le droit à l’erreur. Pour que la transparence puisse être réellement vécue chez aqoba, il est indispensable que chacun d’entre nous se sente en sécurité dans l’environnement au sein duquel il évolue.

En mission, nous nous considérons comme des partenaires de nos clients, et non des fournisseurs. Nous sommes donc transparents avec eux sur ce qui fonctionne ou ne fonctionne pas. Nous attendons de leur part la même transparence quant à notre travail. C'est elle qui nous fait grandir et assure le succès de nos missions.

# Conviction 

Nous attendons de chaque aqobien qu'il vienne renforcer l'équipe avec ses propres convictions, des convictions fortes et sincères qu'il / elle a construit au fil de ses expériences. Elles nous permettent de nous questionner et d’apporter des solutions claires et cohérentes aux problèmes que nous rencontrons.

En interne, les convictions portées par chaque aqobien sont une source d’échange et de challenge lors de nos rencontres. Elles nous permettent de nous affirmer et nous obligent à construire des argumentaires et un discours clair pour défendre nos points de vue tout en écoutant les autres. 

Chez nos clients, les convictions nous servent de guide pour résoudre de façon claire et cohérente les problématiques auxquelles nous sommes confrontés. En apportant des réponses dans lesquelles nous croyons réellement, nos convictions nos permettent de faire adhérer le plus grand nombre aux solutions que nous pouvons .

Avoir des convictions ne signifie pas avoir une vision unique des réponses à apporter à un problème, mais plutôt croire réellement à ce que nous faisons. C’est aussi, écouter et challenger ceux qui nous entourent pour élargir et faire évoluer nos points de vue.

# Altruisme

Chez aqoba, l’altruisme, c’est la volonté de ne pas tourner toutes nos décisions uniquement vers le profit. C’est la capacité que chacun doit avoir, à collaborer avec le plus grand nombre, de façon désintéressée, pour faire avancer nos idées et nos convictions. 

En interne, c’est la transparence nécessaire sur la situation de l’entreprise, pour que chaque aqobien puisse juger par lui-même de la capacité de l’entreprise à se lancer dans des actions désintéressées qui porteront nos valeurs et nos convictions. C’est aussi la possibilité de solliciter n’importe quel aqobien pour répondre un problème complexe sans que la question de la facturation ne soit un frein.

Chez nos clients, c’est la volonté de chaque aqobien d’aider son client sans objectif direct de profit, simplement parce que nous sommes alignés sur une vision et des valeurs communes. Ceci permet d’apporter honnêtement, les meilleures réponses aux problématiques de nos clients, en créant un véritable esprit de collaboration.

Bien évidemment, l’altruisme ne peut pas être notre seul mode de fonctionnement, mais nous sommes tous convaincus qu’affirmer régulièrement nos convictions de façon altruiste ne peut être que profitable à nos clients sur le court terme, et à aqoba sur le moyen terme. 

# Partage

La connaissance est un bien que l’on peut donner sans avoir à s’en séparer. Chez aqoba nous souhaitons pouvoir organiser le plus largement possible le partage des savoirs et des expériences. En interne, avec nos clients et avec nos pairs.

En interne, cela signifie que chaque aqobien doit avoir du temps pour partager son savoir et s'ouvrir à de nouveaux apprentissages . Il doit participer activement aux échanges, pour apporter son point de vue et son expérience aux autres membres du groupe. L’ouverture à d’autres points de vue est un élément essentiel à ce processus de partage,  en permettant à chacun, quelle que soit son expérience, de participer et d’enrichir les échanges.

Pour nos clients, c’est la garantie de trouver dans chaque aqobien une personne qui se questionne et qui apprend en continu. Pour aqoba, le partage passe aussi par la diffusion à nos clients de tous nos savoir-faire afin de les rendre autonomes dans leur transformation.
