---
draft: false
title: Ils ne veulent pas revenir sur site ? Mes 5 clés pour réussir le télétravail
authors:
  - thomas-clavier
values:
  - partage
  - conviction
  - altruisme
tags:
  - remote
  - teletravail
  - rex
  - succès
date: 2022-09-27T12:54:25.210Z
thumbnail: travel_backpack_detail_14.jpg
subtitle: Une recette qui fonctionnent pour de vraie.
aliases:
  - /posts/20220927-ils-ne-veulent-pas-revenir-sur-site-mes-5-clés-pour-réussir-le-télétravail/
---
Depuis 2 ans, j'ai lu des dizaines d'articles consacrés au télétravail qui, pour beaucoup, a révolutionné notre manière de vivre dans l'entreprise. Mais voilà qu'aujourd'hui, alors que le premier confinement COVID a été ordonné il y a plus de 2 ans, les avis se polarisent : il y a celles et ceux pour qui il est impensable qu'on les oblige à revenir en présentiel. Et les autres qui décident de sonner la fin de la récré, et de ramener tout le monde sur site.

Le télétravail fait partie de ma vie depuis 15 ans. Je voudrais donc partager une voie qui permet à chaque organisation de trouver un équilibre autour du télétravail..

# Un peu de contexte

Pour commencer quelques éléments sur mon passé de télétravailleur : 

* J'ai pratiqué avec beaucoup de plaisir le télétravail durant mes années Argia, en tant que manager, pendant 4 ans de 2007 à 2011. 4 ans exclusivement en télétravail,
* Convaincu par le logiciel libre, je participe à plusieurs communautés de logiciels libres qui sont systématiquement réparties autour de la planète.
* Enfin chez aqoba, nous sommes 5 en télétravail total.

Pour autant, je reste convaincu qu’être dans la même pièce apporte de très nombreux avantages :

* Il est possible de partager très facilement des messages à tout le monde sans avoir besoin d’organiser une réunion.
* Il est possible d’entendre des échanges entre 2 personnes et d’intervenir pour rétablir une forme de vérité.
* Il est possible de voir une personne débordée et lui venir en aide avant qu’il ne soit trop tard.
* Il est facile de s'asseoir à côté d’un membre de l’équipe pour l’aider à résoudre un problème.
* Il est facile d’avoir des échanges informels, par exemple lancer une discussion sur le transport maritime des bananes à cause d’une simple odeur dans le bureau.
* Il est facile de lever la tête et de demander de l’aide.
* Il est facile de bougonner face à un problème et de provoquer inconsciemment une proposition d’aide d’un autre membre de l’équipe.
* Il est très facile de passer de petits groupes à un grand groupe et réciproquement.

La question qui se pose c’est : **comment atteindre ces propriétés à distance ?**

# 5 règles

Je vous partage les règles que j'ai vécues et appliquées depuis ces 4 ans en tant que manager d’une équipe en “full remote”. Bien entendu, je ne vous partage pas les outils de l'époque mais leur équivalent moderne.

## Règle numéro 0 : Choisir des outils adaptés

Faire du télétravail sans outil adapté c’est compliqué. Autant en présentiel il est facile de se débrouiller avec du papier et des feutres, autant à distance, ça ne passe pas. Il faut donc : 

* Une messagerie instantanée écrite avec des systèmes de notification performant, des bots et des canaux dynamiques. Pour ça je vous conseille element.io, slack ou XMPP.
* Un outil de communication audio pour faire :
  * des échanges 2 à 2
  * des échanges en groupe avec la capacité de changer la composition des groupes très rapidement comme faire des sous-groupes, les regrouper, re-découper différemment, etc. 
  * L’outil de communication audio doit aussi avoir la capacité de proposer une attente active. J’entends par là, être à l’écoute du groupe et pouvoir être interpellé par quelqu’un d’autre sans avoir rien à faire de plus pour répondre que de parler. Pour ça, je vous conseille mumble, element.io, workadventu.re, gathertown ou discord.
* Des outils permettant de travailler à plusieurs en même temps sur la même chose comme un document partagé ou du code partagé. Pour ça, je vous conseille etherpad, google doc, nextcloud, office 365, code with me, live share, klaxoon, ou miro.
* Du bon matériel : 
  * Un casque avec un micro de qualité, j’ai un jabra avec réduction de bruit et j’en suis très content.
  * Un micro et caméra d’ambiance pour le mode hybride, pour ça c’est une enseinte micro anker avec une bonne webcam.
  * Et une chaise de bureau confortable.

Soulignons que les entreprises qui pratiquent le télétravail fournissent dans un kit de bienvenue à la fois les licences et tout le matériel necessaire au télétravailleur.

## Règle numéro 1 : Ritualiser de très nombreux points

À distance, il y a tout un tas d’échanges qui n'auront pas lieu, l’échange informel, détecter une personne débordée, détecter une personne qui a besoin d’aide et qui n’ose pas le dire. Pour tout ça, il est indispensable de ritualiser fréquemment des moments d’échanges pour détecter des signaux faibles chez les membres de l'équipe. Alors oui, ça va multiplier les moments les réunions managers / managés mais c’est le prix à payer pour prendre soin de l’équipe.
Chez aqoba nous avons par exemple un moment de café informel tous les matins ou chacun apporte son mug. Chez Argia, nous avions ritualisés les points suivants : 

* Une fois par semaine
  * un tête à tête avec le manageur sur ce qui va et ne va pas hors projets.
  * la rétrospective
  * la démo
* Une fois par jour :
  * Le daily
  * une demi journée de pairing, mais nous faisions souvent plus.
  * Les pauses cafés étaient ritualisés aussi
  * En fin de journée, nous avions aussi le chaudoudou : imaginez un tête à tête aléatoire mais planifié de 5 à 10 minutes avec un autre membre de l’entreprise pour se remercier mutuellement.

## Règle numéro 2 : Passer du tout synchrone à un mode hybride

Partager par écrit c’est perdre une grande part de spontanéité. Écrire, c’est 20 fois plus lent que parler et je n’ai rien trouvé pour remplacer efficacement le “Je lève la tête et je dis : j’ai besoin de quelques secondes de votre attention … ” J’attends que tout le monde soit attentif et je peux faire mon annonce. Seulement en télétravail, cette spontanéité n’est pas possible, une partie de la communication orale doit donc être remplacée par sa version écrite.

La première étape consiste à définir les informations qui doivent rester synchrones (orales) et celles qui au contraire vont passer en mode asynchrones (écrites).

### À propos d'asynchrone

Avoir des échanges asynchrones implique de les avoir par écrit ou par messages enregistrés. L’Inconvénient majeur de ce mode de communication c’est qu’il est lent, les mots doivent être choisis, la vidéo pré-enregistrée doit être préparée, en bref, il y a des choses à rédiger. 

* Avantage c’est facilement archivé et indexé. Il est donc très simple de s’y référer a posteriori. 
* Cependant, notez que si la majorité des échanges se fait en asynchrone, le retour au synchrone risque d’être difficile. Les horaires de travail décalés sont un acquis très difficile à supprimer.

### Les communautés du libre : le tout asynchrone

Dans le monde du logiciel libre, les échanges sont tous asynchrones. Chaque discussion commence par un échange écrit et public. Au cours de l'échange certains acteurs vont formaliser qu'il y a une décision à prendre.

À ce moment un petit groupe, souvent des experts, va se former et proposer une nouvelle RFC (Request For Comment), c'est un document qui décrit ce choix dans le détail (pourquoi, ce que ça va apporter, les risques, les obligations, les interdictions, les recommandations et l'impact sur les autres RFC qu'il faudra amender). Une fois la première version rédigée, les documents sont proposés à la communauté dans la version brouillon, un canal d’échange associé est créé et l’ensemble de la communauté va débattre, argumenter et décider. Après de nombreux échanges, la version finale de la RFC est validée. On passe alors dans la phase de publication. Il s’agit de mettre à disposition de tout le monde le document validé accompagné des RFC impactées amendées et validées ainsi que l’ensemble des échanges qui ont eu lieu.

Chaque communauté a son propre système de RFC, avec des règles de création, dès règles de validation et un nommage différents en fonction des projets : JEP, PEP, XEP, etc. Certaines personnes ont partiellement généralisé ce processus sous le nom : Architecture Decision Records (ADR).

### À propos de synchrone

Avoir des échanges synchrones présente un avantage majeur : c’est très rapide. En revanche, un message synchrone est plus difficilement opposable et il est quasiment impossible de faire une recherche dans l’historique des échanges oraux.

### Conclusion

Je n’ai pas de liste des communications à maintenir synchrone ou non. En revanche, depuis quelque temps,  j’ai pris l’habitude de proposer aux équipes qui pratiquent le télétravail de faire un atelier dédié pour décider collectivement de ce qu’elle souhaite garder synchrone et ce qu’elle souhaite passer en asynchrone.  La rétrospective étant le moment idéal pour retravailler cette liste et les décisions associées.

## Règle numéro 3 : Avoir un canal audio permanent avec tout le monde

Un canal audio où tout le monde est connecté du matin au soir, pour pouvoir passer d’une discussion à une autre, pour pouvoir demander à l'oral car dans de nombreux cas, l’écrit est un frein à la demande. L’idée c’est à la fois de :

* garder un contact oral avec le reste du groupe, j’entends mon voisin de bureau râler malgré les kilomètres,
* mais aussi de pouvoir très facilement interpeller les collègues, avec une phrase du genre : “Quelqu’un à une idée pour …”
* garder une forme de présence.

Le bon matériel est crucial pour permettre la mise en œuvre de cette règle. En effet, un casque de mauvaise qualité est difficilement supportable toute une journée. 

Il est important que l’outil de communication audio supporte ce que j’ai appelé plus haut l’attente active, il est facile d’être connecté et à l’écoute sans aucun bruit tout en restant interpellable par la voix. Ceci implique de facto un présupposé de confiance.

## Règle numéro 4 : Organiser des séminaires ou des jours de travail en physique.

Il est important d’avoir des moments de travail en réel avec tout le monde pour que les gens se connaissent, prennent soin les uns des autres et fassent équipe. J’ai croisé des entreprises et des communautés aux rythmes variés :

* 1 journée par semaine, 
* 1 journée par mois, 
* 1 semaine par trimestre, 
* 1 semaine par an, 

 À vous de trouver le rythme adapté à votre organisation, que ce soit au vert dans un château perdu du centre de la France ou dans vos locaux.

Quand j’étais chez Argia, nous avions un séminaire d’une semaine par trimestre dans un lieu facile d’accès pour tout le monde.

Chez l’un de mes clients récent, les équipes se réunissent au moins 1 journée par mois dans leurs locaux et l’ensemble de l’entreprise se regroupe 2 jours par an pour une convention d’entreprise.

Enfin j’ai récemment croisé une entreprise ou chaque équipe se retrouve une journée par semaine sur site et la direction regroupe toute l’entreprise une journée par mois sur l’un de leurs sites.

# Les pièges à éviter

## Le paternalisme et le micro-management

En multipliant les points d’échanges, en multipliant les moments de pairing, en rendant explicites les moments d’échanges et d’attention aux signaux faibles, en formalisant tout un ensemble d’échanges par écrit, un certain nombre de personnes se sentent moins autonomes. Certains vont même évoquer un sentiment de paternalisme ou de micro-management. Pour éviter ça, il va falloir expliquer le pourquoi, expliquer l’intention, expliquer le rôle du manager et comment, pour prendre soin de l’équipe il est indispensable de passer beaucoup de temps ensemble. Il faudra aussi faire très attention à ne pas donner de conseils sur le comment faire son travail durant ces entretiens. Rester du bon côté de la limite entre ressenti de micro-management et attentions aux collaborateurs reste un exercice difficile.

## L’isolement

Un télétravailleur passe la majorité de son temps en ligne avec son équipe, tout ça à distance. C'est-à-dire avec des gens qui ne font pas de sport, pas de sorties culturelles, pas de soirées bières ou restos à côté de chez lui. Une fois le lien avec l’entreprise coupée, il y a un risque d’isolement très fort pour les salariés en télétravail. Dans une entreprise où tout le monde habite la même ville, il y a des incitations à participer à des activités extra-professionnelles. En mode télétravail, l’entreprise et le CSE ont un rôle à jouer pour proposer des activités locales aux salariés. Le CE d’un de mes clients à par exemple donné accès à toute la billetterie Fnac sans distinction de lieu et rembourse une partie non négligeable de l’inscription en club de sport. Les réductions sont certes moins importantes, mais elles sont justes et équitables pour l’ensemble des salariés quelque soit leur lieu de résidence.

## Le surmenage

“Il ne me reste que ça à finir !”, “Encore 2 minutes et j’arrive !” Combien de fois j’ai pu dire ce genre de choses à ma compagne et ne descendre que 2 heures plus tard. Un des gros risques du télétravail c’est de se laisser absorber par le travail et d’avoir du mal à en sortir. Sur un plateau avec des collègues, quand à 18h00 tout le monde part, il est difficile de ne pas voir qu’il est temps de partir. La communication non verbale de l’équipe permet de garder une notion du temps qui s’écoule. Pour éviter le surmenage, prendre l’habitude de pousser une petite notification à tout le monde pour rappeler quelques moments clés de la journée et la encore le management dans son travail d’écoute des signaux faibles se doit de surveiller que personne ne travaille trop.

## Le perte de confiance

Loin des yeux, loin du cœur dit le dicton. Loin des yeux, loin de l’intimité du travail quotidien, un manque de confiance peut s'immiscer dans la relation, la peur qu’un collaborateur soit distrait par son environnement, la peur qu’il n’alerte pas sur une baisse de charge, la peur qu’il profite du système pour se faire payer sans travailler. Il est en effet plus facile de mesurer la présence des collaborateurs que leur productivité. Le réflexe est donc humain, faire au plus simple et ne mesurer que la présence. Rassurez-vous, si un collaborateur décide de ne plus travailler, il le fera aussi bien chez lui qu’au bureau. Je ne veux pas dire par là qu’il faut mettre du contrôle sur l’activité de chacun, bien au contraire, ce serait dommage de faire des règles pénalisantes pour 80% des collaborateurs tout ça pour gérer une minorité. Si les objectifs sont donnés au groupe et pas individuellement, le groupe va pratiquer une forme d’autorégulation. Et gardez confiance, l’Homme est bon, si en plus vous restez à l’écoute de l’ensemble des signaux faibles pour anticiper les décrochages, je crois que vous avez toutes les billes pour ne pas perdre confiance.

# Conclusion

En résumé, voici les 5 règles du manager pour réussir le télétravail dans vos équipes : 

1. Avoir des outils adaptés.
2. Ritualiser fréquemment des échanges aux objectifs précis.
3. Passer du tout synchrone à un mode hybride.
4. Avoir un canal audio permanent avec tout le monde.
5. Organiser des moments de travail en réel avec tout le monde.

Et les pièges à éviter :

* Le paternalisme et le micro-management
* L'isolement
* Le surmenage
* La perte de confiance.

La recette n’est pas infaillible, mais elle devrait fonctionner dans beaucoup de cas. Je l’ai expérimentée durant de nombreuses années et j’ai aujourd’hui des clients qui l’appliquent avec succès.

Chez aqoba nous sommes convaincus que le manager a encore une place importante dans l’entreprise et encore plus avec le télétravail. À toi manager qui dois trouver ta place dans une organisation en pleine mutation, j’espère que ces quelques règles t'aideront.
