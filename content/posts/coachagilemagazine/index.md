---
draft: false
title: Les archives du Coach Agile Magazine
authors:
  - olivier-marquet
values:
  - partage
tags:
  - Humour
  - Agile
  - CoachAgileMagazine
  - CAM
offers:
  - unlock-transformation
  - unlock-delivery
  - unlock-management
  - unlock-project
date: 2024-01-01T08:00:00.000Z
thumbnail: couverture-article-cam.png
subtitle: Le magazine des coachs transformatifs
---
Régulierement nous publions un fausse couverture du magazine fictif "Coach Agile Magazine". Retrouvez ici l'intégrale. Abonnez-vous sur les réseaux sociaux pour ne plus jamais prendre de retard.

<section id="cam" >
<style> 
    img.centered-content.zoom:hover {
        -ms-transform: scale(2); /* IE 9 */
        -webkit-transform: scale(2); /* Safari 3-8 */
        transform: scale(2);
}
</style>
    <div class="centered-content">
        <div class="three-cols-layout">
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Nov24.jpg" alt="Magazine Coach Agile Novembre 24">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Ete24.png" alt="Magazine Coach Agile été 24">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Avril24.png" alt="Magazine Coach Agile Avril 24">
                </div>
            </div>        
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Fev24.png" alt="Magazine Coach Agile Fevrier 24">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Dec23.png" alt="Magazine Coach Agile Décembre 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Oct23.png" alt="Magazine Coach Agile Octobre 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Sept23.png" alt="Magazine Coach Agile Sept 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Ete23.png" alt="Magazine Coach Agile été 23">
                </div>
            </div>        
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Mai23.png" alt="Magazine Coach Agile Mai 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Avr23.png" alt="Magazine Coach Agile Avril 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Fev23.png" alt="Magazine Coach Agile Fev 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Jan23.png" alt="Magazine Coach Agile Jan 23">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Dec22.png" alt="Magazine Coach Agile Déc 22">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                       <br>   <img class="centered-content zoom" src="Magazine_Coach_Agile_Nov22.png" alt="Magazine Coach Agile Nov 22">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                      <br>    <img class="centered-content zoom" src="Magazine_Coach_Agile_Oct22.png" alt="Magazine Coach Agile Oct22">
                </div>
            </div>
            <div class="col col--margined ">
                <div>
                      <br>  <img class="centered-content zoom" src="Magazine_Coach_Agile_Sept22.png" alt="Magazine Coach Agile Sept 22">
                </div>
            </div>
            </div>
    </div>
</section>                  





