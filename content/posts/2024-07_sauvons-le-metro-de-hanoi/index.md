---
draft: false
title: Il faut sauver le métro de Hanoï
authors:
  - thomas-clavier
values:
  - conviction
  - transparence
tags:
  - sauvetage
  - challenge
  - lean
  - onepeaceflow
offers:
  - unlock-project
date: 2024-09-30T10:54:00.000Z
thumbnail: hanoi-metro.jpg
subtitle: Non, ce n'est pas l'histoire d'un film de guerre en indochine
---

Il était une fois l’histoire d’un directeur des Opérations Billettique France & International chez [RATP Smart Systems](https://www.ratpsmartsystems.com/), qui m’appelle pour me dire la chose suivante :

> Tu te rappelles quand avec Olivier nous avions sauvé le projet Navigo ? Et bien il faut recommencer ! Nous devons faire toute la billettique de la ligne 3 du métro de Hanoï, c’est pour dans 1 an et toutes mes équipes me disent qu’il faut au moins 3 ans ! Olivier pourrait venir nous sauver ?

Olivier n'étant pas disponible, j'y suis allé. Et nous avons vécu une très belle aventure que je vais vous conter. Étant français avec un grand père vietnamien, j’avais une grande fierté de porter les couleurs de cette belle entreprise.

## Résumé

Il n’y a pas de projet informatique mort né ! Il n'existe que des projets dans lesquels on peut mettre les outils lean, agile et craft au service du succès.
Dans cette aventure j’ai utilisé la culture lean agile et craft appliquée avec une grande rigueur pour assurer la victoire. 

Si vous n’avez pas le courage de tout lire, retenez ces quelques points :

* Commencez par recruter les bonnes personnes
* Ritualisez un entrainement d’une journée par semaine
* Utilisez les outils en lignes
* Gardez de l’autonomie sur les technos
* Organisez-vous en one piece flow
* Prenez le temps de supprimer **tout** ce qui vous ralenti
* Faites du TDD et du BDD

{{< figure  src="viatique.png" alt="Viatique" class="center-small">}}

Et maintenant, je vous propose d'enfiler le costume de mon héro et de le suivre dans son aventure.

## Un monde ordinaire

Nicolas est un directeur "ordinaire". Au sens où il n'a aucun super-pouvoir, comme nous tous. Surtout pas celui de ralentir le temps et de faire tenir 3 ans dans 12 mois ! Sa direction a signé un gros contrat : livrer toute la billettique de la futur ligne 3 du métro de Hanoï. C’est facile, on gère déjà la billettique de Paris. Faut juste adapter un peu et traduire en vietnamien.

{{< figure  src="contrat-signe.png" alt="Annonce du contrat signé" class="center-large">}}

Alors on lui demande de livrer le tout pour dans moins d’un an. Faut dire que l'inauguration est déjà organisée et planifiée par le client. Ce sera pour la fête du têt 2021. Or nous sommes en janvier 2020, ce qui nous laisse moins de 13 mois.
Nous sommes dans un cadre très classique, tout le monde s’est engagé sans vraiment savoir combien cela va prendre de temps et d’énergie. Nous n’avions d’ailleurs pas trop le choix, certains racontent que la promesse a été faite par Édouard Philippe pendant sa visite de 2018.
Il y a eu des approximations et c’est normal, une estimation minutieuse coûte beaucoup trop cher. Alors on demande à la direction projet de tout faire pour que ça rentre dans les délais, dans le budget sans toucher au périmètre tout en garantissant la qualité.

Pour celles et ceux qui en connaissent le principe, c’est une négation du concept du triangle de fer où s’interconnectent les 3 dimensions Coût, Délais, Périmètre du projet avec une incidence sur la qualité de ce qui est livré. En résumé, si le contexte exige de tenir des délais serrés, alors il faudra jouer sur le budget et le périmètre du projet pour maintenir un niveau de qualité acceptable.

## La quête de l’impossible

Nicolas commence par demander à ses équipes combien de temps il faut pour réaliser tout ça. Et c’est le drame. Les approximations étaient beaucoup trop fantaisistes, il faut 3 fois plus de temps que ce qui a été vendu !
Nicolas commence donc par expliquer que ce n’est pas possible, que personne n’est capable de faire ça en si peu de temps ! Seulement, le contrat est signé, les pénalités sont exorbitantes : il est trop tard pour faire demi-tour.
En disant ça, le fantôme d’un vieux projet refait surface et lui rappelle qu’il y a quelques années il avait participé à un projet qui était annoncé par certains comme perdu d’avance. Faire tout le logiciel des automates de rechargement des pass Navigo en beaucoup beaucoup moins de temps que nécessaire. Comme pour cet ancien projet, Nicolas décide qu'il aura besoin d'une aide extérieure : et c'est là que j'entre en piste.

### Monter une équipe

Première étape, et non des moindres, constituer une équipe de choc. Nous avions déjà en interne :

* Un Product Owner (PO) fraîchement formé
* un assistant PO
* l'architecte qui avait bossé sur l’avant vente
* 1 QA à temps partiel

Et c’est tout. Aidé de l’architecte, nous avons recruté des profils C#, des profils Android, et des profils C++.
Nous n’avons qu'un an pour tout faire, il est impossible de perdre du temps à gérer des problèmes de qualité. Je décide donc de recruter de très bons développeurs avec des profils en T et capables de faire du TDD (Test Driven Development). Mais je jargonne : laissez-moi vous expliquer ces deux termes ci-dessous.

#### TDD ?

Le TDD ou le développement piloté par les tests en français, est une méthode de développement de logiciel qui consiste à concevoir un logiciel par des itérations successives très courtes (ou petits pas), telles que chaque itération est accomplie en formulant un sous-problème à résoudre sous forme d'un test avant d'écrire le code source correspondant, et où le code est continuellement remanié dans une volonté de simplification.

La méthode est découpée en 3 étapes :
1. écrire un seul test qui décrit une partie du problème à résoudre et vérifier que le test échoue, autrement dit qu'il est valide, c'est-à-dire que le code se rapportant à ce test n'existe pas ;
1. écrire juste assez de code pour que le test réussisse ; vérifier aussi que les autres tests passent tous ;
1. remanier le code, c'est-à-dire l'améliorer sans en altérer le comportement, qu'il s'agisse du code de production ou du code de test.

{{< figure  src="tdd-full.png" alt="Triple boucle TDD" class="center-small">}}

#### Profil en T

Chez Valves, l’éditeur de jeux vidéos, quand ils recrutent de nouveaux développeurs, ils demandent à chacun d’avoir un profil en T (voir le [Valve's Handbook for New Employees](https://www.valvesoftware.com/en/publications)). Dans un jeu vidéo, le spécialiste de l’arme lourde est souvent à l’arrière du groupe et protège le reste du groupe en tirant de loin. Mais s’il faut lancer une attaque au couteau avec toute l’équipe, alors il est indispensable que tout le monde accepte de partir à l’attaque. En synthèse, une personne avec un profil en T possède une expertise forte dans un domaine (compétence représentée par la barre verticale du T) mais est capable de polyvalence (sa barre horizontale).

{{< figure  src="t-shaped.png" alt="Profil en T de chez Valve" class="center-small">}}

#### Pierre de rust

Autant en C# et en Android j’ai trouvé sans trop de difficultés des développeurs avec des profils en T aguerris au TDD. Pour le C++ c’était autre chose ! Je ne trouvais pas. Impossible de trouver des développeurs suffisamment à l’aise avec le TDD en C++.

{{< figure  src="linux-rust.png" alt="Linux rust powered" class="center-small">}}

Fin août 2019, un projet avait fait beaucoup de bruit dans la communauté linux : et si le futur du noyau linux n’était autre que Rust[^rust] ? en 2020 d’ailleurs le projet sera officiellement annoncé pour intégrer la branche 6.1 du noyau. Comme Linus Torvald, je me dis que pour recruter la nouvelle génération de développeurs, il est nécessaire de le faire avec des langages plus modernes. Armé de mon courage et de ma carte “Projet Hanoï” j’ai négocié avec le DSI et les RH de changer l’annonce pour ne plus recruter des développeurs C++ mais des développeurs Rust et C++.
[^rust]: Rust est un langage de programmation compilé inventé chez Mozilla. Voir [la page wikipedia de Rust](https://fr.wikipedia.org/wiki/Rust_(langage)) et [le site officiel](https://www.rust-lang.org/fr)

Grâce à cet écart de règle. Nous avons trouvé la perle rare, un développeur qui venait de chez Red Hat et qui avait l’habitude de travailler sur le noyau Linux, hyper content de pouvoir pousser le premier pilote Linux écrit en Rust.


Nous avons rapidement constitué l’équipe :
1 PO, 1 assistant PO, 3 C#, 1 Android (qui souhaitait faire du Kotlin et pas du java), 2 C++ et moi-même. L'aventure pouvait démarrer, nous étions 7 externes et 2 internes, une belle pizza team. Le recrutement du scrum master était moins urgent.

Le démarrage en grande pompe était calé, tout allait pour le mieux, rendez-vous le 9 mars 2020 à Noisy-Le-Grand pour le kickoff et le 23 mars 2020 avec le premier sprint.

### Un départ compromis

Dans un contexte national inédit lié à l’accélération galopante de pandémie de coronavirus en France, Emmanuel Macron a fait une nouvelle allocution le lundi 16 mars pour annoncer que seuls seront désormais autorisés les trajets « absolument nécessaires » et « toute infraction à ces règles sera sanctionnée ».

{{< figure  src="macron-confinement.jpg" alt="Annonce du confinement" class="center-large">}}

Nous étions confinés !
Ancien d’une société entièrement en télétravail, ce mode de tfonctionnemen était pour moi très simple à mettre en place : des ordinateurs portables pour tout le monde, une messagerie instantanée, une solution de chat audio et vidéo, un peu de partage d’écran et le premier sprint pouvait démarrer.
Si vous souhaitez plus d’information sur les clés du télétravail réussi, je vous invite à lire : [Mes 5 clés pour réussir le télétravail](https://aqoba.fr/posts/20220927-ils-ne-veulent-pas-revenir-sur-site-mes-5-cl%C3%A9s-pour-r%C3%A9ussir-le-t%C3%A9l%C3%A9travail/)

### À la croisée des chemins

{{< figure  src="tet.jpg" alt="Fête du têt" class="center-small">}}

Une grande question se pose alors : **Va-t-on réussir à tout livrer dans les temps ?** L’enjeu est de taille, nous sommes un des rares projets à se poursuivre normalement malgré le confinement, et ce serait dommage de se retrouver à devoir payer des indemnités de retard.

Le truc que j’aime beaucoup quand on parle de planification dans l’agilité, c’est que l’on ne se prend pas pour Madame Irma. On ne fait pas d’estimation du temps qu’il va falloir, on n’essaye pas non plus de deviner le temps qu’il nous faudra pour faire les choses, mais on estime la complexité relative du travail, puis après seulement, on mesure notre vélocité, c’est à dire le temps que l’on met à livrer 1 point de complexité relative. Une simple règle de 3 permet alors de faire une prédiction sur la date de livraison. Prédiction qui sera chaque jour un peu plus juste.

{{< figure  src="burn-up.png" alt="Burn-up théorique" class="center-small">}}

Là nous avions 2 objectifs que la planification devait pouvoir résoudre :

* alerter le plus vite possible en cas de dérapage,
* être en capacité de demander un arbitrage en cas de problème (réduire le périmètre, augmenter l’équipe, sous-traiter une partie, etc.)

Étant donné une équipe de 8 personnes qui va travailler sur le projet pendant 10 à 12 mois. Combien faut-il d'US[^US] ?
Comme 1 binôme livre de 1 à 3 US par sprint.
Et qu’il y a 8 personnes / 2 = 4 binômes dans l’équipe
Et que les sprints durent 2 semaines soit 12 mois × 2 = 24 sprints
Alors dans un projet de 12 mois, on peut réaliser entre 4 × 1 = 4 et 4 × 3 = 12 US par sprint soit entre 24 × 4 = 96 et 24 × 12 = 288 US pour le projet.

[^US]: User Strory ou Histoire utilisateur en français, correspond à un petit morceau de travail à réaliser.

Nous avons donc demandé aux PO de découper l’intégralité de tout le travail à faire en 200 petits morceaux de tailles approximativement égales. Les POs ont eu beaucoup de mal à produire ces 200 US avec juste des titres. Ils avaient très peur d’oublier des éléments, mais bien accompagnés, le travail a été fait. Les 200 US ont été regroupées en 2 épopées[^epic].
L’étape suivante à été de macro-estimer l’intégralité du backlog en équipe avec un atelier d'extrême cotation, et dès la fin avril, nous étions capable de dire avec précision le temps de travail qu’il restait … Nous étions dans le cas : il n’est pas possible de tout livrer dans les temps.

[^epic]: Epic ou Épopée en français, c'est une regroupement d'histoires utilisateur.

Vous vous souvenez du triangle de fer ?
Si on ne doit pas toucher aux délais ni au budget du projet, alors on doit pouvoir adapter son périmètre : nous décidons, en accord avec la direction et le client, de livrer le projet en 2 fois :
* D'abord le titre de transport le plus rapide à produire pour la date prévue, c'est-à-dire pour la fête du Têt
* Ensuite, les autres types de titres de transport pendant la période de garantie qui suit cette date butoire.

La livraison continue que nous avions mise en place, et nous y reviendrons plus tard, avait permis en moins de 2 mois de rassurer le client sur notre capacité à livrer les fonctionnalités au fil de l’eau sans impacter la production.

### One piece flow

{{< figure  src="one-piece-flow.jpg" alt="One piece :: flow" class="center-small">}}

Dans le monde du lean et de kanban, il y a un truc qui a littéralement transformé ma façon de penser : la découverte du “one piece flow”.

Le principe de base est le suivant  : il est beaucoup plus efficace de fédérer l’équipe au complet pour finir une US dans son ensemble, plutôt que d’occuper les gens sur leur spécialité avec de nouvelles tâches en se disant que l’on peut anticiper du travail tout en exploitant au mieux leurs compétences.

Dit autrement et avec un exemple, si l’US 12 demande à être développée à la fois sur le terminal de vente et sur les portiques alors, au moment de prendre l’US, un binôme se forme et se partage le travail. Puis chacun va travailler dans son coin.  Quand le développeur C++ termine sa partie et dit : “Pour moi l’US 12 est finie” et que le développeur .Net nous dit : “de mon côté il reste au moins 3 jours de travail”, il est plus efficace d’avoir le développeur C++ qui aide le développeur .Net à finir que de le voir partir sur une autre US. 

Ce principe est d'ailleur très bien décrit dans la maxime *"Stop Starting, Start finishing"* et dans le [mini livre d'Arne Roock du même nom](https://shop.leankanban.com/products/stop-starting-start-finishing-arne-roock-english-ipad-epub-ebook-digital-edition).

Reste à motiver toute l’équipe pour généraliser ce comportement … Quand Dan Pink nous parle de motivation de groupe, il dit qu’il faut 3 choses :

* le sens : la compréhension du pourquoi
* la compétence
* et l’autonomie, la possibilité de dire "non".

Pour que le sens soit partagé, j’ai exploité de nombreux jeux de simulation kanban qu’il a fallu adapter à un mode de jeu à distance.

{{< icon-block icon="lightbulb-o" >}}
1/2 journée d'entraînement par semaine pour délivrer avec efficience
{{< /icon-block >}}

Pour la compétence, j’ai ritualisé un atelier “craft” tous les lundis après-midi, une après midi dédié à l'amélioration technique et à la recherche de l'excellence.
Durant ces après-midi, nous avons fait des katas en mob programming dans un des 3 langages de l’équipe. Et il s’est passé de très belles choses, en particulier les développeurs ont appris à apprécier des langages qui n’étaient pas les leurs, à comprendre les particularités et les limites de chacun de ces langages, ils ont cherché à définir des standards de développement commun, à partager leurs façons de faire, et surtout ils se sont sentis compétents pour binômer réellement sur des tâches en-dehors de leur expertise.

Pour l'autonomie, j'ai rappelé et clarifié qu’il n’y avait pas d’obligation à aider les autres. L’esprit d’équipe s’est chargé d’atténuer ce droit à dire "non".

### Je les livre où ?

{{< figure  src="tvm.png" alt="Terminal de vente" class="center-small" title="Terminal de vente ou Ticket Vending Machine (TVM)">}}

Un matin, Nicolas reçoit un étrange coup de téléphone : “Je le livre où ?” sous entendu : "Je le livre où le terminal de vente ?", vous imaginez, une grosse machine de 400kg à poser dans le salon d’un développeur ? Ce coup de téléphone a éveillé tout plein de questions : faut-il mieux avoir le TVM chez l’un des développeurs .Net ? Ou dans la salle de test à Noisy ? Et si c’est chez l’un des développeurs, son appartement est-il suffisamment grand ? Son plancher suffisamment solide ? Et comment les QA pourrons tester ?

{{< figure  src="gate.png" alt="Portillon de test" class="center-small" title="Portillon automatique (gate)">}}

Nicolas a réuni l’équipe pour prendre une décision collective.

J’ai alors fait un grand atelier sur les différents modes de prise de décision :

* **Autoritaire** : un seul membre prend la décision.
* **Majoritaire** : le groupe le plus nombreux.
* **Minoritaire** : un sous-groupe moins nombreux.
* **Unanimité** : l’ensemble des membres est d’accord.
* **Compromis** : l’ensemble des membres cherche une solution acceptable.
* **Consentement** : les membres débattent puis ne s’opposent pas à la décision. Dans cette catégorie de prise de décision, on trouve aussi les décisions par *"sollicitation d'avis"* qui seront abordées plus tard.
* **Consensuelle** : les membres se mettent d’accord sur la base d’une analyse approfondie d’arguments, en essayant d’éviter les biais cognitifs.

Nous avons décidé que ce serait une décision de type "consentement par élimination". Étaient éliminées les propositions physiquement impossibles du style, mon plancher ne supportera pas le poids de la machine... mes voisins vont râler... Au final le TVM de test est arrivé dans le salon de l’un des développeurs C# et le portillon dans la salle de test à Noisy. Durant l’été tout à été regroupé dans la salle de tests à Noisy.

Après cet événement, quasiment toutes nos décisions ont été prises par consentement avec une incroyable efficacité.

### Progresser même pendant l’aventure

{{< figure  src="popcornflow2.jpg" alt="popcornflow" class="center-small">}}

Comment promouvoir la culture de l’excellence ? Même si durant le recrutement, nous avions porté une attention particulière à de nombreux savoir-être comme la curiosité ou l’attention portée aux choses bien faites, nous avions besoin de mettre en place un cadre dédié à l’excellence opérationnelle pour suivre et donc promouvoir cet état d’esprit.
J’avais testé un kanban des problèmes avec un autre client et les résultats avaient été spectaculaires. J’avais très librement adapté un [Popcorn Flow](https://agilesensei.com/) de  [Claudio Perrone](https://www.linkedin.com/in/claudioperrone/). Nous avons donc remis en place et adapté ce kanban des trucs qui grattent, freinent, bloquent ou ralentissent.

Le fonctionnement est le suivant :
* En daily, on se pose la question : avons-nous eu l’impression d’attendre, de perdre notre temps, d’avoir été ralenti ou bloqué par quelque chose ? Si oui, quoi ?
* On ajoute alors une carte dans la première colonne. Si la carte existe déjà, on ajoute 1 point rouge dessus.
* En fin de daily, s’il n’y a plus de “problème” en cours de résolution, on fait une rapide priorisation pour faire émerger le problème le plus important à résoudre.
* Quelqu’un va alors prendre le problème et le faire passer dans les différentes colonnes.
    * **Problème** : La liste de tous les problèmes priorisés.
    * **Mesure** : Dans cette colonne le porteur va chercher à trouver le bon indicateur qui permet de mesurer le problème. À quoi verrons-nous que le problème est bien résolu ? Et on note la mesure actuelle.
    * **Alternatives** : Dans cette colonne le porteur va identifier au moins 3 alternatives pour résoudre le problème et va en choisir une.
    * **Test** : Dans cette colonne l’équipe teste l’alternative sélectionnée à l’étape précédente.
    * **Bilan** : Dans cette colonne on vérifie que l’action testée a bien apporté la valeur souhaitée pour clore le problème. Si ce n'est pas le cas, on revient en colonne **Alternatives**.
    * **Fini**

Le porteur n’est pas seul, c’est lui qui fait avancer la carte dans les différentes colonnes, mais chaque fois qu’il a besoin de travailler avec les autres, son sujet est plus important que le reste des US et il peut mobiliser toute l’équipe autant de fois que nécessaire. Il n’y a que dans la colonne Test que ce n’est plus vrai.
Il va chercher à avoir l'adhésion de toute l’équipe que ce soit en passant du temps avec chacun ou en organisant des points collectifs. 
C'est une décision par **consentement** piloté par un porteur. 
Dans son livre [*Reinventing organizations*](https://www.diateino.com/reinventing-organizations-p-8473.html), Frédéric Lalou parle de prise de décision par ***sollicitation d'avis*** pour ce genre de démarche. C’est à dire que toute personne peut prendre une décision après avoir demandé conseil :

1. à toutes les personnes qui seront impactées de manière significative, et
2. aux personnes compétentes en la matière.

Pour rester dans une forme de décision par consentement, les conseils reçus doivent être pris en considération. Il ne s'agit pas de créer un compromis édulcoré qui répond aux souhaits de chacun. Il s'agit d'accéder à la sagesse collective en vue d'une décision pertinente. Compte tenu de tous les conseils et de tous les points de vue qu'elle a reçus, la personne qui décide choisit ce qu'elle estime être la meilleure ligne de conduite.

### L’enfer des spécifications

{{< figure  src="contrat.jpg" alt="Signature du contrat" class="center-small">}}

Si un projet n’était qu’un long fleuve tranquille, il faut avouer que l’on s'ennuierait. Nouvelle semaine, nouvelle embûche, le client nous annonce qu’il a absolument besoin de spécifications détaillées en anglais décrivant avec précision tout ce qu’il va y avoir dans l’ensemble de nos applications. Sans cet élément contractuel, il ne sera pas en capacité de nous payer. Seulement voilà, tout notre backlog est dans JIRA avec deux niveaux de détails très contrastés :

* les US terminées ainsi que celles que nous allons faire très prochainement sont très bien détaillées
* les US que nous ne ferons que dans 3 semaines ou plus n’ont qu’un titre.

Comment agréger tout ça sous forme d’un magnifique cahier de spécifications détaillées …. sans compter que tous ces éléments sont rédigés en français. Et cerise sur le gâteau, c’est à livrer pour la "semaine dernière".

Nous avons décidé de stopper tous les développements et de mettre l’ensemble de l’équipe sur cette nouvelle tâche urgente. Le financement de notre projet était en jeu.

Nous avons eu quelques réticences comme :

> Je ne suis pas payé pour faire des specs !
Signé le developpeur.

En expliquant bien le "pourquoi" nous avons mobilisé tout le monde.

Étant donné l’urgence de la situation, avec le PO, nous avons découpé les 3 documents à faire en sections et sous-sections. Nous avons listé tout ça sous forme de post-it virtuels dans un tableau kanban priorisé par l’ordre des chapitres :

* À faire,
* En cours de rédaction,
* Rédigé,
* En cours de relecture tech,
* Relu tech,
* En cours de relecture traduction,
* Relu traduction,
* En cours de relecture mise en forme,
* Fini.

Après avoir réexpliqué à toute l’équipe la nécessité de ce travail nous avons dit : « Organisez vous en binôme ou trinôme, on fait du “one piece flow“ par mini équipe. Chaque section devra être relue techniquement par un autre binôme et relue linguistiquement pour avoir une unité rédactionnelle ? Rendez-vous dans 3h. »
Nous avons planifié 3 rituels par jour à 9h00, à 14h00 et 17h30.
Aidé d’un éditeur collaboratif (word dans o365) nous avons rédigé comme des fous, et toutes les 3 heures nous nous retrouvions tous ensemble pour suivre l’avancement :

* Ce que nous avons fini depuis la dernière fois.
* Quels sont les éléments qui nous ralentissent ou nous bloquent ?
* Que pouvons-nous faire pour optimiser le flux ? Qui s’en occupe ?
* Ce que nous allons faire pour la prochaine fois.

Au bout de 24h, nous avons intégré une spécialiste de l’anglais dans l’équipe et notre vélocité à augmentée à chaque mini sprint de 3h.

Bilan :

* 3 documents contractuels de plusieurs centaines de pages,
* tous les développeurs se sont appropriés l’ensemble des fonctionnalités à réaliser, bien avant d’avoir à les développer,
* et une bien meilleure appropriation, compréhension de l’utilité des rituels agiles. Avec notre tableau des problèmes qui avait montré toute son efficacité.

Cet enfer à duré 2 semaines ! Je ne dis pas qu’il faut écrire des specs, mais que nous en avons malgré tout tiré du positif.

### On aperçoit la lumière

#### Chaîne de CI/CD

{{< figure  src="code.jpg" alt="Du code" class="center-small">}}

Un des problèmes rapidement remonté à été le manque de CI/CD[^CICD] et le retour beaucoup trop lent à chaque merge, à chaque compilation, à chaque tentative de déploiement sur le matériel de test.

[^CICD]: intégration continue/livraison ou déploiement en continu.

Pour être le plus rapide possible dans la mise en place de la chaîne de CI/CD, nous avons décidé, avec le soutien de la direction, de ne pas utiliser les outils internes mais uniquement des outils en ligne :

* [gitlab.com](https://about.gitlab.com) avec sa [“docker registry”](https://docs.gitlab.com/ee/user/packages/container_registry/), sa [“package registry”](https://docs.gitlab.com/ee/user/packages/package_registry/index.html), ses workers windows 64bits, linux amd64 et ses pages statiques
* et sonarcloud.

Il a fallu longuement négocier et mettre en perspective le risque de sécurité versus la rapidité de déploiement.

Une des difficultés a été de trouver des workers arm32 pour construire les packages android, nous avons testé avec succès les VM ARM de scaleway.

Rapidement nous avons eu une chaîne complète de CI/CD qui, à chaque commit, permettait de jouer :

* l’ensemble des tests unitaires,
* l'ensemble des tests E2E
* construire les artefacts
* les déployer sur notre matériel de test.

Avoir une boucle de rétroaction aussi rapide nous à permis de rapidement voir ce que nous devions améliorer pour livrer sans bug du premier coup :

* notre expertise des tests unitaires,
* nos compétences en tests d’intégrations,
* et à bien maîtriser les techniques d'architecture pour limiter au maximum les tests d’IHM.

Des sujets que nous avons traités le lundi en groupe.

#### Du BDD

> Ce qui part en production c’est ce que le développeur a compris !

* Quoi de mieux que le Behaviour Driven Development pour s’assurer que le développeur a bien compris ?
* Réponse : Faire écrire les tests d'acceptation par les développeurs !

Je vous vois froncer les sourcils, je vais rentrer dans le détail et vous allez comprendre mon point de vue.

Pour illustrer notre utilisation du BDD, partons d'une des demandes faites par le client : ce dernier exigeait une documentation utilisateurs pour chacun de nos matériels : terminal de vente, portique et terminal de contrôles.
Pour ne pas avoir de désynchronisation entre le code réellement en production et la documentation utilisateur, nous avons opté pour une documentation vivante. La rédaction de la documentation utilisateur était à la charge des QA en plus de valider le bon fonctionnement global.
La démarche BDD nous a permis de mener, en parallèle, la construction du code et celle de la documentation utilisateurs.

Avant de vous expliquer notre façon de faire du BDD, j’insiste sur la notion d’apprentissage. En tant qu’enseignant, on se pose souvent la question de comment s’assurer que ce que nous avons transmis à nos élèves a été correctement assimilé. En d’autres termes, comment s’assurer que nos élèves ont bien compris et retenu tout ce que nous avons transmis ? La première idée c’est de faire des “contrôles”, c’est-à-dire d’interroger les apprenants à intervalles réguliers pour s’assurer qu’ils ont bien compris et retenu.
La seconde solution, beaucoup plus adaptée à des petits groupes, mais aussi beaucoup plus efficace, c’est de demander aux élèves de nous refaire le cours, de nous expliquer avec leurs mots ce qu’ils ont retenu et compris. Cette dernière technique étant très utilisée en pédagogie inversée. C’est exactement ça ce que j'appelle “faire écrire les tests d'acceptation par les développeurs”. 

De façon moins provocatrice : Le BDD répond exactement à cette logique de pédagogie inversée. Un développeur qui écrit lui-même les tests d'acceptation d'une US démontre, avec ses mots, qu'il a compris tout ce qu'il fallait faire pour la finaliser.

{{< figure  src="steps-bdd.png" alt="Étapes du BDD façon RSS" class="center-large">}}

Nous avons donc déroulé cette façon de faire du BDD :

1. À l’ouverture d’une US
1. Rédaction des cas de tests d’acceptation par le trio dev / QA / PO dans XRay durant un atelier d'exemples mapping. **Le PO étant le seul à ne pas avoir le droit de prendre le stylo**. La majorité du temps, à ce moment-là, c'est le développeur qui joue le rôle de secrétaire.
1. Au moment du développement les devs choisissent comment réaliser les tests (Unitaire ou par l’IHM) et ajoutent dans le code la référence du test XRay.
1. La chaîne de CI/CD remonte directement dans XRay les résultats des tests avec les screenshots.
1. Depuis JIRA/Xray les QA peuvent extraire les US livrées et les tests associés pour produire la documentation utilisateurs.

Avoir tout le patrimoine de tests dans XRay rattaché aux différentes fonctionnalités, avec à chaque fois les tests d’acceptation rédigés avant le début des développements nous à permis de surveiller avec attention la couverture de tests fonctionnels.

## Victoire

{{< figure  src="inauguration.png" alt="Inauguration" class="center-small">}}

Le 12 février 2021, jour de la fête du têt, la ligne 3 du métro de Hanoï a été inaugurée avec 3 stations et 1 seul type de titre de transport.

{{< figure  src="prolongation.png" alt="Inauguration" class="center-small">}}

Le 11 septembre 2024 c’était l’inauguration d’une partie aérienne de la ligne 3 du métro de la capitale avec 5 nouvelles stations.
Aujourd’hui le projet est passé en mode produit avec des évolutions de lignes et de billettiques. La prochaine ouverture est prévue pour 2027 avec 7 nouvelles stations ce qui portera à 19 le nombre total de stations sur cette ligne.

## Viatique

Dans le dictionnaire la définition de viatique est la suivante :

* Argent, provisions que l’on donne à quelqu’un pour un voyage.
* Sacrement de l’eucharistie donné à un chrétien en danger de mort.
* Littéraire. Ce qui aide et soutient pour les besoins de l’existence.

Voilà ce que je vous laisse pour votre prochain voyage dans le monde des projets informatiques infaisable que vous allez réussir :

{{< figure  src="viatique.png" alt="Viatique" class="center-large">}}

<figure>
<iframe width="560" height="315" src="https://www.youtube.com/embed/0C1fAV8pGgc?si=bNloH3eQL4IvkgUX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<figcaption>La vidéo de l'intervention à Agile en Seine 2024</figcaption>
</figure>
