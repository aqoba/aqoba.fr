---
draft: false
title: "Communication NonViolente en entreprise : La clé pour des relations
  harmonieuses et une collaboration efficace"
authors:
  - laurent-dussault
values:
  - humanite
tags:
  - cnv
offers:
  - unlock-management
date: 2023-08-28T10:05:46.516Z
thumbnail: cnv-titre.png
subtitle: Découvrez comment l'OSBD et le DESC peuvent transformer votre
  communication professionnelle (et personnelle ?)
---
La communication est au cœur des relations humaines, que ce soit dans notre vie personnelle ou professionnelle. Dans le contexte de l'entreprise, une communication claire, empathique et  non-violente est essentielle pour favoriser un environnement de travail harmonieux et productif. Dans cet article, nous allons explorer deux méthodes puissantes de communication : 

{{< table class="table-with-blue-header table-alternate table-center" >}}
| OSBD          | DESC        | 
| -----------   | :--------:  |
| Observation   | Description |
| Sentiment     | Émotion     |
| Besoin        | Solution    |
| Demande       | Conséquence |
{{</ table >}}

Ces approches fournissent des outils pratiques pour améliorer la qualité des échanges professionnels, encourager l'écoute active, la compréhension mutuelle et la résolution constructive des problèmes.

# Introduction à la Communication NonViolente (CNV)

La Communication NonViolente (CNV) est un processus de communication développé par Marshall Rosenberg. \
La CNV encourage l'empathie envers soi-même et les autres, ainsi que l'expression authentique de nos émotions et de nos besoins. \
Elle favorise une communication constructive, la résolution des conflits et le renforcement des relations interpersonnelles.

# Comprendre l'OSBD (Observation, Sentiment, Besoin, Demande)

L'OSBD est un acronyme qui représente les différentes étapes de la Communication NonViolente.

### O. Observation : Décrire objectivement les faits observés

 \
Votre observation doit décrire le plus objectivement possible un comportement spécifique sans jugement. \
 \
Contre-exemple : "Tu es toujours en retard aux réunions." 

Cette formulation contient un jugement et une généralisation. 

Une observation plus objective serait : 

"Lors de la réunion d'hier, tu es arrivé 10 minutes après l'heure prévue." 

### S. Sentiment : Exprimer les émotions ressenties

En exprimant les émotions ressenties, on favorise une meilleure compréhension de l'impact de la situation sur soi-même. 

Contre-exemple : 

> Tu me mets en colère lorsque tu ne réponds pas à mes messages. 

Cette déclaration attribue la responsabilité de l'émotion à l'autre personne. Un exemple plus approprié serait : 

> Je me sens frustré lorsque je ne reçois pas de réponse à mes messages, car j'ai besoin de clarté et de communication efficace.

On a coutume de dire que : le “TU” tue. Il faut donc clairement préférer le JE dans l’expression de ses émotions.

### B. Besoin : Identifier les besoins sous-jacents

Ici, il s’agit de s’efforcer à exprimer le besoin sous-jacent à la situation et de mettre l'accent sur ce qui est important pour la personne.

Contre-exemple : 

> Tu devrais m'aider plus souvent. Tu ne te soucies pas des autres.

Ce contre-exemple implique un jugement et une demande implicite. Une approche plus constructive serait : 

> J'ai besoin de soutien et de collaboration pour atteindre nos objectifs communs.

### D. Demande : Formuler une demande claire et réalisable

Formuler une demande claire et réalisable permet de communiquer ses attentes et de proposer des solutions pour améliorer la situation

Contre-exemple : 

> Tu dois terminer ce projet immédiatement.

Cette demande est autoritaire et ne tient pas compte des contraintes ou des priorités de l'autre personne. Une demande plus respectueuse serait : 

> Pourrions-nous discuter d'un délai réaliste pour terminer ce projet 

{{< figure  src="cnv.jpg" alt="Illustration des interactions en miroir" title="Connexion établie en miroir" class="center-large">}}

### Aider à reformuler, par le questionnement

Comme le schéma ci-dessus le suggère, il s’agit d’établir une connexion. \
Voici donc un exemple de comment vous pouvez aider une personne à se connecter à ce mode de communication : \
 \
Quelqu’un vous expose son point de vue d’une manière “violente” : 

* "Je ne peux plus supporter Marc, il était en retard ce matin, comme d'habitude, il faut qu'il change !!"

En adoptant une posture de coach vous pourriez l’aider à utiliser l’OSBD en le questionnant :

**Observation** : "Vous dites que Marc était en retard ce matin, comme d'habitude. Pouvez-vous m'en dire plus sur ce que vous avez observé ?"

**Sentiment** : "Quand Marc est en retard, cela vous frustre et vous met en colère. Pouvez-vous mettre des mots sur ces sentiments ?"

**Besoin** : "Quel besoin n'est pas satisfait lorsque Marc est en retard ? Est-ce le besoin de fiabilité, de respect ou autre chose ?"

**Demande** : "Qu'aimeriez-vous que Marc fasse différemment ? Aimeriez-vous qu'il soit à l'heure ou qu'il vous prévienne s'il va être en retard ?"

Vous pouvez également aider la personne à identifier les jugements ou les évaluations qu'elle porte sur Marc et à voir si elle peut recadrer ses déclarations de manière plus objective. Cela peut aider à réduire la charge émotionnelle de la situation et faciliter une conversation future productive avec Marc.

Il est important de noter que les contre-exemples donnés ici, démontrent des communications qui peuvent être perçues comme agressives, accusatrices ou peu constructives. La Communication NonViolente encourage une approche empathique, basée sur l'observation objective, l'expression des émotions ressenties, l'identification des besoins sous-jacents et la formulation de demandes claires et réalisables.

# Décortiquer le DESC (Description, Émotion, Solution, Conséquence)

Le DESC est fortement inspiré de l’OSBD, et n’est pas réellement un outil estampillé CNV, il est issu du livre « *Asserting yourself* », écrit par Sharon Anthony Bower et Gordon Howard Bowe dans les années 70.

C’est une approche qu’on retrouve dans de nombreuses formations pour manager, comme proposition pour un comportement assertif.

Regardons les nuances qu’on peut y trouver :

### D. Description : Décrire objectivement la situation problématique

En décrivant les faits de manière objective, nous créons un terrain commun pour aborder le problème.

Exemple 1 : 

> Lors des dernières présentations, j'ai remarqué que certaines parties étaient incomplètes ou manquaient de cohérence.

Exemple 2 : 

> Nous avons constaté que les retards dans le traitement des demandes clients ont entraîné une insatisfaction croissante chez nos clients.

### E. Émotion : Exprimer les émotions suscitées par la situation

En exprimant nos émotions, nous permettons aux autres de comprendre l'impact émotionnel de la situation.

Exemple 1 : 

> Je me sens préoccupé et frustré lorsque nous ne parvenons pas à offrir des présentations de haute qualité, car j'ai à cœur de représenter notre entreprise de manière professionnelle.

Exemple 2 : 

> Je suis stressé et inquiet face aux retours négatifs des clients, car j'ai à cœur de fournir un service de qualité et de maintenir notre réputation.

### S. Solution : Proposer des idées ou des actions pour résoudre le problème

En proposant des solutions constructives, nous encourageons l'engagement et la recherche de résolutions efficaces.

Exemple 1 : 

> Je suggère de mettre en place un processus de préparation et de révision plus rigoureux pour nos présentations, afin d'améliorer leur qualité globale.

Exemple 2 : 

> Je propose de réorganiser notre système de traitement des demandes clients en mettant en place des étapes de suivi plus précises et une répartition équitable des tâches.

### C. Conséquence : Expliquer les bénéfices attendus en mettant en place la solution

En exposant les bénéfices potentiels, nous motivons les autres à soutenir et à participer activement à la mise en œuvre des solutions.

Exemple 1 : 

> En améliorant la qualité de nos présentations, nous pourrons mieux communiquer notre expertise et renforcer notre crédibilité auprès des clients.

Exemple 2 : 

> En optimisant notre processus de traitement des demandes clients, nous pourrons offrir un service plus rapide et plus efficace, ce qui se traduira par une satisfaction client accrue et de meilleures relations commerciales.

# Comparaison et application des deux approches

L'OSBD et le DESC partagent des similitudes dans leur approche centrée sur la communication efficace, mais présentent également des différences dans leur structure et leur focus. Si les deux premières étapes sont très similaires, notez que les 2 dernières ont une intention très différente. 

* Dans le cas de l’OSBD, l’attention est sur le problème (le Besoin), puis sur une Demande de contribution à la Solution. On se met au même niveau que l’interlocuteur.
* Dans le cas du DESC, on pointe sur une Solution puis on la “vend”, en vantant ses conséquences. On prend donc une position de lead, plus assertive, sur les actions à venir.

### Exemples concrets d'application de l'OSBD et du DESC en entreprise

Voici deux exemples concrets d'application de l'OSBD et du DESC en entreprise :

#### Utilisation de l'OSBD pour résoudre des conflits interpersonnels :

Vous êtes manager d'équipe et vous constatez, lors de votre réunion hebdomadaire des tensions récurrentes entre 2 de vos collègues. Plutôt que de laisser la situation s'envenimer, voilà comment vous pouvez appliquer l'OSBD

Observation : "Lors de notre réunion d'équipe, j'ai observé des tensions entre deux collègues."

Sentiment : "Cela me rend mal à l'aise et inquiet quant à l'impact sur notre collaboration."

Besoin : "J'ai besoin d'un environnement de travail harmonieux et de relations positives entre les membres de l'équipe."

Demande : "Pouvez-vous tous les deux discuter ensemble pour comprendre les problèmes et trouver une solution qui convienne à tous ?"

#### Application du DESC pour améliorer les processus opérationnels :

Le DESC est approprié pour proposer des solutions sur un problème systémique :

Description : "Nous constatons des erreurs fréquentes dans le processus de gestion des stocks."

Émotion : "Cela me frustre et nuit à notre efficacité globale."

Solution : "Je propose de mettre en place un système de vérification des stocks plus rigoureux et d'organiser une formation sur les bonnes pratiques de gestion."

Conséquence : "En mettant en œuvre ces mesures, nous réduirons les erreurs de stock et améliorerons notre productivité."

# Quelles avantages vous tirerez à appliquer la CNV dans votre environnement professionnel ?

### D'abord, vous renforcez des relations interpersonnelles et de l'esprit d'équipe

Une Communication NonViolente favorise un climat de confiance, d'écoute active et de respect mutuel, ce qui renforce les relations interpersonnelles et contribue à un meilleur travail d'équipe \
En utilisant la CNV, les employés développent une compréhension mutuelle et un soutien empathique, ce qui facilite la collaboration et la résolution de problèmes en équipe.

Une Communication NonViolente encourage la reconnaissance des efforts et des contributions de chacun, renforçant ainsi le sentiment d'appartenance et l'engagement envers les objectifs communs.

### Ensuite, vous améliorerez très efficacement la résolution de problèmes et de la prise de décision

La CNV permet d'identifier les besoins et les intérêts de chaque partie, favorisant ainsi une résolution de problèmes plus efficace et une prise de décision plus éclairée.

En utilisant l'OSBD, les employés peuvent exprimer leurs besoins et leurs préoccupations lors de discussions sur des problèmes complexes, ce qui permet de trouver des solutions plus créatives et adaptées à tous.

En appliquant le DESC, les équipes peuvent évaluer objectivement les différentes options, exprimer leurs émotions et proposer des solutions concrètes pour atteindre un consensus et prendre des décisions éclairées. 

# Pour aller plus loin : le lien avec la ségrégation de la pensée et les Chapeaux de Bono

(ce paragraphe fait référence à [un précédent article](/posts/20230614-les-six-chapeaux-de-bono/)… )

Les Chapeaux de Bono, développés par Edward de Bono, sont un outil qui encourage la réflexion et la communication structurées en utilisant différents modes de pensée.

Par exemple, le Chapeau Rouge représente les émotions, permettant d'exprimer les sentiments et les intuitions liés à une situation.Le Chapeau Vert, lui, favorise la créativité et la génération d'idées, en encourageant la pensée divergente et l'exploration de nouvelles possibilités.

En utilisant les Chapeaux de Bono en conjonction avec la CNV, il est possible d'élargir la pensée, de considérer différentes perspectives et d'éviter les biais cognitifs qui pourraient limiter la communication et la résolution de problèmes.

 \
Exemple 1 : En combinant l'OSBD avec le Chapeau Jaune (qui représente le raisonnement positif), il est possible de formuler des observations objectives tout en mettant l'accent sur les aspects bénéfiques et les solutions potentielles.

Exemple 2 : En intégrant le DESC avec le Chapeau Noir (qui représente le jugement critique), on peut exprimer les émotions liées aux aspects négatifs d'une situation tout en identifiant les problèmes et les risques potentiels.

La CNV et les Chapeaux de Bono sont des approches complémentaires qui peuvent être utilisées conjointement pour clarifier sa pensée et ainsi avoir une communication plus efficace.

# À vous de jouer !

La Communication NonViolente est un outil puissant pour favoriser des interactions harmonieuses et constructives au sein des entreprises. En utilisant l'approche OSBD, les individus peuvent exprimer leurs observations, sentiments, besoins et demandes de manière claire et respectueuse. De plus, le modèle DESC permet de décrire objectivement les problèmes, exprimer les émotions, proposer des solutions et anticiper les conséquences. En combinant ces approches, les professionnels peuvent améliorer leur communication, résoudre les conflits, renforcer les relations interpersonnelles et améliorer l'efficacité organisationnelle. En intégrant également les Chapeaux de Bono, il est possible d'élargir la réflexion, d'éviter les biais cognitifs et d'encourager une pensée plus créative et globale. La Communication NonViolente et les Chapeaux de Bono se complètent mutuellement pour favoriser une communication efficace, une résolution de problèmes approfondie et une prise de décision éclairée au sein des entreprises.
