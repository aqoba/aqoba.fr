---
draft: false
title: People Ops, le futur des RH est déjà là !
values:
  - humanite
  - conviction
summary: Le mouvement People Ops représente un changement radical dans la façon
  dont les entreprises gèrent leur capital humain en mettant l'accent sur
  l’expérience collaborateur et des pratiques innovantes de gestion des
  ressources humaines. People Ops ne remplace pas les fonctions régaliennes
  d’une RDH mais vient les intégrer dans une approche centrée sur l'expérience
  collaborateur.
authors:
  - olivier-marquet
tags:
  - RH
  - PeopleOps
  - Culture
  - PeopleFirst
date: 2023-03-16T06:42:40.169Z
thumbnail: peopleops_smileys.jpeg
subtitle: Tout ce que vous devez retenir du mouvement qui bouscule la culture RH
  traditionnelle
---
L'approche People Ops tire ses origines de la société Google, où elle a été développée dans les années 2000 sous le nom de "People Operations" (ou POPS en abrégé). Google avait alors fait le constat que pour rester compétitif sur un marché en constante évolution, il était crucial de s'assurer d'embaucher les bons collaborateurs, mais aussi que ceux-ci soient engagés et motivés, et que leur travail soit aligné sur les objectifs de l'entreprise.

Pour atteindre ces objectifs, Google a créé une équipe spéciale dédiée aux ressources humaines, qui avait pour mission de mettre en place des politiques et des pratiques innovantes pour recruter les meilleurs talents et surtout les garder. Cette équipe était dirigée par Laszlo Bock, qui a depuis publié un livre sur le sujet intitulé "Work Rules!".

Nous avons d’ailleurs écrit un article basé sur ce livre et expliquant [comment Google à façonné sa culture d’entreprise](https://aqoba.fr/posts/20220609-comment-google-a-t-il-fa%C3%A7onn%C3%A9-sa-culture-dentreprise/).

Google a également mis en place une série de pratiques novatrices pour améliorer l'expérience collaborateur, telles que la flexibilité dans les horaires de travail, la culture de l'innovation et de l'expérimentation, le développement des compétences et des carrières, ou encore la transparence et la communication ouverte.

L'approche People Ops de Google a été saluée pour son innovation et son efficacité, et elle a inspiré de nombreuses autres entreprises à adopter des pratiques similaires. 

## Comment peut-on résumer cette approche People Ops aujourd’hui ?

People Ops est une nouvelle approche des Ressources Humaines qui met l'expérience des employés et leur engagement au centre de ses préoccupations et ce depuis le recrutement jusqu’à la sortie de l’entreprise. En effet, l'expérience collaborateur est devenue un enjeu crucial pour les entreprises modernes. Les entreprises qui réussissent à créer un environnement de travail positif et engageant peuvent non seulement attirer et retenir les meilleurs talents, mais également améliorer la satisfaction, l'engagement et la performance de leurs employés, ainsi que leur marque employeur.

Contrairement aux méthodes traditionnelles de gestion des ressources humaines, qui sont souvent centrées sur les processus administratifs et la conformité réglementaire, l'approche People Ops est donc axée sur les besoins des employés en tant qu'individus et sur la création d'un environnement de travail sain et collaboratif. 

{{< figure  src="peopleops_retourfutur.jpeg" alt="People Ops de retour du futur" class="center-small">}}

L'approche People Ops est fondée sur trois piliers principaux : la culture d'entreprise, la communication et l’ergonomie. En combinant ces trois éléments, les entreprises peuvent créer une expérience de travail cohérente et engageante pour leurs employés.

**La culture d'entreprise** est la base de l'approche People Ops. Les entreprises qui réussissent à créer une culture forte et positive sont mieux à même de retenir les meilleurs talents et d'inspirer l'engagement des employés. Pour y parvenir, les entreprises doivent travailler à recruter les bonnes personnes et développer une culture qui valorise l'innovation, la collaboration et la responsabilité.

**La communication** est aussi un élément clé de l'approche People Ops car elle permet de créer un environnement de travail transparent, ouvert et collaboratif. Les entreprises qui réussissent à communiquer efficacement avec leurs employés peuvent inspirer l'engagement, la motivation et la loyauté de ces derniers.

Dans l'approche People Ops, la communication est bidirectionnelle ce qui signifie que chacun s'écoute et prend en compte les feedbacks de l'autre. Les employés sont encouragés à partager leurs idées et préoccupations, tandis que les responsables s'engagent à leur fournir des informations claires et régulières sur les objectifs et les attentes de l’entreprise ainsi que les opportunités d’évolution. Les feedbacks des employés sont pris en compte pour améliorer le fonctionnement de l'entreprise et les conditions de travail. Cet échange permet une collaboration plus efficace et une meilleure compréhension mutuelle.

**Enfin, l’ergonomie** joue un rôle important dans l'approche People Ops car elle permet de créer une expérience de travail personnalisée et positive pour les employés. Le travail de l'ergonomie est l'adaptation de l'environnement de travail (outils, matériel, organisation…) aux besoins de l'utilisateur et va passer aussi en partie par la mise en place des outils les plus adaptés. 
Cela est d’autant plus vrai aujourd’hui avec la standardisation du télétravail, du flex office ou du nomadisme et le besoin d’outils efficaces pour travailler à distance. Les entreprises peuvent choisir les meilleurs outils pour favoriser les comportements attendus (et changer les mauvais qui les freinent) et ainsi se démarquer. 
Exemple: un process de saisie des temps et de pose de congé rapide et facile, une application mobile facilitant la réservation de bureaux, des logiciels de communication qui facilitent réellement l’expression et l’échange, ...
L’utilisation d’outils numériques permet aussi l’automatisation des tâches RH les plus manuelles et rébarbatives pour libérer du temps et permettre de se concentrer sur les activités à forte valeur ajoutée.

## People Ops est une approche différente des RH traditionnelles:

<br />

<style>
table {
	border-collapse: collapse;
    font-family: Tahoma, Geneva, sans-serif;
}
table td {
	padding: 15px;
}
table thead td {
	background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;
}
table tbody td {
	color: #636363;
	border: 1px solid #dddfe1;
}
table tbody tr {
	background-color: #f9fafb;
}
table tbody tr:nth-child(odd) {
	background-color: #ffffff;
}
table th:first-of-type {
    width: 50%;
}
table th:nth-of-type(2) {
    width: 50%;
}
</style>

| RH traditionnelle                                                    | People Ops                                                                                   |
| -------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| Centré sur les processus administratifs et le réglementaire          | Centré sur l'expérience collaborateur et sur la maximisation de la valeur                    |
| Est informé des décisions et sommé d'exécuter                        | Est consulté sur les décisions stratégiques et est à l'initiative d’une partie des décisions |
| Fonctionne dans les limites étroites de ses prérogatives historiques | Adopte une vision plus large et holistique de l'entreprise et travaille la culture           |
| Répond aux problèmes au fur et à mesure qu'ils surviennent           | Anticipe les problèmes de manière proactive                                                  |
| Embauche un remplaçant lorsqu'un poste est vacant                    | Travaille la rétention des collaborateur                                                     |
| Travaille séparément des autres départements                         | Connectent tous les départements entre eux et travaille avec eux en étroite collaboration    |

{{< figure  src="peopleops_collaborateur.jpeg" alt="Collaborateur souriant" class="center-small">}}

## People Ops et Product Management

Se préoccuper de l'expérience collaborateur dans l'approche People Ops peut être comparé à l'approche produit dans le Product Management qui est centrée sur l'utilisateur, car dans les deux cas, l'objectif est de comprendre les besoins et les attentes des utilisateurs (qu'ils soient des clients ou des employés) et de créer des solutions qui répondent à ces besoins.

Dans l'approche produit, les Product Managers passent du temps à comprendre les besoins et les attentes des clients, à recueillir des commentaires sur les produits existants, à mener des études de marché et à travailler en étroite collaboration avec les équipes de conception et de développement pour créer des produits qui répondent aux besoins des utilisateurs.

De la même manière, dans l'approche People Ops, les responsables des ressources humaines passent du temps à comprendre les besoins et les attentes des employés, à recueillir des commentaires sur les politiques et les pratiques existantes, à mener des enquêtes et à travailler en étroite collaboration avec les équipes de gestion pour créer des politiques et des pratiques qui répondent aux besoins des employés.

{{< figure  src="peopleops_creatif.jpeg" alt="Créativité" class="center-small">}}

## Les outils du Product Management au service des RH

Les outils du Product Management et de l’UX design peuvent être utilisés pour améliorer l'expérience collaborateur dans le cadre de l'approche People Ops.

Par exemple, la Customer Journey (le parcours client) peut être utilisée pour cartographier les différentes étapes du parcours d'un employé, depuis la recherche d'un emploi jusqu'à la fin de son emploi, en passant par la sélection, l'intégration, la formation, la performance, la reconnaissance, le développement, la rémunération, etc. En cartographiant le parcours du collaborateur, les responsables des ressources humaines peuvent identifier les points de friction, les irritants, les opportunités d'amélioration et les moments clés qui ont le plus d'impact sur l'expérience de l'employé.

De même, l’Impact Mapping (la cartographie des impacts) peut être utilisé pour identifier les facteurs qui ont le plus d'impact sur l'expérience collaborateur, tels que la culture d'entreprise, le leadership, le développement professionnel, la rémunération et les avantages sociaux, etc. En cartographiant ces facteurs, les responsables des ressources humaines peuvent identifier les leviers les plus importants à mettre en œuvre pour améliorer l'expérience collaborateur et allouer les ressources nécessaires pour y parvenir.

En utilisant les outils du Product Management, les responsables des ressources humaines peuvent obtenir une vue d'ensemble de l'expérience collaborateur et identifier les domaines où des améliorations sont nécessaires. Ils peuvent ensuite travailler en étroite collaboration avec les équipes de gestion pour mettre en place des solutions qui répondent aux besoins des employés et ainsi améliorer leur expérience globale.

Nous invitons donc tout particulièrement les responsables des ressources humaines qui voudraient s’engager dans la voie du People Ops à découvrir ces outils du Product Management qui seront probablement une grande source d’inspiration.

## En conclusion ...

Le mouvement People Ops représente un changement radical dans la façon dont les entreprises gèrent leur capital humain en mettant l'accent sur l’expérience collaborateur et des pratiques innovantes de gestion des ressources humaines. People Ops ne remplace pas les fonctions régaliennes d’une DRH mais vient les intégrer dans une approche centrée sur l'expérience collaborateur.

En investissant dans le People Ops, les entreprises peuvent se positionner à l'avant-garde de cette nouvelle culture RH et bénéficier d'une meilleure performance globale, d'une plus grande fidélisation des employés et d'une image de marque employeur plus forte. En somme, le People Ops est bien plus qu'un simple mouvement, il s'agit d'une transformation en profondeur de la gestion des ressources humaines pour mieux répondre aux besoins et attentes des employés d'aujourd'hui.

Sur un marché de l’emploi très tendu, certaines DRH ont déjà pris ce virage, leurs entreprises sont désormais mieux armés pour attirer et fidéliser les meilleurs talents. Chez aqoba nous sommes convaincus que les organisations françaises doivent être pionnières de ce mouvement.
