---
draft: false
title: "Agilité en famille : la lettre au Père Noël"
authors:
  - thomas-clavier
values:
  - conviction
  - partage
tags:
  - agilité en famille
  - product owner
  - agile
offers:
  - unlock-delivery
date: 2023-11-27T08:54:48.016Z
thumbnail: lettrenoel.jpg
subtitle: Comment la culture agile s'immisce partout
---
Je suis tombé dans l'agilité au début des années 2000, je ne crois pas qu’il y ait eu un élément déclencheur. Je me souviens qu’en 2003 j’étais au XP Days Benelux et en 2005 mon client de l'époque avait demandé au stagiaire de défricher pour nous le sujet d'eXtreme Programming, j'avais trouvé ça dommage de ne pas avoir été consulté. En 2006 ma fille est arrivée 2 mois avant que je n'aille à la première édition des XP days France. Encore en 2006 j’ai eu mon premier post de scrum master. Mais tout ça c’était dans le monde professionnel. À la maison aussi j’ai utilisé quelques éléments de la culture agile.

Cet article était prévu comme le premier d’une série d'articles narrant quelques histoires qui se sont déroulées chez moi durant ces années agiles en famille. Finalement, c’est le second de la série.

## Une liste au père Noël pour se mettre dans la peau d'un product owner

En 2006 ou 2007, j’ai travaillé avec [Jean Claude Grosjean](https://www.linkedin.com/in/jeanclaudegrosjean/), alors quand en 2008 il publie [un premier article sur la liste au père noël](http://www.qualitystreet.fr/2008/11/25/des-priorites-pour-le-pere-noel/) j’étais impatient de pouvoir l’expérimenter avec ma fille. Je n'ai eu que 2 ans à attendre et je crois que la dernière année où nous l’avons fait c’était en 2019

Cet exercice, très attendu chaque année par mes enfants se déroulait de la façon suivante : 


* Collecte des vœux des enfants en découpant les magazines de jouets. Et déjà là,  il y a des choix à faire, les magazines sont imprimés en recto-verso.


{{< figure  src="decoupage.jpg" alt="Première priorisation : le découpage" class="center-small">}}


* Sélection et priorisation des vœux en utilisant la largeur de la table comme limite : le plus important à droite, le moins important à gauche, pas deux vœux qui se superposent.

{{< figure  src="selection.jpg" alt="2ème priorisation : la longueur contrainte" class="center-small">}}

* Mise en forme sur une grande feuille A3 de tous ces vœux avant de l’envoyer aux –père Noël– grands parents.

{{< figure  src="lalettre.jpg" alt="le lettre sans la déco" class="center-small" caption="La lettre avant la décoration et les règles de lecture">}}

Dans cet atelier, nous avons les étapes du premier diamant du désign sprint, la divergence, découper tout pleins de cadeaux dans les magazines, puis la convergence, avec la priorisation et la sélection. La 3ème étape est cruciale : le partage. Car comme pour le père noël, le PO doit absolument se faire comprendre des équipes techniques, c’est ce qui sera compris par les développeurs qui partira en production. Du côté du père noël, même chose, la compréhension des grands parents est capitale, ce serait dommage de ne pas avoir la bonne boite de légo. 

Si vous souhaitez plus de détail sur ces ateliers je vous invite à lire [les 5 articles de JC sur le sujet](http://www.qualitystreet.fr/2012/11/18/une-lettre-au-pere-noel-agile-acte-5/)

## C’est bientôt Noël !

C’était l’épisode le plus ancien de la série “l’agilité en famille”, une série d’histoires racontant la façon dont l’agilité s’est retrouvée distillée à la maison. J’espère que cela sera source d’inspiration pour vous.
