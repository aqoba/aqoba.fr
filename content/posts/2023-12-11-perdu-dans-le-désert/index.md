---
draft: false
title: Perdu dans le désert
authors:
  - arnaud-bracchetti
values:
  - partage
  - conviction
tags:
  - Serious Game
offers:
  - unlock-management
date: 2023-12-11T20:39:01.941Z
thumbnail: perdu_desert.png
subtitle: L'importance du defrief
---
Passionné de jeux de société depuis toujours, je me suis naturellement tourné vers l'univers fascinant des serious games (ou agile games). Ces jeux, au-delà de leur aspect ludique, offrent une occasion unique de réflexion sur nos méthodes de travail et nos comportements interpersonnels, le tout dans un cadre convivial et engageant. Mais l'aspect le plus crucial de ces jeux, souvent sous-estimé, réside dans leur phase de débriefing.

Un débrief efficace, mené par un animateur avec une perspective extérieure, transforme une simple session de jeu en une expérience d'apprentissage profonde et réfléchie. C'est durant ce moment que les participants peuvent analyser et comprendre leurs comportements et interactions durant le jeu.

Pour illustrer concrètement cette dynamique, prenons l'exemple d'un jeu qui me tient particulièrement à cœur : «Perdu dans le désert». Au fil de cet article, je partagerai les enseignements tirés d'une dizaine de sessions, révélant comment un débriefing bien conduit peut enrichir l'expérience de jeu et offrir des insights précieux tant sur le plan personnel que professionnel.


## Le jeu 

Le jeu 'Perdu dans le désert' est un serious game qui met en lumière les comportements de chacun dans la dynamique de groupe et dans les prises de décisions collectives.

Imaginez : suite à un crash d'avion, vous vous retrouvez, avec votre équipe, en plein désert. Vous allez devoir survivre, mais parmi les 15 objets que vous avez récupérés, lesquels sont les plus cruciaux ?

Le principe du jeu est le suivant :



1. L'animateur du jeu présente la [situation aux joueurs](Perdus_dans_le_désert_-_description.pdf) : Ils sont les seuls survivants d'un crash d'avion, et se retrouvent en plein désert avec une liste de 15 objets à leur disposition.
2. Dans un premier temps, l'animateur demande à chaque personne de faire un classement personnel des objets à disposition en les numérotant de 1 à 15 du plus prioritaire au moins prioritaire.
3. Dans un second temps, les participants au jeu doivent tous ensemble refaire le même exercice. Tous les participants doivent donc avoir la même liste. C’est le moment crucial du jeu, l’animateur doit être extrêmement vigilant sur ce point et ne pas hésiter à prendre des notes qui pourront être utilisés lors de débriefing.
4. L'animateur donne alors [les réponses](Perdu_dans_le_desert_-_debrief.pdf) de références (issues de [spécialistes de la survie en milieu hostile](Desert-Survival-Expert-Rationale.pdf)). Les joueurs doivent calculer pour chaque objet, la valeur absolue de la différence, entre la réponse donnée et leurs choix personnels, pour leur choix personnel, et le choix du groupe. 
5. Les joueurs calculent ensuite leur score personnel en additionnant les résultats du premier calcul pour chaque objet, et le score du groupe en additionnant les résultats du deuxième calcul pour tous les objets. Plus ce score est faible, plus vous êtes proche des résultats préconisés par les experts.


## Le Débriefing : Le Moment Clé de tout serious game

Au-delà des scores obtenus par chacun, en tant qu'animateur, mon rôle est d'amener les joueurs à réfléchir sur les raisons des résultats obtenus. 

Pour cela, j'emploie plusieurs techniques :

**Questions ouvertes** : Je commence par des questions larges pour encourager la réflexion individuelle, puis j'oriente progressivement la discussion vers des aspects plus spécifiques du jeu.

**Réflexion guidée** : J'amène les participants à réfléchir non seulement sur leurs choix, mais aussi sur leur comportement et leur interaction avec les autres. Pour cela, j’utilise les observations que j’ai faites lors du jeu, et en particulier sur la phase collective.

**Exemples concrets** : J'utilise des situations spécifiques survenues pendant le jeu pour ancrer la discussion dans des expériences réelles.


## Les leçons à tirer du jeu pour l’animateur.

Chaque session de 'Perdu dans le désert' offre des leçons uniques. Même si on retrouve certains comportements d’un groupe à l’autre, chaque groupe se comporte différemment. C’est un point important à considérer pour l’animateur, le débriefing ne peut pas être préparé à l’avance. La règle d’or ici est l’observation et la mise en évidence de certains comportements pour faire réfléchir le groupe. Et n’oublions pas qu’un debrief n’est pas forcément là pour mettre l’accent sur les points négatifs, si les choses se passent bien, il faut le dire afin de faire prendre conscience à chacun de ces bons comportements qui parfois sont faits de façon inconsciente.


## Mon expérience personnelle

J'ai animé ce jeu une dizaine de fois et j'aimerais partager un bilan des deux problèmes qui reviennent le plus régulièrement.


### Le manque de vision

Ce problème est le plus courant. Souvent, les groupes ne prennent pas le temps, au début de l'exercice, de définir clairement leur vision ou stratégie. Comment vont-ils s'en sortir ? Il y a en réalité deux orientations possibles :

Soit rester sur place en attendant les secours (qui est la solution préconisée par les experts de la survie), en donnant la priorité au signalement de notre position.

Soit se déplacer pour rejoindre une zone civilisée, en donnant la priorité à la mobilité et à l’orientation.

Parmi tous les groupes avec lesquels j'ai joué, un seul a vraiment pris le temps de définir sa vision. Ils ont eu une discussion approfondie sur la stratégie à adopter et se sont mis d'accord sur une vision commune. Et, sans surprise, c'est ce groupe qui a obtenu le meilleur score, et ce, en moins de temps que les autres.

Si on transpose ceci à la gestion de projet, ce jeu peut révéler l'importance de partager une vision commune dès le début. Une fois que tout le monde (développeurs, product owners, scrum masters, clients, sponsors, etc.) partage la même vision, le processus de décision devient plus simple et plus efficace. J'utilise d'ailleurs ce jeu dans mes formations pour illustrer l'importance de la vision dans un projet.


### Le consensus mou

Une dynamique que j'ai souvent observée est le phénomène du «consensus mou». Ce terme décrit une situation où, dans le désir de «faire plaisir à tout le monde», les participants évitent les confrontations et aboutissent à des décisions tièdes et non optimales. À ce sujet, je recommande la lecture de l’ouvrage de Patrick Lencioni, [Optimisez votre équipe](https://www.librairiesindependantes.com/product/9782892256031/), qui explique l'importance du conflit dans les équipes performantes.

Le consensus mou résulte souvent d'un manque d'orientation et de stratégie. Bien qu'il semble bénéfique à court terme car il minimise les conflits, il est souvent contre-productif car il empêche l'émergence de solutions innovantes ou tranchées. Dans le jeu, cela se traduit par des choix qui reflètent une moyenne des opinions plutôt que la meilleure pensée collective. Le débriefing devient alors un espace crucial pour discuter de ce phénomène.


### Le manque d'écoute

Ce point revient aussi fréquemment, surtout quand le groupe n'a pas de vision claire. Le manque d'écoute se manifeste souvent par une ou deux personnes prenant le leadership du groupe et imposant leurs choix, souvent de manière inconsciente. Dans ces groupes, les personnes plus réservées n'osent pas s'exprimer.

Avoir des leaders est bénéfique pour dynamiser un groupe, mais cela ne signifie pas que leurs idées sont toujours les meilleures. L'écoute est cruciale pour prendre de bonnes décisions collectives. Le groupe doit trouver un moyen de favoriser l'expression et l'écoute de chacun.

Encore une fois, en termes de gestion de projet, ce jeu souligne l'importance du rôle du Scrum Master dans le groupe. Le Scrum Master n'est pas là pour prendre des décisions, mais pour observer le fonctionnement du groupe avec du recul et veiller à ce que tout le monde puisse s'exprimer et contribuer efficacement.


## En Conclusion

À partir d'un simple jeu, il est possible, en débriefant correctement le comportement des joueurs, d'illustrer des points clés du comportement de groupe. Cela demande toutefois un peu d'expérience de la part de l'animateur pour bien identifier et discuter des comportements pertinents.
