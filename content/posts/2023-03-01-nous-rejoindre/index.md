---
draft: false
authors:
  - olivier-marquet
  - arnaud-bracchetti
  - antoine-marcou
  - laurent-dussault
  - thomas-clavier
values:
  - altruisme
  - conviction
  - humanite
  - partage
  - transparence
date: 2022-03-11T08:00:23.549Z
thumbnail: banner-nous-rejoindre.jpeg
aliases:
  - /nousrejoindre
  - /apropos
  - /ditesmenplus
title: La FAQ aqoba
tags:
  - inside
  - aqoba
  - recrutement
  - Nos valeurs
subtitle: Réponses aux questions que vous vous posez peut être sur aqoba
---
Et si vous voulez postuler, c'est [ICI](https://www.linkedin.com/company/aqoba-fr/jobs/?viewAsMember=true) que ca se passe ou en nous contactant directement sur [contact@aqoba.fr](mailto:contact@aqoba.fr) !

-------


{{< toc >}}

<a id='qui'></a>

## Qui êtes-vous ?

C’est simple : nous sommes une [équipe](https://aqoba.fr/team/) de coachs et de consultants. Une petite équipe. Qui vit par et pour ses valeurs. Qui choisit les personnes avec lesquelles elle travaille, que ce soit nos collaborateurs ou nos clients. Et qui cherche à changer les choses, en contribuant à redonner du sens au monde du travail tout en améliorant la performance des entreprises françaises. 

<a id='raison'></a>

## Quelle est la raison d’être d’aqoba ?

Nous nous levons le matin pour aider les organisations françaises à se transformer. Nous leur apprenons à changer pour améliorer leur performance et le bien-être de leurs collaborateurs. En un mot, aqoba vise à améliorer la qualité de vie au travail et la réussite de nos entreprises françaises.

<a id='valeurs'></a>

## Pourquoi les valeurs sont si importantes chez aqoba ?

Nos valeurs sont un élément crucial pour nous car elles constituent la boussole qui guide nos actions et nos décisions. Chez aqoba, ces valeurs sont au cœur de notre culture d'entreprise et de notre manière de travailler. Elles ne sont pas là pour être affichées sur un mur. Mais pour être vécues par chaque membre de l’équipe et font partie intégrante de notre identité.

Nos 5 valeurs : [Humanité](#humanite), [Transparence](#transparence), [Conviction](#conviction), [Altruisme](#altruisme) et [Partage](#partage)

Nous sommes convaincus que l'adhésion à ces valeurs est la clé de notre réussite et de celle de nos clients. Nous recherchons donc des collaborateurs qui partagent ces valeurs et qui souhaitent s'investir dans une équipe exigeante et engagée.

<a id='incarnation'></a>

## Comment s'incarnent ces valeurs pour les salariés ?

Pour nous, la notion de valeur est extrêmement importante. Dès les prémices de la création d’aqoba, nous les avons gravées et nous sommes faits la promesse de les suivre jour après jour, décision après décision. Nous ne voulions, pas en faire un élément marketing, mais un guide, un outil qui pourrait permettre à chaque aqobien de bien comprendre ce qu’on attend de lui. 

Ces valeurs sont: [Humanité](#humanite), [Transparence](#transparence), [Conviction](#conviction), [Altruisme](#altruisme) et [Partage](#partage).

Ce ne sont pas simplement des mots : elles guident toutes nos actions et nos comportements en interne et avec nos clients.

**Au travers de cette FAQ, nous avons cherché à expliquer et à illustrer comment ces valeurs sont réellement incarnées chez aqoba.**

<figure class="center-small">

<video controls width="100%">
  <source src="/videos/aqoba-header.mp4" type="video/mp4">
  <source src="/videos/aqoba-header.webm" type="video/webm">
  <p>Votre navigateur ne prend pas en charge les vidéos HTML5.</p>
</video>

</figure>

<a id='humanite'></a>

### Vous pouvez m’en dire plus sur la valeur Humanité ?

En interne, nous souhaitons construire une équipe dans laquelle chaque personne s'épanouisse et apporte sa pierre à la culture du groupe. Pour cela, nous accompagnons chaque aqobien avec 3 ambitions :

* que chacun se sente pleinement appartenir au groupe
* que chacun se sente autonome
* et que chacun trouve sincèrement du sens à son travail

Ainsi, chacun se sentira légitime pour apporter sa pierre à l'édifice. 

Avec nos clients, cette valeur se traduit par le fait que nous pensons nos missions non pas comme l'accompagnement d'une structure, mais plutôt des personnes qui la composent.

**Concrètement, tout est mis en œuvre pour permettre à chaque aqobien de travailler dans le respect de ses valeurs et de se constituer une identité de groupe forte qui lui permettra d’agir pour le bien de la structure.**

**In fine, c'est l'intérêt du collaborateur qui sera le plus important.**

<a id='transparence'></a>

### Vous pouvez m’en dire plus sur la valeur Transparence ?

Chez aqoba, la transparence c’est la capacité de chacun de pouvoir collaborer de façon honnête et sans arrière pensée avec ses différents interlocuteurs. 

En interne, la transparence appelle la tolérance, la bienveillance et le droit à l’erreur. Pour que la transparence puisse être réellement vécue chez aqoba, il est indispensable que chacun d’entre nous se sente en sécurité dans l’environnement au sein duquel il évolue.

**Concrètement, nous comprenons que nous avons tous le droit à l’erreur au sein d’aqoba, nous sommes avant tout une équipe qui s'entraide. Nous sommes aussi transparents sur nos actions en interne et nous partageons tous les mois l’avancement des sujets internes à l’entreprise que ce soit coté commerce, marketing, recrutement ou autre.**

En mission, nous nous considérons comme des partenaires de nos clients, et non des fournisseurs. Nous sommes donc transparents avec eux sur ce qui fonctionne ou ne fonctionne pas. Nous attendons de leur part la même transparence quant à notre travail. C’est elle qui nous fait grandir et assure le succès de nos missions.

<a id='conviction'></a>

### Vous pouvez m’en dire plus sur la valeur Conviction ?

Nous attendons de chaque aqobien qu’il vienne renforcer l’équipe avec ses propres convictions, des convictions fortes et sincères qu’il / elle a construites au fil de ses expériences. Elles nous permettent de nous questionner et d’apporter des solutions claires et cohérentes aux problèmes que nous rencontrons.

**Concrètement, en interne, les convictions portées par chaque aqobien sont une source d’échange et de challenge lors de nos rencontres. Elles nous permettent de nous affirmer et nous obligent à construire des argumentaires et un discours clair pour défendre nos points de vue tout en écoutant les autres.**

Chez nos clients, les convictions nous servent de guide pour résoudre de façon claire et cohérente les problématiques auxquelles nous sommes confrontés. En apportant des réponses dans lesquelles nous croyons réellement, nos convictions nous permettent de faire adhérer le plus grand nombre aux solutions que nous pouvons.

Avoir des convictions ne signifie pas avoir une vision unique des réponses à apporter à un problème, mais plutôt croire réellement à ce que nous faisons. C’est aussi, écouter et challenger ceux qui nous entourent pour élargir et faire évoluer nos points de vue.

<a id='altruisme'></a>

### Vous pouvez m’en dire plus sur la valeur Altruisme ?

Chez aqoba, l’altruisme, c’est la volonté de ne pas tourner toutes nos décisions uniquement vers le profit. C’est la capacité que chacun doit avoir, à collaborer avec le plus grand nombre, de façon désintéressée, pour faire avancer nos idées et nos convictions.

**Concrètement, en interne, c’est la transparence nécessaire sur la situation de l’entreprise, pour que chaque aqobien puisse juger par lui-même de la capacité de l’entreprise à se lancer dans des actions désintéressées qui porteront nos valeurs et nos convictions. C’est aussi la possibilité de solliciter n’importe quel aqobien pour répondre un problème complexe sans que la question de la facturation ne soit un frein.**

Chez nos clients, c’est la volonté de chaque aqobien d’aider son client sans objectif direct de profit, simplement parce que nous sommes alignés sur une vision et des valeurs communes. Ceci permet d’apporter, honnêtement, les meilleures réponses aux problématiques de nos clients, en créant un véritable esprit de collaboration.

Bien évidemment, l’altruisme ne peut pas être notre seul mode de fonctionnement, mais nous sommes tous convaincus qu’affirmer régulièrement nos convictions de façon altruiste ne peut être que profitable à nos clients sur le court terme, et à aqoba sur le moyen terme.

<a id='partage'></a>

### Vous pouvez m’en dire plus sur la valeur Partage ?

La connaissance est un bien que l’on peut donner sans avoir à s’en séparer. Chez aqoba, nous souhaitons pouvoir organiser le plus largement possible le partage des savoirs et des expériences. En interne, avec nos clients et avec nos pairs.

En interne, cela signifie que chaque aqobien doit avoir du temps pour partager son savoir et s’ouvrir à de nouveaux apprentissages . Il doit participer activement aux échanges, pour apporter son point de vue et son expérience aux autres membres du groupe. L’ouverture à d’autres points de vue est un élément essentiel à ce processus de partage, en permettant à chacun, quelle que soit son expérience, de participer et d’enrichir les échanges.

**Concrètement, nous nous retrouvons chaque mois pour une journée complète d’échange entre nous, l’[aqoday](#aqoday), entièrement déstaffée, ou nous organisons les lectures en groupe, des ateliers, des REX, bref des échanges afin de ressortir de cette journée avec plus de connaissances, d’apprentissages, ou d’alignement.**

Pour nos clients, c’est la garantie de trouver dans chaque aqobien une personne qui se questionne et qui apprend en continu. Pour aqoba, le partage passe aussi par la diffusion à nos clients de tous nos savoir-faire afin de les rendre autonomes dans leur transformation.

<a id='aqobien'></a>

## Ça implique quoi d’être un aqobien ?

Être un aqobien implique d'avoir certains droits et devoirs. En tant que collaborateur, tu as le droit de dire "je ne sais pas", de demander de l'aide, de dire "non", de dire que tu n'as pas compris, de demander à ce que les choses soient explicitées, de tester de nouvelles choses, de faire des erreurs, de choisir où travailler, y compris en mission (lorsque cela est compatible avec [les pratiques du client](#teletravail)), de dire que tu aimerais travailler sur d'autres sujets, de contribuer à l'amélioration de nos principes de gouvernance et de ne pas consulter tes mails tout le temps. Tu as également le droit et le devoir de signaler tout comportement abusif, hostile ou en désaccord avec nos principes.

En tant qu'aqobien, nous sommes tous responsables de maintenir l'image d'excellence de notre société. Notre réputation est une responsabilité collective et individuelle, et nous devons tous la préserver.

Nous sommes reconnaissants envers nos clients, qui paient nos salaires, et nous leur devons un service irréprochable. En retour, ils nous doivent une relation équilibrée, loin de la relation client/fournisseur, car ils nous sollicitent pour des sujets stratégiques sur lesquels nous devont agir en partenaires.

Nous sommes également au service les uns des autres, en faisant preuve de bienveillance, d'entraide, de disponibilité, de générosité et de respect des engagements pris envers nos collègues.

Enfin, nous sommes collectivement garants de notre modèle économique en faisant preuve de bon sens, de rigueur dans le respect des processus, de maturité et de frugalité.

<a id='evenements'></a>

## Quels sont les événements qui rythment la vie chez aqoba ?

<a id='aqoday'></a>

### C’est quoi l’aqoday ?

L’aqoday, c’est la [journée de partage](#partage) de tous les aqobiens. Une fois par mois, nous nous retrouvons tous, hors de nos missions et de notre quotidien, pour apprendre et partager nos expériences avec nos pairs. 

{{< figure  src="extreme-reading.jpeg" alt="Atelier d'extrem reading lors de l'aqoday" title="Atelier d'extrem reading lors de l'aqoday" class="center-small">}}

Cette journée est organisée et animée par les aqobiens eux même en y proposant des activités extrêmement variées : 

* Échange sur des situations rencontrées en mission
* Extrem Reading
* Présentation formelle autour d’un sujet
* Présentation et expérimentation d’atelier
* Présentation de problèmes rencontrés en mission pour avoir un œil neuf sur le sujet
* Rétrospective sur les modes de fonctionnement d’aqoba
* Retour sur les projets “[aqobadusens](#aqobadusens)”

Cette journée est aussi une occasion d’apprendre à mieux se connaître et à créer un sentiment d’appartenance, afin de créer un [groupe fort](#humanite), toujours prêt à s’entraider.

<a id='agora'></a>

### C’est quoi l’agora ?

L’agora, c’est une demi-journée tous les trimestres où nous cherchons à réfléchir avec des acteurs du terrain à l'avenir des organisations en rassemblant des personnes pertinentes pour traiter d'un sujet particulier. Contrairement à l'[aqoday](#aqoday), où les aqobiens se retrouvent entre eux, l'agora est ouvert à l'extérieur pour permettre des échanges plus larges sur des sujets plus diversifiés.

{{< figure  src="agora.jpeg" alt="l'agora sur une péniche" title="l'agora sur une péniche" class="center-small">}}

Au travers de l'agora, nous avons pu traiter de nombreux sujets, tels que l'avenir du manager et les nouveaux modèles de management, l'agilité et les ressources humaines, ou encore l'agilité et la data. Ces sujets abordent des problématiques actuelles et pertinentes pour l'avenir des organisations, et illustrent bien nos valeurs d'[altruisme](#altruisme) et de [conviction](#conviction), nous permettant de participer activement à une transformation sociétale positive en [partageant des connaissances](#partage) et en favorisant la collaboration avec des acteurs externes.

<a id='aqoffee'></a>

### C’est quoi l’aqoffee ?

Le télétravail et les missions exécutées chez nos clients ne favorisent pas le [sentiment d’appartenance](#humanite). C’est pour cela que nous avons mis en place, l’aqoffee. Tous les matins, tous les aqobiens qui le souhaitent, peuvent se retrouver virtuellement, pour prendre un café et discuter de choses et d’autres.

<a id='aqothink'></a>

### C’est quoi l’aqothink ?

L’aqothink est un moment que nous passons tous ensemble, toutes les deux semaines, et où nous partageons en toute [transparence](#transparence) les dernières nouvelles de l'entreprise dans les domaines du recrutement, du marketing et des demandes clients. Nous discutons aussi des actions en cours sur chacun de ces thèmes pour les faire avancer. Celles-ci sont portées par des [binômes volontaires](#humanite), sans aucune obligation de participation et chacun peut proposer de nouveaux sujets s’il le souhaite et lancer les nouvelles [initiatives qui l'intéressent](#conviction).

<a id='aqobadusens'></a>

### C’est quoi l’aqobadusens ?

Nous pensons que notre engagement doit aller [au-delà de nos missions](#altruisme). C’est pourquoi nous réservons [une partie de nos bénéfices](#transparence) à des actions qui sont choisies par les aqobiens eux-mêmes. Ainsi, chaque année, les aqobiens qui le souhaitent, peuvent monter un dossier afin de présenter un [projet qu’il souhaite que l’entreprise finance](#conviction). Chaque année, 1 à 2 projets sont choisis à la majorité des aqbobiens selon le principe : 1 personne = 1 voix. 

<a id='aqomachin'></a>

### C’est quoi tous ces trucs en aqomachins ?

On a beau être exigeants avec nous-mêmes et avec nos clients, on est persuadés qu’il ne faut jamais se prendre au sérieux. Alors, si on trouve encore des jeux de mots avec aqo… il n’est pas impossible qu’on organise un nouvel évènement, un nouveau rituel ou un nouveau goodies. 

{{< figure  src="aqoboisson.jpeg" alt="aqobar" title="aqobar" class="center-small">}}

<a id='accompagnement'></a>

## Quel accompagnement proposez-vous à quelqu’un qui rejoint l’équipe ?

aqoba ne recrute pas sur mission, mais sur profil.

Avant de faire une proposition à quelqu’un, on décide de l’accompagnement à apporter au futur aqobien / à la future aqobienne. Nous décidons de celui ou celle qui l’[accompagnera](#management) et nous lui proposons un parcours adapté, que nous ajustons avec lui / elle : formations, littérature, meet-ups, temps de coaching individuel, environnement de mission. L’objectif est de permettre à chacun de se sentir progresser concrètement et de pouvoir expérimenter ses connaissances sur son terrain de mission.

<a id='formations'></a>

## Il y a des formations ? des conférences ?

Une des valeurs d’aqoba est le [partage](#partage). Cette valeur fonctionne dans les 2 sens, chaque aqobien doit pouvoir donner sa connaissance, mais il doit aussi pouvoir récupérer la connaissance des autres. Dans ce cadre, il est évident que la formation des aqobiens au travers de formations académiques ou de participation à des conférences est un point essentiel pour nous.

{{< figure  src="conference.jpeg" alt="aqoba en conférence" title="aqoba en conférence" class="center-small">}}

Toutefois, afin que [tous les aqobiens se sentent bien](#humanite), il est important d’avoir une équité au sein du groupe. Nous sommes donc vigilants à la bonne répartition des activités d’apprentissage. 

<a id='situation'></a>

## C’est une bonne situation ça, aqobien ?

{{< figure  src="situation.jpeg" alt="C’est une bonne situation ça, aqobien ?" class="center-small">}}

Vous savez, moi je ne crois pas qu'il y ait de bonne ou de mauvaise situation. Moi, si je devais résumer ma vie aujourd'hui avec aqoba, je dirais que c'est d'abord des [rencontres](#partage). Des gens qui m'ont tendu la main, peut-être à un moment où je ne pouvais pas, où j'étais seul chez moi. Et c'est assez curieux de se dire que les hasards, les rencontres, forgent une destinée... Parce que quand on a le goût de la chose, quand on a [le goût de la chose bien faite](#partage), le beau geste, parfois on ne trouve pas l'interlocuteur en face je dirais, le miroir qui vous aide à avancer. Alors ça n'est pas mon cas, comme je disais là, puisque moi au contraire, j'ai pu : et je dis merci à la vie, je lui dis merci, je chante la vie, je danse la vie... [je ne suis qu'amour](#transparence) ! Et finalement, quand beaucoup de gens aujourd'hui me disent « Mais comment fais-tu pour avoir cette [humanité](#humanite) ? », et bien je leur réponds très simplement, je leur dis que c'est ce goût de l'amour ce goût donc qui m'a poussé aujourd'hui à entreprendre une construction mécanique, mais demain qui sait ? Peut-être simplement à me mettre au [service de la communauté](#altruisme), à faire le don, le don de soi… avec aqoba.

<a id='management'></a>

## Ça ressemble à quoi le ‘management’ chez vous ?

Chez aqoba, nous croyons que chaque collaborateur a un potentiel unique, et nous mettons tout en œuvre pour [l'aider à grandir](#humanite) et à atteindre ses objectifs professionnels. Chaque collaborateur dispose d’un Référent qui travaillera régulièrement avec lui pour [élaborer un plan](#accompagnement) de développement pour lui permettre de s'épanouir et de se perfectionner tant sur les Hard Skills que les Soft Skills. Nous encourageons une culture de feedback régulier, de mentorat et de coaching, afin que chaque collaborateur puisse progresser, atteindre son plein potentiel et se projeter dans le temps chez aqoba.

<a id='salaire'></a>

## On peut parler du salaire chez aqoba ?

Chez aqoba, nous sommes convaincus qu'un salaire compétitif et équitable est essentiel pour établir une relation de travail saine et durable avec nos collaborateurs. C'est pourquoi nous proposons un salaire au prix du marché, en prenant soin de le rendre aussi le plus juste et équitable possible au sein de l’entreprise. Nous nous engageons à travailler avec chaque collaborateur pour comprendre leurs objectifs et leurs aspirations, et à les récompenser en conséquence.

Nous complétons le salaire par de nombreux autres avantages.

<a id='teletravail'></a>

## Quelle est la politique de télétravail chez aqoba ?

Un grand nombre de nos clients ont adopté une politique de travail à distance, qui permet aux collaborateurs de travailler à domicile en général deux jours par semaine et de se rendre sur site pour les trois jours restants. Dans cet esprit, nous avons adopté une politique de télétravail similaire pour nos propres collaborateurs, afin de mieux nous adapter aux besoins de nos clients et de garantir une collaboration efficace. En d’autres termes, nous nous adaptons à la politique de télétravail de nos clients et essayons de proposer le maximum de flexibilité à nos collaborateurs pour un équilibre travail-vie privée optimal.

<a id='recrutement'></a>

## Vous recrutez uniquement sur Paris ou aussi en province ?

En tant que consultant, notre métier, centré sur l’humain, nous amène à travailler en étroite collaboration avec nos clients et nécessite des déplacements fréquents dans leurs locaux. La majorité de nos clients est basée à Paris et la plupart de nos missions requièrent notre présence sur site [plusieurs jours par semaine](#teletravail) : c’est essentiel pour nouer une relation de confiance avec les personnes que nous accompagnons et pour avoir l’impact nécessaire aux transformations que nous portons.

Cette exigence nous pousse à recruter principalement en Île-de-France, afin de limiter le temps de déplacement de nos collaborateurs. Ce dernier doit rester limité, pour conserver un équilibre vie pro - vie perso.

<a id='ambigramme'></a>

## C’est quoi un ambigramme ?

Un ambigramme est un type de dessin, de calligraphie ou de typographie qui peut être lu de manière identique ou différente lorsque l'image est tournée ou réfléchie dans un miroir. 

C’est le cas pour le logo d’aqoba, que vous pouvez toujours lire en le retournant !

{{< figure  src="logo-tournant.gif" alt="aqoba en logo tournant" class="center-small">}}
