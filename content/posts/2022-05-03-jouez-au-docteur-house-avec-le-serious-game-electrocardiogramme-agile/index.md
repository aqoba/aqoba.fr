---
title: "Jouez au Docteur House avec le serious game: electrocardiogramme Agile"
authors:
  - olivier-marquet
values:
  - partage
tags:
  - agile
  - seriousgame
  - vélocité
  - projection
offers:
  - unlock-delivery
date: 2022-05-09T06:30:00.000Z
thumbnail: ecg.png
subtitle: Un jeu sérieux pour découvrir et apprendre à manipuler la mesure de "vélocité"
---
Au travers de ce serious game venez découvrir comment calculer et utiliser la mesure de vélocité, mais aussi telle l'équipe du Docteur House, comment interpréter un historique de vélocité et poser un diagnostic.

<!--more-->

Dans un article précédent sur “[Comprendre les évaluations agiles](https://aqoba.fr/posts/20220420-comprendre-les-%C3%A9valuations-agiles/)” nous avions tenté de donner du sens aux pratiques d'évaluation, en se posant non pas la question du COMMENT, mais celle du POURQUOI.

Dans le suivant sur “[Comment utiliser la vélocité pour piloter ses activités ?](https://aqoba.fr/posts/20220503-comment-utiliser-la-v%C3%A9locit%C3%A9-pour-piloter-ses-activit%C3%A9s/)” nous avions vu comment utiliser les évaluations relatives afin de réussir à se projeter et à piloter nos activités en toute connaissance de cause.

![ECG](electrocardio.jpg)

Nous vous proposons cette fois ci un serious game, l'Électrocardiogramme Agile, qui va vous permettre de mettre en œuvre une partie des principes vus précédemment sur les évaluations agiles afin de mieux les appréhender:

* Qu’est ce que la vélocité ?
* Comment calculer une vélocité pertinente et utile ?
* Comment s’en servir ?
* Comment lire un historique de vélocité ?

Ce sont les questions auxquelles nous tenterons de répondre avec cet atelier. Les participants s'aligneront sur leur compréhension de concepts clés (Story points, vélocité) et apprendront à projeter une vélocité sur un Backlog. Une fois la bases posées nous irons plus loin en nous entraînant à diagnostiquer, telle l’équipe du Docteur House, des graphiques d’historiques de vélocité. Nous pourrons ainsi tenter de poser nos interprétations et recommandations avant de débriefer collectivement.

Fun et mauvaise foi garantie.

![ECG](ecg-slide1.jpg)

Cette atelier est disponible en version opensource sur OpenSeriousGame à l’adresse suivante: <https://openseriousgames.org/electrocardiogramme-agile/>

Et aussi en suivant le lien direct vers le Google Slide: <http://bit.ly/3y7UeUI>

Nous vous invitons à lire la section des annexes (à la fin du jeu de slides) ou vous trouverez le déroulé et les instructions d'animation. Tout n'est pas forcément expliqué dans le détail mais le principe n'est de toute façon pas très compliqué et j'espère que la plupart des slides sont autoporteuses (vous pouvez aussi vous référer aux articles cités plus haut).

Pour être efficace, cet atelier doit pouvoir être animé par un coach ou un scrum Master expérimenté qui saura expliquer les concepts utilisés ici. Pour la partie “Docteur House”, gardez en tête qu’il n’y a pas qu’une seule réponse possible. Échanger, confronter les points de vue est souvent la meilleure façon de progresser. 

**N'hésitez pas à nous partager sur [contact@aqoba.fr](mailto:contact@aqoba.fr) vos feedbacks et vos idées d'amélioration, ou tout simplement nous poser des questions sur l'atelier.**
