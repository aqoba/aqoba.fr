---
draft: false
title: "Approche systémique : Voir globalement et agir localement"
authors:
  - laurent-dussault
values:
  - conviction
  - partage
tags:
  - systemique
offers:
  - unlock-management
date: 2023-03-07T07:28:32.955Z
thumbnail: ei745zrxgaaojxu.jpeg
subtitle: « Penser global, agir local » René DUBOS (lors du premier sommet sur
  l'environnement en 1972)
---
Après avoir abordé quelques bases en parlant des boucles simples ([Utilisez un système pour obtenir le changement souhaité](/posts/20220830-approche-systémique-utiliser-un-système-pour-obtenir-le-changement-souhaité/)), penchons nous maintenant sur le cas des systèmes plus complexes. Mais avant de rentrer dans le vif du sujet, quels sont les types de reproches qu’on entend auprès des gens qui aimeraient voir un changement.

Le premier type d’intervention s’oriente vers des choses qui illustrent un manque de vision globale

{{< figure  src="capture-d’écran-2023-03-06-à-21.01.34.png" alt="Il ne voit pas plus loin que le bout de son nez !! Une voiture électrique super… mais son électricité n’est pas décarbonatée." class="center-large">}}

On pourrait trouver tout un tas d’exemples, où finalement l’intention est saluée, mais elle est considérée comme vaine.

**Agir sans vision globale**, c’est “avoir le nez dans le guidon”.

Le second type de remarques ressemble à : 

{{< figure  src="capture-d’écran-2023-03-06-à-21.01.47.png" alt="Tableau large et riche, mais en fait, ça ne débouche sur rien de tangible.. dans la vraie vie.    Pensée profonde, Mais on en fait quoi ?  Ce n’est pas pragmatique…" class="center-large">}}

En gros, merci pour la leçon, mais le problème me dépasse et vous êtes en train de **penser sans agir**.

C’est une spécificité humaine d’être capable de **pensée globale**, sur un niveau où on ne peut pas vraiment ni percevoir et ni agir.
On s’appuie sur nos croyances pour les valider et on ne fait rien pour les tester, pas d'expérience directe.

*Cette capacité à penser globalement est une **ressource** :*\
Elle permet de libérer la pensée des contraintes de l’expérience. 
On est dans le domaine de l’abstraction, le maniement de concepts…

*Toutefois cette capacité à penser globalement est relativement **limitante** :*
\
Il est peu probable que des idées suffisent, on ne peut se passer d’acte pour changer un système.

Ce que je souhaite aborder ici c’est comment réunir les deux c’est à dire identifier tout de même des actions locales en prenant en compte une complexité qui dépasse souvent le champ de l’actionnable.

## Souplesse de vision globale

Quelques pistes pour avoir un aperçu d’un système complexe :

### Les changements de points de vue

Vous pouvez commencer par le faire intuitivement, en identifiant les différentes parties prenantes du système que vous souhaitez observer. Essayez de voir quelles actions des uns provoquent telle ou telle réaction.

Puis rentrer dans votre modèle : exercez votre capacité à changer de point de vue.
Pour chacun des acteurs, posez vous des questions (en étant le plus possible en empathie) : 

* Quels sont vos enjeux, besoins, buts, espoirs ?
* Que percevez-vous, que ne percevez-vous pas ?
* Quelles sont vos forces ? Qu’y a-t-il de bon dans la situation ?
* Quelles sont vos limites ? Qu’est qui ne va pas pour vous dans cette situation ?
* Quel regard portez-vous sur les autres acteurs ?
* Quels sont les comportements récurrents ?
* Quelle énergie leur restent-ils pour faire autrement ?
* Quelles sont les situations porteuses de ressources ?
* Où cela mène à la fin ?

Faites ainsi le tour de tous les “joueurs” du système, lentement. Après chacun d’entre eux, demandez vous ce que ça vous a appris du système.

### Les changements de “grammaire”

Nos systèmes complexes sont faits d’individus, de leurs interactions, d’organisations et de leur processus…

**Concernant les individus et leurs interactions**, vous pouvez opter pour plusieurs prismes (liste non exhaustive) : 

* Analyse transactionnelle : pour creuser les jeux psychologiques, et peut être même les scénarios dans lesquelles certains cherchent à répéter (sans forcément en avoir conscience)
* Process Comm’ : pour schématiser les types de personnalité qui sont en jeu
* PNL (Programmation Neuro Linguistique, pas le groupe de rap) et je pense en particulier au VAKOG (Visuel, Auditif, Kinesthésique, Olfactif, Gustatif) pour s’interroger sur les différents canaux sensoriels les plus sensibles de chacun.
* …

**Pour les organisations**, souvent manipuler dans un modèle hiérarchique, des moyens de représenter les différents types d’équipes et leurs interactions existent (non exhaustif) : 

* unFIX dont nous vous proposons [une présentation en français](https://aqoba.fr/posts/20221129-%C3%A0-la-d%C3%A9couverte-dunfix/)
* Team topologies ([Notre extrem-reading du livre...](https://aqoba.fr/posts/20221017-nous-avons-tent%C3%A9-lexp%C3%A9rience-extrem-reading/))
* …

{{< figure  src="capture-d’écran-2023-03-06-à-21.02.47.png" alt="3 modes d'interaction selon Team topologies" class="center-small">}}

**Si votre objectif est de fluidifier un système**, il y a de forte chance que les outils du Lean soient votre allié dans votre modélisation avec un outil comme la VSM (Value Stream Map).

L'idée que je souhaite évoquer ici est celle qu’il n’y a pas pas forcément de meilleure grille de lecture que d’autres. Elles ont chacune leur grammaire qui nous offre un angle de perception du problème qu’on observe. Ce changement de “langue” est source d’apprentissage. Si vous maîtrisez plusieurs langues, vous êtes parfois bien embêté pour exprimer quelque chose qu’une seule langue désigne précisément. Et bien c’est l’idée, élargissez votre vocabulaire pour décrire votre système, en parlant plusieurs langues.

Les différents outils que je viens de citer sont plus ou moins complexes. Dans tous les cas, il ne s’agit que de modélisation nettement plus simple que le réel dont la complexité est sans limite. Toutefois, il nécessite un apprentissage (voir des précautions d’emploi) : 

> Rendez les choses aussi simples que possible, mais pas plus simples.
>
> ###### **\- Albert Einstein**

## Agir - Mesurer et recommencer…

Le **premier acte** de changement est **l’envie réelle d’agir** pour ce changement.

S’attaquer à un système complexe peut être long, très long et nécessite de l'abnégation.  Il va parfois falloir voir petit, se tromper, recommencer… 

### Avant tout définir ma zone d’intervention

Vous ne pouvez agir que dans votre zone d’influence !

Vous ne pouvez pas changer le comportement des autres, vous ne pouvez pas changer leur ressenti et donc leur réaction en le décrétant.

On oublie donc le “les autres n’ont qu’à”. Dans la plupart des cas, ça ne va pas se faire tout seul.

Il faut donc dans votre nouvelle représentation, identifier quelle action vous pouvez mettre en place (vous !) qui va changer la perception/réaction d’un ou plusieurs acteurs du système.\
Si vous avez accès à des acteurs du systèmes dont la zone d’influence est plus grande que la vôtre : quelle action pouvez vous mettre en place pour l’inciter à modifier son comportement (une formation ? lui offrir des fleurs ?…”c’est à la guise de l'imaginaire !”)

### Avancer pas à pas

La **complexité** peut être et doit être considérée… **le vivant est complexe**.

Bien que j’ai beaucoup parlé jusqu’ici de la modélisation du problème, il s’agit maintenant d’aller au-delà de ce dernier : la solution. Si aucune action ne vous paraît évidente questionnez-vous comme on le ferait en **solution focus** :

* Y a-t-il des fois où le problème n’est pas là ? Creusez cette situation, comment faire pour  qu’elle se reproduise plus souvent ?
* Que verrais je de différent si le problème était moindre ?\
  Pour celà utilisez une échelle de 1 à 10 : 

  * 10 : votre problème a disparu
  * 1 : l’inverse
  * Où en êtes-vous aujourd'hui ?
  * Qu’est ce qui fait que vous en êtes déjà là ?
  * Que verriez-vous de différents un cran plus loin sur cette échelle ? (+1)
  * Pouvez-vous identifier des actions pour atteindre ce +1 ?

### Mise en œuvre et mesures

Après le premier acte, je repasse en mode observation du système dans l’ensemble, en m’aidant des questionnements vus précédemment pour mesurer l’évolution de mon système.

* Quoi de mieux, de différent ?
* Ma zone d’intervention s’est-elle élargie ?
* Votre cible est-elle réévaluée par ce que vous avez appris ?
* Quel est le prochain pas ?

Toute ressemblance avec la boucle de Deming (PDCA) serait intentionnelle.

## Conclusion

### des outils complémentaires

{{< figure  src="capture-d’écran-2023-02-26-à-12.07.54.png" alt="Des outils complementaires pour modéliser, analyser, agir et mesurer..." class="center-large">}}

### Vous voulez creuser ?

Comme dans l’article précédent, je ne peux que vous conseiller les videos disponibles sur [la chaine de Luc RAMBALDI](https://www.youtube.com/watch?v=jsZ8OFnVlRw&list=PLYJr_BvPM0cvNnnW5E3LJuPy_xXF9X7pE)

Je vous conseille également de vous intéresser à la “culture orientée solution” et notamment le livre  Gery Derbier : [Solution Focus : Coaching, Leadership, Conversations Constructives](https://www.lulu.com/fr/shop/g%C3%A9ry-derbier/solution-focus-coaching-leadership-conversations-constructives/paperback/product-1q59qw9m.html) 

### À vous de jouer

N’oubliez pas, plus vous agissez sur un système, plus vous faites partie de ce dernier.
Il est de plus en plus difficile de prendre le recul nécessaire.
