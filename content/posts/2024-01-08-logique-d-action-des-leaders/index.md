---
draft: false
title: Les Logiques d'Action des Leaders
authors:
  - laurent-dussault
values:
  - partage
  - conviction
tags:
  - Leadership
  - Logiques d'action
offers:
  - unlock-management
date: 2024-01-08T21:07:00.584Z
thumbnail: 7_logiques_d_action.png
subtitle: 7 TYPES DE LEADERS AU TRAVAIL !
---
Bienvenue dans un voyage fascinant à travers les arcanes du leadership, où nous explorerons les sept logiques d'action qui guident les leaders dans leurs parcours professionnels. Nourri par l'article fondateur de David Rooke et William R. Torbert, [Seven Transformations of Leadership](https://hbr.org/2005/04/seven-transformations-of-leadership), cet article, un peu dense, se veut un premier survol des différentes logiques d’actions des leaders qui peuvent évoluer à différents stades de notre vie professionnelle. Un remerciement spécial à Myriam Roux, qui m’a partagé ce modèle enrichissant et a ouvert la porte à une compréhension plus profonde du leadership.

Dans ce monde mouvant, que vous soyez leader, manager, coach… il est crucial de comprendre que notre approche du leadership est multiple. Elle influence notre interaction avec les autres, notre prise de décision et notre impact sur l'ensemble de l'organisation. Les sept logiques d'action, que nous explorerons ici, offrent une perspective sur la façon dont les leaders abordent les défis, prennent des décisions et évoluent dans leur carrière. Et vous quel jeu avez vous en main aujourd’hui ?

## Avant Propos : Des Poupées Russes du Sens

Les transitions entre les Logiques d'Action ne suivent pas un parcours linéaire, mais plutôt un processus riche et complexe. Chaque changement transformationnel, présenté visuellement comme des "stades" dans le Leadership Development Framework, est comparable à l'ouverture successive de poupées russes, où chaque niveau appréhende une complexité croissante à la fois externe et interne.

{{< figure  src="leadership-development-framework-harthill.png" alt="Modèle Leadership Development Framework Harthill" title="Modèle Leadership Development Framework - Harthill" class="center-small">}}

Comprendre les Logiques d'Action revient à explorer le "comment on fait sens" des informations reçues, indépendamment de la personnalité de base. Cela ne définit pas une personne de manière définitive, mais plutôt un instantané des couches qui la composent, comme une série de poupées russes imbriquées. Ces "stades" ne désignent pas des niveaux, il ne s'agit pas de gravir une échelle mais plutôt de ressentir une aspiration vers des stades qui embrassent une complexité croissante, tant dans la perception du monde qu'à l'intérieur de sa personne.

Naviguer dans ce modèle se fera dans différents axes :  

* Verticalement :  Cette dynamique peut être illustrée par l'analogie du développement de l'enfant à l'adolescent puis à l'adulte, prolongé par le concept de "développement vertical de l'adulte". Ainsi on quitte un stade sans avoir besoin d’y renoncer totalement. C'est un mouvement évolutif qui transcende les étapes pour accueillir une compréhension plus nuancée et riche du monde.
* Horizontalement : En filant cette image, un adolescent qui met le focus sur la relation et l'appartenance au groupe, va incarner différemment les comportements suivants : la façon de s'habiller, le langage…Il va <span style="text-decoration:underline;">étendre ainsi cette logique</span> d’action peu à peu dans différents domaines, sans mettre en place une nouvelle logique.

Il est crucial de noter que ces "Logiques" ne sont ni intrinsèquement positives ni négatives. Leur mise en œuvre peut servir aussi bien le bien que le mal, et aucune logique n'est moralement meilleure ou pire. Leur compréhension ne repose pas sur une évaluation morale, et une logique qui peut vous sembler étrangère n'est pas nécessairement négative. Bien sûr ceci s’applique aussi à moi, rédacteur de cet article et je m’excuse par avance pour la maladresse avec laquelle je vais tenter de décrire des logiques d’action que je ne maîtrise pas.\
 \
Enfin, on notera que les termes qui désignent les logiques d’action ne sont que des noms. Je garderai d’ailleurs les termes anglais, car ils suffisent à les désigner et une traduction n'apporterait qu’une tentative de donner du sens à un nom qui en a assez peu, car bien loin de définir la logique concernée en un seul mot.  

## Opportunist

{{< figure  src="opportunist.png" alt="opportunist" class="float-left-tiny">}}

L'Opportunist est un tacticien, adoptant une approche axée sur la victoire à tout prix. Ne perdant jamais son propre objectif de vue, il peut afficher une propension à la manipulation dans des environnements où le "pouvoir fait la loi". Son horizon est court-termiste, mettant l'accent sur des réalisations concrètes, mais il peut également se montrer sélectif dans les informations qu’il analyse, rejetant les retours d'information et externalisant la responsabilité.

Cette logique d’action se caractérise par une confiance fragile et peut afficher un contrôle de soi précaire, où la chance est vue comme un élément central. Les règles sont perçues comme une entrave à sa liberté. Dans cette logique d’action, la vie consiste à gagner, et on prendra toute opportunité qui se présente pour maximiser ses gains.

Il est souvent perçu comme astucieux et opportuniste (donc), prêt à tout pour réussir. Ainsi, cette logique d’action séduisante au début, est toutefois limitante car elle peut céder la place à une perception d'instabilité et de manque de fiabilité à long terme. Très peu de leaders sont principalement Opportunist…

On pourrait comparer cette logique d’action à celle d’un enfant, et quand un enfant est débrouillard, bah c’est mignon non ? mais on a hâte qu’il grandisse des fois.

 \
**Force principale :**  \
Très à l’aise dans les situations d'urgence et lors des négociations commerciales.

## Diplomat

{{< figure  src="diplomat.png" alt="Diplomat" class="float-left-tiny">}}

Le Diplomat, dans le monde des logiques d'action, préfère éviter les conflits ouverts, aspirant plutôt à appartenir et à créer une harmonie au sein du groupe. Orienté vers le respect des normes du groupe, il évite tout bouleversement, observant scrupuleusement le protocole établi. Avec cette logique d’action, un leader favorise la conformité, s'exprimant souvent à travers des clichés, des expressions courantes et des platitudes. Son engagement se manifeste par la recherche d'adhésion et de statut au sein de son cercle immédiat, sa loyauté étant réservée à ce groupe plutôt qu'à une organisation distante ou à des principes abstraits. Le Diplomat veille à maintenir une cohésion en s'occupant des affaires sociales, en maniant un langage spécifique au groupe et aux individus.

Cependant, il évite non seulement les conflits externes, mais aussi les conflits internes, ressentant de la gêne s'il devait “enfreindre” les normes établies. Son interaction sociale vise à fournir un soutien sans causer de préjudice, soulignant l'importance de ne pas perdre la face pour tous les membres du groupe. Ainsi, le Diplomat représente un élément stabilisateur au sein du groupe, veillant à ce que les relations restent harmonieuses et évitant tout comportement susceptible de blesser les autres.

On pourrait comparer, à l’excès, cette logique d’action à celle qu’on voit à l'œuvre chez les adolescents. L’important c’est l’appartenance.(et la rose oui Gilbert !).

**Force principale :**  \
Rassemble, aide à la cohésion.

## Expert

{{< figure  src="expert.png" alt="Expert" class="float-left-tiny">}}

Les individus ayant une logique d'action Expert accordent une importance particulière à la logique et à la compétence dans le but de réaliser des performances exceptionnelles. Ils sont axés sur la recherche constante d'amélioration et d'efficacité rationnelle, que ce soit dans leur propre développement ou dans les processus qu'ils entreprennent. Ces leaders valorisent leur perspective, considérant leur manière de voir le monde comme LA réalité, la plus valable. Souvent orientés vers la résolution de problèmes, ils privilégient l'efficacité plutôt que l'efficience et peuvent tendre vers le perfectionnisme. Leur approche peut parfois être dogmatique, évaluant et critiquant, eux-mêmes et les autres, en fonction de leur système de croyances.

Les Experts possèdent la capacité de percevoir différents points de vue, mais ils ont une préférence marquée pour les commentaires provenant d'experts “objectifs” dans LEUR domaine. Ils accordent une valeur particulière aux décisions basées sur des certitudes ou des faits tangibles. Malgré leur tendance à la constance, ils peuvent apporter une contribution individuelle significative,  dans la poursuite de l'amélioration,  au sein d'un groupe ou d'un système plus vaste. Leur engagement envers la logique et l'expertise peut enrichir le groupe, bien que parfois au détriment d'une approche plus souple ou holistique. Les Experts incarnent une logique d'action où l'excellence et la rationalité sont les piliers fondamentaux de leurs efforts. 

Toute ressemblance avec un Geek serait fortuite.\
 \
**Force principale :** 

Excellent contributeur, en solo. (non pas Han Solo les geeks…)

## Achiever

{{< figure  src="achiever.png" alt="Achiever" class="float-left-tiny">}}

Les personnes orientées vers une logique d'action Achiever ont pour objectif de réaliser des buts stratégiques, en atteignant des résultats de la manière la plus efficace possible. Dans l'ensemble, ces leaders se sentent comme des initiateurs créatifs axés sur des objectifs à moyen terme, percevant un futur émergent et motivant. 

Leur orientation vers l'efficacité et les résultats les conduit à adopter, plutôt qu'à créer, des objectifs majeurs. Les Achievers cherchent des solutions pour contourner les problèmes afin de livrer le résultat final souhaité.

Les Achievers apprécient la complexité et adoptent une perspective systémique. Leur focalisation sur le succès et l'action les distingue, faisant d'eux des contributeurs précieux orientés vers l'atteinte des objectifs précis.

On voit l’intérêt d’une telle logique d’action en entreprise. Cependant il y a des zones aveugles : quid des impacts des décisions sur le long terme ? Quid de la remise en cause de l’objectif même ?

**Principale force :**  \
Adapté aux rôles de “manager”, tel qu'on le voit le plus souvent dans les entreprises, axé sur l'action et les objectifs.

## Individualist

{{< figure  src="individualist.png" alt="Individualist" class="float-left-tiny">}}

L'Individualist, c'est un explorateur dans l'âme. Il cherche sa propre façon unique de contribuer au groupe. Pas de vérités figées ici, il adopte plutôt une position relativiste, remettant en question tout, des idées qu'il se fait de lui-même à celles des autres et du système. C'est un enquêteur radical, toujours en quête de conscience de soi, plongeant profondément dans la complexité de l'expérience humaine et jonglant avec les paradoxes.

Avec cette logique d’action, la quête ne se limite pas aux relations superficielles, non, l'Individualist veut du profond. Il devient de plus en plus conscient du processus, jonglant avec différents rôles tout en essayant de rester fidèle à lui-même. Il recherche différents points de vue, et il pose des questions dans tous les domaines de la vie. Il est curieux, vraiment curieux, et il sait que pour innover, il doit être conscient de ses points faibles, des revers qui accompagnent ses forces.

Avec des perspectives innovantes et une pensée de plus en plus libre, l'Individualist est en constant apprentissage. Il défie les vérités “absolues”, embrassant une approche novatrice de la vie et du travail. C'est un aventurier de l'authenticité, prêt à explorer ce qu'il croyait savoir et à repousser les frontières pour créer quelque chose de nouveau.

Cette logique d’action est très visible dans les conf’ agiles (mais chut !).

**Points forts :** 

Efficace dans les rôles liés à la création d'entreprise et au conseil.

## Strategist

{{< figure  src="strategist.png" alt="Strategist" class="float-left-tiny">}}

Le Strategist, c'est celui qui recherche des transformations, à la fois personnelles et organisationnelles. Cette logique d’action apporte de la fluidité à la vie en naviguant de position en position, adaptant continuellement son être au monde dans la poursuite de valeurs qui elles restent stables (les valeurs). Ce leader reconnaît l'importance des principes, des engagements, de la théorie et du jugement. Il ne se contente pas de suivre aveuglément les règles et les coutumes. Doué pour résoudre les conflits de manière créative, le Stratège observe les systèmes dans leur ensemble, tout en gardant une vision claire des processus et des objectifs. Son ouverture au paradoxe, à la contradiction et à l'incertitude le distingue, tout en étant conscient que sa perception du monde dépend de sa propre vision du monde (même s'il peut parfois s'embarquer dans une posture de théoricien en toge !).

Le Strategist accorde une grande valeur à l'individualité, aux différentes spécificités et aux moments de bascules particuliers. Jouant habilement différents rôles, il manifeste un humour spirituel et existentiel. Cependant, il est également conscient du côté sombre du pouvoir et peut être tenté de l'utiliser à des fins personnelles, risquant ainsi d'abuser de ses propres capacités et de manipuler les autres. Avec une perspective aiguisée et une compréhension approfondie de la complexité, le Strategist se révèle être un acteur clé pour stimuler les transformations nécessaires à l'épanouissement personnel aussi bien qu’organisationnel. \
 \
**Point fort :**  \
Efficace en tant que leader d’une transformation.

## Alchimist

{{< figure  src="alchemist.png" alt="Alchimist" class="float-left-tiny">}}

L'Alchimist, dans le monde des logiques d'action, est le créateur de transformations personnelles et sociales. Ils s'engagent dans une danse délicate entre la conscience, la pensée, l'action et les répercussions. Leur quête les conduit à transformer non seulement eux-mêmes mais aussi les autres et les systèmes qui les entourent.

L'Alchimist aspire à participer activement aux transformations historiques et sociales. Ils se présentent comme des créateurs d'événements qui prennent une aura presque mythique, recadrant ainsi les situations et ouvrant de nouvelles perspectives. Ancrés dans le présent inclusif, les Alchimistes ont la capacité de percevoir la lumière ET l'obscurité dans toutes les situations, ce qui alimente leur compréhension nuancée de la vie.

Le modus operandi de cette logique d’action implique de travailler harmonieusement avec l'ordre ET le chaos, mêlant les opposés pour créer des jeux "gagnant-gagnant". Ils exercent leur attention de manière continue, explorant l'interaction complexe entre la pensée, l'action et les effets sur le monde extérieur. Plutôt que de voir le temps et les événements de manière linéaire et littérale, les Alchimistes ajoutent une dimension poétique à leur perspective.

En quête constante de nouvelles expressions spirituelles, les Alchimists peuvent également jouer le rôle de guides, aidant les autres dans leurs propres quêtes de vie. Leur positionnement souvent à la marge de la société ou parfois au cœur de changements controversés reflète leur disposition à embrasser la complexité et à remettre en question le statu quo. En résumé, l'Alchimiste incarne la transformation sous toutes ses formes, cherchant à <span style="text-decoration:underline;">transcender</span> avec une sagesse qui <span style="text-decoration:underline;">transcende</span> les limites du conventionnel.  \
 \
En pratique les alchimistes sont rares et précieux, un exemple emblématique serait Nelson Mandela, dont la capacité à symboliser l'unité de l'Afrique du Sud a renversé les barrières historiques du rugby, de la politique et de la société.

**Point fort :** \
Diriger des transformations à l'échelle de la société dans son ensemble.  

## Et qu’est ce qu’on fait de tout ça ?

### Encore un modèle ?

Chaque nouveau modèle dans notre compréhension du monde est comparable à l'acquisition d'un nouveau vocabulaire pour décrire la réalité.  \
Chaque modèle, tout en étant une simplification nécessaire, offre un éclairage nouveau sur certains aspects de notre expérience.  \
 \
En croisant ces différentes simplifications, nous élargissons notre répertoire conceptuel, nous offrant ainsi un autre point de vue, une nouvelle lentille à travers laquelle percevoir et interagir avec le monde. Ces modèles ne prétendent pas à la vérité absolue, mais ils enrichissent notre langage mental, nous permettant d'exprimer des idées et de discuter de concepts avec une nuance et une précision accrues.  \
 \
Pour résumer, c’est un peu comme quand pour exprimer certains concepts, bah c’est plus facile dans une langue donnée, parce qu’il y a un mot pour ça ! J’ai suivi récemment un thread qui cherchait à traduire simplement en français l’idée de DISAGREE and COMMIT (vous avez 2 heures)...

### Et après ?

En clôturant cet aperçu des logiques d'action, vous avez maintenant la carte des sept territoires du leadership. Chacun de ces types offre une perspective unique, révélant des forces et des zones de croissance. Cependant, la vraie magie se produit lorsque vous commencez à explorer vos propres logiques d'actions : apprendre à identifier les pièces de votre mosaïque personnelle, et ainsi amorcer une réflexion sur l’agencement des pièces que vous maitrisez, ou encore comment acquérir une nouvelle pièce…

L'exploration personnelle peut être facilitée par des outils tels que le **Leadership Development Framework**, qui offre des tests pour identifier votre ou vos logiques d'action prédominantes dans votre contexte <span style="text-decoration:underline;">actuel</span>. J'ai eu la chance d'expérimenter cela avec une coach qualifiée par **Harthill**, [Myriam Roux](https://www.linkedin.com/in/myriamroux/), dont la passion et l'expertise m’ont ouvert la voie à une compréhension plus profonde de ma manière d'agir en tant que leader.

Le processus de test et de débriefing ne se limite pas à vous catégoriser dans un seul type, mais il explore également le potentiel des actions de niveaux “supérieurs”. C'est une opportunité de prendre conscience de vos forces, mais aussi d'identifier les domaines où vous pourriez évoluer pour atteindre un leadership plus efficace et surtout plus épanouissant. Car il ne s’agit pas d‘une route à suivre, une destination pour laquelle vous pouvez décider de partir. Vous trouverez plutôt des options qui seront des aspirations et d’autres qui vous feront l’effet de “oui peut être un jour, mais alors pas du tout en ce moment, c’est inimaginable pour moi <span style="text-decoration:underline;">actuellement</span> !”. Cela ouvre la porte à une croissance continue, à une meilleure compréhension de soi, et à une influence positive sur les autres.

En fin de compte, le voyage du leadership est une aventure personnelle et collective. En découvrant vos logiques d'action et en envisageant l'émergence de niveaux “supérieurs”, vous vous positionnez pour un leadership plus authentique, influent et transformateur. Alors, prêt à explorer votre propre carte du leadership ? Le voyage en vaut certainement la peine.
