---
draft: false
title: Le site web d'une entreprise écologique
authors:
  - thomas-clavier
values:
  - conviction
tags:
  - greenit
  - écologie
  - écologique
  - green it
date: 2022-06-28T07:32:30.316Z
thumbnail: dsc00221.jpg
subtitle: Parce que nos petites actions cumulées sauveront notre planète
---
En 1866 le biologiste allemand Ernst Haeckel, invente le mot écologie pour désigner «la science des relations des organismes avec le monde environnant, c’est-à-dire, dans un sens large, la science des conditions d’existence». Chez aqoba, nous sommes écologistes, nous avons des convictions fortes sur la nécessité de préserver notre environnement et notre planète. Nous pensons qu’en faisant chaque jours de petits efforts nous pouvons participer à notre échelle à la sauvegarde de notre environnement.

Un de ces petits gestes, c’est notre site web. Nous avons réfléchi à comment réduire l'empreinte écologique de notre site, et nous sommes arrivé à la conclusion suivante :

* Notre site doit être peu énergivore : il est économe en ressources depuis sa création jusqu’à son exploitation.
* Notre site doit être peu gourmand en bande passante : il permet à chaque visiteur de trouver l’information tout en étant le plus frugale possible.
* Notre site doit respecter tous nos visiteurs : il est accessible à toutes et tous sans distinction et ne collecte aucune donnée personnelle.

Si vous ajoutez à celà que je suis convaincu que nous ne pouvons pas faire confiance à du code que l'on ne peut pas auditer, nous arrivons à l’ensemble suivant.

## Un site web static

{{< figure  src="raspberrypi-solar.jpg" alt="Un Raspberry Pi avec un panneau solaire" class="center-small">}}

Et si nous pouvions héberger notre site sur un serveur de la taille d’une carte de crédit alimenté par un tout petit panneau solaire ? Et bien c’est possible, pour ce faire, nous avons utilisé [Hugo](https://gohugo.io/) qui produit un site web statique, c'est-à-dire que toutes les pages web sont construites une seule fois à la publication et que nous ne reconstruisons pas les pages pour chaque visiteur comme le ferait un CMS plus classique. Celà nous permet de réduire les cycles CPU utilisés pour produire nos pages web. En effet, reconstruire une page web pour chaque visiteur, y compris les robots d'indexations, c'est une source de gaspillage énergétique non négligeable. Le défaut de ce choix c’est que nous ne pouvons pas adapter les pages de notre site à chacun de nos visiteurs, mais est-ce vraiment un problème ?

En prime, nous obtenons un site web extrêmement sécurisé, contrairement à un site construit avec un CMS plus classique qu’il faut impérativement maintenir à jour.

Pour réduire la bande passante consommée par chaque visiteur, nous utilisons la propriété [srcset](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/srcset) afin de laisser le navigateur choisir l’image la plus adaptée à la taille réelle de l’écran des visiteurs.

Il reste cependant des fonctionnalités qui demandent habituellement un minimum de calcul côté serveur, le moteur de recherche ou le suivi de l’activité.

## Moteur de recherche

{{< figure  src="logo-lunr.png" alt="Logo LunR" class="center-small">}}

Le principe de fonctionnement d’un moteur de recherche est le suivant :

* collecter l’ensemble du contenu
* construire l’index de recherche
* l’utilisateur peut alors contacter le moteur de recherche pour effectuer une recherche.

[LunR](https://lunrjs.com/) est un moteur de recherche écrit en javascript, il est donc possible de l’exécuter dans un navigateur. De façon à ne pas refaire le travail d'indexation dans le navigateur de chacun de nos utilisateurs, économie énergétique oblige, nous réalisons la collecte et l’indexation à la génération du site. Vous pouvez d’ailleurs consulter l’index [ici](https://aqoba.fr/search.json). Ce qui permet au navigateur de nos visiteurs de ne faire que la lecture de l’index et la recherche à l'intérieur de celui-ci.

## Suivi de l’activité

{{< figure  src="logo-plausible.png" alt="Logo Plausible" class="center-small">}}

Le défi était le suivant : trouver un outil à la fois simple et efficace capable de suivre l’activité sur notre site web tout en respectant nos utilisateurs. 

Nous sommes très attentifs à ne surtout pas collecter de données pouvant s’apparenter à des données personnelles. Dans ces conditions, impossible de mettre du [Google Analytics](https://analytics.google.com/) ou du XiTi. [La décision de la CNIL rendue publique le 10 février 2022](https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/questions-reponses-sur-les-mises-en-demeure-de-la-cnil-concernant-lutilisation-de-google-analytics) nous donne d’ailleurs raison, «les mesures mises en place par Google ne sont pas suffisantes pour garantir le secret des données personnelles des utilisateurs»

[PLausible](https://plausible.io/) a retenu notre attention, il répond en effet à tous nos critères. Et en plus, comme il est libre, il est très simple de vérifier par nous même qu'il ne collecte pas trop de données. Pour déployer plausible sur une petite machine virtuelle afin de garder l'intégralité des données collectées uniquement chez nous, nous avons utilisé Ansible. Encore un logiciel libre, qui nous garantit que si nous perdons la machine, il faut 4 minutes et 9 secondes pour en reconstruire une nouvelle.

## Un éditeur WYSIWYG

{{< figure  src="logo-netlify.svg" alt="Logo Netlify" class="center-small">}}

Tous Les auteurs du site ne sont pas des fervents défenseurs du markdown, il est encore difficile pour certains de se dire je me concentre sur le fond et la structure, la mise en forme est assurée par le convertisseur markdown vers HTML et la feuille de style. Nous avons donc cherché un éditeur WYSIWYG facilement utilisable avec Hugo. Netlify nous a séduit. Il est en effet très simple à configurer, libre et très bien intégré à gitlab.com

## Accessible

{{< figure  src="logo-pa11y.svg" alt="Logo Pa11y" class="center-small">}}

Respecter son environnement c'est aussi être accessible à toutes et tous. En effet, nos visiteurs font partie intégrante de notre environnement. 

En plus d'avoir travaillé la possibilité de profiter de toutes les fonctionnalités du site sans souris, nous avons mis un point d'honneur à avoir des couleurs suffisamment contrasté, des textes alternatifs sur nos images et bien d'autres détails rendant le site accessible au plus grand nombre.

Pour vérifier que notre site continue à être accessible par tous, nous lançons avant chaque déploiement un audit d'accessibilité en automatique avec [pa11y](https://pa11y.org/). Au moindre défaut, le site ne se déploie pas. Ce qui nous permet de garantir, dans la durée, un site d'une grande qualité.

## Conclusion

Hébergé sur les pages gitlab, déployé à la moindre modification, audité avant chaque déploiement, accessible à toutes et tous, frugale en cycle CPU et en bande-passante, ne collectant aucune donnée personnelle, n'utilisant que des logiciels libres, notre site est à l'image de l'excellence que nous voulons incarner. Écologique dans ses moindres détails, respectueux de notre planète et de l'ensemble de nos utilisateurs. Chez aqoba nous vivons nos convictions. Nous portons nos valeurs et nos convictions sans compromis entre nous ainsi que dans notre environnement. Regarderez vous encore notre site de la même manière ?
