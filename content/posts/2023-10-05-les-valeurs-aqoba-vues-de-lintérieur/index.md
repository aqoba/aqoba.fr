---
draft: false
title: Les valeurs aqoba vues de l'intérieur
authors:
  - marion-bialecki
values:
  - altruisme
  - conviction
  - humanite
  - partage
  - transparence
tags:
  - Nos valeurs
  - valeurs
  - inside
date: 2023-10-05T07:46:29.294Z
thumbnail: valeurs-aqoba-par-marion.png
subtitle: Une employée raconte...
---
Il y a maintenant quelques mois, j’ai rejoint l’équipe aqoba car nous partagions des valeurs communes, dont aqoba a fait sa marque de fabrique.

Mais alors concrètement, ces valeurs, comment une jeune aqobienne les vit au jour le jour ?

# L'Humanité en entreprise, qu'est-ce c'est ?

{{< figure  src="humanité.jpeg" alt="Illustration de l'humanité chez aqoba, par Fix" class="center-small">}}


▶ C’est une philosophie qui met l'accent sur la valeur et la dignité des gens. Être une entreprise humaniste c’est favoriser l'épanouissement personnel, la créativité et l'autonomie en offrant des opportunités d'apprentissage et de développement professionnel.

▶ C’est ne jamais oublier que les organisations sont avant tout composées d’individus.

▶ Je suis plus importante que la mission. Et ça chez aqoba je l’ai senti dès le premier jour.

▶ Un atelier de **Moving Motivator** a rapidement été effectué pour identifier mes leviers de motivation et de bien être et trouver comment répondre à ce qui est vraiment important pour moi (l’ordre, la maîtrise et l’acceptation si vous demandez).

▶ Mon équilibre pro/perso est totalement respecté et l’alignement entre ce qui m’est demandé au quotidien et mes valeurs personnelles est sondé fréquemment.

▶ Je suis écoutée, mon avis est pris en compte et je me sens parfaitement intégrée parmi cette bande de barbus... même si moi je n'ai pas de barbe, j'ai pourtant bien tenter de la faire pousser mais rien...

▶ Je le ressens aussi auprès de nos clients, chaque atelier, chaque prise de décision se fait dans le respect des gens qui constituent les équipes, selon les rythmes et les sensibilités de chacun.

Avouez qu’avec tout ça, il y a de quoi retrouver foi en l’humanité, non?

# Le Partage en entreprise, qu'est-ce que c'est ?

{{< figure  src="partage.jpeg" alt="Partage chez aqoba illustré par Fix" class="center-small">}}

▶ C’est une valeur qui encourage à transmettre nos connaissances, nos compétences et nos ressources avec tous les membres de l'équipe ainsi que tous nos clients.

▶ Partager au sein d’une équipe, c’est améliorer la qualité et l’efficacité de son travail. C’est faire émerger des solutions innovantes et des résolutions plus rapides de problèmes.

▶ C’est créer une culture d'entreprise plus collaborative et renforcer la cohésion et la confiance qu’on se porte.

➡ Profane de la transformation d’entreprise j’ai pourtant immédiatement été intégrée à une très grande majorité des discussions stratégiques et ma voix a compté dans les choix.

➡ Débutante en SAFe j’ai tout de suite été prise sous l’aile du meilleur RTE possible ([Laurent](https://aqoba.fr/team/laurent-dussault/)) et ma montée en compétence sur la connaissance du framework et de ses enjeux a été fulgurante.

➡ Aimant le contact (15 ans de relation clientèle ça ne s’oublie pas) et les défis, j’ai été embarquée pour la première fois dans l’animation d’un atelier à la conférence Agi’Lille... et j’ai adoré :)

➡ Entourée des bonnes personnes, j’ai appris en 6 mois l’équivalent de ce que j’ai expérimenté sur plusieurs années auprès d’autres groupes.



Et le partage ne s’arrête pas là... j’ai découvert des types de bières dont je ne soupçonnais même pas l'existence( et pourtant je suis pas débutante là, hein...)

# La Conviction en entreprise, qu'est-ce que c'est ?

{{< figure  src="conviction.jpeg" alt="Conviction chez aqoba illustrée par Fix" class="center-small">}}

La conviction, c’est cette croyance profonde envers les valeurs, la vision et les objectifs de son équipe. Elle encourage détermination et engagement. Elle contribue aussi à forger une culture d’entreprise solide.

Pouvoir affirmer mes convictions, pour moi, c’est vital.

▶ J’ai besoin d’être certaine que je serai supportée par mon équipe lorsque je m'exprime, que ce soit au sein d’aqoba ou chez mes clients, y compris lorsque je suis en désaccord avec une décision ou une direction prise. Savoir que ma voix compte, que je peux peser dans le choix et la direction que prendra ma mission, c’est essentiel.

▶ Et je l’ai ressenti dès ma première mission avec aqoba : dans le respect du cadre de la transformation à laquelle je participe, je peux affirmer mes convictions au travers de mes paroles, de mes actions ou des outils que j’utilise. En particulier, lorsque j’ai eu la conviction d’être allée au bout de ce que je pouvais apporter à l’équipe que j’accompagnais : alors qu’un autre employeur m’aurait peut-être poussée à étirer mon intervention en longueur, mon équipe chez aqoba m’a écoutée et nous avons oeuvré, aux côtés de notre client, pour faire pivoter ma mission, dans l'intérêt de la transformation.
Aujourd’hui, je me repositionne auprès d’autres équipes où mon énergie trouvera plus d’impact positif, et auprès desquelles je m’épanouirai plus.

▶ Mes opinions et conviction sont respectées donc j’ose les livrer. Mais ça, je le savais, après tout j’ai rejoint une entreprise qui refuse des contrats quand elle ne partage pas les valeurs de l’entreprise qui fait appel à elle.

# L'Altruisme en entreprise, qu'est-ce que c'est ?

{{< figure  src="altruisme.jpeg" alt="Altruisme chez aqoba illustré par Fix" class="center-small">}}


C’est le fait d’accomplir des actes de générosité désintéressés, d'empathie et de compassion envers autrui, sans rien attendre en retour (𝘤𝘰𝘮𝘮𝘦 𝘥𝘪𝘴𝘢𝘪𝘵 𝘍𝘭𝘰𝘳𝘦𝘯𝘵 𝘗𝘢𝘨𝘯𝘺).
 
▶ Chez aqoba, l’altruisme, c’est d’aller au-delà de notre mission, d’aider des équipes hors des périmètres habituels.

▶ Chez nos clients, c’est aller plus loin que l’engagement initial, ne pas compter nos jours si on estime que cet effort peut contribuer plus efficacement.
Nous avons souvent mis en relation des experts de notre réseau avec des équipes qui étaient bloquées sur certains sujets et ce sans aucune contrepartie.

▶ Dans notre communauté c’est d’investir tous les 3 mois dans l’[agora](https://aqoba.fr/posts/20220311-la-faq-aqoba/#agora), événement auquel nous invitons notre réseau pour discuter, débattre et parfois même répondre à des problématiques autour des sujets d’actualité et s’ouvrir les chakras.

▶ L’altruisme aqobien c’est aussi de réserver une partie des bénéfices pour porter des causes qui nous sont chères et ce au delà des limites défiscalisables, pour que don ne rime pas avec opération financière mais bien avec engagement sincère.
C’est de s'engager en temps qu'équipe à effectuer et suivre notre bilan carbone (vive l’[ADEME](https://www.linkedin.com/company/ademe/)!) afin de définir des actions d’améliorations concrètes.

▶ Et puis c’est ce luxe du choix que l’on s’octroie: s’équiper uniquement du matériel nécessaire, avec le maximum de produits durables, en sélectionnant lorsque c’est possible des fournisseurs qui s’engagent également pour un monde plus sain, plus juste.

Et ça c’est bon pour le moral ! *(la compagnie créole est d'accord)*

# Le Partage en entreprise, qu'est-ce que c'est ?

{{< figure  src="transparence-1.jpg" alt="Le Partage chez aqoba illustré par Fix" class="center-small">}}

C’est partager ouvertement et honnêtement les informations pertinentes avec les autres, sans délai. Cela favorise la confiance et la compréhension.

D'un point de vue humain cela implique d’être authentique et ouvert dans ses interactions, en partageant ses pensées, émotions et intentions de manière sincère pour des relations plus honnêtes, profondes et significatives.

▶ Je me suis toujours prévalue d’être très transparente (parfois un peu trop). Pas franchement experte au jeu de la politique, j’ai toujours parlé vrai et directement. Là où parfois on m’a demandé d’enjoliver, d’enrober, je ne me suis pas sentie à ma place.

▶ Chez aqoba je me suis toujours sentie libre de dire ce que je pense (toujours dans le respect des autres évidemment), même pendant ma période d’essai :)
J’ai été encouragée dès le début à faire connaître mon ressenti, mes idées, mes coups de gueule. Échanger sur mes attentes mais aussi sur mes déceptions. Sur mes points forts mais aussi sur mes failles ou mes fragilités.
C’est pour moi extrêmement gratifiant de pouvoir être moi-même, sans jugement de valeur, mais toujours avec un retour constructif.

▶ Et si on m’encourage à cette transparence c’est que l’entreprise se l’applique aussi.
Dès la première semaine j’ai été inclue dans les réunions de vie de l’entreprise, pendant lesquelles nous échangeons sur ses choix marketing, commerciaux et RH. 
Avoir accès à ces informations qui parfois sont sensibles et pouvoir prendre part aux décisions, appelle chez moi à une réflexion sur mon rôle dans l’entreprise, l’impact que je peux avoir et fait résonner un vrai sens de la responsabilisation.
Est ce que je deviens actrice de ces sujets? Est ce que je reste dans l’observation : j’ai le choix, sans culpabilité ,d’adapter mon périmètre jusqu’à ce que je me sente prête et légitime à porter ma pierre à l’édifice.

Et ça, ça m’aide drôlement à me projeter sur un bon bout de chemin avec mes barbus préférés!

# 5 bonnes raisons qui m’ont convaincue qu’aqoba était un entreprise différente.

5 bonnes raisons qui m’ont encouragé à quitter une situation stable et confortable de freelance pour la rejoindre .

Je ne cherchais pas de changement de statut, je ne cherchais même pas particulièrement de changement d’entreprise , mais le hasard de conversations, de rencontres et d’échanges m’a convaincu de la force du modèle d’intégration proposé…et qu’il n’y a pas vraiment de hasard, que les choses arrivent toujours pour une raison :)

Au final, nous sommes tous les 6 ( bientôt 7!) très différents mais on se regroupe sous des mêmes couleurs. C’est ce qui fait la force et la richesse d’aqoba.

Et aujourd’hui nous recherchons de nouveaux profils, avec de nouveaux points de vue pour venir encore enrichir nos débats en interne, et le panel de compétences que nous proposons à nos clients.

**Merci beaucoup au talentueux dessinateur [Fix](https://www.fix-dessinateur.com/), qui a su illustrer nos valeurs de manière à la fois ludique et inspirante ❤️ !**
