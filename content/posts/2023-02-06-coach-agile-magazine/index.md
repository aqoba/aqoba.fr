---
draft: false
title: Coach Agile Magazine
authors:
  - olivier-marquet
  - thomas-clavier
values:
  - conviction
tags:
  - humour
  - cam
date: 2023-02-08T06:38:45.437Z
thumbnail: couverture-article-cam.png
subtitle: Ou comment faire de la veille avec humour.
---
En septembre dernier suite à l’initiative d’ Olivier MARQUET, nous avons lancé un premier “Coach Agile Magazine”, une couverture de magazine factice composée uniquement de blagues sur le monde de l’agilité. Avec cette publication mensuelle, sur LinkedIn, notre intention est  de faire rire avec bienveillance, et de pointer du doigt les travers de notre communauté agile.

Mais en regardant attentivement les couvertures de “Coach Agile Magazine”, nous nous rendons compte qu’elles sont loin d’être anodines : elles disent beaucoup de la veille que nous réalisons, des préoccupations qui nous occupent chez nos clients et des réflexions que nous menons avec nos pairs.

Bref, en observant avec attention ces blagues, il est possible de les relier avec les actualités de notre communauté agile. Retour sur ces 5 mois d’actualités pas toujours en lien avec l’agilité.

## Août et Septembre 2022

{{< figure  src="magazine-coach-agile-sept22-maj-couv.png" alt="Coach agile Magazine #1" class="center-small">}}

Bilan du BAC 2022, 573 personnes ont été interpellées pour fraudes.

En septembre, nous avons vu fleurir un certain nombre d'offres de coaching de coach, quelques-uns ont profité d’Agile en Seine 2022 pour annoncer leur nouvelle offre. Si je cherche “Supervision coach” dans linkedin, le moteur me remonte 38 contacts … probablement un signe : comme la corporation des thérapeutes, celle des coachs agiles est en train de se structurer et de s’enrichir.

Arnaud Lemaire avait lancé un pavé dans la marre en 2019 avec son [article](https://medium.com/@Lilobase/pourquoi-jai-un-probl%C3%A8me-avec-les-ice-breaker-energizer-2378ef9dbbf6) lors de la reprise des Agiles Tours en présentiel le débat sur la ratio bénéfices / risques des ice-breakers / energizers revient sur les réseaux sociaux. Comme Arnaud, Thomas Clavier vote contre l’usage quasi-systématique de ces petits jeux. Même si d’autres membres d’aqoba en sont plutôt promoteurs.

## Octobre 2022

{{< figure  src="magazine-coach-agile-oct22.png" alt="Coach agile Magazine #2" class="center-small">}}

En février 2021, Atlassian (l’éditeur de l’outil JIRA) avait arrêté de vendre des licences “Serveur” qui permettaient à une entreprise d’héberger chez elle et de faire évoluer sa propre version de l’outil. En février 2022 c’était l'arrêt des mises à jour de ces versions "Serveur". Début octobre Atlassian rappelait à tous ses clients qu’il était urgent de prendre des licences “Cloud” avant la fin du support de la version "Serveur". C’est un chamboulement important chez toutes les entreprises utilisatrices de JIRA que nous accompagnons avec beaucoup de conduite du changement.

Après le SAFe Summit du mois d'août 2022 et ses annonces, de nombreux articles fleurissent pour expliquer les nouveautés du Framework. En particulier sur le blog de scaledagileframework.com avec des articles sur : l’introduction des OKRs (Objectives, Key Results), les nouvelles formations, les traductions et des retours d’expériences non-IT. SAFe reste le framework d’agilité à l’échelle le plus plébiscité : ses mises à jour sont donc largement scrutées par notre communauté.

A l’automne 2022, nous lisons plusieurs articles et une forte activité sur les réseaux sociaux autour des OKRs et de la difficulté à les mettre en œuvre sans accompagnement. Au sein d’aqoba, on croise effectivement plusieurs directions qui nous partagent des récits d'échecs : l’utilisation d’OKR ne s’improvise pas.

## Novembre 2022

{{< figure  src="magazine-coach-agile-nov22.png" alt="Coach agile Magazine #3" class="center-small">}}

Fin octobre c’était la conférence FlowCon, sources d’innombrables apprentissages. En dehors des conférences, les échanges sont très nombreux. Parmi toutes ces discussions, après la conférence de [Julien Topçu](https://www.linkedin.com/in/julien-top%C3%A7u/) sur la Loi de Conway, il y a eu tout un débat sur l’attention à porter sur le fait qu’il ne faut pas confondre la loi de Conway inversée et l’inversion de la loi de Conway. Ça vous paraît abscons ? Si vous n’êtes pas familier de cette théorie, faites-nous signe : on se fera un plaisir de vous l’expliquer.

Chez bon nombre de nos clients, depuis le début de l’année scolaire, c’est le retour progressif en présentiel obligatoire. Certain(e)s sont plus que réticents à revenir dans les locaux de leur entreprise. Nous voyons cette situation comme un véritable marqueur de la culture d’entreprise. ([voir l'article](https://aqoba.fr/posts/20220927-ils-ne-veulent-pas-revenir-sur-site-mes-5-cl%C3%A9s-pour-r%C3%A9ussir-le-t%C3%A9l%C3%A9travail/))

Depuis fin septembre la peur d'un manque d'énergie pour passer l'hiver sereinement monte de plus en plus. Chacun de nos politiques y va de sa petite communication “sobriété” : pull à col roulé, doudoune et étendoirs sont mis en avant, etc.

Chaque fois que nous intervenons auprès d’une nouvelle équipe ou d’une nouvelle organisation, nous sommes confrontés à des personnes qui pointent notre vocabulaire de spécialiste. Nous faisons notre maximum pour rendre les concepts que nous utilisons aussi accessibles que possible. Mais, comme tous les coachs et consultants agiles, force est de constater que nous avons des progrès à faire.

## Décembre 2022

{{< figure  src="magazine-coach-agile-dec22.png" alt="Coach agile Magazine #4" class="center-small">}}

Même si 2022 est l’année la plus chaude jamais mesurée en France, début décembre nous avons eu droit à quelques millimètres de neige. Quelques millimètres qui sont parfois synonymes de perturbations sur les routes et les réseaux de transport urbains dans les villes de France où ils sont rares.

Fin octobre, Elon Musk a finalisé le rachat de Twitter. S'en est suivie toute une série d'annonces et de rebondissements illustrant sa façon de diriger. L'histoire s'est terminée sur un sondage ou 9,7 millions de personnes lui demandent de se retirer de la direction du réseau social.

Certains coachs de notre entourage se demandent si l'acronyme VUCA (Volatility, Uncertainty, Complexity et Ambiguity) popularisé par l'armée américaine dans les années 90 ne serait pas en train d'être remplacé par l'acronyme BANI (Brittle, Anxious, Non-linear, Incomprehensible).

## Janvier 2023

{{< figure  src="magazine-coach-agile-jan23.png" alt="Coach agile Magazine #5" class="center-small">}}

ChatGPT est dans toutes les bouches en ce début d’année. Et si on demandait à l’Intelligence Artificielle de coacher des équipes elle-même.

Dan Pink est un auteur américain qui a publié “La vérité sur ce qui nous motive” et ses conclusions sont largement partagées au sein de la communauté agile. Il défend notamment que les individus sont motivés au sein d’un univers professionnel qui leur donne du sens, de l’autonomie et la capacité à gagner en expertise. 

Nous avions déjà parlé de JIRA Cloud dans le numéro d’octobre. Avec le début d’année et le renouvellement des licences logicielles, nos clients sont encore plus nombreux à se confronter au passage à JIRA Cloud qui modifie l’utilisation au quotidien de cet outil clé dans la vie des équipes.

unFIX est le résultat des recherches de l’équipe de Jurgen Appelo. Il propose une nouvelle approche pour analyser et faire évoluer une organisation. Le débat au sein de la communauté agile est vif pour déterminer s’il s’agit d’un nouveau framework à appliquer ou simplement d’un outil complémentaire aux frameworks existants. Si le sujet pique votre curiosité, prenez 10’ pour lire [l’article de référence en français](https://aqoba.fr/posts/20221129-%C3%A0-la-d%C3%A9couverte-dunfix/) que nous avons édité pour faire découvrir unFIX en France.

## Conclusion

Coach Agile Magazine est un exutoire. Une façon de nous rappeler qu’on ne doit pas se prendre au sérieux au sein de notre communauté agile. Car si nous avons toutes et tous envie de changer le monde, il nous arrive de le faire avec pas mal de maladresses !

Si vous aussi, vous voulez participer aux prochains Coach Agile Magazines, n'hésitez pas à nous partager les blagues qui vous viennent. Si elles sont bienveillantes, nous serons très heureux de les intégrer sur une prochaine couverture.\
\
Et pour ne pas rater un numéro, abonnez vous à [notre page LinkedIn](https://www.linkedin.com/company/78857629) ou à [notre compte Mastodon](https://pouet.chapril.org/@aqoba).
