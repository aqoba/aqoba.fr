---
draft: false
title: Prenez conscience des bénéfices de Kanban avec le Kanban Pizza Game
authors:
  - marion-bialecki
  - benjamin-feireisen
values:
  - partage
  - conviction
tags:
  - Serious Game
  - Kanban
offers:
  - unlock-delivery
date: 2024-04-05T10:54:00.000Z
thumbnail: kanban-pizza-game.png
subtitle: Le Serious Game pour vous faire découvrir le flux tiré
---
## La vérité sur Kanban

Vous qui arrivez ici, vous voulez transmettre ce qu’est réellement Kanban ? Vous entendez vos équipes dire que c’est juste un tableau visuel, un board Jira où vous avancez des tickets dans des colonnes ?

Kanban est bien plus que ça : c’est une stratégie pour livrer de la valeur avec des principes et des règles clés vous permettant de mieux travailler ensemble et de faire évoluer votre processus de travail !

## Pourquoi utiliser Kanban ?

Là où Kanban est intéressant, c’est que dès que vous avez un processus, soit du travail qui passe dans différentes étapes, vous pouvez tout de suite l’utiliser !

Vous avez pu observer ces différents cas : 

* Quand vous regardez l’âge de vos tickets, et qu’ils sont en moyenne commencés depuis 150 jours
* Que vous avez une équipe de 6 personnes avec 43 tickets en cours
* Que vous ne savez plus comment améliorer votre manière de travailler
* Que le QA a beaucoup trop de travail par rapport au reste de l’équipe

C’est là que Kanban peut vous être utile… Donc pour à peu près toutes les équipes, non ?

Et la meilleure manière d’apprendre, c’est en expérimentant.
Pour vous permettre de faire expérimenter Kanban, vous pourrez utiliser ce jeu. Il va vous faire vivre les concepts de flux tirés en limitant le travail en cours, et vous faire réaliser les contraintes liées au stockage et ce que cela amène pour votre delivery au quotidien. Voici notre version de ce jeu !

Ce que vous obtiendrez en testant ce jeu, c’est avant tout de vivre le flux tiré. Dans l’informatique, où le travail est immatériel, cela peut sembler difficile de le mettre en place… En le vivant une fois, il est bien plus simple de réitérer l’expérience dans son équipe.

De plus, vous découvrirez les 6 pratiques Kanban et leur mise en place, qui sont une manière efficace de mettre en place ce flux tiré et d’améliorer votre delivery.

Mieux encore, en le faisant avec vos équipes, cela vous permettra de lancer des discussions sur comment mettre en place Kanban pour vous !

Chez aqoba, nous avons joué cet atelier de nombreuses fois, auprès de public très différents ( coachs, équipes dev, managers..). Nous avons ainsi pu observer les comportements, et parfois les participants ont eu des réactions ou des questions qui nous ont étonnés. On vous livre ici notre retour d’expérience et quelques astuces pour vous aider à appréhender l’atelier.

{{< figure  src="illustration-pizza-game.png" alt="Illustration Pizza Game" class="center-small">}}

## Description de l’atelier

Après une brève session de présentation des origines de la méthode (Toyota tout ça….) et un rappel des 4 piliers lançons-nous dans la phase de jeu. Nous glissons nos participants dans la peau de pizzaïolos qui ouvrent boutique et dont le but est de concevoir le plus efficacement possible le plus grand nombre de parts de pizzas. Pour cela on fait constituer des équipes de 4 à 5 participants, pas plus. Il faut moins de participants que de tâches dans la conception pour forcer l’organisation et l’entraide.

Le jeu se déroule en 4 rounds de “fabrication de pizzas” et une dernière phase de conception visuelle de l'organisation de l’équipe.

On transmet quelques règles (c’est quoi une belle part de pizzas, on la conçoit avec quel matériel), on distribue un récapitulatif visuel de ces règles. Parmi ces règles se trouve une contrainte forte, celle du four, qui ne permettant la cuisson que de 3 éléments en même temps et sur une durée minimum de 30 secondes, sera un limiteur naturel du flux de l’équipe.

La durée des rounds dépend du temps imparti pour votre atelier, les seules règles étant de conserver un temps de fabrication identique pour les 4 rounds et de ne pas le communiquer aux participants (vous verrez pourquoi après). Nous sommes partis sur des rounds de 6mn.

Ready, Steady…GO!

{{< figure  src="dowload-slides.png" alt="Lien de téléchargement des slides" caption="Téléchargez le support en cliquant sur l'image" link="https://bit.ly/PizzaGameAqoba" class="center-small">}}

## ROUND 1

{{< figure  src="process.png" alt="process" class="float-left-tiny">}}

Le principe de la première phase est de lancer l’équipe dans son auto-organisation. Pour cela, on leur laisse 2 minutes pendant lesquelles les participants se répartissent les rôles et on lance l’expérimentation rapidement. À ce stade, on pousse à la spontanéité, moins à la réflexion.

Au terme des 6 minutes de fabrication, on arrête tout. Et on introduit 2 nouvelles informations : le contrôle qualité (toute part non conforme à l’attendu sera rejetée ) et le décompte des points. 

Chaque part produite a une certaine valeur, mais chaque ingrédient fabriqué et non utilisé impacte négativement le score de l’équipe.

L’équipe, qui au round 1 se focalise sur le nombre à produire, a généralement un score négatif tant il est impacté par les découpes superflues !

Et pour pimenter tout ça (sans huile) on rapporte les points dans un tableau global, pour lancer une petite compétition entre les différentes tables.

{{< figure  src="enseignements.png" alt="Apprentissages" class="float-left-tiny">}}

Même en ayant présenté les 4 principes de Kanban, sans les pratiques, en général, les équipes se mettent bille en tête à créer encore et encore… Avec un focus sur les résultats plus que sur la manière de faire. De nombreuses parts de pizzas sont créées sans réfléchir au vieillissement possible de la sauce tomate ou du fromage ! 
Cette attitude ressemble en tout point à ce qui peut être vécu par une équipe dont les règles de fonctionnement ne sont pas claires. Par exemple, une équipe dev a qui on demande de réaliser des US qui ne sont pas bien définies, dont les critères d'acceptation ne sont pas explicites, va avoir tendance à clôturer rapidement sans pour autant prendre en compte toutes les contraintes associées.
Dans l’atelier la plupart des équipes se retrouvent à créer le plus de stock possible plutôt que de livrer rapidement une première pizza. Chacun travaille sur sa partie et ne regarde pas ce que fait l’autre : on ne regarde pas assez le processus en entier !
C’est le cas d’une équipe focalisée sur la production et non sur le résultat et  la valeur délivrée.

{{< figure  src="tips.png" alt="Tips" class="float-left-tiny">}}

L’idée ici est bien évidemment de laisser un flou dans les directives. Certes, la chasse au gaspillage est rappelée dans le scénario d’introduction, mais nous n’insisterons volontairement pas dessus pour créer la surprise au moment du décompte des points, et que la prise de conscience de l’impact du reliquat soit totale.

En plus de la contrainte du four, nous avons volontairement limité par table le matériel disponible (1 ciseau, 1 marqueur) mais avons laissé à la vue de tous sur une table à part, du matériel supplémentaire. A voir qui aura l’idée de se lever et se servir pour faciliter l’entraide et fluidifier leur travail.

## ROUND 2

{{< figure  src="process.png" alt="process" class="float-left-tiny">}}

Avant de lancer la 2eme phase, il est important de faire un focus sur la notion de flux tiré et sur les 6 pratiques de Kanban 

{{< figure  src="les-6-pratiques-kanban.png" alt="Les 6 pratiques Kanban : Visualiser le flux de travail, Limiter le travail en cours, Gérer le flux, Mettre en place des boucles de feedback, Rendre les règles explicites, S’améliorer collectivement et en continu" class="center-small">}}

Il est alors donné aux participants un temps de rétrospective durant laquelle ils devront mettre en place ces derniers enseignements et revoir la qualité de leur production si nécessaire.
On passe dans les équipes pour répondre aux questions et observer les discussions, les dynamiques et les actions prises, puis on relance un round.

{{< figure  src="enseignements.png" alt="Apprentissages" class="float-left-tiny">}}

Ces 6 pratiques, une fois abordées, font prendre conscience que Kanban n’est pas qu’un tableau visuel. Les participants peuvent vraiment se rendre compte de l’intérêt de limiter le travail en cours, et de regarder ce que les autres font. Le debriefing est bien souvent bien plus joyeux qu’au premier round. En général, les participants font de biens meilleurs scores pour deux raisons : 

* Ils connaissent toutes les règles. À ce stade, il faut donc les amener à faire le parallèle avec le monde réel : que se passe-t-il quand vous ne connaissez pas les règles dans votre travail ?
* En limitant le travail en cours, cela force les participants à se parler pendant l’atelier, et à voir comment améliorer le flux de travail. Lors d’un daily, les limites permettent de focaliser nos discussions sur les obstacles et sur comment améliorer notre delivery.

Là, les équipes découvrent réellement le cœur de Kanban : Arrêter de commencer (des pizzas) et commencer à les finir (parce que nos clients ont faim) ! En limitant le travail en cours, naturellement un flux tiré se crée.

Parfois les équipes peuvent dire que c’est surtout parce qu'elles ont déjà fait un premier round ensemble que les scores sont bien meilleurs. C’est sûrement en partie vrai, mais en mettant ces pratiques en place les participants étaient d’accord pour dire qu’elles accélèrent la connaissance de leur système !

{{< figure  src="tips.png" alt="Tips" class="float-left-tiny">}}

Sur ce round il est important d’observer les améliorations tables par tables sans trop orienter les pratiques, cela servira de support de discussion en restitution finale en demandant aux participants de présenter leur voyage, les étapes par lesquelles ils sont passés dans leur process d’amélioration.

Si ce round donne généralement un bien meilleur score par équipe, il est aussi celui des possibles débordements et passions(!), c’est là que l’esprit de compétition peut faire rage. On a effectivement vu des “leaders” d’équipes tellement passionnés que le ton montait, si le rendement de certains équipiers était jugé insuffisant.. Là c’est notre rôle de facilitateur de cadrer et faire retrouver une sérénité de déroulement. Apprendre oui, jouer oui, mais toujours dans le respect des autres.

## ROUND 3

{{< figure  src="process.png" alt="process" class="float-left-tiny">}}

Le round 3 illustre l’évolution de la vie de nos produits ou de nos environnements.

On y introduit une nouvelle recette. Les équipes sont donc amenées à produire 2 types de pizzas différents. Pour piloter les demandes, est ajouté une gestion des commandes par tickets ( ex ticket 1, 2 part de pizza X et 1 part de pizza Y) : On arrête de produire pour se faire plaisir, on écoute les besoins du client. Le flux n’est plus tiré par la capacité à produire, mais par une demande.

Les points ne sont comptés cette fois que si la commande est complète, et toute part produite hors commande est déduite.

Ce changement profond nécessite une modification de l’organisation actuelle et ajoute des postes supplémentaires (la gestion de commande et la clôture de commande), sans changer l’effectif de l’équipe.

Le parallèle avec le monde réel et ses ajouts constants de contraintes, les évolutions de produits ou même la multiplicité de projets en cours est toute faite!

{{< figure  src="enseignements.png" alt="Apprentissages" class="float-left-tiny">}}

Généralement les résultats sont d’un coup plus faible qu’au second round. D’où cela vient ?

Tout le monde l’a ressenti : de l’introduction de nouvelles contraintes ! 

Si l’on change quelque chose dans une équipe, par exemple : 

* Une règle imposée qui fait que les testeurs ne font désormais que du test et les développeurs ne peuvent pas aider,
* Une baisse d’effectif non prévu,
* Une nouvelle personne qui intègre l’équipe,
* Une nouvelle Definition of Done imposée à l’équipe.

Toutes ces contraintes externes vont perturber le flux de travail !

En les ajoutant, il existe un temps incompressible qui demande aux équipes de s’adapter. Malgré tout, elles ont pu avoir des scores intéressants grâce aux 6 pratiques déjà mises en place dès le second round : elles ont maintenu leur flux tiré.

{{< figure  src="illustration-flowcon.png" alt="Illustration issue de l’animation de l’atelier à la FlowCon 2024" caption="Illustration issue de l’animation de l’atelier à la FlowCon 2024" class="center-small">}}

{{< figure  src="tips.png" alt="Tips" class="float-left-tiny">}}

Lors de la phase de préparation et d’amélioration continue, le facilitateur peut être plus insistant sur l’application des pratiques Kanban. 

Nous conseillons ici de bien matérialiser les postes de travail, là-bas d’indiquer les WIP, sur une autre table de chronométrer le temps passé entre certaines étapes.. tout pour faire vivre les enseignements Kanban selon la maturité des équipes…. et **EX-PE-RI-MEN-TER!**
<br><br><br>

## ROUND 4

{{< figure  src="process.png" alt="process" class="float-left-tiny">}}

Le dernier round n’introduit aucune nouvelle notion .Les équipes ont simplement l'opportunité de recommencer le round précédent en appliquant les enseignements tirés de leur première expérience.

Cela souligne qu’une 2ème itération avec des règles et contraintes similaires, et assimilées par l’équipe donne forcément des résultats supérieurs. Cela illustre que la stabilité et la communication au sein d’une équipe qui applique les règles élémentaires de KANBAN entraîne forcément des actions positives d’amélioration continue.

Ce sera le round de conclusion sur lequel nous clôturerons la phase de production de pizzas et nous clôturons le décompte de point. Évidemment toutes les équipes sont félicitées pour leur évolution ( pensez aux récompenses, bonbons/goodies, ça fait toujours plaisir)

{{< figure  src="enseignements.png" alt="Apprentissages" class="float-left-tiny">}}

En testant de nouvelles pratiques par un round d’expérimentation, comme mesurer le temps de cycle ou bien changer de poste de travail pour faire autre chose, les équipes ont pu prendre ce temps pour expérimenter d’autres manières de fonctionner.

Pour beaucoup sans s’en rendre compte, il y a eu plus de swarming : “travailler ensemble sur une même tâche”, et donc du partage de connaissances.
<br><br> <br>

{{< figure  src="tips.png" alt="Tips" class="float-left-tiny">}}

C’est la phase pendant laquelle on peut le plus échanger avec les équipes pendant la conception. Le geste est devenu plus mécanique, plus sûr et souvent les participants peuvent exprimer leur ressenti tout en continuant à produire. C’est souvent là que l’on va tirer les meilleures informations pour illustrer la conclusion.

Nous avons parfois présenté le round 4 comme un round d’innovation.

Malgré les contraintes introduites en round 3 certaines équipes obtiennent  des résultats très corrects. Au contraire d’autres montrent des signes de lassitude quand à la répétition des phases de production. Il est alors important de savoir adapter le format selon l’ambiance de l’atelier et le comportement des participants.

On peut alors proposer de changer les WIP, d’inverser les rôles, de prendre le matériel supplémentaire (rarement demandé spontanément) si personne n’y avait songé auparavant.

## LA CONSTRUCTION DU BOARD

{{< figure  src="process.png" alt="process" class="float-left-tiny">}}

C’est maintenant l’heure de la conception visuelle. On demande à chaque équipe de réfléchir collectivement aux étapes par lesquelles sont passées leur part de pizza, de sa commande à sa conception. 

L’équipe aura une dizaine de minutes pour matérialiser le board représentatif de ces étapes, les règles et contraintes pour chaque étape et idéalement le temps passé sur chaque étape, à l’instar des tableaux dans les usines de production TOYOTA que l’on présentait en introduction de l’atelier.

L’objectif : le board doit être compréhensible de tous, représentatif de l’équipe et surtout… beau!

{{< figure  src="illustration2-flowcon.png" alt="Illustration issue de l’animation de l’atelier à la FlowCon 2024" caption="Illustration issue de l’animation de l’atelier à la FlowCon 2024" class="center-large">}}

{{< figure  src="enseignements.png" alt="Apprentissages" class="float-left-tiny">}}

L’une des premières choses qui frappent les équipes, c’est que dans chaque étape du processus existe en réalité deux sous étapes : “en cours” et “terminé”. Quand les équipes découpent les parts de pizzas, il y a le moment où elles sont découpées, et le moment… Ou elles ont fini d’être découpées !

En rendant cela visible sur leur board et en mettant une limitation du travail en cours à chaque étape, cela leur a vraiment permis de comprendre l’intérêt du flux tiré.

On arrête de ne penser qu’à son travail personnel, et on réfléchit ensemble à la meilleure manière de gérer son processus et d’avoir un rythme soutenable !

De même, l’intérêt de rendre visible les règles sur chaque étape, par exemple avec des Définitions de Fini par étape sous chaque colonne, aide à contrôler qu’elles soient suivies.

Un petit rappel : les tickets vieillissent. L’âge des tâches, c’est facile à suivre, et ça donne instantanément un état de santé de votre processus. Et votre de processus, c’est quoi son âge moyen ?

{{< figure  src="illustration3-flowcon.png" alt="Illustration issue de l’animation de l’atelier à la FlowCon 2024" class="center-small">}}

{{< figure  src="tips.png" alt="Tips" class="float-left-tiny">}}

C’est la fin de l’atelier, à ce stade les participants sont fatigués (surtout les producteurs de sauce tomate que la tendinite guette), l'ambiance est alors plus légère. Les échanges sont facilités et spontanés mais l’animateur doit à ce stade rester vigilant, la conception visuelle est un élément clé de Kanban et ne doit pas être minimisé.

Une fois les boards conçus, on demande aux équipes de les présenter rapidement et surtout d'indiquer le round qui pour eux a été décisif, celui dans lequel ils ont trouvé LA règle à appliquer qui leur a permis de faciliter leur production.

Ce qui ressort des participants : 

* De faire attention à l’âge et au temps de livraison : toutes les pratiques Kanban cherchent à réduire l’âge, car plus un ticket vieillit, plus il a de risque de vieillir encore plus !
* De vivre le flux tiré une première fois, qui permet d’équilibrer le travail dans une équipe et de garder un rythme soutenable. Et par cette expérience, d’avoir envie de le mettre en place.
* De limiter le travail en cours, et que ça peut aider à plein de choses : réduire les dépendances, réduire la charge mentale des équipes, lancer des discussions sur comment s’améliorer ensemble.
* D’observer les contraintes : d’abord, le manque de ciseaux, puis le four avec 3 parts de pizza en même temps. Le goulot (ou goulet, choisissez) d’étranglement non négociable est le four, et il devra impacter les limites de tout le système. Et s’il y avait eu deux fours dès le premier round : pensez-vous que les équipes auraient été plus efficaces ?

## A vous !

Chez aqoba on adore cet atelier qui, selon nous, permet très simplement et efficacement d’illustrer l’importance de l’observation du flux et les quelques mesures qui peuvent aider son amélioration.

Les retours des participants tant en termes d’enseignement que d’aspect ludique sont globalement très bons.

De nôtre côté, on réfléchit déjà à une version un peu plus durable (Des légos ? De la pâte à modeler ?), qui permettrait de consommer moins de papier. Même si à chaque atelier nous tentons de recycler le plus de feuilles et de post it possible!

Alors, prêts à vous lancer et revenir aux bases de Kanban?
