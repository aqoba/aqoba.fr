---
draft: false
title: Reboostez vos équipes avec une approche Fluide (featuring FaST)
authors:
  - olivier-marquet
values:
  - partage
  - transparence
tags:
  - fast
  - fluide
  - ""
offers:
  - unlock-transformation
date: 2025-01-22T01:00:00.000Z
thumbnail: boostfast.png
subtitle: Repenser son organisation et sortir du cadre pour plus d’agilité et d’impact
---
Au sein d'un programme que j'accompagnais en 2018 nous avions 22 collaborateurs répartis en 3 équipes. Cette organisation a bien fonctionné au départ, permettant une gestion efficace des tâches et une forte collaboration. Cependant, au fil du temps, les équipes ont dû gérer de plus en plus d'adhérences entre elles. En travaillant sur des fonctionnalités transverses, elles ont dû collaborer de plus en plus étroitement pour créer de la valeur ensemble. En parallèle, nous avons constaté une baisse de la motivation au sein des équipes, et un gros challenge en terme de tenue de dates de livraisons

Cette situation nous a conduit à proposer une réorganisation des équipes pour plusieurs raisons :

* S’organiser par composants n’étant plus adaptée aux fonctionnalités transversales, il fallait recentrer les équipes sur les besoins utilisateurs et leur donner plus d’autonomie pour atteindre des objectifs complets
* Permettre une meilleure circulation des connaissances et favoriser la progression des compétences au sein des équipes.
* Stimuler la motivation et l'engagement pour retrouver un esprit d'initiative.

En réorganisant les équipes avec ces objectifs en tête, nous espérions améliorer la collaboration, renforcer l'engagement et augmenter la performance globale de notre programme pour atteindre les objectifs de celui-ci.

Je me suis alors orienté vers la créations d'équipes temporaires centrées sur des objectifs. Rétrospectivement (on était en 2018), cette approche ressemblait beaucoup à l'approche Fluid Scaling Technology (FaST). Mais avant de zoomer sur FaST, laissez moi vous raconter comment on a fait.

## Retour d'expérience sur la mise en place d'équipes "Fluides"

Nous étions organisés sur un modèle inspiré de SAFe, avec des projections d'objectifs pour le trimestre à venir (style PI Planning sur 1 journée). Nous avons donc testé l'auto-organisation autour de thématiques à améliorer centrées sur des cas d'usages utilisateurs, le 4ème thème étant un thème technique pour améliorer les modes de livraisons. Des objectifs ont été proposés sur chaque thème en amont. Ces objectifs pouvaient être revus par les équipes lors de l'exercice de projection sur le trimestre à venir en fonction de ce qu'ils pensaient atteignable.

Cette image illustre le déroulement de l’atelier, avec les thèmes représentés par de grands post-its jaunes, les objectifs associés représentés par des post-its roses et oranges, et les membres des équipes positionnés en dessous, identifiés par des post-its jaunes fluo pour les PO et bleus pour les équipiers.

{{< figure  src="equipes_fluides.png" alt="exemple de mise een oeuvre d'équipes fluides" class="center-small">}}

Une fois ces objectifs partagés avec le collectif, c'était alors le moment de constituer les équipes, les règles de constitutions étaient les suivantes:

* Vous construisez les équipes !
* Les équipes sont un mélange des équipes actuelles.
* Toutes les équipes comptent entre 4 et 6 membres.
* Chaque équipe possède toutes les compétences nécessaires pour atteindre ses objectifs.
* Ne vous regroupez pas par affinité, mais parce que c'est pertinent.
* Prenez en compte ce dont vous avez besoin pour avoir une équipe autonome.
* Trouvez le meilleur équilibre entre les différentes équipes afin d'atteindre les objectifs.

Nous avons donc testé ce mode de fonctionnement sur le trimestre, en voici les enseignements remontés par les équipes elles mêmes:

**Ce qui a marché de leur point de vue :**

* Bon mélange des équipes d'origine
* Connaissances accrues et partagées à travers les équipes
* Esprit d'entraide
* Bonne dynamique et structuration sur le trimestre

**Ce qui a moins bien marché de leur point de vue :**

* Objectifs pas vraiment compris ni assimilés sur le moment du PI Planning
* Des réorganisations d'équipes nécessaire dans le cours du trimestre (rétrospectivement c'est très Fluide, mais les équipes ont identifié ca comme un irritant)

Nous avons décidés de réitérer ce mode d'organisation sur le trimestre suivant, en changeant 2 choses:

* en anticipant le partage des objectifs sur les thèmes en amont du PI Planning
* en laissant ouvert une place de marché ou les volontaires pouvaient se placer en avance pour constituer les équipes
* en proposant certaines répartitions d'expertise dans certaines équipes en amont car c'est parfois difficile de ne pas se répartir par affinité.

Ainsi je jour du PI Planning, seuls restaient le rappel des enjeux et des objectifs, quelques ajustements d'équipes, et la projections sur le trimestre à venir, et ça a bien fonctionné !

Comme sur tout grand programme, des dates étaient engagées, elles ont été tenues:

> L'histoire retiendra que vous êtes la première équipe à mettre en production un projet dans les délais, dans l'entreprise, une belle réussite collective !

Il n'y a pas eu de 3ème trimestre, le programme ayant atteint ses objectifs il a été intégré au sein d'un autre plus gros programme avec lequel il avait des adhérences, formant alors un groupe de plus de 100 personnes. Les équipes se sont alors re-stabilisées pour faciliter la gestion à cette échelle.

## Que retenir d'une auto-organisation en équipes fluides ?

Laisser des équipes s’auto-organiser et se reconstruire à chaque trimestre semble possible avec des groupes < 30, même si FaST permet théoriquement de monter beaucoup plus haut, je trouve que c'est le sweet spot pour ce type d'auto-organisation.

{{< figure  src="dunbar.png" alt="Nombre de Dunbar" class="center-large">}}

Pour que l’auto-organisation fonctionne, un certain niveau de maturité et d’autonomie des équipiers est essentiel. Les membres doivent être capables de se répartir en fonction des besoins pour atteindre les objectifs. Si ce prérequis n’est pas là, il peut être utile de proposer des pré-compositions d’équipes et de s’appuyer sur quelques personnes clés pour guider la structure.

Ce mode d'organisation, en permettant de changer de types d’objectifs chaque trimestre et en se recomposant, redonne de la dynamique au groupe et *in fine* de la motivation. Il assure en plus la répartition des connaissances, car au fur et à mesure des recompositions, plusieurs personnes deviennent sachantes sur différentes briques techniques et fonctionnelles, ce qui mécaniquement réduit le risque de bus factor.

Pour creuser un peu plus le sujet des équipes Fluides, plongez vous dans ce résumé de l'approche FaST, qui vous propose un mode d'organisation complet, avec plusieurs équipes, mais que vous pouvez instancier même au sein d'une seule équipe en remplaçant (dans votre tête) le terme "équipe" par "individu".

## Résumé de l'approche FaST : Fluid Scaling Technology

L’approche **FaST (Fluid Scaling Technology)** est une méthode ultra-légère, conçue pour permettre aux organisations de s’adapter rapidement tout en évitant les lourdeurs structurelles. Contrairement à des frameworks plus formels comme SAFe, FaST mise sur une organisation fluide et émergente pour maximiser l’impact et l’efficacité.

### Les principes clés

* **Simplicité structurelle** : Les équipes se recomposent en fonction des besoins, sans échelons hiérarchiques complexes.
* **Adaptabilité** : Les organisations s’ajustent naturellement à l’évolution des tâches et des objectifs.
* **Autonomie** : Les collectifs sont encouragés à prendre leurs propres décisions et à travailler de manière autonome.

### Mise en place initiale

1. **Former un Collectif** : Réunir toutes les personnes nécessaires pour accomplir le travail dans un groupe autonome et autogéré appelé "Collectif".
2. **Définir une vision globale** : Établir une mission et un objectif communs pour le Collectif.
3. **Visualiser le travail** : Représenter visuellement la compréhension actuelle et les progrès du travail du Collectif.

### Cycle de Valeur

FaST fonctionne par cycles continus appelés "Cycles de Valeur" (Value Cycle), qui incluent trois étapes qui se répètent :

1. **S’auto-organiser** : Faciliter une réunion pour que les membres s'organisent en équipes autour des tâches à réaliser.
2. **Travailler** : Les équipes planifient et collaborent sur les tâches identifiées.
3. **Synchroniser** : Le Collectif se réunit régulièrement pour partager les apprentissages et les progrès, assurant une compréhension partagée de l'état du produit et des conditions actuelles.

### Les rôles dans FaST

* **Membre** : Tous les participants du Collectif.
* **Le Gestionnaire de Produit (Product Manager)** : Un membre qui comprend les mécanismes de la stratégie, du discovery et de la livraison de valeur aux clients.
* **Le Gardien d’Équipe (Team Steward)** : Un membre qui se porte volontaire pour guider une équipe pendant un Cycle de Valeur.
* **Le Gardien de Fonctionnalité (Feature Steward) en optionnel** : Un membre responsable de comprendre et de discuter les progrès d'une fonctionnalité spécifique.

### Les outils FaST

* **Accord collectif** : Document vivant décrivant les modalités de travail du Collectif.
* **Tableau de la place de marché** : Outil visuel pour voir les éléments de travail choisis pour le Cycle de Valeur en cours et la composition des équipes.
* **Visualisation du travail** : Affichage des informations sur la compréhension actuelle et les progrès du travail.

### Réunions FaST

La réunion FaST est le cœur battant du Collectif, permettant la synchronisation, l'adaptation et l'émergence du travail et des structures.

1. **Synchronisation du Collectif** : Mise à jour concise de chaque équipe sur les réalisations et les apprentissages du Cycle de Valeur précédent.
2. **Discours du Product Manager** : Ouverture du Cycle de Valeur avec un message inspirant et des mises à jour sur le produit.
3. **Auto-organisation en équipes autour du travail** : Création d'un marché de travail où les membres choisissent comment contribuer (inspiré du Forum Ouvert).

### La Loi de la Mobilité

Centrale dans l'approche FaST, la mobilité permet aux membres de changer d'équipe selon les besoins, favorisant adaptabilité, autonomie et résolution efficace des problèmes. Elle renforce l'engagement et la motivation en offrant flexibilité et responsabilisation, et optimise l'utilisation des compétences pour une meilleure synergie et innovation au sein du Collectif.

### Charte du Collectif

La Charte du Collectif est un document vivant qui décrit les modalités de fonctionnement et de collaboration au sein du Collectif. Elle inclut :

* **Décisions** : Comment les décisions sont prises collectivement.
* **Résolution des conflits** : Les mécanismes de gestion des conflits pour assurer une coopération harmonieuse.
* **Rôles et responsabilités** : Clarification des rôles de chaque membre pour éviter les malentendus.
* **Valeurs partagées** : Les valeurs communes qui guident les comportements et les interactions au sein du Collectif.

### Gestion des dépendances

Enfin, la gestion des dépendances est cruciale pour le succès de FaST. Voici comment elle est abordée :

* **Auto-organisation et ajustements** : Les dépendances les plus évidentes sont résolues lors des réunions FaST par l'auto-organisation et les ajustements.
* **Communication continue** : Les équipes interconnectées gèrent les dépendances émergentes par la communication et l'intelligence collective.
* **Réunions impromptues** : Les équipes peuvent organiser des réunions à tout moment pour discuter et résoudre des conflits potentiels ou réels.
* **Solutions créatives** : Les équipes ont la liberté de trouver des solutions innovantes pour surmonter les obstacles, telles que fusionner temporairement, changer de priorités, ou mener des expérimentations parallèles.

### FaST en 2 mots

FaST propose un modèle opératoire agile et fluide, permettant aux équipes de s'adapter rapidement aux changements et de maximiser leur efficacité par une organisation autogérée et collaborative, basée sur la recomposition des équipes. Cette méthode, combinée à une charte claire et une gestion proactive des dépendances, peut permettre de créer un environnement de travail résilient et performant.

## Références

<https://www.fastagile.io/>

<https://psychsafety.co.uk/psychological-safety-82-dunbars-number-and-team-size/>

<https://medium.com/orgtopologies/case-study-from-component-teams-to-team-topologies-to-fast-agile-6e3424d5b2e5>
