---
draft: false
title: "L'Essence du devops : Au-delà de l'Automatisation"
authors:
  - laurent-dussault
values:
  - conviction
  - partage
tags:
  - devops
  - collaboration
  - lean
  - mesure
  - resilience
  - Automatisation
date: 2023-11-11T07:46:29.294Z
offers:
  - unlock-delivery
thumbnail: devops_CLMR.png
carousels:
  "1":
    - DevOps se rendre dispensable.jpg
    - DevOps se rendre dispensable (1).jpg
    - DevOps se rendre dispensable (2).jpg
    - DevOps se rendre dispensable (3).jpg
    - DevOps se rendre dispensable (4).jpg
    - DevOps se rendre dispensable (5).jpg
    - DevOps se rendre dispensable (6).jpg
    - DevOps se rendre dispensable (7).jpg
subtitle: Non, la culture devops ne se limite pas à de la technique !
---
 
Si vous vous intéressez à la transformation des pratiques et de la culture des organisations tech, vous avez certainement entendu parler de "devops", non ? On réduit souvent devops à l'automatisation  des processus de développement et d'exploitation. Mais en réalité, il s'agit d'une philosophie globale qui englobe la Collaboration, l'Automatisation, le Lean, la Mesure, et la Résilience (CALMR). 

Vous trouverez d’autres anagrammes, dans la littérature, qui mettent en avant les notions de Culture ou encore de Partage (les axes CAMS ou CALMS). Le “CALMR” utilisé ici n'est donc qu’un parti pris qui ne prétend pas avoir plus raison que d’autre définition, mais qui expose un point de vue dans l’objectif d’appréhender une vision plus large du devops.

Cet acronyme, **CALMR**, encapsule les principaux piliers de devops, chacun contribuant à la transformation des entreprises et à l'amélioration de leur agilité. Dans cet article, nous explorerons ces aspects clés de devops et montrerons pourquoi cette approche est bien plus que de l'automatisation.

## Collaboration (C)

Devops favorise la collaboration étroite entre les équipes de développement et d'exploitation, brisant les silos traditionnels pour créer des synergies.

## Automatisation (A)

L'automatisation des tâches répétitives et manuelles permet de gagner en efficacité et de réduire les erreurs. Et c’est bien parce que trop souvent on limite le devops à celà, que cet article ne parlera que du reste !

## Lean Flow (L)

Le flux, tout est là. \
L’objectif de tout le mindset concentré dans “devops” est celui d’optimiser le fonctionnement du flux. Prendre conscience que des silos ne sont que les maillons d’une chaîne et ainsi  commencer à travailler les propriétés de la chaîne au global, et pas uniquement des maillons.

## Mesure (M)

La mesure constante de son produit mais aussi de toutes les étapes du cycle de vie de l'application fournit les données nécessaires pour la prise de décision éclairée. Et sans mesure, pas d'amélioration ni de pilotage.

## Résilience/Récupération (R)

La capacité à rétablir  rapidement un service en cas d'incident ou de panne pour maintenir la valeur fournie à un client. Devops vise à créer des systèmes anti-fragiles qui non seulement résistent aux perturbations, mais en bénéficient.

Nous plongerons plus en profondeur dans chaque aspect de C(A)LMR devops, en mettant en lumière leur importance et leur impact sur la transformation numérique des entreprises. Préparez-vous à découvrir pourquoi devops est bien plus qu'une simple automatisation.

## Qu'est-ce que devops ?

Devops est bien plus qu'un simple rôle ou une tâche à ajouter à une équipe existante. Il s'agit d'une philosophie et d'une approche globale qui vise à améliorer la collaboration, initialement entre les équipes de développement et d'exploitation. Aujourd’hui des tas de nouveaux buzz words montrent que cet état d’esprit est nécessaire ailleurs : Bizdevops, Devsecops… mais il me semble inutile de créer de nouveaux mots, seuls les acteurs changent, l’esprit est le même.

Et pour mieux comprendre ce qu'est devops, commençons par examiner ce qu’il **n'est pas**.

### Devops n'est pas un nouveau rôle :

L'une des idées fausses courantes à propos de devops est de le considérer comme un nouveau rôle, une fiche de poste. En réalité, c'est tout le contraire de l'esprit devops. Créer un nouveau rôle revient à ajouter un autre silo entre les équipes de développement et d'exploitation, ce qui va à l'encontre du but principal de devops : éliminer les barrières et favoriser la collaboration.

Au lieu de cela, devops encourage la réunion des compétences et des responsabilités des équipes de Dev et d'Ops, en travaillant de concert pour atteindre des objectifs communs. Il s'agit d'une approche où les développeurs comprennent les opérations et les opérationnels comprennent le développement, créant ainsi une équipe transversale plus efficace et collaborative.

{{< figure  src="devops_pasdevops.png" alt="Devops pas devops">}}

Si vous nommez un médiateur pour régler un conflit entre deux parties, vous renforcez la raison d'être des deux parties. Vous résolvez peut-être le conflit pour un temps, mais vous perdez tout espoir d'en faire une seule et même communauté.

## C comme Collaboration - L'esprit devops : se rendre “dispensable” pour ne pas bloquer le flux

Un aspect fondamental de devops est l'idée de se rendre dispensable. Cela peut sembler paradoxal, mais le meilleur moyen de collaborer, c’est de tout faire pour que l’autre n’est pas besoin que vous agissiez pour atteindre leur but.

L'objectif ultime de devops est de permettre aux processus de fonctionner de manière si fluide que les interventions manuelles et les obstacles sont réduits au minimum. En d'autres termes, les praticiens devops visent à automatiser et à optimiser leurs processus de manière à ce que le flux de travail puisse avancer sans interruption.

Cela signifie que les membres des équipes travaillent sur l'outillage, la mise à disposition de produits en self-services, plutôt que d'intervenir constamment pour résoudre des problèmes. En se rendant “dispensables”, ils contribuent à garantir que le flux de développement & déploiement reste fluide, sans blocages inutiles.

En résumé, devops n'est pas un nouveau rôle, mais une approche qui promeut la collaboration et la dissolution des silos entre les équipes. L'esprit devops implique de travailler ensemble pour se rendre “dispensable”, afin de maintenir un flux de travail continu et efficace. 

{{< carousel duration="5000" ordinal="1">}} 

## L comme Lean Flow et Pipeline

Chaque entreprise, qu'elle en soit consciente ou non, possède un pipeline de livraison. C'est le trajet emprunté par une idée ou un concept depuis sa conception jusqu'à sa mise en service. Ce qui distingue les entreprises performantes dans le domaine de devops, c'est leur capacité à optimiser ce flux.

Le Lean Flow, un concept emprunté au Lean Management, consiste à repenser ce pipeline pour qu'il soit aussi fluide et efficace que possible. Comme tout flux, votre pipeline est une séquence d’actions qui s'enchaînent pour aboutir à une production de valeur. Contrairement à ce qu’il se passe dans des systèmes silotés, où on va chercher à optimiser chaque éléments, l’approche consiste à prendre conscience du pipeline dans son ensemble et à oeuvrer sur ce système globale pour pratiquer des améliorations significatives…

Une approche devops revient donc à challenger les outils et les processus pour améliorer les interactions (première valeur du manifeste agile) et à avoir une approche systémique pour maitriser un flux, dans son efficacité, c’est à dire en gérant le débit et la qualité (lean mindset).

### Visualisation du flux & Kanban

Comment améliorer ce flux ? L'une des premières étapes pour améliorer un processus est de le visualiser. Cela signifie que chaque étape du processus est clairement identifiée, et que les personnes impliquées peuvent voir comment les tâches avancent de l'une à l'autre. La visualisation du flux permet de repérer les goulets d'étranglement et les inefficacités.

Plus globalement, il s’agit de mettre en place des Kanban pour piloter son flux. \
Le Kanban est une méthode de gestion visuelle du flux de travail qui découle de l'industrie automobile japonaise. Il consiste à utiliser des tableaux pour suivre les tâches en cours, celles qui sont prêtes à être traitées, et celles qui sont terminées. Les équipes peuvent ainsi mieux gérer leur charge de travail, réduire les retards et améliorer la communication.

{{< figure  src="exemple board kanban.png" alt="exemple de board kanban">}}

Ce board, loin d’être exemplaire dans la séquence de tâches du flux qu’il propose, illustre la visualisation d’un goulet d'étranglement.

### Value Stream Mapping (VSM)

Dans le Lean, le moyen le plus complet de prendre conscience, puis d’optimiser son flux est le Value Stream Mapping. 

La VSM est une technique qui consiste à cartographier l'ensemble du processus de développement et de livraison d'une application. Elle permet d'identifier les étapes qui ajoutent de la valeur et celles qui génèrent du gaspillage, que ce gaspillage provienne de délai ou de non-qualité. Grâce à la VSM, les équipes devops peuvent optimiser le flux en éliminant les étapes superflues (sans valeur ajoutée) ou en les automatisant. \
Améliorer le Lead Time (temps de traversée de notre pipeline) c'est d'abord chercher à éliminer les délais (les étapes non-travaillées du flux de delivery) avant de chercher à travailler plus vite.

{{< figure  src="exemple VSM.png" alt="exemple de VSM">}}

*(Ne me demandez pas pourquoi je dis UN Value Stream Mapping et UNE VSM 🙂)*

### Diagrammes de flux cumulatif

J’anticipe sur la rubrique “Mesure”: Quand on pilote son flux, l’outil phare est le Diagramme de Flux Cumulatifs.

Même si son premier abord est un peu indigeste, il permet de suivre la progression globale du travail au fil du temps. Il montre la quantité de travail restante, le temps moyen nécessaire pour terminer une tâche, et la variation du flux. Ces diagrammes fournissent des données précieuses pour améliorer la prévisibilité et la performance de votre pipeline.

{{< figure  src="diagramme de flux cumulatif.png" alt="exemple de diagramme de flux cumulatif">}}

## M comme Mesurer tout : Informations pour la Décision

L'un des piliers fondamentaux de devops est la mesure constante de toutes les étapes du cycle de vie de l'application. Pourquoi cette obsession pour les mesures ? Parce que les données sont la clé de la prise de décision éclairée et de l'amélioration continue.

### Les mesures pour la prise de décision

Les données ne sont pas simplement recueillies pour le plaisir de les avoir ou pour "fliquer" les membres d'une organisation, mais plutôt pour permettre aux équipes devops et aux décideurs de prendre des décisions éclairées. Ces décisions peuvent concerner l'optimisation des processus, l'allocation des ressources, la résolution de problèmes ou la planification future.

### Rendre les décideurs autonomes

Un problème devrait pouvoir être résolu au niveau de l'organisation où il apparaît.

Une caractéristique importante de devops est la mise à disposition des informations pertinentes pour ceux qui prennent des décisions. Plutôt que de dépendre d'une équipe dédiée pour analyser les données, devops vise à rendre les décideurs autonomes en leur fournissant des tableaux de bord et des rapports compréhensibles. Ainsi, ils peuvent rapidement évaluer la performance, identifier les tendances et prendre des mesures en conséquence.

Cela favorise une réactivité accrue et une meilleure adaptation aux besoins changeants du projet. Les données deviennent un outil précieux pour aligner les efforts de développement et d'exploitation sur les objectifs commerciaux.

Toutefois attention à l’ “infobésité” : Noyer quelqu’un sous les données ne l’aide pas à mieux décider, bien au contraire… Peu de personnes savent maîtriser un cockpit de 747 !!

{{< figure  src="cockpit 747.png" alt="cabine d'un 747">}}

### Mesurer la valeur pour les utilisateurs

Dans la culture devops, il est essentiel de mesurer la valeur que les utilisateurs tirent du produit. Cela signifie examiner de près les usages et l'utilité du produit, ainsi que la satisfaction des utilisateurs. Mesurer la valeur permet de prendre des décisions informées sur le développement et l'amélioration du produit. Il peut également conduire à l'abandon de fonctionnalités peu utilisées ou boudées par les utilisateurs au profit de celles qui apportent une réelle valeur. \
En pratique, il s’agit d’appliquer un des éléments du *Lean Startup* : Formuler des hypothèses, mettre en place des nouveautés dans vos produits et mesurer, dans une boucle d’apprentissage la plus courte possible, la véracité de vos hypothèses. Ainsi, sur la base de données <span style="text-decoration:underline;">factuelles</span> vous décidez de persévérer (continuer à investir dans cette direction), ou bien de pivoter (abandon de cette piste pour en financer une autre).

### Mesurer l'efficacité de vos outils et processus

Il est tout aussi important de mesurer l'efficacité de vos propres outils de travail et de vos processus internes. Cela inclut l'examen de la performance de votre propre pipeline de développement : l'utilisation des méthodes telles que la VSM. Mesurer l'efficacité interne vous aide à identifier les goulots d'étranglement, à réduire les gaspillages et à optimiser vos opérations internes.

### Les 4 métriques clés et la Maturité devops

Difficile de parler devops sans faire référence aux "4 Key Metrics". 

Ces indicateurs, issus du livre "Accelerate" (Nicole Forsgren PhD, Jez Humble, Gene Kim, éditions IT Revolution Press), mesurent la performance d'une organisation en matière de livraison continue et de déploiement. Ils comprennent la fréquence des déploiements, le temps de cycle, le taux de changement réussi, et le temps de restauration en cas d'incident. Ces métriques sont devenues des repères essentiels pour évaluer la maturité devops d'une organisation et sa capacité à innover et à fournir rapidement de la valeur à ses utilisateurs.

{{< figure  src="4 key metrics.png" alt="4 Key Metrics">}}

Voir notre article sur [Accelerate, la boussole de vos transformation](../../posts/20220319-accelerate-la-boussole-indispensable-de-votre-transformation/).

## R comme Résilience et Anti-Fragilité

Dans le monde complexe et en constante évolution de la technologie, la résilience est une qualité précieuse. devops ne se limite pas à la livraison continue, mais cherche également à renforcer la résilience des systèmes. En fait, il vise à aller au-delà de la simple **résilience** pour atteindre l'anti-fragilité.

### De la peur de la panne à l'anti-fragilité

Une culture traditionnelle de l'informatique peut être caractérisée par la peur de la panne. Les équipes ont tendance à éviter les changements majeurs de peur de perturber la stabilité du système. Cependant, cette approche peut conduire à une stagnation et à une réticence à innover.

L'anti-fragilité, telle que promue par des entreprises comme Netflix avec leur approche de "chaos engineering" est l'idée que les systèmes devraient non seulement résister aux chocs, mais en bénéficier. Au lieu de craindre les pannes, l'anti-fragilité les utilise comme des opportunités d'apprentissage. En provoquant délibérément des pannes contrôlées, les équipes devops peuvent identifier les vulnérabilités, renforcer la résilience et améliorer la capacité du système à évoluer.

### Le changement dans la perception des changements

Dans une culture axée sur la peur de la panne, les changements sont redoutés. Ils sont vus comme des risques potentiels pour la stabilité du système. Cela peut entraîner des retards dans les mises à jour, des processus bureaucratiques lourds, et une réticence à expérimenter de nouvelles technologies. L’objectif ici est de construire un système **robuste**, qui ne tombe jamais en panne.

En revanche, une culture de l'anti-fragilité embrasse le changement. Elle voit les changements, les pannes, comme une occasion d'apprendre, d'innover et de s'améliorer. Cette approche favorise la mise en place de pratiques telles que le déploiement continu, le test en production, et le "chaos engineering" pour s'assurer que les systèmes sont résilients. \
 \
Pour ceux et celles qui ont buggé sur “test en production”, on parlera ici de stratégie de *Release*. En découplant la notion de mise en production de celle de l’activation de nouveaux services, on peut commencer à planifier des stratégies d’activation progressive, en commençant par exemple avec un tout petit sous-ensemble d'utilisateurs qui vont “tester”, avant une ouverture globale… 

En adoptant un état d'esprit qui embrasse le changement, l'incertitude et les pannes comme des opportunités, les équipes devops sont mieux préparées à prospérer dans un environnement technologique en constante mutation. Elles deviennent capables de créer des systèmes qui non seulement résistent aux chocs, mais qui s'améliorent à chaque adversité, renforçant ainsi leur capacité à rester compétitives sur le marché. 

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Objectif : ROBUSTESSE       | Objectif : RÉSILIENCE |
| -------------------------   | :-------------------: |
| Peur des changements        | Changements bienvenus |
| Sentiment d’invulnérabilité | Gestion des erreurs   |
| Recherche de coupable       | Recherche de solution |
| Faible coopération          | Forte coopération     |
{{</ table >}}

## Vous avez les bases, vous pouvez automatiser

Nous avons exploré les “autres” principaux piliers de devops, symbolisés par l'anagramme CALMR : Collaboration, ~~Automatisation~~, Lean, Mesure et Résilience. Ces concepts ne sont pas des étapes isolées, mais plutôt des éléments d'une transformation plus large.

Il est crucial de comprendre que l'automatisation, bien que puissante, n'est ni le point de départ, ni une fin en soi. Avant de plonger tête la première dans l'automatisation, il est essentiel de cultiver l'état d'esprit de devops. La Collaboration, le Lean,  la Mesure et la Résilience forment le socle sur lequel repose l'automatisation.

En adoptant cet état d'esprit, vous créez un environnement avide d'automatisation. Vous découvrez où et comment l'automatisation peut apporter une réelle valeur ajoutée. Vous comprenez que l'automatisation n'est pas une fin en soi, mais un moyen d'atteindre des objectifs plus vastes, tels que l'amélioration de la collaboration, la réduction des erreurs et la fourniture plus rapide de produits de haute qualité.

Alors, ne vous focalisez pas sur l'automatisation. Prenez d'abord le temps de développer la culture devops au sein de votre organisation. Cultivez la collaboration, mesurez, apprenez de vos échecs et renforcez votre résilience. L'automatisation, une fois introduite dans ce contexte, devient un outil puissant pour libérer le plein potentiel de devops.

En résumé, commencez par l'État d'Esprit, l'Automatisation suivra naturellement. En adoptant cette approche, vous êtes prêt à transformer votre entreprise, à accélérer vos processus de développement et de déploiement, et à prospérer dans un environnement technologique en constante évolution.
