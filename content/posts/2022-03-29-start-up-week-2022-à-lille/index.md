---
title: Édition 2022 de la Start-up week à Lille
authors:
  - thomas-clavier
values:
  - partage
tags:
  - startupweek
  - agilite
date: 2022-03-29T07:02:23.354Z
thumbnail: img_20190326_134510.jpg
subtitle: Planter les graines d’une nouvelle culture de travail en France
---
Je suis très fier d’être à l’initiative de la [Start-up week](https://startupweek.fr) à l’[IUT de Lille](https://iut.univ-lille.fr/) dont la dernière édition se termine aujourd'hui mardi 29 mars 2022.

## La Start-Up Week, ça sert à quoi ?

Lors de la Start-Up Week, notre objectif est double : 

* Aider concrètement des start-ups de la région lilloise en consolidant leur projet et en concevant un prototype digital adapté à leurs enjeux : site web et appli mobile.
* Et donner aux étudiants l’expérience d’une collaboration pluridisciplinaire et agile concrète.

En plus des transformations d’entreprise que nous menons chez aqoba, cet évènement est une autre manière pour nous :

* de diffuser une culture de travail innovante et collaborative en laquelle nous croyons profondément,
* et d’encourager les meilleurs de nos étudiants à rejoindre le tissu de PMEs françaises plutôt que de rejoindre un grand groupe établi.

## Comment ça fonctionne ?

La recette est assez simple et se construit autour de profils complémentaires ayant tous le même objectif : aider la start-up à passer du stade de l’idée à celui d’expérimentation.

![startupweek](startupweek.svg)

En créant des équipes pluridisciplinaires et autonomes, nous permettons, en 5 jours, de réaliser un site et une appli mobile pour chacune des start-ups représentées. L’important est d’être dans l’action, la réalisation.

Les 3 règles d’or que nous appliquons pour aider les étudiants et les responsables de projet à réussir la Start-Up Week

### Constituer des équipes agiles

Les équipes sont constituées pour 5 jours pleins et fonctionnent selon le principe de l’auto-organisation, autour d’un Product Owner. Elles réalisent des sprints de 2 heures au bout desquels elles démontrent leur avancement du projet au porteur (le PO) qui valide ou pivote.

### Former 

Former les porteurs de projet au rôle de Product Owner et les développeurs aux bonnes pratiques du craftsmanship

En amont de la semaine, une journée complète est consacrée à la formation des porteurs de projet au rôle de Product Owner. Ce rôle est clé pour faire réussir une équipe agile. Et il ne s’improvise pas. Nous les aidons à comprendre en quoi il est essentiel, quels en sont les outils et comment les utiliser. Sans devenir des PO professionnels, ils apprennent les premières ficelles et les pièges à éviter.

De la même manière, nous amenons dans les équipes des développeurs que nous avons formés, durant 2 ans à l'iut, aux bonnes pratiques du craftsmanship : ce sont des développeurs qui délivrent un code de qualité, propre et évolutif. Le TDD est par exemple de rigueur.

Ces temps de formation sont clés pour permettre de délivrer des produits digitaux qui visent au-delà de l’expérimentation. Ils préfigurent ce que le porteur du projet mettra sur le marché.

### Donner du sens

La Start-Up Week n’est pas un événement où l’on vient pour passer le temps. 

D’une part, les étudiants viennent aider des porteurs de projet qui prennent le temps de leur expliquer en quoi leur projet est primordial pour eux. Ils s’y investissent donc à 100%.

D’autre part, par leur participation, ils contribuent à renforcer le tissu entrepreneurial de leur région. Cette dimension locale est clé pour éviter de monter un événement hors sol.

Enfin, les enseignants qui les appuient pendant cette semaine, et que je remercie pour leur enthousiasme, transmettent pendant cette semaine leurs valeurs qui sont aussi celles d’aqoba : humanité, partage, altruisme, conviction et transparence.

Un grand merci à tous mes étudiants et aux enseignants qui ont fait de cette Start-Up Week un cru exceptionnel !
