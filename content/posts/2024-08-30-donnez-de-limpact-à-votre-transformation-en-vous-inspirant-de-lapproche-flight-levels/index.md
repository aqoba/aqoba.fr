---
draft: false
title: Donnez de l'impact à votre transformation en vous inspirant de l'approche
  Flight Levels
authors:
  - olivier-marquet
values:
  - conviction
  - partage
tags:
  - FlightLevels
offers:
  - unlock-transformation
date: 2024-09-09T01:57:00.000Z
thumbnail: flighlevels.png
subtitle: L'importance des coalitions de pilotage
---
Les Flight Levels (Niveaux de Vol) représentent un modèle structuré pour améliorer l'agilité organisationnelle en optimisant les flux de valeur à différents niveaux de l'organisation. Développée par Klaus Leopold, cette approche permet de visualiser et de coordonner les activités à travers trois niveaux distincts, tout en appliquant cinq activités clés pour atteindre une agilité véritable.

{{< figure  src="flight-levels-pillars.png" alt="3 niveaux de vol" class="center-large">}}

Chez aqoba nous pratiquons depuis plusieurs années maintenant cette visualisation des différents niveaux de l'organisation, accompagnée d'une mise en oeuvre des améliorations autour d'une coalition du changement.

Vous pouvez retrouver nos articles et des retours d'expérience video sur le sujet ici:

* [Une coalition pour les guider tous !](https://aqoba.fr/posts/20221005-une-coalition-pour-les-guider-tous/)
* [Agile en Seine 2022 - Guiding Coalition SACEM](https://www.youtube.com/watch?v=WJwuXaJgE-Y)
* [Agile en Seine 2023 - Faites mentir les « Lois de Larman » grâce à la coalition de transformation : le REX Air Liquide](https://www.youtube.com/watch?v=RPNVT3gtZ1g)

Mais regardons plus en détail ce qu'est l'approche **Flight Levels**

## Les 3 niveaux de vol

{{< figure  src="flight-levels-explained.jpg" alt="3 niveaux de vol expliqués">}}

### **1. Flight Level 1 : le niveau opérationnel**

Ce niveau correspond aux équipes qui effectuent le travail quotidien, qu'il s'agisse du développement de produits, du marketing, des ventes ou du service client. Les équipes choisissent librement leurs méthodes de travail car le modèle n’impose pas de prescriptions et chaque équipe adapte ses activités selon son contexte.

   * **Son objectif :** Optimiser le flux de travail des équipes individuelles tout en assurant que leurs efforts contribuent à l'efficacité globale de l'organisation.
   * **Les activités spécifiques à ce niveau :** Ajuster le flux de travail interne de chaque équipe pour maximiser leur efficacité et leur performance dans leur contexte spécifique.

### **2. Flight Level 2 : la coordination**

Ce niveau traite de la coordination entre différentes équipes pour garantir que toutes travaillent de manière harmonieuse vers un objectif commun. Il peut aussi traiter de la coordination de plusieurs Flight Level 2.
   * **Son objectif :** Visualiser et optimiser les flux de valeur qui traversent plusieurs équipes pour créer des produits ou services de valeur.
   * **Les activités spécifiques à ce niveau :** Coordination des flux de valeur, gestion des interdépendances entre équipes, et optimisation des processus inter-équipes.


### **3. Flight Level 3 : la gestion stratégique du portefeuille**


Ce niveau est axé sur la stratégie globale de l'organisation, impliquant la définition des objectifs à long terme et la planification stratégique.
   * **Objectif :** Assurer l'alignement entre la stratégie de l'entreprise et les opérations quotidiennes, en intégrant des boucles de rétroaction pour ajuster les actions selon les résultats obtenus.
   * **Les activités spécifiques à ce niveau :** Définir la stratégie, coordonner les objectifs stratégiques avec les activités opérationnelles, et suivre les progrès pour adapter la stratégie en continu.

## **Les 5 activités clés**

Pour atteindre une véritable agilité organisationnelle, les cinq activités suivantes doivent être appliquées continuellement à tous les niveaux de vol :

1. **Visualiser la situation**

   * **Objectif :** Cartographier les systèmes de travail existants et leurs interdépendances pour identifier les points d'amélioration potentiels.
   * **Outils :** Utilisation de tableaux Kanban, de cartes des flux de valeur, etc..
2. **Créer du focus**

   * **Objectif :** Identifier et prioriser les domaines qui nécessitent une amélioration immédiate.
   * **Méthodes :** Analyse des goulots d'étranglement, définition des priorités stratégiques.
3. **Établir des interactions agiles**

   * **Objectif :** Assurer une communication et une collaboration efficaces entre les différentes équipes et niveaux de l'organisation.
   * **Approches :** Réunions de coordination, ateliers collaboratifs, rétrospectives régulières.
4. **Mesurer les progrès**

   * **Objectif :** Mettre en place des indicateurs de performance pour suivre l'avancement des initiatives et ajuster les actions en conséquence.
   * **Indicateurs :** Mesures de la vélocité, taux de livraison, satisfaction client.
5. **Implémenter des améliorations**

   * **Objectif :** Introduire des changements concrets et mesurables dans les processus de travail pour améliorer continuellement la performance.
   * **Actions :** Expérimentations, ajustements basés sur les retours d'expérience, cycles d'amélioration continue.

## **Comprendre le fonctionnement actuel de votre organisation avec le Work System Topology**

{{< figure  src="worksystemtopology.png" alt="Topologie des Systèmes de Travail">}}

Le **Work System Topology** (Topologie des Systèmes de Travail) est un concept clé dans l'approche des Flight Levels, qui nécessite de visualiser et de comprendre les différents systèmes de travail existants dans une organisation ainsi que leurs interconnexions. Cette visualisation peut se faire au travers d’un tableau kanban, d’une VSM (Value Stream Mapping) d’un tableau de dépendances, ou autre (cf image plus haut). Cela permet d'identifier où des améliorations peuvent être réalisées pour augmenter l'agilité et l'efficacité globale de l'organisation.

Un **Flight Level System** (ou Système de Travail) est une section de l'organisation où les cinq activités clés des Niveaux de Vol sont appliquées (c.f Level 1, 2 et 3). Chaque système de travail est représenté par une boîte dans la topologie, indiquant comment les différentes sections de l'organisation sont configurées pour réaliser le travail.

La topologie des systèmes de travail est une visualisation qui montre quels systèmes de niveau de vol sont utilisés dans une organisation et comment ils sont interconnectés. Cela implique d'examiner l'organisation pour les systèmes existants et de comprendre les processus et les dépendances entre eux.

Comprendre et visualiser la topologie des systèmes de travail permet à une organisation de :

* **Identifier les points de friction et les goulots d'étranglement :** Permet de cibler les zones nécessitant des améliorations spécifiques.
* **Optimiser les flux de valeur** : En comprenant les interdépendances et en améliorant la coordination entre les différents systèmes.
* **Créer une agilité organisationnelle :** En facilitant la communication et la collaboration entre les équipes et les niveaux stratégiques, de coordination et opérationnels.

## **Comment mettre en œuvre les Flight Levels**

Pour introduire les Flight Levels dans une organisation, il est essentiel de suivre ces étapes :

1. **Clarifier le point de départ** \
   Comprendre la situation actuelle de l'organisation, faire un état des lieux des différents Flight Levels et identifier les défis spécifiques et les opportunités d'amélioration.
2. **Créer du focus pour l'amélioration** \
   Définir clairement les objectifs de l'initiative d'amélioration et s'assurer qu'ils sont alignés avec la stratégie de l'organisation. Basez vous sur l'analyse des différents Flight Levels pour identifier les objectifs.
3. **Construire une coalition de pilotage** \
   Impliquer des sponsors et des leaders clés pour soutenir et guider le processus de changement.
4. **Engager les personnes** \
   Utiliser des ateliers interactifs et des formats de communication ouverts pour impliquer un large éventail de collaborateurs et de parties prenantes.
5. **Appliquer une approche agile au changement** \
   Concevoir le processus de changement de manière itérative et co-créative, en intégrant constamment le feedback des personnes impliquées.

## **Construire et faire fonctionner une coalition de pilotage pour les Flight Levels**

La création d'une coalition de pilotage est une étape cruciale pour le succès de toute initiative d'amélioration basée sur les Flight Levels. 

Cette coalition joue un rôle déterminant dans la mise en œuvre et la conduite des changements nécessaires à l'optimisation des flux de valeur au sein de l'organisation et elle joue un rôle essentiel de coordination et de soutien à tous les niveaux.

Elle s’assure que les équipes opérationnelles (niveau 1) optimisent leurs processus, que la coordination entre équipes (niveau 2) est efficace et alignée sur les objectifs stratégiques, et qu’au niveau stratégique (niveau 3), les initiatives sont en phase avec les priorités à long terme de l’organisation. En favorisant la communication, l’alignement des actions et l’adaptation continue, ainsi qu’en donnant les moyen du changement, la coalition garantit une amélioration continue et cohérente à travers tous les niveaux de l’organisation.

Comment construire et faire fonctionner efficacement une telle coalition ?

### **1. Comprendre l'importance de la coalition de pilotage**

Une coalition de pilotage est composée de sponsors et de personnes engagées qui mettent en œuvre les initiatives d'amélioration de manière agile. Cette coalition est essentielle pour maintenir le focus et la direction de l'initiative, garantir les ressources nécessaires et co-créer le processus de changement.

### **2. Définir les rôles et responsabilités**

Il est important de définir clairement les rôles et les responsabilités au sein de la coalition de pilotage. Voici les principaux rôles à considérer :

* **Le sponsor :** Le sponsor est un décideur clé qui soutient l'initiative. Il ou elle définit le "quoi" et le "pourquoi" de l'amélioration et co-crée le processus de changement. Sans un sponsor engagé, les tentatives d'introduire les Flight Levels risquent l'échec.
* **L'équipe de changement :** Un groupe de personnes diverses avec des compétences, des niveaux d'expérience, des capacités de leadership et des personnalités variées. Cette équipe représente le réseau de collaboration inter-silos et hiérarchique que les Flight Levels cherchent à promouvoir.
* **Les agents du changement :** Un groupe de personnes qui non seulement soutiennent le changement, mais qui le propagent activement à travers l’organisation. Ils sont ambassadeurs du changement.
* **Les parties prenantes :** Tous ceux qui sont affectés par le changement et qui, idéalement, bénéficieront des améliorations souhaitées.

{{< figure  src="rôles-et-responsabilités.png" alt="Rôles et responsabilités" class="center-large">}}

### **3.Suivre les étapes clés pour construire une coalition de pilotage efficace**

Pour assurer le succès d’une initiative de changement, il est essentiel de constituer une coalition de pilotage solide. Voici les étapes qu’il ne faut pas oublier pour former une équipe capable de guider efficacement le processus de transformation au sein de l’organisation :



* **Identifiez et engagez les sponsors :** Trouvez un ou plusieurs sponsors ayant l'autorité de décision et l'influence nécessaires pour soutenir l'initiative.
* **Formez une équipe de changement diversifiée :** Incluez des membres de différentes unités de l'organisation pour promouvoir une perspective large et inclusive.
* **Nommez des agents de changement :** Sélectionnez des individus influents qui peuvent propager les idées et encourager l'adhésion au changement.
* **Impliquez les parties prenantes :** Assurez-vous que les parties prenantes sont informées et engagées dès le début pour obtenir leur feedback et leur soutien.


### **4. Fonctionner de manière agile**

Pour garantir l’efficacité de la coalition de pilotage, il est crucial d’adopter un mode de fonctionnement agile. Voici les principes et pratiques à ne pas oublier et qui permettent à la coalition de rester coordonnée, flexible, réactive et collaborative tout au long du processus de changement :



* **Réunions de coordination :** Organisez des réunions régulières pour discuter des progrès, des obstacles et des prochaines étapes.
* **Ateliers :** Utilisez des ateliers interactifs pour impliquer un large éventail de collaborateurs et recueillir des idées et des suggestions ou travailler sur des solutions possibles.
* **Feedback et ajustements :** Mettez en place des boucles de feedback régulières pour ajuster les actions en fonction des retours des équipes et des parties prenantes.
* **Communication transparente :** Maintenez une communication ouverte et transparente pour garantir que tout le monde est aligné et informé des évolutions.


## **Conclusion**

Les Flight Levels (Niveaux de Vol) offrent ainsi une approche complète et adaptable pour renforcer l'agilité organisationnelle. En mettant l'accent sur la coordination et la communication entre les différents niveaux et systèmes de travail, ce modèle permet d'optimiser les flux de valeur de manière holistique et efficace, contribuant ainsi à une amélioration continue des performances organisationnelles.

La création et le fonctionnement d'une coalition de pilotage est essentielle pour la réussite des initiatives de transformation basées sur les Flight Levels. En engageant les bonnes personnes, en définissant clairement les rôles et en maintenant une communication et un feedback constants, vous pouvez conduire un changement efficace et durable dans votre organisation. Cette approche collaborative et agile garantit que les initiatives d'amélioration sont bien alignées avec les objectifs stratégiques et opérationnels, permettant ainsi d'optimiser les flux de valeur et d'atteindre une véritable agilité organisationnelle.

**Références:** \
[Flight Levels - A short introduction](https://www.flowsphere.ch/wp-content/uploads/2024/02/Flight-Levels-A-Short-Introduction.pdf)
