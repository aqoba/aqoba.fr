---
draft: false
title: Process Communication Model
authors:
  - laurent-dussault
values:
  - humanite
  - partage
tags:
  - ProcessCom
offers:
  - unlock-management
date: 2023-04-19T02:09:00.000Z
thumbnail: process-com.png
subtitle: Modéliser les interactions pour mieux les comprendre
---
Nous sommes tous différents, mais quels sont les besoins et les aspirations de chacun. La process Com’ est un outil qui peut vous permettre de mieux vous connaître, et de mieux comprendre les autres, dans le but de faciliter les relations entre les individus. Bien entendu, dans toutes nos interactions avec les autres, votre personnalité, celle de vos interlocuteurs des mécanismes sont à l'œuvre. Et bien ouvrons la boite à outil qu’est la process com’...

Cet article vise à vous faire découvrir ce modèle et à vous encourager à aller plus loin par le biais de formation par exemple. Après avoir lu cet article, ne vous lancez pas dans une analyse de votre entourage, ne jouez pas aux apprentis sorciers…

Pour aller plus loin, je ne pourrai que vous conseiller de vous former et de pratiquer dans des environnements “sécurisés”.

## À l’origine…

Le créateur de la Process Com’ est **Taibi Kahler**, un psychologue américain expert en **analyse transactionnelle.** Ces travaux l’ont conduit à travailler avec la NASA, dans les années 70 afin de réfléchir à la partie humaine de l’exploration spatiale : Quel groupe constituer pour former un équipe qui part dans l’espace, pour longtemps dans un environnement clos et légèrement stressant ?
Il fût lauréat en 1977 du prix scientifique **Eric Berne** (concepteur de l'analyse transactionnelle - AT).
Il se réfère aussi aux [portes de communication](https://analysetransactionnelle.fr/p-Portes_de_Communication_et_Structuration_du_Temps) du **Dr Paul Ware**, en offrant ainsi une lecture plus "facile".

## 5 canaux pour communiquer

Un peu de vocabulaire pour commencer:\
On distingue 5 canaux de communication, dans une communication saine. Chaque personne est plus ou moins sensible à ces canaux, que ce soit en émission ou en réception.

{{< figure  src="process-com-canaux.png" alt="Canaux de communication" class="center-small">}}

### Le canal Emotif / Ludique

Pour entrer en relation sur un mode plutôt émotionnel et assez superficiel . sur un mode spontané et souvent amusant. L’objectif est de créer un lien ludique\
Ex : C’est cool !

### Le canal Nourricier

Il vise un lien émotionnel chaleureux et profond. On est essentiellement dans le ressenti des choses, pas dans les faits.\
Ex : Je me sens bien avec vous.

### Le canal Informatif / Interrogatif

Si on devait qualifier le canal des IA, elles navigueraient ici. Des faits, des opinions sur un ton neutre (sans émotion). Par ce canal on vise à échanger de l’information.

Ex : \
**Fait** : La souris est mangée par le chat...\
**Opinion** : Je pense que c'est la bonne chose à faire..\
Dans les 2 cas, il s'agit d'échange d'information.

### Le canal directif

Quand on donne des ordres, dans le but de déclencher un comportement.\
Ex : Fais ceci !

### Le canal interruptif / d’urgence

Il vise à arrêter un comportement chez l’autre, que ce soir une action ou l’expression d’une émotion.\
Ex : Stop !

## 6 Types de personnalité

De même que l’ADN est composé de 4 nucléobases (A-T-C-G) et permet de “coder” l’ensemble des possibles de notre génome, voici les 6 ingrédients du modèle Process Com’ qui vont nous permettre de décrire n'importe quelle personnalité.\
Avant de voir ce que donne le mélange, regardons nos 6 ingrédients.

Remarque : 
Tous les types sont bons, pas de jugement de valeur
Ils sont bien plus que l’adjectif qui les nomme

{{< figure  src="process-com-6-types.png" alt="6 types de personnalité : empathique, persévérant, promoteur, travaillomane, rêveur, rebelle" class="center-large">}}

Notez également que d'autres traductions sont suggerées dans une intention d'éliminer la part négative que peuvent porter ces terminologies:
* Travaillomane = *Analyseur*
* Rêveur = *Imagineur*
* Rebelle = *Energiseur*

## Comment s'assemblent-ils ?

### Immeuble de personnalité

Un individu n’est pas d’un seul type de personnalité.\
Chacun est un assemblage des 6 composantes, dans des proportions plus ou moins grandes.\
Voici donc à quoi peut ressembler un “immeuble de personnalité”: chaque type de personnalité s'empile, en commençant, en bas par celui qui vous "marque" le plus, en décroissant jusqu'au sommet

{{< figure  src="process-com-immeuble.png" alt="Immeuble de personnalité" class="center-small">}}

### La Base

La base de notre immeuble se fixe dès l’enfance (entre 5 et 7 ans). Elle restera votre base toute votre vie, tout comme la structure (l'ordre des étages) de votre immeuble.\
En terme de communication, **nous entendons avec la base.**

### La phase (actuelle)

La phase est le type de personnalité en cours de développement. Au cours d’une vie, cette phase va pouvoir remonter dans votre immeuble. Pour faire simple, quand vous avez explorer un étage, que vous le maitrisez parfaitement, vous commencez à explorer l'étage juste au dessus... ainsi de suite jusqu'au dernier étage. Vous avez certainement déjà vu des gens (ou vous-même) changer "brutalement" de but et de comportement... probable que vous ayez assisté à un changement de phase.\
**La motivation est dans la phase**.


Bien sûr la Process Com’ n’est qu’un “modèle”, il se veut donc une simplification du vivant qui est complexe. Notez toutefois que ce modèle propose 4320 combinaisons pour essayer de nous comprendre. *(6 positions de phase possibles dans l’immeuble x  6!=720 assemblages possibles pour le constituer)*

## Description des types de personnalité

Voici donc la pierre angulaire du modèle.\
Je vous rappelle à ce stade, qu'aucun type n'est bon ou mauvais, mais vous allez voir que certains vous paraissent plus sympathiques que d'autres... à méditer.

### Travaillomane (Analyseur)

{{< figure  src="process-com-travail.png" alt="Travaillomane : points forts = logique, responsable, organisé. Canal / perseption = informatif, interrogatif, pensée factuelle. Besoins psychologique = Être reconnu pour son travail. Question existentielle = Suis-je compétent ?  ">}}

### Persévérant

{{< figure  src="process-com-perse.png" alt="Persévérant">}}

### Rebelle (Énergiseur)

{{< figure  src="process-com-rebel.png" alt="Rebel">}}

### Empathique

{{< figure  src="process-com-empath.png" alt="Empathique">}}

### Promoteur

{{< figure  src="process-com-promo.png" alt="Promoteur">}}

### Rêveur (Imagineur)

{{< figure  src="process-com-reve.png" alt="reveur">}}

## Environnements préférés

Chaque type de personnalité est plus ou moins à l’aise dans les interactions avec les autres.\
Ainsi chacun est plus confortable avec :

* Personne d’autre : Je préfère être seul
* 1 personne : Interaction 2 à 2
* 1 groupe stable : Qui n’évolue que peu dans le temps
* Plusieurs groupes avec des dynamiques qui peuvent être différentes

Sur un autre axe, la “capacité” à être à l’initiative des interactions est variable d’un type à l’autre.

Voici comment se répartissent ces types :

{{< figure  src="process-com-repart.png" alt="Environnements préférés" class="center-large">}}

## Énergie

La largeur des étages de votre immeuble est inversement proportionnelle à la quantité d’énergie qu’il vous faut pour entrer en phase (et donc communiquer) avec ce type de personnalité. Donc plus vous êtes obligé de monter dans votre immeuble, plus cela vous demande de l'énergie, au risque de vous stresser.\
Donc plus le mode de communication nous force à monter, plus on fatigue, plus on descend, plus on se ressource.

{{< figure  src="process-com-immeuble.png" alt="Immeuble" class="center-small">}}

## Répartitions dans la populations (mesures biaisées)

A titre d’information, voici la répartition des “bases”  dans la population. A savoir, cette mesure est biaisée car elle provient exclusivement des personnes ayant fait un test de personnalité Process Communication Model, or il ne s’agit pas vraiment d’un échantillon représentatif.

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Type          | Population | Homme | Femme |
| -----------   | :--------: | :-:   | :-:   |
| Empathique    | 30%        | 25%   | 75%   |
| Travaillomane | 25%        | 75%   | 25%   |
| Persévérant   | 10%        | 75%   | 25%   |
| Rêveur        | 10%        | 40%   | 60%   |
| Promoteur     | 5%         | 60%   | 40%   |
| Rebelle       | 20%        | 40%   | 60%   |
{{</ table >}}

## Et sous stress ?

### Notions d'ANALYSE TRANSACTIONNELLE

Les travaux de Taibi Kahler sont basés sur certains éléments issus de l’AT.
Zoomons sur 2 concepts en particulier pour comprendre le comportement de nos types de personnalités sous stress

#### LES DRIVERS

Les drivers sont des injections qui “gouvernent” nos comportements.
Ils sont au nombre de 5. et plus exactement il faudrait dire 4 + 1.
Le dernier de cette liste étant cumulable avec les autres.
Ils résultent bien souvent des injonctions qu'on a reçues enfant.

* **Sois parfait**
* **Sois fort**
* **Fais des efforts**
* **Fais plaisir**
* **Dépêche toi**

#### POSITIONS DE VIE ET SCÉNARIO

Les différentes positions de vie se résument à : 

* Comment je me considère ?
* Comment je considère les autres ? 


{{< table class="table-with-blue-header table-alternate table-center" >}}
|               | Comment je me considère ? | Comment je considère les autres ? |
| :-----------: | ------------------------- | --------------------------------- |
| **+/+**       | Je suis OK                | les autres sont OK                |
| **+/-**       | Je suis OK                | les autres ne sont pas OK         |
| **-/+**       | Je ne suis pas OK         | les autres sont OK                |
| **-/-**       | Je ne suis pas OK         | les autres ne sont pas OK         |
{{</ table >}}

Par exemple dans le cas -/+ , j’ai une croyance qui me dit : moi je suis nul et les autres sont formidables…

Et bien ces croyances, nous allons mettre en place des scénarios pour les confirmer !
Pour poursuivre mon exemple, je vais me dévaloriser, et comme je ne montre pas ma valeur, les autres ne la voient pas… donc je reste nul à leur yeux… mais ça confirme mon postulat, ils sont formidables eux.

### Types de personnalité sous stress

Chacun de ces types de personnalité va avoir un driver dominant, une position de vie et un scénario qui ne demande qu'à être validé.

Vous retrouverez ici, les comportements que nos différents types de personnalité vont mettre en oeuvre, sous stress, en réponse à leur driver,  pour confirmer leur croyance profonde. Ces comportements sont illustrés par des masques.

On distingue ici, un stress léger (1er degré) d'un stress plus marqué (2ème dégré).


___

{{< figure  src="process-com-stress-promo.png" alt="Promoteur sous stress">}}
___

{{< figure  src="process-com-stress-reve.png" alt="Reveur sous stress">}}
___

{{< figure  src="process-com-stress-travail.png" alt="Travaillomane sous stress">}}
___

{{< figure  src="process-com-stress-perse.png" alt="Persévérant sous stress">}}
___

{{< figure  src="process-com-stress-rebel.png" alt="Rebel sous stress">}}
___

{{< figure  src="process-com-stress-empath.png" alt="Empathique sous stress">}}
___

### Exemple de descente à la cave

> Prends soin de ma phase sinon gare à ma base

Comment va réagir un individu sous stress, il descend à la cave de son immeuble, en voici le plan : 

{{< figure  src="process-com-stress.png" alt="exemple de descente à la cave sous stress" class="center-large">}}


**1er Niveau** : 
Il va exprimer le stress de 1er degré de sa phase (active)
Dans l’exemple ci dessous, un profil avec une phase “Travaillomane” et une base “Rebelle”.
Naturellement, notre individu va avoir du mal à déléguer, voulant tout faire lui même, pour être sûr que ce soit bien fait.
Pour calmer ce stress : Nourrissez sa phase

**2ème niveau** : 
Si le stress dure ou s’amplifie, il va exposer son stress de 1er degré de sa base.
Dans notre exemple, il va brutalement inviter les autres à faire par eux-même : “Puisque c’est comme ça, faites le sans moi”
On peut donc avoir quelqu’un qui passe d’une non délégation à un “abandon” (et ça s’explique du coup)
Pour calmer ce stress : Nourrissez sa base

**3ème niveau** : 
On continue de plonger, mais vers le stress de 2nd degré de la phase.
Dans notre exemple : du sur-contrôle.
Notre individu ici, revient dans le game, en mode “je reprends le contrôle, je fais ou je vérifie tout ce qui est fait, en traquant les erreurs”
Pour calmer ce stress : Nourrissez sa phase

**4ème niveau** : 
A partir de là, l’individu revient à sa nature profonde. On atteint le stress de 2nd degré de la base, en blâmant les autres (source de tous les problèmes)
Pour calmer ce stress : Nourrissez sa base

**5ème niveau** : 
Ici, on atteint un niveau où il est difficile de remonter, le 3ème degré de stress (de la base).
Je ne les ai pas abordés.
Pour calmer ce stress : Consultez un spécialiste (et pas un article ou un forum en ligne)


### Si vous suivez encore : Triangle dramatique : Triangle de Karpman

Dans un jeu psychologique, il y a **3 rôles**. Ceci est relativement connu.
Il est moins évident que ces rôles ne sont pas figés… ils tournent.
Par exemple… je suis une victime… mais à force de me plaindre, je deviens le persécuteur de mon sauveur (qui devient la victime…) etc..

{{< figure  src="process-com-stress-karp.png" alt="Triangle dramatique" class="center-small">}}

#### Rôles et Types de personnalité

Les types de personnalité ont une préférence dans les jeux psychologiques :

{{< table class="table-with-blue-header table-alternate table-center" >}}
| Type          | 1ier Rôle                               | 2nd Rôle                       |
| ------        | -----------                             | ----------                     |
| Travaillomane | Sauveur                                 | Persécuteur (se croit victime) |
| Persévérant   | Sauveur                                 | Persécuteur                    |
| Empathique    | Sauveur (fortement)                     | Victime                        |
| Rêveur        | Victime                                 | Victime (fuite)                |
| Rebelle       | Victime (de soi-même)                   | Persécuteur (alternance)       |
| Promoteur     | Sauveur (pour lui-même le plus souvent) | Persécuteur                    |
{{</ table >}}

## Pour finir

J'espère que cet article vous a donné envie d'aller plus loin et de creuser le modèle Process Com'. Et retenez que nos personnalités sont riches d'une base et de plusieurs phases qui évoluent dans le temps (une seule active). Apprendre à se connaître et mieux comprendre les autres est un outil super-puissant qui peut changer votre manière de vivre en équipe. Mais vous en avez eu un aperçu ici : c'est un outil délicat qui nécessite une grande maîtrise.

Alors, si vous voulez aller plus loin, lisez, formez-vous, commencez à modéliser des interactions simples dans un milieu "protégé" et échangez avec d'autres "initiés". 

Et comptez sur nous : vous en ressortirez grandi(e) !

### Références

[www.processcommunication.fr](https://www.processcommunication.fr)
