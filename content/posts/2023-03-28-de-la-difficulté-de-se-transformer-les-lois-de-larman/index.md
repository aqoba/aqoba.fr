---
draft: false
title: "De la difficulté de se transformer: les lois de Larman"
offers:
  - unlock-transformation
values:
  - partage
  - conviction
summary: Imaginez que vous travaillez dans une entreprise qui souhaite se
  transformer et adopter de nouvelles façons de faire pour être plus agile et
  plus efficace. Vous êtes enthousiaste à l'idée de faire évoluer ses modes de
  fonctionnement et sa culture de travail, mais vous vous heurtez à des
  obstacles qui vous empêchent d'avancer. Si cette situation vous est familière,
  vous n'êtes pas seul. Il est temps de découvrir les lois de Larman !
authors:
  - olivier-marquet
tags:
  - loi
  - larman
  - change
  - culture
  - transformation
date: 2023-04-05T05:52:16.896Z
thumbnail: change.png
subtitle: Ou pourquoi “Culture follows structure”
---
{{< toc >}}

## Origines

Imaginez que vous travaillez dans une entreprise qui souhaite se transformer et adopter de nouvelles façons de faire pour être plus agile et plus efficace. Vous êtes enthousiaste à l'idée de faire évoluer ses modes de fonctionnement et sa culture de travail, mais vous vous heurtez à des obstacles qui vous empêchent d'avancer. Si cette situation vous est familière, vous n'êtes pas seul.

Craig Larman, consultant en organisation depuis plusieurs dizaines d’années et co-créateur du framework d’agilité à l'échelle [LeSS](https://less.works/), a identifié un certain nombre de schémas courants qui empêchent les organisations de mettre en place des changements significatifs. De ses constats, il a formulé en 2013 les lois qui portent son nom : les lois de Larman, une série de cinq principes incontournables qui décrivent les difficultés les plus fréquentes rencontrées lors de la transformation d'une entreprise. Redécouvrons ces lois ensemble.

## Loi N°1

**Les organisations sont implicitement optimisées pour éviter de modifier le statu quo des managers intermédiaires et de premier niveau ainsi que celui des postes d’« expert » et celui des structures de pouvoir.**

L’homéostasie du système, cette résistance instinctive au changement, permet d’éviter les changements qui pourraient remettre en question la position de pouvoir ou l'influence de certains employés clés. Par exemple, les managers intermédiaires résistent quasi-systématiquement aux changements qui pourraient affecter leur pouvoir ou leur influence dans l'organisation, tandis que les experts peuvent être réticents à adopter de nouvelles pratiques qui pourraient diluer ou remettre en question leur statut d’expert reconnu dans l’entreprise.

## Loi N°2

**En corollaire de la loi 1, toute initiative de changement sera réduite à redéfinir ou à surcharger la nouvelle terminologie pour signifier fondamentalement la même chose que le statu quo.**

Qui n’a pas déjà rencontré cette situation… La deuxième loi de Larman suggère que, dans les organisations, toute initiative de changement sera souvent réduite à un simple changement de terminologie, en la surchargeant d’un jargon qui signifiera fondamentalement la même chose que ce qui existait déjà. Une chef de projet renommé PO mais qui se comporte comme avant par exemple. En d'autres termes, les changements réels sont souvent limités et superficiels, les organisations préférant conserver leur façon de faire les choses plutôt que d'adopter de nouvelles méthodes ou approches qui pourraient être plus efficaces.

{{< figure  src="larman2.png" alt="Illustration de la loi 2" class="center-large">}}

## Loi N°3

**En corollaire de la loi 1, toute initiative de changement sera ridiculisée comme « puriste », « théorique », « révolutionnaire », « religieuse » et « nécessitant une personnalisation pragmatique aux préoccupations locales » - ce qui détournera de la résolution des faiblesses constatées et du sujet du statu quo du manager/expert.**

Cette loi suggère que lorsque des initiatives de changement sont proposées dans une organisation, elles sont souvent critiquées et dénigrées par les personnes qui ont intérêt à maintenir le statu quo, notamment les managers intermédiaires et les experts. 

“On n’est pas dans le monde des bisounours !”

“Ça marche peut-être ailleurs, mais chez nous, c’est plus compliqué…”

Ces critiques peuvent dissuader les promoteurs du changement, en particulier ceux qui sont moins enclins à la confrontation, et maintenir l'état actuel des choses. Puis il faut le dire, c’est dur de lutter contre la mauvaise foi. Cela peut conduire à un manque d'innovation et de progrès dans l'organisation, qui reste coincée dans des processus inefficaces ou obsolètes. Certains acteurs du changement (coachs, consultants) trop peu expérimentés, peuvent se sentir décrédibilisés par ces attaques et baisser les bras ou revoir leurs ambitions à la baisse : ils/elles donnent alors raison à leurs détracteurs en ne poussant que des changements de façade et sans impact réel.

## Loi N°4

**En corollaire de la loi 1, si après que la transformation ait été altérée certains managers / experts bougent dans l'organisation, ils deviennent coachs / formateurs du changement. Ils renforcent alors généralement les lois 2 & 3 et créent la fausse impression que les changements "ont été faits", trompant le top-management et biaisant les futures tentatives de transformation. Les mêmes coachs / formateurs deviennent ensuite des consultants en transformation.**

Cette loi décrit comment, lorsque des changements sont mis en place dans une organisation, certains managers et experts qui étaient auparavant en position de pouvoir peuvent être repositionnés en tant que "coachs/formateurs" ou accompagnateurs du changement. Cela donne au top-management l'impression trompeuse que "le changement a été fait". Alors que ceux en charge de changer les choses en profondeur ne connaissent pas d'autre modes de travail ou d'organisation : ils ne provoquent donc que des changements superficiels et renforceront le statu-quo.

La bonne illustration de cette problématique se trouve dans cette citation d’Albert Einstein, “On ne peut pas résoudre un problème avec le même mode de pensée que celui qui a généré le problème.”

{{< figure  src="larman5.png" alt="Illustration de la loi 5" title="la 4ème loi a été la dernière créée en 2017, c’est techniquement la 5ème mais insérée en 4ème position, d'où le titre du dessin" class="center-large">}}

## Loi N°5

**Dans les grandes organisations établies, la culture suit la structure. Et dans les petites et jeunes organisations, la structure suit la culture.**

Dans les grandes organisations établies, la structure organisationnelle est souvent très complexe, avec des hiérarchies, des processus et des systèmes de gestion du personnel bien établis. Ces structures sont souvent en place depuis longtemps et ont été conçues pour répondre aux besoins de l'organisation à un moment donné. Cependant, avec le temps, ces structures peuvent devenir rigides et obsolètes, et il peut être difficile de les modifier pour répondre aux besoins changeants de l'organisation.

Dans de telles situations, la culture est souvent fortement influencée par la structure organisationnelle existante. Les comportements, les attitudes et les croyances des employés sont souvent façonnés par les règles, les politiques et les procédures en place, ainsi que par les attentes et les normes de comportement qui sont communiquées par les managers et les dirigeants (sans parler d’un très important driver de comportement : la façon dont sont évalués les collaborateurs en fin d’année et sur quel types d’objectifs). 

Dans les petites start-ups, au contraire, la culture est souvent le moteur qui façonne la structure organisationnelle. Dans ces organisations, les valeurs et les croyances des fondateurs influencent la conception de l'organisation, qui est généralement informelle et flexible. À mesure que l'organisation se développe, elle doit adopter une approche plus formelle et structurée pour accommoder la complexité croissante de ses opérations.

## Le lien avec Peter Drucker, John Shook et Edgar Schein

{{< figure  src="culture-eats-strategy-for-breakfast-01.png" alt="Citation de Peter Drucker" class="center-small">}}

Comment sur cette dernière loi ne pas évoquer cette phrase très connue de Peter Drucker: “La culture mange la stratégie au petit déjeuner”. 
Cette phrase signifie que la puissance de la culture forcera le statu quo dans n’importe quelle stratégie de changement. Ça ne veut pas dire qu’il ne faut pas essayer, bien au contraire, mais il faudra l’avoir bien en tête pour conduire le changement et aider à changer une partie de la culture, via la structure et de nouvelles habitudes, pour supporter la nouvelle organisation de l’entreprise.

Les observations de Larman et Drucker font directement référence aux travaux de Shook et Schein sur comment changer une culture d’entreprise.

{{< figure  src="shook-schein-models.jpg" alt="Les modèles de Shook et Schein" class="center-large">}}

John Shook est un expert renommé en Lean Management et un anthropologue industriel, connu pour son travail chez Toyota et ses livres sur le Lean. Dans le cadre de son travail pour mettre en place l'entreprise conjointe Toyota-GM NUMMI, il a conceptualisé l’approche que changer de culture commence par changer ce que les gens font : la nouvelle façon de penser viendra. Il a décrit cela dans un modèle basé sur le [modèle original de culture d'entreprise d'Edgar Schein](https://www.amazon.fr/Organizational-Culture-Leadership-Edgar-Schein/dp/1119212049/) et que vous pourrez retrouver ici : [How to Change a Culture: Lessons From NUMMI](https://www.lean.org/downloads/35.pdf)

## De la difficulté de se transformer

Les lois de Larman nous rappellent donc qu’il est essentiel de comprendre la relation entre la culture et la structure pour créer des modèles efficaces de gestion du changement. L'état d'esprit seul n'est pas suffisant pour apporter des changements durables dans les grandes organisations et par conséquent, si l'on souhaite vraiment changer la culture, il sera souvent nécessaire de commencer par modifier la structure organisationnelle tout en prenant en compte la résistance au changement et en accompagnant ce dernier. 
Dans le cas de réorganisations d’équipes par exemple, il ne suffira donc pas d’invoquer magiquement Conway et sa manœuvre inverse, ou Team Topologies, pour se transformer sans avoir anticipé aussi les impacts et mis en place la conduite du changement correspondante.

Les lois de Larman ne sont pas là pour nous décourager de transformer nos modes de travail ou nous inciter au statu quo. Au contraire, nous les voyons comme un guide utile et toujours d'actualité pour bien amorcer et ancrer en profondeur une transformation : ces 5 lois nous rappellent les pièges à éviter qui se présentent systématiquement lorsqu'on tente de changer sincèrement les choses.

Devant ces constats, il est donc crucial pour les entreprises d’**apprendre à changer** :)

- - -

## Lois de Larman : [version originale](https://www.craiglarman.com/wiki/index.php?title=Larman%27s_Laws_of_Organizational_Behavior) et traduction française

**1. Organizations are implicitly optimized to avoid changing the status quo middle- and first-level manager and “specialist” positions & power structures.**

Les organisations sont implicitement optimisées pour éviter de modifier le statu quo des managers intermédiaires et de premier niveau ainsi que celui des postes d’« expert » et celui des structures de pouvoir.

**2. As a corollary to (1), any change initiative will be reduced to redefining or overloading the new terminology to mean basically the same as status quo.**

En corollaire de la loi 1, toute initiative de changement sera réduite à redéfinir ou à surcharger la nouvelle terminologie pour signifier fondamentalement la même chose que le statu quo.

**3. As a corollary to (1), any change initiative will be derided as “purist”, “theoretical”, “revolutionary”, "religion", and “needing pragmatic customization for local concerns” — which deflects from addressing weaknesses and manager/specialist status quo.**

En corollaire de la loi 1, toute initiative de changement sera ridiculisée comme « puriste », « théorique », « révolutionnaire », « religieuse » et « nécessitant une personnalisation pragmatique aux préoccupations locales » - ce qui détournera de la résolution des faiblesses constatées et du sujet du statu quo du manager/expert.

**4. As a corollary to (1), if after changing the change some managers and single-specialists are still displaced, they become “coaches/trainers” for the change, frequently reinforcing (2) and (3), and creating the false impression ‘the change has been done’, deluding senior management and future change attempts, after which they become industry consultants.**

En corollaire de la loi 1, si après que la transformation ait été altérée certains managers / experts bougent dans l'organisation, ils deviennent coachs / formateurs du changement. Ils renforcent alors généralement les lois 2 & 3 et créent la fausse impression que les changements "ont été faits", trompant le top-management et biaisant les futures tentatives de transformation. Les mêmes coachs / formateurs deviennent ensuite des consultants en transformation.

**5. (in large established orgs) Culture follows structure. And in tiny young orgs, structure follows culture.**

Dans les grandes organisations établies, la culture suit la structure. Et dans les petites et jeunes organisations, la structure suit la culture.
