---
draft: false
title: Découvrez les comportements qui sabotent (déjà) la culture de votre
  entreprise !
authors:
  - laurent-dussault
values:
  - conviction
tags:
  - sabotage
  - performance
  - culture
  - leadership
offers:
  - unlock-management
date: 2024-11-25T17:31:00.000Z
thumbnail: leonardo_diffusion_xl_a_large_vertical_ladder_in_the_center_of_0.jpg
subtitle: QUELS LES COMPORTEMENTS ÉLIMINER POUR UN IMPACT CULTUREL?
---
La culture d’entreprise n’est pas simplement le résultat de valeurs affichées sur des murs ou de grandes annonces de mission. Elle est le fruit des comportements quotidiens et des choix, petits et grands, que les leaders acceptent, encouragent ou tolèrent. Dans cet article, nous explorerons comment une prise de conscience autour de l’impact des comportements peut transformer la culture d’une organisation et comment un simple exercice de « sabotage » peut aider à révéler les comportements nuisibles qui affectent déjà le collectif.

## Qu’est-ce que la culture d’entreprise ?

Pour comprendre l’importance de l’impact de nos comportements, il est essentiel de revenir aux bases : 

La culture d’une organisation est le mélange unique de valeurs, de croyances, de normes et de comportements observés chaque jour par les collaborateurs. Ce sont notamment ces derniers qui influencent la manière dont les collaborateurs interagissent, prennent des décisions et se motivent, ou pas.

## Ce qui élève ou abaisse la culture d’entreprise

La culture est façonnée par les comportements, mais tous les comportements n’ont pas le même poids. Voici deux forces principales :

1. **Les comportements que les leaders choisissent d’amplifier** : Ces comportements définissent ce qui est valorisé dans l’entreprise. Par exemple, un leader qui valorise le feedback constructif et l’entraide renforce une culture de soutien et d’amélioration continue.
2. **Les comportements que les leaders tolèrent** : Ce sont souvent les comportements les plus nuisibles. En tolérant des attitudes telles que le manque de respect, les rivalités interpersonnelles ou la réticence à communiquer, le leader envoie un signal puissant que ces comportements sont acceptables, voire normaux. Les signaux négatifs sont bien plus puissants que les signaux positifs : il suffit de quelques comportements tolérés pour que l’ensemble de la culture soit affecté.

## Le changement de culture : plus qu’une déclaration, une transformation quotidienne

Un changement de culture ne se décrète pas par une simple annonce. Il nécessite une transformation des comportements quotidiens et une vigilance constante pour aligner les actions avec les valeurs souhaitées. Si les leaders désirent une culture constructive et motivante, ils doivent initier des comportements exemplaires et se montrer intransigeants face aux attitudes qui contredisent ces valeurs. En effet, chaque membre a un rôle à jouer pour modeler la culture, mais cette responsabilité commence avec la prise de conscience.

## Exercice de sabotage : révéler les comportements à risque

Afin de prendre conscience des comportements de sabotage existants, l'exercice de sabotage est un outil efficace et souvent surprenant pour les équipes. Il se déroule en trois étapes clés :

### Étape 1 : Imaginer les actions destructrices...

Le groupe est invité à imaginer des actions qui, si elles étaient mises en place, pourraient être des freins dans l’organisation. 
Par exemple :

**Démotiver/Déresponsabiliser les collaborateurs :** \
Limiter le plus possible la mise en place de relation de confiance.

Exemple :
*Se focaliser sur les erreurs en cherchant des coupables, Distribuer des tâches sans vision globale …*

**Limiter la communication,**\
Empêcher des interactions efficaces entre les personnes et les équipes

Exemple :\
*Distinguer les gens qui vont en réunions des gens qui produisent…
Avoir des “équipes” éclatée géographiquement (un étage suffi)
…*

**Déconnecter la mesure de la réalité (ou de l’objectif)**,\
Suivre des indicateurs qui révèlent plus de la foi que d’un suivi d’avancement.

Exemple :\
*Déclarer des taches “presque fini”, 
Suivre les dépenses (budgets) plus que les livrables, 
…*

**Mettre les équipe sous l’eau,** \
Être sûr de bien surcharger tout le monde ! 

Exemple :\
*Fixer des objectifs irréalisables
Ne pas tenir compte de toutes les actions à réaliser dans les estimations
…* 

Cet exercice, souvent réalisé dans une ambiance légère, permet aux collaborateurs d’exprimer des idées qui révèlent des comportements problématiques, en se défoulant, "sans filtre".

### Étape 2 : Observer les comportements actuels

Une fois ces actions de « sabotage » imaginées, proposez à votre groupe de les reprendre, un par un. \
La question suivante est posée : \
\
**Ces comportements existent-ils déjà dans l’entreprise, d’une manière ou d’une autre ?** \
\
Bien souvent, les participants constatent que nombre de ces « mauvaises idées » sont des comportements déjà visibles dans l’organisation, parfois sous une forme atténuée mais persistante.

Cette étape est un choc pour de nombreux leaders, car elle expose des habitudes et des tolérances qui minent silencieusement l’ambiance et la performance de l’équipe.

### Étape 3 : Prise de conscience collective

L’exercice se termine par une discussion sur les actions concrètes pour éradiquer ces comportements de sabotage. Qui à part les managers peut influer sur ces comportements et donc sur la culture ? La pensée magique qui consiste à souhaiter que ça cesse sans agir est vaine.

Au-delà des managers, chacun peut jouer un rôle dans cette démarche, en s’engageant à reconnaître et à éliminer les comportements nuisibles. Encourager tous les managers et tous les collaborateurs à être vigilants renforce la cohésion et permet à la culture positive de se diffuser plus largement et plus solidement.

On peut aussi considérer cet atelier comme un dérivé de l'atelier TRIZ des [liberating structures](https://aqoba.fr/posts/20230516-des-r%C3%A9unions-plus-efficaces-gr%C3%A2ce-aux-liberating-structures/)

## Comment intégrer la prise de conscience dans la culture au quotidien

Prendre conscience de l’impact de chaque comportement sur la culture est une première étape, mais la transformation se joue au quotidien. Voici quelques actions pour instaurer une culture de vigilance et de positivité :

1. **Encourager le feedback régulier** : Offrir un feedback fréquent et constructif permet aux collaborateurs de se sentir valorisés et d’ajuster leurs comportements en fonction des valeurs de l’entreprise.
2. **Promouvoir la transparence et la communication ouverte** : La transparence au niveau des actions et des décisions aide à prévenir les malentendus et instaure une atmosphère de confiance, essentielle pour une culture positive.
3. **Incarner les valeurs de l’entreprise** : Les leaders doivent être les premiers à démontrer les comportements qu’ils souhaitent voir se diffuser. Cela inclut aussi de dénoncer avec bienveillance les comportements de sabotage lorsqu’ils apparaissent.
4. **Responsabiliser toute l’équipe** : Au-delà des managers, chacun doit se sentir responsable de la culture de l’entreprise. Inviter les collaborateurs à signaler et à discuter des comportements qui minent la cohésion permet de mieux ancrer les valeurs dans le quotidien de l’organisation.

## En pratique

Ce type d’atelier, nous l’avons mené de manière généralisée au sein de RBS, la partie SI de la banque de détail de la Société Générale. “Généralisée” car tous les leaders (managers/scrum masters) l’ont fait en l’espace de quelques semaines… donnant naissance ainsi à une armée de regards sur les mauvais comportements et une prise de conscience collective que laisser faire ces comportements c’est accepter qu’ils font partie de la culture de l’entreprise. 

Retrouvez le mise en place de cette démarche dans notre talk d’Agile en seine 2024

<iframe width="560" height="315" src="https://www.youtube.com/embed/C6gCbp_pb7o?si=CqlK2UDnBk2dRXI2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## À vous !

Une culture d’entreprise positive et inspirante ne s’impose pas par des slogans, elle se construit par des choix et des comportements cohérents. La prise de conscience des comportements de sabotage et l’engagement collectif pour les éliminer sont des leviers puissants de transformation. Alors, et si dès aujourd'hui vous commenciez par faire le point sur les comportements que vous tolérez et que vous aimeriez voir disparaître ? Ensemble, leaders et équipes peuvent bâtir un environnement où chacun se sent motivé, engagé et prêt à contribuer au succès collectif.
