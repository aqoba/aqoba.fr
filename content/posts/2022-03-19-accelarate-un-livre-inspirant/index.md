---
title: "Accelerate : la boussole indispensable de votre transformation"
authors:
  - laurent-dussault
values:
  - conviction
  - partage
tags:
  - devops
  - mesure
offers:
  - unlock-delivery
date: 2022-03-19T10:23:56.051Z
thumbnail: accelarate-illust.png
subtitle: Une étude de 4 ans sur la performance du delivery et comment l'optimiser.
---
Les « agilistes » sont convaincus depuis longtemps que certains comportements sont bénéfiques à la performance des entreprises.
*ACCELERATE* revient sur une étude, menée pendant 4 ans, auprès de 355 entreprises dans des domaines diverses. Elle a démontré les différents liens entre comportements, performances du delivery IT et performance de l’entreprise. C'est un de nos livres de chevet et une boussole pour les transformations d'entreprise que nous menons, chez aqoba. Dans cet article, nous vous en proposons un Digest et vous expliquons comment nous utilisons cette boussole chez nos clients pour optimiser la performance de leur delivery.

## La mesure de la « Delivery IT performance »

Au delà d’une corrélation statistique, l’étude démontre la prédictibilité de la Performance de l’entreprise en fonction de sa performance du delivery IT. 
*ACCELERATE* nous propose un indicateur simple de cette dernière mesure. Plus précisément 4 indicateurs clé :

* **Le delivery lead time**, c’est à dire le temps qui s’écoule entre le dernier commit de votre code et le déploiement en production du code en question. C’est l’indicateur Lean par excellence d’après Taïshi Ono.\
  En plus d’être un témoin de la vitesse, quand cet indicateur est performant, il est également prédictif d’une capacité d’apprentissage décuplée par cette boucle de feedback rapide.\
  Cette mesure exclut la durée de design et de développement, ce qui lui fait subir moins de variabilité que le Lead Time total.
* **La fréquence de déploiement** (en production), qui donne de nombreuses informations :\
  C’est un état des opportunités de déploiement, mais également en se projetant un peu plus, une possibilité que la taille des *changes* soit réduite.\
  Ainsi, une nouvelle fois dans une approche Lean : le signale de la réduction de la taille de batch, annonciateur un flux rapide, subissant peu de variabilité.
* **Le Mean Time To Restore** (MTTR) : le temps moyen pour restituer le service initial.\
  En soit le choix de cet indicateur est déjà révélateur d’un état d’esprit et d’un comportement.\
  On l’oppose fréquemment un Mean Time Between Failure (MTBF).\
  Là où le MTTR fait état de la capacité à réagir, le MTBF mesure lui la robustesse, en considérant le temps moyen entre 2 interruptions.\
  En choisissant lui ou l’autre, vous venez de prendre partie dans la fable du Chêne et du Roseau.\
  Et La Fontaine avait déjà son favori bien avant notre ère numérique.\
  Un MTTR court est la promesse d’un service résilient… et du coup, le signal d’un environnement capable d’accueillir le changement, car on ne se focusse plus sur la crainte d’une interruption de service.
* Le **Change Fail Percentage**\
  Je vous entends déjà  réagir au paragraphe précédant : Bah oui, on va finir par tester en prod avec ce raisonnement…\
  Tout d’abord, ce n’est pas parce qu’un système deviendrait résilient qu’on abandonne tous les principes.\
  Donc on se propose ici, de mesurer la proportion de *changes* qui aboutissent à un échec (nécessitant un hotfix ou un rollback)\
  Nous avons donc ici, un indicateur de la qualité de ce qui est produit.

  ```
   
  ```

## Quelles pratiques pour influencer ces 4 indicateurs clé ?

L’étude relatée dans cet ouvrage va beaucoup plus loin. Elle remonte le fil des corrélations et a permis d’identifier 24 pratiques qui entrainent delivery IT performant.

Ces 24 pratiques sont regroupées en 5 catégories :

**Continuous delivery** :  des pratiques techniques qui couvrent tout le CI/CD (Continuous Integration/Continuous Delivery).

**Architecture** :  limiter les adhérences et permettre l’autonomie des équipes dès la conception.

**Product and process** :  autour du Lean  product development.

**Lean management and monitoring** :  les principes Lean, le focus client et l'observabilité pour une utilisation pro-actives des informations.

**Cultural**  :  des comportements à destinations des leaders pour ancrer la culture.

Vous retrouverez le détail de ces pratiques dans le schéma ci-dessous. Les liens mettent en avant les prédictibilités constatées entre les différents éléments.

Ainsi, parmi les bénéfices, nous avons bien la performance globale (aussi bien financière que « non-commerciale ») mais également des bénéfices annexes non négligeables comme le bien être au travail.

![accelarate 24 capabilities](accelarate24capabilities.png)

## Comment nous appliquons les enseignements d'Accelerate dans les transformations que nous menons ?

Inspiré par cette lecture, nous avons mis en place un *assessment* pour nos clients qui ambitionnent d'améliorer leur performance IT. Nous utilisons les 4 indicateurs clé pour localiser les flux de valeur dans une matrice de performance representée sur 2 axes :

* **Vitesse** : Aggregation des 2 premiers indicateurs (delivery lead time et fréquence de deploiement)
* **Stabilité** : Aggregation des 2 autres indicateurs (MTTR et Change Fail Percentage)

Nos identifions ainsi, en les colorisant, le ou les flux de travail où l'amélioration est la plus pertinente.

<table>
 <tr>
 <td class="left">

![matrice performance it](matriceperformanceit.png)

</td>
 <td class="middle">

![exemple de matrice](exemplematrice.png)

</td>
 </tr> 
</table>



Cet *assessment* collecte également des informations sur les 24 pratiques citées dans *ACCELERATE*. Ainsi, après avoir identifié le **où**, nous avons une bonne idée des meilleurs leviers (le **quoi**) à notre disposition pour améliorer la performance, ensemble.

Notez qu'il ne s'agit pas d'un formulaire ultime : certaines informations nécessitent des infos locales, au niveau équipe, d'autres, notamment sur les interactions inter-équipes autour du flux de valeur. Ainsi, nous couplons ce questionnaire d'équipe avec des interviews plus traditionnelles. Toutefois, cette boussole nous permet de définir le cap et de suivre régulièrement avec nos clients, les impacts de leur transformation.

Accelerate a révolutionné la manière d'appréhender la performance du Delivery et les transformations d'entreprises. Vous ne devez et ne pouvez pas passer à côté. Et bien sûr, si vous le souhaitez, nous sommes là, chez aqoba, pour vous en parler.

## Réference

**[ACCELERATE](https://itrevolution.com/book/accelerate/)**
*The Science of Lean Software and DevOps: Building and Scaling High Performing Technology Organizations*
**NICOLE FORSGREN,JEZ HUMBLE,GENE KIM**