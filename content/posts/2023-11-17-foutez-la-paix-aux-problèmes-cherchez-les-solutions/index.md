---
draft: false
title: Foutez la paix aux problèmes, cherchez les solutions
authors:
  - laurent-dussault
values:
  - conviction
  - partage
tags:
  - SolutionFocus
offers:
  - unlock-management
date: 2023-11-17T14:32:38.253Z
thumbnail: solution-focus.png
subtitle: "Naviguez vers des Solutions Positives : Solution Focus et  Processus EOLE"
---
Manager ou opérationnel(le). Vieux briscard ou jeune embauché(e). Interne ou externe. On a toutes et tous des casse-têtes à régler tous les jours, au boulot. Chez aqoba, c'est le lot quotidien auprès de nos clients qui cherchent à se transformer.

Mais on s'épuise parfois à rester le nez dans les problèmes. Et si, au lieu de prendre le sujet par le problème, on cherchait à le prendre par la solution ?

Solution Focus est une approche tellement orientée vers les solutions que le problème initial n'a même pas besoin d'être rationnel. Elle se distingue par son efficacité dans la résolution de problèmes, même les plus farfelus. C'est d'ailleurs cette caractéristique qui en fait une technique de choix dans le domaine de la thérapie brève. Les pratiques décrites ici sont inspirées de la **Solution-Focused Brief Therapy (SFBT)**, une approche thérapeutique qui privilégie les solutions plutôt que les problèmes.

## Comprendre Solution Focus

{{< figure  src="monstre-sous-le-lit.png" alt="solution focus : l'histoire du monstre sous le lit" class="float-left-tiny">}}

Imaginez un homme, nous l'appellerons Cédric, qui consulte un psy un jour, car il a une peur irrationnelle. Chaque nuit, il redoute la présence d'un monstre terrifiant tapi sous son lit. Ses nuits sont hantées par cette créature imaginaire, et il ne sait plus où donner de la tête. Il est au bout du rouleau. Il partage sa peur avec le psy, espérant désespérément trouver une solution à ce problème qui l'obsède.

Mais voilà, quelques séances plus tard, Cédric ne réapparaît plus dans le cabinet du psy. Le psy, intrigué, se demande ce qui a bien pu se passer. Un jour, par hasard, il croise Cédric dans la rue et décide de l'interroger : "Pourquoi ne venez-vous plus aux séances ?"

Cédric, avec un sourire malicieux, répond : "Ah ! Je n'ai plus de problème. Mon cousin m’a aidé à  scier les pieds de mon lit !" 

Cette anecdote, bien que fictive, illustre parfaitement l'esprit Solution Focus, une approche qui consiste à trouver des solutions simples et créatives aux problèmes qui semblent insolubles. Plutôt que de chercher à chasser le monstre sous le lit, Cédric a opté pour une solution inattendue qui a résolu son problème de manière efficace.

Ainsi, embarquons ensemble dans l'univers orienté solution et découvrons comment cette approche peut vous aider à transformer les défis en opportunités

Solution Focus, c'est comme prendre le chemin du positif. Plutôt que de rester bloqué à ressasser les problèmes, nous nous concentrons sur les solutions. C'est un peu comme changer de lunettes : soudainement, vous voyez toutes les opportunités qui vous entourent.

L'approche Solution Focus repose sur quelques principes simples, mais efficaces. Elle considère que les solutions sont déjà présentes, que le changement est possible, et que la clé réside dans la collaboration et la construction de petits succès.

Maintenant, attachez vos ceintures, car nous allons explorer le processus EOLE : une trame de questionnement qui va aider à entraîner vers l’étude des solutions, en réduisant volontairement la voilure sur l’étude du problème…

## Le Processus EOLE : Embarquement, Où veux-tu aller, L'échelle, En avant

* Des équipes qui n'arrivent jamais à se coordonner pour tester sur leur solution ensemble avant de les mettre en service…
* Quelle sera la trajectoire pour atteindre la vision à 3 ans de l'entreprise ?
* Une équipe qui a, par manque de confiance mutuelle, du mal à s’engager.
* Un monstre sous le lit qui vous empêche de dormir
* …

Le processus qui suit peut vous aider dans bien des situations : 

### Embarquement (E) : Qu’espères-tu de mieux comme résultat de cet échange ?

L'étape de l'embarquement est cruciale, car c'est là que nous fixons notre cap. Nous nous lançons dans cette conversation avec un objectif clair : déterminer qui est intéressé par quoi dans cette histoire et en quoi cette discussion a de la valeur.

Dans cette phase, nous pouvons utiliser une approche structurée, telle que celle de Ben Furman, qui comprend les étapes suivantes :

1. **Écouter ce que la personne a à dire** : C'est le point de départ. Nous écoutons attentivement, relevant des indices sur la direction que la personne souhaite prendre. Quels sont les signes, les indices de son désir de changement ?
2. **Accepter** : Pour signaler que nous avons écouté et accepté le compte-rendu de la situation. Cela crée un espace sûr pour la personne, lui permettant de s'exprimer librement.
3. **Identifier ce que la personne veut** : Nous tentons ensuite une reformulation pour comprendre précisément ce que la personne souhaite. C'est l'étape où nous transformons les préoccupations en objectifs concrets.
4. **Vérifier les bénéfices attendus** : Nous nous assurons que la conversation vaut la peine d'être engagée. Les bénéfices attendus sont essentiels pour motiver le changement. Quelle est la récompense à la fin du voyage ?
5. **S'assurer que la personne est prête à faire quelque chose** : Enfin, nous nous assurons que la personne est prête à prendre des mesures. Le changement nécessite de l'action, et il est important que la personne soit prête à s'engager dans le processus de résolution.

Les réponses à ces questions doivent être formulées à la forme affirmative. Nous nous concentrons sur la description de ce qui sera à la place du problème, sur ce qui doit être différent pour que la conversation ait une valeur réelle. Nous aidons ainsi la personne à envisager un futur préféré et à se préparer à passer à l'action.

En somme, l'embarquement est le point de départ de notre voyage vers des solutions, où nous définissons notre destination, identifions nos ressources, et nous préparons à naviguer vers un futur meilleur.

Dans l’exemple qui nous concerne, Cédric aurait put dire à son cousin : 

“J’en ai marre de ce monstre sous mon lit, il faut absolument que je trouve une solution pour le faire partir".

### Où veux-tu aller (O) : Qu’est-ce qui serait différent si le problème n'était plus là ?

Maintenant que vous avez embarqué, il est temps de fixer votre cap.

L'étape "Où veux-tu aller" est là pour étudier son futur préféré. C'est un peu comme si nous avions une baguette magique. Imaginez que vous puissiez faire disparaître le problème d'un simple coup de baguette. Maintenant, décrivez-moi ce qui serait à la place.

Au lieu de se focaliser sur le problème, nous nous concentrons sur la solution. Quand le monstre sous le lit disparaît grâce à la baguette magique, qu'est-ce que vous voyez ? Comment serait votre nuit préférée ? Le monstre n’est plus là, d’ailleurs il n’a plus de place pour se cacher. Cette approche Solution Focus consiste à peindre un tableau vivant et positif de ce futur.

Il est essentiel de noter que la formulation positive est la clé. Imaginez que vous alliez faire les courses. Plutôt que de faire une liste de "non commissions" en excluant ce que vous ne voulez pas, vous faites une liste de ce que vous voulez acheter. De même, dans "Où veux-tu aller", nous vous invitons à décrire ce que vous souhaitez, ce qui sera différent une fois que le problème aura disparu. Cela renforce votre vision positive du futur et crée un point de référence clair pour avancer. En d'autres termes, au lieu de vous concentrer sur ce que vous ne voulez pas, vous vous concentrez sur ce que vous voulez.

Solution Focus, c’est comme une baguette magique qui vous permet de transformer les problèmes en opportunités, de visualiser un futur où le monstre sous le lit n'existe plus, et de mettre en lumière ce qui le remplace, ouvrant ainsi la voie vers des solutions positives.

### L'échelle (L) :

#### Où te situes-tu ?

{{< figure  src="echelle.jpg" alt="Solution focus : l'echelle" class="float-left-tiny">}}

Lorsque nous arrivons à l'étape de l'échelle, il est temps d’observer notre position actuelle sur la route vers nos objectifs. Imaginez que vous vous trouvez sur une échelle de 1 à 10, où 10 est votre futur préféré et 1 l’inverse. Le 1 doit rester volontairement flou.

L'idée de l'échelle est d'évaluer votre situation actuelle de manière objective. Où vous situez-vous sur cette échelle par rapport à votre objectif ? Si vous avez des difficultés à évaluer votre position, vous pouvez vous référer à des outils comme "The Scaling Walk" de Paul Z. Jackson. Cette technique consiste à marcher le long d'une ligne imaginaire, représentant l'échelle, et à vous arrêter là où vous estimez être actuellement. Notez que pour certaines personnes, la projection dans cet espace est un énorme atout pour se concentrer et conscientiser le travail vers une solution, pour d’autres, peut être dont le problème n’est pas un vrai problème, celà peut être vécu comme une chorégraphie ridicule… Attention donc.

#### Qu’est-ce qui fait que tu es déjà là ?

Maintenant, posez-vous la question : qu'est-ce qui fait que vous n'êtes pas à 1 sur l'échelle ? Identifiez vos ressources, vos forces, et les éléments qui sont déjà en place pour vous aider à avancer. Dans la technique “Scaling Walk”, tournez vous vers le 1 et demandez : qu’est-ce qui fait que j’en suis <span style="text-decoration:underline;">déjà</span> là ?  En d'autres termes, reconnaissez les aspects positifs de votre situation actuelle, même s'ils semblent minimes. Cela vous donnera une base solide pour avancer. \
Pour vous aider à réfléchir, posons quelques questions supplémentaires :

* **Quand est-ce que ton futur est <span style="text-decoration:underline;">déjà</span> un peu présent ?**  \
  Identifiez les moments où vous avez déjà expérimenté des éléments de ce futur souhaité. Peut-être que cela s'est produit de manière sporadique ou dans des circonstances particulières.
* **Qu’est-ce que tu as fait différemment à ce moment-là ?**  \
  Réfléchissez à ce que vous avez fait de manière différente lors de ces occasions. Quels changements avez-vous apportés à votre comportement, à votre approche, ou à vos actions ?
* **Qu’est-ce que les autres te voient faire ?**  \
  Envisagez comment les autres vous perçoivent lorsque vous avancez vers ce futur préféré. Comment vos actions influencent-elles votre entourage ? Parfois, les observateurs extérieurs peuvent offrir des perspectives précieuses.

L'échelle vous permet de visualiser votre position actuelle de manière claire, de reconnaître les éléments qui fonctionnent déjà en votre faveur, et de déterminer le chemin à suivre pour progresser vers votre futur préféré.

Maintenant que vous avez mesuré votre position sur l'échelle, vous êtes prêt à passer à l'étape suivante, "En avant," où vous allez planifier des petits pas concrets pour avancer vers vos objectifs.

### En avant (E) : À quoi ressemblerait un +1 ? Qu’est-ce qui serait différent à +1 ?

{{< figure  src="petits-pas.jpg" alt="Solution focus : petits pas" class="float-left-tiny">}}

Dans l'étape "En avant", notre objectif est d'identifier les éléments concrets qui vous feront dire que vous avez progressé d'un pas (+1) vers votre futur préféré. Il s'agit de déterminer comment vous saurez que vous êtes sur la bonne voie, que le changement positif est en marche.

Pour faciliter cette réflexion, posez-vous des questions spécifiques :

**Quels signes, petits ou grands, indiqueraient que vous avancez dans la direction de votre futur préféré ?  **  \
Identifiez les indicateurs tangibles de progrès.

**Qu'est-ce qui serait différent à +1 ?**  \
Imaginez que vous ayez déjà fait un petit pas vers votre objectif. En quoi votre vie, votre travail, ou votre situation personnelle serait-elle différente ?

**Quels comportements, actions ou attitudes refléteraient votre progression ?** \
Identifiez les changements concrets que vous pourriez observer chez vous.

**Comment ces changements se manifesteraient-ils dans votre quotidien ?**  \
Visualisez comment ces améliorations se traduiraient dans votre vie de tous les jours.

**Que percevraient les autres si vous avanciez vers votre futur préféré ?**  \
Réfléchissez à l'impact que des changements pourraient avoir sur votre entourage.

En fin de compte, cette étape est l'occasion de définir des objectifs concrets et d'identifier les mesures que vous pouvez prendre pour les atteindre. Vous pouvez envisager ce "petit pas" comme une brique dans la construction de votre futur idéal. Il vous permettra de mesurer vos progrès.

Si vous utilisez "The Scaling Walk", lors de cette phase de la discussion, tournez vous vers le 10 et à la fin de l’échange… marquez le début du chemin en faisant un pas en avant vers le 10 ! 


## Après EOLE : Où Allons-Nous Maintenant ?

Une des merveilleuses caractéristiques de Solution Focus, avec son processus EOLE, c'est qu'une seule conversation peut suffire. Il n'y a aucune obligation d'avoir une suite. Si vous êtes satisfait des résultats et que tout va mieux depuis la dernière fois, vous pouvez considérer que vous avez atteint votre destination, du moins pour l'instant.

Cependant, si vous décidez de poursuivre le voyage, vous pouvez poser la question : **"Quoi de mieux** depuis la dernière fois ?" C'est un excellent moyen de maintenir votre focus sur les progrès réalisés. Vous pouvez explorer les éléments de progrès en détail :

* **Quoi** ?  \
  Identifiez spécifiquement ce qui s'est amélioré.  \
  Quels sont les changements concrets que vous avez observés ?
* **Quand** ?  \
  Déterminez quand ces améliorations ont eu lieu.  \
  Est-ce que c'était soudain ou progressif ?
* **Où** ?  \
  Où avez-vous remarqué ces changements ?  \
  Dans quelles situations ou contextes ?
* **Avec qui** ?  \
  Avez-vous observé des différences dans vos interactions avec certaines personnes ?
* **Qu’a vu l’entourage** ?  \
  Demandez à votre entourage s'ils ont remarqué des changements.  \
  Leur perspective peut être précieuse.
* **Quelles réactions a eu l’entourage** ?  \
  Explorez comment votre entourage a réagi aux changements que vous avez apportés.

En outre, vous pouvez poser des questions telles que "Qu’as-tu fait ?" et "Comment as-tu fait ?" pour comprendre quelles actions spécifiques ont contribué à ces progrès. Cela vous permettra d'apprendre de vos expériences et de renforcer votre compréhension de vous-même.

Il se peut les choses n’aillent pas mieux ou semblent même empirer, préférez peut être la question **"Quoi de <span style="text-decoration:underline;">différent</span> ?"**. Cette question encourage l'exploration des changements dans la situation ou dans votre propre perspective. Elle vous aide à identifier les aspects qui ont évolué, même si ce n'est pas nécessairement dans la direction que vous aviez anticipée.

Offrez vous une occasion de réfléchir aux changements survenus et de décider de la prochaine étape. Vous pouvez choisir de mettre fin au voyage, de poursuivre votre exploration, ou de réajuster votre cap en fonction de ce que vous avez appris. Solution Focus vous donne la flexibilité nécessaire pour vous adapter à votre propre rythme et à vos besoins changeants. Bien souvent, vous constaterez que le 10 n’est pas atteint et pourtant vous passerez à autre chose…

## Votre Voyage vers des Solutions Positives

Solution Focus est une approche polyvalente, utilisée dans des contextes variés. \
Alors que nous arrivons à la fin de ce voyage à travers Solution Focus et le processus EOLE, il est temps de réfléchir à la manière dont cette approche peut changer votre perspective sur les problèmes et les défis qui se présentent à vous.

En entreprise, elle se révèle être un outil précieux pour encourager la collaboration, définir des objectifs clairs et motiver les équipes à atteindre leurs buts. A intégrer donc dans vos réflexions sur vos transformations/changements. Pensez aussi à vos rétrospectives agiles, c’est un outil adapté, pour des discussions productives et constructives, permettant d’aborder les axes d’amélioration avec un état d'esprit axé sur les solutions.

Mais ne nous limitons pas au cadre professionnel. Solution Focus peut également être appliquée dans votre vie personnelle. Elle vous aide à vous concentrer sur ce qui fonctionne, à définir des objectifs positifs, et à prendre des mesures concrètes pour atteindre vos futurs préférés. Vous pouvez l'utiliser pour améliorer votre bien-être, renforcer vos relations, et surmonter les obstacles qui se dressent sur votre chemin.

Enfin, je vous conseille également le livre de Gery Derbier : [Solution Focus : Coaching, Leadership, Conversations Constructives](https://www.lulu.com/fr/shop/g%C3%A9ry-derbier/solution-focus-coaching-leadership-conversations-constructives/paperback/product-1q59qw9m.html).

Jetez aussi un coup d'œil à notre article sur la [systémique](https://aqoba.fr/posts/20230307-approche-syst%C3%A9mique-voir-globalement-et-agir-localement/) où vous trouverez des idées complémentaires pour le questionnement.

Bon voyage vers des solutions positives !
