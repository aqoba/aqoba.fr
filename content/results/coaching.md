---
title: De manager à servant leader
identifier: coaching
order: 2
color: yellow
figures:
  - value: "70"
    summary: Le nombre de top-managers que nous avons coachés ces 3 dernières années
  - value: 65%
    summary: La part de top-managers appartenant aux directions Métier ou Directions
      Générales que nous avons aidés sur cette période
  - value: 60%
    summary: La part de nos clients avec lesquels nous redéfinissons une vision
      d’entreprise avant de la déployer auprès des équipes opérationnelles
---
Pour réussir une transformation à la fois centrée sur la performance et l'humain, de nombreuses organisations françaises doivent faire évoluer leur culture.  

Nous aidons les dirigeants et managers à adopter les pratiques, outils et postures du servant leader pour faire changer la culture d'entreprise, et ainsi tirer le meilleur de leurs équipes et, améliorer la performance de leur périmètre.  

Ce qui fait notre différence :  

* Nous pensons qu'un changement de mindset des managers passe par le changement de leurs pratiques 
* Nous bousculons, de manière bienveillante mais efficace, les habitudes managériales
* Nous donnons du sens au changement culturel