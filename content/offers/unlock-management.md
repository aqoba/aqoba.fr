---
title: Unlock Management
subtitle: Redonnons de l'impact à vos leaders
icon: /uploads/unlock-management_icon.svg
iconBgBlack: /uploads/unlock-management_icon_bg-black.svg
thumbnail: /uploads/unlock-management_illustration.jpg
codename: unlock-management
---
L’offre **unlock management** s’adresse aux responsables de transformation qui se retrouve confrontés à un plafond de verre car le middle management a été oublié dans les actions de transformation mises en place jusque-là.

* Des managers centrés sur l’opérationnel court terme plutôt que sur la gestion de l’organisation sur le long terme
* Absence de développement des personnes et des équipes
* Un manque d’incarnation des choix de l’entreprise qui n'entraîne pas leurs équipes vers le changement
* Un manque de reconnaissance des nouveaux rôles accessible aux managers
* Des managers isolés avec leurs problèmes

Chez **aqoba**, nous vous proposons de faire prendre conscience aux managers du chemin à parcourir et des nouvelles possibilités qui s’ouvrent à eux. Nous les guidons ensuite vers un nouveau positionnement, au travers de formations, d’accompagnements individuels et de groupe. Nous les aidons à réfléchir à leur rôle et leur positionnement dans la nouvelle organisation et nous leur donnons les outils pour réussir dans ce nouveau contexte avec l’aide des RH et le top management.

## En quoi l’offre d’aqoba est différenciante ?

* **Nous ne culpabilisons pas les managers** sur leurs pratiques actuelles, mais les amenons à **prendre conscience** qu’ils peuvent avoir encore plus d’impact
* Un **format d’accompagnement 360 et innovant** : il ne s’agit pas d’une formation “top-down” classique, mais d’un parcours d’accompagnement qui mêle : 
    * Un parcours de plusieurs **sessions de formation**
    * **renforcées par des accompagnements terrain** en individuel ou en groupe
    * la mise en place d’**un lieu d’échange au travers des communautés de pratiques** afin de permettre aux managers de continuer à grandir ensemble
* Pour rendre possible et **ancrer les nouveaux comportements managériaux**, nous accompagnons l’évolution **des processus et pratiques RH** de l’entreprise

