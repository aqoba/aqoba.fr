---
title: Partage
identifier: partage
order: 5
thumbnail: /uploads/values-partage.jpeg
illustration: /uploads/illustration_values-partage.jpg
---
La connaissance est un bien que l’on peut donner
sans pour autant avoir à s’en séparer. Fort de ce 
constat, nous souhaitons pouvoir organiser 
le plus largement possible le partage des savoirs
et des expériences.

