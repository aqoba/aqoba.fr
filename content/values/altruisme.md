---
title: Altruisme
identifier: altruisme
order: 3
thumbnail: /uploads/values-altruisme.jpeg
illustration: /uploads/illustration_values-altruisme.jpg
---
Quand on dit nous, c'est aqoba
et sa communauté, nous sommes engagés
dans une transformation sociétale.
