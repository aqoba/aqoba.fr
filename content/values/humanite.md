---
title: Humanité
order: 1
identifier: humanite
thumbnail: /uploads/values-humanite.jpeg
illustration: /uploads/illustration_values-humanite.jpg
---
Les femmes et les hommes,
forment le cœur de nos organisations, c’est pour
cela que l’humain constitue le point central
de nos préoccupations.
