---
title: Flow and unFlow by Kniberg - LKFR (10:00)
lien: https://www.youtube.com/live/0mDBLTu1G9s?si=sRx7rzCVhJwC9nKf&t=12038
thumbnail: "../../video/LKFR.mp4"
theme: flux
nouveaute: no
---
Être occupé n'est pas être productif... LIVE DEMO de 10 min tirées d'une journée entière.
