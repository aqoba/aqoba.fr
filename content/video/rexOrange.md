---
title: REX Orange - Du cadre, pas des ordres... (5:00)
lien: https://www.youtube.com/watch?v=qWyd0EV8QV8&t=172s
thumbnail: "../../video/rexOrange.jpeg"
theme: leadership
nouveaute: no
---
Nejma OUADI, une cadre dirigeante d'Orange livre un feedback du changement de posture managériale...(video (R)évolution des Leaders)
