---
title: Why good leaders make you feel safe | Simon Sinek (12:00)
lien: https://www.youtube.com/watch?v=lmyZMtPVodo
thumbnail: "../../video/Sinek_leader_safe.mp4"
theme: leadership
nouveaute: no
---
Créer confiance et sécurité signifie prendre de grosses responsabilités...
