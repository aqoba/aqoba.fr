---
nouveaute: no
title: Stanley McChrystal - Listen, learn ... then lead (15:39)
lien: https://www.youtube.com/watch?v=FmpIMt95ndU
thumbnail: "../../video/McChrystal.jpeg"
theme: leadership
theme2: aucun
---
Construire un sentiment de but commun entre des personnes d'âges et de compétences très divers. Par un général...
