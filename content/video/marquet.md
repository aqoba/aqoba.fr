---
title: La capacité d'excellence selon David Marquet (9:48)
lien: https://www.youtube.com/watch?v=6rt9HDfYdPg
thumbnail: "../../video/turnShip.mp4"
theme: leadership
nouveaute: no
---
Arrêtez de donner des ordres, donner l'intention... Vision rapide de son livre "Turn the ship around"

