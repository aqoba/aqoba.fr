---
title: The power of vulnerability | Brené Brown | TED (20:50)
lien: https://www.youtube.com/watch?v=iCvmsMzlF7o
thumbnail: "../../video/vulnerability.mp4"
theme: leadership
nouveaute: no
---
Et si le contrôle n'était pas votre ami...
