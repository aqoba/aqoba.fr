---
title: 100 personnes testent une méthode d'INTELLIGENCE COLLECTIVE (20:31)
lien: https://www.youtube.com/watch?v=YO2QqBLfmXM&ab_channel=Fouloscopie
thumbnail: "../../video/IntelligenceCollective.mp4"
theme: comportement
nouveaute: no
---
Le jugement de la foule est-il toujours fiable ? Comment influencer l’opinion collective ?...
