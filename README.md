# aqoba.fr

Site web d'aqoba

# Installation

## Prérequis

Les logiciels suivants doivent préalablement être installés, configurés et opérationnels :
* [Hugo](https://gohugo.io/getting-started/quick-start/)
* [Git](https://git-scm.com/)

## Étapes

Ce document fournit les instructions pour récupérer les sources, installer, configurer et démarrer le projet en local en vue de le développer ou de rédiger des articles.

1. Cloner le repository

```
git clone git@gitlab.com:aqoba/aqoba.fr.git && cd
```

2. Lancer le serveur

```shell
make hugo-memory
```

3. Accéder à l'application

Sous MacOS :
```
open http://localhost:1313/
```

Sous linux
```shell
x-www-browser http://localhost:1313/
```

# Éditer un article

## Image
Pour ajouter une image dans le flux de l'article

    {{< figure  src="unfix-base-lr.png" alt="unFIX base">}}

La même chose mais en utilisant le style plus petit et centré

    {{< figure  src="unfix-base-lr.png" alt="unFIX base" class="center-small">}}

# Notes pour les développeurs

* dans DecapCms, les collections utilisées en "relation" doivent pour l'instant absolument être des "folder collection with files", problème lié au bug : https://github.com/decaporg/decap-cms/issues/4092
