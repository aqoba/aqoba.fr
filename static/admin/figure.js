CMS.registerEditorComponent({
    id: "figure",
    label: "Illustration",
    fields: [{
        name: "caption",
        label: "Titre",
        widget: "string",
    }, {
        name: "alt",
        label: "Description",
        widget: "string"
    }, {
        name: "class",
        label: "Style",
        widget: "select",
        options: ["center-small", "center-large", "float-left-tiny", "float-right-tiny"],
    }, {
        name: "link",
        label: "Lien",
        widget: "string"
    }, {
        name: "src",
        label: "Image",
        widget: "image"
    }],
    pattern: /{{<\s*figure(.*)>}}/,
    fromBlock: function (input) {
        let output = {alt: "", src: "", caption: ""}
        let options = input[1].match(/\w+\s*=\s*"[^"]*"/g);
        if (options) {
            options.forEach((i) => {
                const keyValue = i.split("=");
                output = {...output, [keyValue[0]]: keyValue[1].replace(/"/g, '')}
            });
        }
        return output;
    },
    toBlock: function (obj) {
        let options = "";
        options += obj.src ? ` src="${obj.src}"` : '';
        options += obj.alt ? ` alt="${obj.alt}"` : '';
        options += obj.caption ? ` caption="${obj.caption}"` : '';
        options += obj.link ? ` link="${obj.link}"` : '';
        options += obj.class ? ` class="${obj.class}"` : '';
        return `{{< figure ${options}>}}`;
    },
    toPreview: (data, getAsset, fields) => {
        const imageField = fields?.find(f => f.get('widget') === 'image');
        const imgSrc = getAsset(data.src, imageField);
        return `<figure class="${data.class}"><img src="${imgSrc}" alt="${data.alt}" link="${data.link}"><figcaption><h4>${data.caption}</h4></figcaption></figure>`;
    },
});
