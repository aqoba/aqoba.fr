CMS.registerEditorComponent({
    id: "toc",
    label: "Table des matières",
    fields: [],
    pattern: /{{< toc >}}/,
    fromBlock: function(match) {
        return { };
    },
    toBlock: function(obj) {
        return `{{< toc >}}`;
    },
    toPreview: function(obj) {
        return `<ol><li>Table des matières</li></ol>`;
    },
});
