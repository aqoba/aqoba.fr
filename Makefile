CONTAINER_NAME = aqoba/aqoba.fr

build:
	docker build --pull -t ${CONTAINER_NAME} .

test: docker docker-network
	docker run -it --network aqoba-fr registry.gitlab.com/azae/docker/pa11y pa11y-ci --sitemap http://aqoba/sitemap.xml --config /etc/pa11y.conf --sitemap-exclude pdf
	#docker run -it --network aqoba-fr lycheeverse/lychee --exclude-mail http://aqoba/sitemap.xml
	docker run -it --network aqoba-fr -v `pwd`:/linkinator registry.gitlab.com/aqoba/docker/linkinator npx linkinator --config ./linkinator.config.json http://aqoba/
	docker run -it --network aqoba-fr aqoba/aqoba.fr ./check-opengraph.sh aqoba
	docker stop aqoba
	-docker rm -f aqoba
	-docker network rm aqoba-fr

docker: build docker-network
	-docker stop aqoba
	-docker rm -f aqoba
	@docker run --name aqoba -d -p 8080:80 --network aqoba-fr ${CONTAINER_NAME}

docker-network:
	-docker network create --driver bridge aqoba-fr

hugo-memory:
	hugo server --logLevel info --bind 0.0.0.0 --buildFuture --buildDrafts --renderToMemory --printUnusedTemplates --logLevel info

hugo-disk:
	rm -rf public
	hugo server --logLevel info --bind 0.0.0.0 --buildFuture --buildDrafts --cleanDestinationDir true --disableFastRender --destination=public/ --printUnusedTemplates --logLevel info

