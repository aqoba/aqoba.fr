#!/bin/sh

[ -z $1 ] && exit 1 || REMOTE=$1


ERR_COUNT=0

for URL in $(curl -s http://$REMOTE/sitemap.xml | grep loc | sed -e 's/<loc>//' -e 's/<\/loc>//')
do
    OG=$(curl -s $URL | pup 'head meta' | grep -c "og:image")
    if [ $OG -eq 0 ]
    then
        ERR_COUNT=$(($ERR_COUNT + 1))
        echo $URL
        curl -s $URL | pup 'head meta' | grep "og:"
    fi
done

exit $ERR_COUNT
