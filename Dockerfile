FROM registry.gitlab.com/aqoba/docker/hugo:latest
COPY . /src
RUN hugo_env=production hugo --logLevel info --cleanDestinationDir --destination=/src/public/
EXPOSE 80
CMD hugo_env=production hugo server --destination=/src/public/ --baseURL http://aqoba/ --logLevel info --bind 0.0.0.0 --port 80
