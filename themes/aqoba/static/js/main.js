window.addEventListener('load', function() {
  console.log('All assets are loaded');

  /* Values section event handlers */
  function updateValuesSectionBackgroundImage(identifier) {
    const values = document.getElementById("valeurs");
    if (values) {
      values.style.backgroundImage = `url(${bg[identifier]})`;
    }
  }

  document.querySelectorAll('.value').forEach((value) => {

    value.addEventListener('mouseover', (event) => {
      updateValuesSectionBackgroundImage(value.dataset.identifier);
    });

    value.addEventListener('keyup', (event) => {
      if (event.code === 'Enter') {
        updateValuesSectionBackgroundImage(value.dataset.identifier);
      }
    });
  });

  document.querySelectorAll('.value-list').forEach((value) => {

    value.addEventListener('mouseover', (event) => {
      updateValuesSectionBackgroundImage(value.dataset.identifier);
    });

    value.addEventListener('keyup', (event) => {
      if (event.code === 'Enter') {
        updateValuesSectionBackgroundImage(value.dataset.identifier);
      }
    });
  });

  updateValuesSectionBackgroundImage('humanite');
});

async function onoff() {
  menubutton = document.getElementById('menubutton');
  currentvalue = menubutton.value;
  sidenav = document.getElementById('sidenav');
  icon = document.getElementById('icon');

  if(currentvalue == "Off") {

    menubutton.value="On";
    sidenav.style.display = "flex";
    sidenav.style.marginRight = "-182px"
    await sleep(100);
    sidenav.style.marginRight = "0"
    await sleep(800);
    icon.className = 'fa fa-times fa-2x fa-inverse';

  } else {

    menubutton.value="Off";
    sidenav.style.marginRight = "-182px"
    await sleep(800);
    sidenav.style.display = "none";
    icon.className = 'fa fa-bars fa-2x fa-inverse';

  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}